# Create Data Project

Create a new data project directory tree and default Jupyter Notebook file.

## Variables to Set

- `NB_SUBDIR` - Subdirectory name to save default notebook to.
- `NB_TEMPLATE` - Path of Jupyter Notebook template file to create default notebook from.
- `PROJ_SUBDIRS` - Names of project subdirectories to create.
- `PROJS_DIR` - Path of projects directory to create new project tree in.