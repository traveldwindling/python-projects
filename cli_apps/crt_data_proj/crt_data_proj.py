#!/usr/bin/env python3
'''\
Create a new data project directory tree and default Jupyter Notebook file.

A log is saved to 'ex_log_path'.

Functions:

    * parse_args() - Parse program arguments.
    * run_init_cks() - Run initialization checks.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * clean_name() - Create a clean object name.
    * main() - Define starting point for execution of the program.

Variables to set:
----------------
NB_SUBDIR
    Subdirectory name to save default notebook to.
NB_TEMPLATE
    Path of Jupyter Notebook template file to create default notebook from.
PROJ_SUBDIRS
    Names of project subdirectories to create.
PROJS_DIR
    Path of projects directory to create new project tree in.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import datetime as dt  # Enable classes for manipulating dates and times
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version as string
from platform import python_version
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable a number of high-level operations on files and collections of files
import shutil
from signal import signal, SIGINT  # Enable use of signal handlers
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type

#############
# Variables #
#############
# Define formatting variables
FB = '\033[1m'
FR = '\033[0m'

D_FORMAT = '%Y-%m-%d'  # Set date format
# Create date stamp using specified date format
D_STAMP = dt.datetime.now().strftime(D_FORMAT)

HOME = os.path.expanduser('~')  # Obtain user home directory
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

NAME_REGEX = r'[^a-zA-Z0-9]'  # Define name regular expression
# Create name regular expression object
NAME_PAT = re.compile(NAME_REGEX)

# Set projects directory path
PROJS_DIR = ''
# Set Jupyter Notebook template file path
NB_TEMPLATE = ''
# Define project subdirectories
PROJ_SUBDIRS = []
# Define notebook subdirectory
NB_SUBDIR = ''

# Create required variables dictionary
REQ_VARS = {
    'NB_SUBDIR': NB_SUBDIR,
    'NB_TEMPLATE': NB_TEMPLATE,
    'PROJ_SUBDIRS': PROJ_SUBDIRS,
    'PROJS_DIR': PROJS_DIR
}


#############
# Functions #
#############
def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Add project name argument
    pn_help = 'Name of new data project.'
    parser.add_argument('proj_name', type=str, help=pn_help)

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def run_init_cks() -> None:
    '''Run initialization checks.'''
    unset_vars = []  # Create unset variables list
    # Check for unset variables
    for req_var_name, req_var_obj in REQ_VARS.items():
        seq_cnd = (
            isinstance(req_var_obj, str)
            or isinstance(req_var_obj, list)
            or isinstance(req_var_obj, tuple)
        )
        if seq_cnd:
            if not req_var_obj:
                unset_vars.append(req_var_name)
        elif isinstance(req_var_obj, dict):
            if not all(req_var_obj.values()):
                unset_vars.append(req_var_name)
    # Display missing variables message and exit
    if unset_vars:
        err_msg = (
            '\nSet the following variable(s) before '
            'running the program again:\n'
            f"{FB}{' '.join(unset_vars)}{FR}"
        )
        print(err_msg)
        sys.exit(1)


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def clean_name(name_pat: re.Pattern, obj_name: str) -> str:
    '''
    Create a clean object name.

    Parameters
    ----------------
    name_pat : re.Pattern
        Name regular expression object that denotes which characters should be
        replaced with an underscore.
    obj_name : str
        String to clean and use as a filename.

    Returns
    ----------------
    str
        Cleaned string to use as a filename.
    '''
    # Replace most non-alphanumeric characters with underscores
    obj_name = re.sub(name_pat, '_', obj_name)
    # Squeeze underscores, lowercase name, strip leading/ending underscores
    obj_name = re.sub(r'([_])(\1+)', r'\1', obj_name.lower()).strip('_')

    return obj_name


def main() -> None:
    '''Define starting point for execution of the program.'''
    _, args = parse_args()

    run_init_cks()

    signal(SIGINT, hndl_sigint)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    logging.debug('Cleaning project name')
    proj_name = clean_name(NAME_PAT, args.proj_name)

    logging.debug('Creating project directory tree')
    try:
        for subdir in PROJ_SUBDIRS:
            subdir_path = os.path.join(PROJS_DIR, proj_name, subdir)
            os.makedirs(subdir_path)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())

        err_msg = f'\nThe following error occurred: {exc}'
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    logging.debug('Creating default Jupyter notebook file')
    try:
        nbt_path = os.path.abspath(NB_TEMPLATE)
        nb_path = os.path.join(
            PROJS_DIR,
            proj_name,
            NB_SUBDIR,
            f'{D_STAMP}_{proj_name}.ipynb'
        )
        shutil.copy2(nbt_path, nb_path)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())

        err_msg = f'\nThe following error occurred: {exc}'
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    logging.debug('Displaying success message')
    proj_dir = os.path.join(PROJS_DIR, proj_name)
    proj_msg = (
        '\nProject tree successfully created at:\n'
        f"{FB}'{proj_dir}'{FR}"
    )
    print(proj_msg)

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
