# Extract Table

Extract HTML tables.

Run the following commands for program documentation:

- `extr_tb.py -h`
- `extr_tb.py sgl -h`
- `extr_tb.py mlt -h`