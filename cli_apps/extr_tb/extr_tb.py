#!/usr/bin/env python3
'''
Extract HTML tables.

A log is saved to 'ex_log_path'.

Functions:

    * val_data_src_arg() - Validate a data source.
    * val_o_path_arg() - Validate an absolute path to save an output file to.
    * val_delim_arg() - Validate a file delimiter.
    * val_file_arg() - Validate a file.
    * parse_args() - Parse program arguments.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * extract_tb() - Extract a table to a CSV file.
    * main() - Define starting point for execution of the program.

Requires
----------------
beautifulsoup4
    A library that makes it easy to scrape information from web pages.
lxml
    Powerful and Pythonic XML processing library combining libxml2/libxslt
    with the ElementTree API.
requests
    A simple, yet elegant, HTTP library.
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import csv  # Enable working with CSV files
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable a class that represents concrete paths of the system's path flavor
from pathlib import Path
# Enable returning Python version as string
from platform import python_version
# Enable regular expression matching operations similar to those found in Perl
import re
from signal import SIGINT, signal  # Enable use of signal handlers
# Enable access to some variables/functions used/maintained by the interpreter
import sys
import tomllib  # Enable an interface for parsing TOML
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type

# Define formatting variables
FB = '\033[1m'
FR = '\033[0m'

bs_parser = 'lxml'  # Set Beautiful Soup parser
# Create mapping of required package name to import statement
REQUIRED_PKGS = {
    'beautifulsoup4': 'from bs4 import BeautifulSoup',
    bs_parser: f'import {bs_parser}',
    'requests': 'import requests'
}
missing_pkgs = []  # Create missing packages list
for pkg_name, imp_stmt in REQUIRED_PKGS.items():
    try:
        exec(imp_stmt)  # Try to import library
    except ImportError:
        missing_pkgs.append(pkg_name)  # Capture missing package
# Exit if packages are missing
if missing_pkgs:
    import_msg = (
        '\nInstall the following packages before proceeding:'
        f"\n{FB}{' '.join(missing_pkgs)}{FR}"
    )
    print(import_msg)
    sys.exit(1)  # Exit with error status

#############
# Variables #
#############
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

# Create delimiter regular expression object
DELIM_PAT = re.compile(r'^[^a-zA-Z0-9]{1}$')
# Define URL regular expression
URL_REGEX = r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.'
URL_REGEX += r'[a-zA-Z0-9()]{1,18}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)'
# Create URL regular expression object
URL_PAT = re.compile(URL_REGEX)

ENC = 'utf-8'  # Define text encoding
# Define generic exception message
EXC_MSG = f'\n{FB}An error occurred...{FR}'


#############
# Functions #
#############
# Validation Functions Start #
def val_data_src_arg(data_src: str) -> str:
    '''
    Validate a data source.

    Parameters
    ----------------
    data_src : str
        Data source to be validated. Can be a URL or local file path.

    Returns
    ----------------
    str
        Validated data source.
    '''
    if bool(URL_PAT.search(data_src)) or os.path.isfile(data_src):
        return data_src
    else:
        err_msg = f'{FB}{data_src}{FR} is not a valid URL or file path.'
        raise argparse.ArgumentTypeError(err_msg)


def val_o_path_arg(o_path: str) -> str:
    '''
    Validate an absolute path to save an output file to.

    Parameters
    ----------------
    o_path : str
        Path to save output file to.

    Returns
    ----------------
    str
        Validated output path.
    '''
    o_path = Path(o_path)

    if os.path.isabs(o_path):
        if os.path.isdir(os.path.dirname(o_path)):
            return o_path
        else:
            err_msg = f'{FB}{o_path}{FR} is not a valid file path.'
            raise argparse.ArgumentTypeError(err_msg)
    else:
        err_msg = f'{FB}{o_path}{FR} is not an absolute file path.'
        raise argparse.ArgumentTypeError(err_msg)


def val_delim_arg(delimiter: str) -> str:
    '''
    Validate a file delimiter.

    Parameters
    ----------------
    delimiter : str
        Delimiter to use. Must be a single symbolic character.

    Returns
    ----------------
    str
        Validated delimiter.
    '''
    if bool(DELIM_PAT.search(delimiter)):
        return delimiter
    else:
        err_msg = f'{FB}{delimiter}{FR} is not a single symbolic character.'
        raise argparse.ArgumentTypeError(err_msg)


def val_file_arg(file_to_val: str) -> str:
    '''
    Validate a file.

    Parameters
    ----------------
    file_to_val : str
        File to be validated.

    Returns
    ----------------
    str
        Validated file.
    '''
    if os.path.isfile(file_to_val):
        return file_to_val
    else:
        err_msg = f'{FB}{file_to_val}{FR} is not a valid file path.'
        raise argparse.ArgumentTypeError(err_msg)
# Validation Functions End #


def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    sp_help = '''\
Determines the mode the program runs in. There are two mutually exclusive
options:

1. Single Table Mode - A single HTML table is extracted and all program
arguments are passed on the command line.
2. Multiple Tables Mode - Multiple HTML tables are extracted. All program
arguments are placed in a TOML configuration file, and the path to the
configuration file is passed as the program argument. Each table to be
extracted is specified as a TOML table in the configuration file.\
'''
    # Create special action object
    subparsers = parser.add_subparsers(help=sp_help)

    # Set sgl mode epilogue to append to subcommand help
    sgl_epilog = f'''\
Example Commands
----------------

URL data source
~~~~~~~~~~~~~~~
'{__file__}' sgl \\
    'https://example.org/example_table' \\
    'example_table.csv' \\
    '|' \\
    'table:nth-child(9)'

File data source
~~~~~~~~~~~~~~~~
'{__file__}' sgl \\
    '/home/tux/pages/example_table.html' \\
    '/home/tux/tables/example_table.csv' \\
    ',' \\
    'table:nth-child(9)'\
'''
    # Create Single Table mode subparser
    sgl_subparser = subparsers.add_parser(
        'sgl',
        epilog=sgl_epilog,
        formatter_class=argparse.RawTextHelpFormatter
    )
    # Add data source argument
    ds_help = 'URL or local file path of data source.'
    sgl_subparser.add_argument('data_src', type=val_data_src_arg,
                               help=ds_help)
    # Add output path argument
    op_help = 'Absolute path to save output CSV file to.'
    sgl_subparser.add_argument('o_path', type=val_o_path_arg,
                               help=op_help)
    # Add delimiter argument
    delim_help = 'Delimiter to use in the output CSV file.'
    sgl_subparser.add_argument('delimiter', type=val_delim_arg,
                               help=delim_help)
    # Add unique table ID argument
    utid_help = 'Unique CSS selector used to target the table of interest.'
    sgl_subparser.add_argument('uniq_tbl_id', type=str, help=utid_help)

    # Set mlt mode epilogue to append to subcommand help
    mlt_epilog = f'''\
Example Command
---------------

'{__file__}' mlt 'example_tables.toml'

Example Configuration File
--------------------------
[example_table]
data_src = "https://example.org/example_table"
o_path = "/home/tux/tables/example_table.csv"
delimiter = "|"
uniq_tbl_id = "table:nth-child(9)"

[example_table_2]
data_src = "https://example.org/example_table_2"
o_path = "example_table_2.csv"
delimiter = ","
uniq_tbl_id = "table:nth-child(16)"

[example_table_3]
data_src = "https://example.org/example_table_3"
o_path = "/home/tux/tables/example_table_3.csv"
delimiter = "|"
uniq_tbl_id = "table:nth-child(19)"\
'''
    # Create Multiple Tables mode subparser
    mlt_subparser = subparsers.add_parser(
        'mlt',
        epilog=mlt_epilog,
        formatter_class=argparse.RawTextHelpFormatter
    )
    # Add configuration file argument
    cfg_help = (
        'Path to file that specifies required arguments in TOML format.'
    )
    mlt_subparser.add_argument('cfg_file', type=val_file_arg, help=cfg_help)

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def extract_tb(data_cont: str, o_path: str,
               delimiter: str, uniq_tbl_id: str) -> None:
    '''
    Extract a table to a CSV file.

    Parameters
    ----------------
    data_cont : str
        Data content to extract a table from.
    o_path : str
        Path to save output CSV file to.
    delimiter : str
        Delimiter to use in the output CSV file.
    uniq_tbl_id : str
        Unique CSS selector used to target the table of interest.
    '''
    # Parse data contents, return BeautifulSoup object
    doc = BeautifulSoup(data_cont, bs_parser)

    # Extract table of interest from BeautifulSoup object, return Tag object
    try:
        table_of_interest = doc.select(uniq_tbl_id)[0]
    except IndexError as exc:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            f'\nError encountered: {FB}{exc}{FR}\n'
            f'Problematic output file: {FB}{o_path}{FR}\n'
            'Verify the unique CSS selector after JavaScript '
            'has been disabled.'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1.')
        sys.exit(1)
    except Exception:
        logging.error(traceback.format_exc().rstrip())
        print(EXC_MSG)
        raise  # Re-raise exception

    # Extract table headers
    header_row = table_of_interest.find('tr')

    # Extract header values
    col_names = [
        header_element.text
        for header_element in header_row
        if header_element != '\n'
    ]
    col_names = [
        column.replace('\n', '')
        for column in col_names
    ]

    # Determine number of rows in table of interest
    num_of_rows = len(table_of_interest.find_all('tr'))
    # Extract data rows from table of interest
    toi_rows = table_of_interest.find_all('tr')

    # Extract table data rows
    data_rows = []  # Create data rows list
    # Convert each row to a list of strings and append it to data_rows
    for i in range(1, num_of_rows):
        row = [
            element.text.replace('\n', '')
            for element in toi_rows[i]
            if element != '\n'
        ]
        data_rows.append(row)

    # Open output file in write mode
    with open(o_path, 'w', encoding=ENC, newline='') as f:
        # Create Writer object
        table_writer = csv.writer(f, delimiter=delimiter)
        table_writer.writerow(col_names)  # Write column row
        # Write rows
        for row in data_rows:
            table_writer.writerow(row)


def main() -> None:
    '''Define starting point for execution of the program.'''
    parser, args = parse_args()

    signal(SIGINT, hndl_sigint)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    if hasattr(args, 'cfg_file'):
        try:
            with open(args.cfg_file, 'rb') as f:
                cfg = tomllib.load(f)  # Load file object contents
        except Exception:
            logging.error(traceback.format_exc().rstrip())
            print(EXC_MSG)
            raise

        tables = [tbl_dict for tbl_dict in cfg.values()]

        for table in tables:
            if os.path.isfile(table['data_src']):
                with open(table['data_src'], encoding=ENC, newline='') as f:
                    # Read file object contents
                    data_cont = f.read()

                # Extract table
                extract_tb(data_cont, table['o_path'], table['delimiter'],
                           table['uniq_tbl_id'])
            else:
                # Obtain page contents as response object
                res = requests.get(table['data_src'])

                # Check for download errors
                try:
                    res.raise_for_status()
                except Exception:
                    logging.error(traceback.format_exc().rstrip())
                    print(EXC_MSG)
                    raise

                data_cont = res.text  # Obtain response object text
                extract_tb(data_cont, table['o_path'], table['delimiter'],
                           table['uniq_tbl_id'])
    elif hasattr(args, 'data_src'):
        if os.path.isfile(args.data_src):
            with open(args.data_src, encoding=ENC, newline='') as f:
                data_cont = f.read()

            extract_tb(data_cont, args.o_path, args.delimiter,
                       args.uniq_tbl_id)
        else:
            res = requests.get(args.data_src)

            try:
                res.raise_for_status()
            except Exception:
                logging.error(traceback.format_exc().rstrip())
                print(EXC_MSG)
                raise

            data_cont = res.text
            extract_tb(data_cont, args.o_path, args.delimiter,
                       args.uniq_tbl_id)
    else:
        logging.debug('No mode specified')

        parser.print_help()
        msg = f'''
~~~

Review the program help and specify a mode before running the program again.

Run the following commands for detailed help on each mode:

- {FB}'{__file__}' sgl -h{FR}
- {FB}'{__file__}' mlt -h{FR}\
'''
        print(msg)

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
