#!/usr/bin/env python3
'''CRUD CSV function tests.'''
import csv  # Enable classes to read and write tabular data in CSV format
from io import StringIO  # Enable text stream using an in-memory text buffer
# Enable a portable way of using operating system dependent functionality
import os
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable access to some variables used/maintained by the interpreter and to
# functions that strongly interact with the interpreter
import sys
sys.path.append('../py_projs')
# Enable a rich set of tools for constructing and running tests
import unittest as ut
from unittest.mock import mock_open, patch

# Enable a collection of functions that validate various inputs
import crud_csv as crud

NAME_REGEX = r'[^a-zA-Z0-9.]'  # Define name regular expression
# Create name regular expression object
NAME_PAT = re.compile(NAME_REGEX)


class TestCleanName(ut.TestCase):
    def test_name_cleaning(self):
        '''Test name cleaning.'''
        obj_name = 'HOW@DOES*A)MAN&DECIDE$IN+WHAT-ORDER#TO=ABANDON!HIS>LIFE'
        clean_name = 'how_does_a_man_decide_in_what_order_to_abandon_his_life'
        self.assertEqual(crud.clean_name(NAME_PAT, obj_name), clean_name)


class TestCreateStringList(ut.TestCase):
    crt_input = ['h1', 'h2', 'h3', 's']
    term_input = ['s']

    prompt = 'Enter column name (s to stop): '

    @patch('builtins.input', side_effect=crt_input)
    def test_create_string_list(self, mock_input):
        '''Test creating a list of strings.'''
        created_list = crud.crt_str_list(self.prompt)
        self.assertEqual(created_list, self.crt_input[:3])

    @patch('builtins.input', side_effect=term_input)
    def test_create_string_list_termination_input(self, mock_input):
        '''Test creating a list of strings with only termination input.'''
        rtn_value = crud.crt_str_list(self.prompt)
        self.assertIs(rtn_value, None)


class TestCreateCsvFile(ut.TestCase):
    f_path = '/tmp/file.csv'
    headings = ['h1', 'h2', 'h3']

    def test_create_csv_file(self):
        '''Test creating a CSV file.'''
        expected_row = 'h1|h2|h3\r\n'

        with patch('builtins.open', mock_open()) as mocked_file:
            crud.crt_csv_file(self.f_path, self.headings)
            mocked_file.assert_called_once_with(self.f_path, 'w',
                                                encoding='utf-8', newline='')
            mocked_file().write.assert_called_once_with(expected_row)

    def test_create_csv_file_with_custom_encoding_and_delimiter(self):
        '''Test creating a CSV file with a custom encoding and delimiter.'''
        c_enc = 'ascii'
        c_delim = ','
        c_expected_row = 'h1,h2,h3\r\n'

        with patch('builtins.open', mock_open()) as mocked_file:
            crud.crt_csv_file(self.f_path, self.headings,
                              enc=c_enc, delim=c_delim)
            mocked_file.assert_called_once_with(self.f_path, 'w',
                                                encoding=c_enc,
                                                newline='')
            mocked_file().write.assert_called_once_with(c_expected_row)


class TestCreateCsvObject(ut.TestCase):
    headings_input = ['h1', 'h2', 'h3', 's']
    no_hdgs_input = ['s']

    f_path = '/tmp/file.csv'
    delim = '|'

    @patch('builtins.input', side_effect=headings_input)
    def test_create_csv_object(self, mock_input):
        '''Test creating a CsvFileMd object.'''
        csv_obj = crud.crt_csv_obj(self.f_path, self.delim)
        self.assertIsInstance(csv_obj, crud.CsvFileMd)
        self.assertEqual(csv_obj.path, self.f_path)
        self.assertEqual(csv_obj.delimiter, self.delim)
        self.assertEqual(csv_obj.num_of_rows, 1)
        self.assertEqual(csv_obj.num_of_cols, 3)
        os.unlink(self.f_path)

    @patch('builtins.input', side_effect=headings_input)
    def test_create_csv_object_exception(self, mock_input):
        '''Test exception capture during CsvFileMd object creation.'''
        self.assertRaises(Exception, crud.crt_csv_obj, self.f_path, '||')
        os.unlink(self.f_path)

    @patch('builtins.input', side_effect=no_hdgs_input)
    def test_create_csv_object_no_headings(self, mock_input):
        '''Test creating a CsvFileMd object if no headings given.'''
        rtn_value = crud.crt_csv_obj(self.f_path, self.delim)
        self.assertIs(rtn_value, None)


class TestViewCSV(ut.TestCase):
    f_path = '/tmp/file.csv'
    delim = '|'

    def test_view_csv(self):
        '''Test viewing a CSV file.'''
        f_content = [
            ('h1', 'h2', 'h3'),
            ('one', 'two', 'three'),
            ('uno', 'dos', 'tres'),
            ('un', 'deus', 'trois')
        ]

        with open(self.f_path, 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=self.delim)
            writer.writerows(f_content)

        output = (
            'Row index 1\n'
            '-----------\n'
            'h1 (0): one\n'
            'h2 (1): two\n'
            'h3 (2): three\n'
            '\n'
            'Row index 2\n'
            '-----------\n'
            'h1 (0): uno\n'
            'h2 (1): dos\n'
            'h3 (2): tres\n'
            '\n'
            'Row index 3\n'
            '-----------\n'
            'h1 (0): un\n'
            'h2 (1): deus\n'
            'h3 (2): trois'
        )
        csv_obj = crud.CsvFileMd(self.f_path, self.delim)

        with patch('sys.stdout', new=StringIO()) as mock_out:
            self.maxDiff = None
            crud.view_csv(csv_obj)
            self.assertEqual(mock_out.getvalue(), output)

        os.unlink(self.f_path)


class TestViewHdgs(ut.TestCase):
    f_path = '/tmp/file.csv'
    delim = '|'

    def test_view_hdgs(self):
        '''Test viewing a CSV file's headers.'''
        f_content = [
            ('h1', 'h2', 'h3'),
            ('one', 'two', 'three'),
            ('uno', 'dos', 'tres'),
            ('un', 'deus', 'trois')
        ]

        with open(self.f_path, 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=self.delim)
            writer.writerows(f_content)

        output = (
            'Headings\n-------'
            '\nh1 (0)'
            '\nh2 (1)'
            '\nh3 (2)'
        )
        csv_obj = crud.CsvFileMd(self.f_path, self.delim)

        with patch('sys.stdout', new=StringIO()) as mock_out:
            self.maxDiff = None
            crud.view_hdgs(csv_obj)
            self.assertEqual(mock_out.getvalue(), output)

        os.unlink(self.f_path)


class TestAddRow(ut.TestCase):
    rows_input = ['uno', 'dos', 'tres', 'y',
                  'un', 'deus', 'trois', 'n']

    f_path = '/tmp/file.csv'
    delim = '|'

    @patch('builtins.input', side_effect=rows_input)
    def test_add_rows(self, mock_input):
        '''Test adding rows.'''
        rows = [
            ('h1', 'h2', 'h3'),
            ('one', 'two', 'three'),
        ]

        with open(self.f_path, 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=self.delim)
            writer.writerows(rows)

        final_content = [
            'h1|h2|h3\r\n',
            'one|two|three\r\n',
            'uno|dos|tres\r\n',
            'un|deus|trois\r\n'
        ]
        csv_obj = crud.CsvFileMd(self.f_path, self.delim)
        csv_obj = crud.add_row(csv_obj)

        with open(self.f_path, 'r', encoding='utf-8', newline='') as f:
            lines = f.readlines()
            self.assertEqual(lines, final_content)

        os.unlink(self.f_path)


class TestDeleteRow(ut.TestCase):
    index_input = ['1']

    f_path = '/tmp/file.csv'
    delim = '|'

    @patch('builtins.input', side_effect=index_input)
    def test_delete_row(self, mock_input):
        '''Test deleting a row.'''
        rows = [
            ('h1', 'h2', 'h3'),
            ('one', 'two', 'three'),
            ('uno', 'dos', 'tres'),
            ('un', 'deus', 'trois')
        ]

        with open(self.f_path, 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=self.delim)
            writer.writerows(rows)

        final_content = [
            'h1|h2|h3\r\n',
            'uno|dos|tres\r\n',
            'un|deus|trois\r\n'
        ]
        csv_obj = crud.CsvFileMd(self.f_path, self.delim)
        csv_obj = crud.delete_row(csv_obj)

        with open(self.f_path, 'r', encoding='utf-8', newline='') as f:
            lines = f.readlines()
            self.assertEqual(lines, final_content)

        os.unlink(self.f_path)


class TestAddColumn(ut.TestCase):
    col_input = ['h4', 'na']

    f_path = '/tmp/file.csv'
    delim = '|'

    @patch('builtins.input', side_effect=col_input)
    def test_add_column(self, mock_input):
        '''Test adding a column.'''
        rows = [
            ('h1', 'h2', 'h3'),
            ('one', 'two', 'three'),
            ('uno', 'dos', 'tres'),
            ('un', 'deus', 'trois')
        ]

        with open(self.f_path, 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=self.delim)
            writer.writerows(rows)

        final_content = [
            'h1|h2|h3|h4\r\n',
            'one|two|three|na\r\n',
            'uno|dos|tres|na\r\n',
            'un|deus|trois|na\r\n'
        ]
        csv_obj = crud.CsvFileMd(self.f_path, self.delim)
        csv_obj = crud.add_column(csv_obj)

        with open(self.f_path, 'r', encoding='utf-8', newline='') as f:
            lines = f.readlines()
            self.assertEqual(lines, final_content)

        os.unlink(self.f_path)


class TestDeleteColumn(ut.TestCase):
    idx_input = ['1']

    f_path = '/tmp/file.csv'
    delim = '|'

    @patch('builtins.input', side_effect=idx_input)
    def test_delete_column(self, mock_input):
        '''Test deleting a column.'''
        rows = [
            ('h1', 'h2', 'h3'),
            ('one', 'two', 'three'),
            ('uno', 'dos', 'tres'),
            ('un', 'deus', 'trois')
        ]

        with open(self.f_path, 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=self.delim)
            writer.writerows(rows)

        final_content = [
            'h1|h3\r\n',
            'one|three\r\n',
            'uno|tres\r\n',
            'un|trois\r\n'
        ]
        csv_obj = crud.CsvFileMd(self.f_path, self.delim)
        csv_obj = crud.delete_column(csv_obj)

        with open(self.f_path, 'r', encoding='utf-8', newline='') as f:
            lines = f.readlines()
            self.assertEqual(lines, final_content)

        os.unlink(self.f_path)


class TestUpdateCell(ut.TestCase):
    cell_input = ['3', '2', 'trois']

    f_path = '/tmp/file.csv'
    delim = '|'

    @patch('builtins.input', side_effect=cell_input)
    def test_update_cell(self, mock_input):
        '''Test updating a cell.'''
        rows = [
            ('h1', 'h2', 'h3'),
            ('one', 'two', 'three'),
            ('uno', 'dos', 'tres'),
            ('un', 'deus', 'trais')
        ]

        with open(self.f_path, 'w', encoding='utf-8', newline='') as f:
            writer = csv.writer(f, delimiter=self.delim)
            writer.writerows(rows)

        final_content = [
            'h1|h2|h3\r\n',
            'one|two|three\r\n',
            'uno|dos|tres\r\n',
            'un|deus|trois\r\n'
        ]
        csv_obj = crud.CsvFileMd(self.f_path, self.delim)
        csv_obj = crud.update_cell(csv_obj)

        with open(self.f_path, 'r', encoding='utf-8', newline='') as f:
            lines = f.readlines()
            self.assertEqual(lines, final_content)

        os.unlink(self.f_path)


if __name__ == '__main__':
    ut.main(buffer=True)
    ut.main()
