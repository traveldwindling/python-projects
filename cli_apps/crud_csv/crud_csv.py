#!/usr/bin/env python3
'''
Perform CRUD operations on CSV files.

A log is saved to 'ex_log_path'.

Classes:

    * CsvFileMd - Model a CSV file's metadata.

Functions:

    * val_dir() - Validate a directory.
    * val_file() - Validate a file.
    * val_delimiter() - Validate a file delimiter.
    * val_idx() - Validate an item index.
    * set_dir() - Set a directory path.
    * set_file() - Set a file path.
    * set_delimiter() - Set a delimiter.
    * set_idx() - Set an item index.
    * parse_args() - Parse program arguments.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * clean_name() - Create a clean object name.
    * crt_str_list() - Create a list of strings.
    * crt_csv_file() - Create a CSV file.
    * crt_csv_obj() - Create a CsvFileMd instance after obtaining headings
                      row values.
    * view_csv() - Display CSV file contents.
    * view_hdgs() - Display CSV file headings.
    * add_row() - Add a row to a CSV file.
    * delete_row() - Delete a row from a CSV file.
    * add_column() - Add a column to a CSV file.
    * delete_column() - Delete a column from a CSV file.
    * update_cell() - Update a cell in a CSV file.
    * menu() - Display main menu and process user choice.
    * main() - Define starting point for execution of the program.
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import csv  # Enable classes to read and write tabular data in CSV format
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version as string
from platform import python_version
import pydoc  # Enable automatic documentation generation from Python modules
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable a number of high-level operations on files and collections of files
import shutil
from signal import SIGINT, signal  # Enable use of signal handlers
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a file-like object that can be used as a temporary storage area
from tempfile import NamedTemporaryFile
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type

#############
# Variables #
#############
# Define formatting variables
FB = '\033[1m'
FI = '\033[3m'
FR = '\033[0m'

# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

NAME_REGEX = r'[^a-zA-Z0-9.]'  # Define name regular expression
# Create name regular expression object
NAME_PAT = re.compile(NAME_REGEX)

# Create delimiter regular expression object
DELIM_PAT = re.compile(r'^[^a-zA-Z0-9]{1}$')

ENC = 'utf-8'  # Define text encoding

# Define invalid file message
INV_MSG = (
    f'\n{FB}Invalid file provided. Confirm that it '
    f'contains data.{FR}'
)
# Define no data message
NO_DATA_MSG = (
    f'\n{FB}There is only a headings row. '
    f'No data to operate on.{FR}'
)

# Define menu characters and title
SEP_CHAR, UND_CHAR = '*', '~'
MENU_TI = 'CRUD CSV'

# Calculate menu length and generate decoration strings
MT_LEN = len(MENU_TI)
SEP_LEN = MT_LEN + 5
UND_STR, SEP_STR = (UND_CHAR * MT_LEN), (SEP_CHAR * SEP_LEN)

# Define menu string
MENU_PRE = f'''
{SEP_STR}

{MENU_TI}
'''

MENU_POST = f'''\
{UND_STR}
 c) Create CSV File
 v) View CSV File
 ar) Add a Row
 dr) Delete a Row
 ac) Add a Column
 dc) Delete a Column
  u) Update a Cell
  s) Set Current File
  q) Quit\
'''


###########
# Classes #
###########
class CsvFileMd:
    '''
    Model a CSV file's metadata.

    Methods:

        * __init__() - Initialize the class with stateful data.
        * __repr__() - Return a string containing a printable representation
                       of an  object.
        * calc_dims() Calculate dimensions (number of rows/columns)
                      of CSV file.
    '''

    def __init__(self, path: str, delim: str) -> None:
        '''
        Initialize the class with stateful data.

        Parameters
        ----------------
        path : str
            Path of CSV file.
        delim : str
            CSV file delimiter.
        '''
        self.path = path
        self.delimiter = delim
        self.calc_dims()

    def __repr__(self) -> str:
        '''
        Return a string containing a printable representation of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'path={self.path!r}, '
            f'delimiter={self.delimiter!r}, '
            f'num_of_rows={self.num_of_rows!r}, '
            f'num_of_cols={self.num_of_cols!r})'
        )

    def calc_dims(self) -> None:
        '''
        Calculate dimensions (number of rows/columns) of CSV file.
        '''
        with open(self.path, 'r', encoding=ENC, newline='') as f:
            reader = csv.reader(f, delimiter=self.delimiter)

            reader_list = list(reader)  # Convert reader to 2d list

            self.num_of_rows = len(reader_list)
            try:
                self.num_of_cols = len(reader_list[0])
            except IndexError:  # Check for empty files
                logging.error(traceback.format_exc().rstrip())
                self.num_of_cols = None


#############
# Functions #
#############
# Validation Functions Start #
def val_dir(dir_to_val: str) -> bool:
    '''
    Validate a directory.

    Parameters
    ----------------
    dir_to_val : str
        Directory to validate.

    Returns
    ----------------
    bool
        Whether provided directory path is valid or not.
    '''
    return os.path.isdir(dir_to_val)


def val_file(file_to_val: str) -> bool:
    '''
    Validate a file.

    Parameters
    ----------------
    file_to_val : str
        File to be validated.

    Returns
    ----------------
    bool
        Whether provided file path is valid or not.
    '''
    return os.path.isfile(file_to_val)


def val_delimiter(delim_pat: re.Pattern, delimiter: str) -> bool:
    '''
    Validate a file delimiter.

    Parameters
    ----------------
    delim_pat : re.Pattern
        Delimiter regular expression object to validate against.
    delimiter : str
        Delimiter to use. Must be a single symbolic character.

    Returns
    ----------------
    bool
        Whether delimiter is a valid delimiter.
    '''
    return bool(delim_pat.search(delimiter))


def val_idx(idx: int, num_of_items: int) -> bool:
    '''
    Validate an item index.

    Parameters
    ----------------
    idx : int
        Index to validate.
    num_of_items : int
        Number of items in the data structure to validate against.

    Returns
    ----------------
    bool
        Whether index is a valid item index.
    '''
    try:
        idx = abs(int(idx))
    except ValueError:
        logging.error(traceback.format_exc().rstrip())
        return False
    else:
        return bool(idx < num_of_items)
# Validation Functions End #


# Set Functions Start #
def set_dir(prompt: str) -> str:
    '''
    Set a directory path.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        A directory path.
    '''
    while True:
        dir_to_val = input(prompt).strip().rstrip('/')

        if dir_to_val == 'q':
            sys.exit(0)
        elif val_dir(dir_to_val):
            return dir_to_val
        else:
            print('\nInvalid directory. Try again or enter q to quit.')


def set_file(prompt: str) -> str:
    '''
    Set a file path.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        A file path.
    '''
    while True:
        file_to_val = input(prompt).strip()

        if file_to_val == 'q':
            sys.exit(0)
        elif val_file(file_to_val):
            return file_to_val
        else:
            print('\nInvalid file. Try again or enter q to quit.')


def set_delimiter(prompt: str) -> str:
    '''
    Set a delimiter.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        A delimiter.
    '''
    while True:
        delimiter = input(prompt).strip()

        if delimiter == 'q':
            sys.exit(0)
        elif val_delimiter(DELIM_PAT, delimiter):
            return delimiter
        else:
            print('\nInvalid delimiter. Try again or enter q to quit.\n')


def set_idx(prompt: str, num_of_items: int) -> int:
    '''
    Set an item index.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.
    num_of_items : int
        Number of items in the data structure to validate against.

    Returns
    ----------------
    int
        An item index.
    '''
    while True:
        idx = input(prompt).strip()

        if idx == 'q':
            sys.exit(0)
        elif val_idx(idx, num_of_items):
            return abs(int(idx))
        else:
            print('\nInvalid index. Try again or enter q to quit.\n')
# Set Functions End #


def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def clean_name(name_pat: re.Pattern, obj_name: str) -> str:
    '''
    Create a clean object name.

    Parameters
    ----------------
    name_pat : re.Pattern
        Name regular expression object that denotes which characters should be
        replaced with an underscore.
    obj_name : str
        String to clean and use as a filename.

    Returns
    ----------------
    str
        Cleaned string to use as a filename.
    '''
    # Replace most non-alphanumeric characters with underscores
    obj_name = re.sub(name_pat, '_', obj_name)
    # Squeeze underscores, lowercase name, strip leading/ending underscores
    obj_name = re.sub(r'([_])(\1+)', r'\1', obj_name.lower()).strip('_')

    return obj_name


def crt_str_list(prompt: str) -> list[str] | None:
    '''
    Create a list of strings.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    list | None
        List of input strings.
    '''
    str_list = []  # Create string list

    # Collect strings
    print()
    while True:
        item = input(prompt).strip()

        if item == 's':
            break
        else:
            str_list.append(item)

    if str_list:
        return str_list
    else:
        return None


def crt_csv_file(f_path: str, headings: list[str],
                 enc: str = 'utf-8', delim: str = '|') -> None:
    '''
    Create a CSV file.

    Parameters
    ----------------
    f_path : str
        Path to save CSV file to.
    headings : list
        CSV file headings.
    enc : str (optional)
        Encoding to use for CSV file. Defaults to utf-8.
    delim : str (optional)
        Delimiter to use in CSV file. Defaults to a |.
    '''
    # Create CSV file and write headings row
    with open(f_path, 'w', encoding=enc, newline='') as f:
        writer = csv.writer(f, delimiter=delim)
        writer.writerow(headings)


def crt_csv_obj(f_path: str, delim: str, enc: str = 'utf-8') \
        -> CsvFileMd | None:
    '''
    Create a CsvFileMd instance after obtaining heading row values.

    Parameters
    ----------------
    f_path : str
        Path to save CSV file to.
    delim : str
        Delimiter to use in CSV file.
    enc : str (optional)
        Encoding to use for CSV file. Defaults to utf-8.

    Returns
    ----------------
    CsvFileMd | None
        CSV file metadata object or None.
    '''
    prompt = 'Enter column name (s to stop): '
    headings = crt_str_list(prompt)

    if headings:
        try:
            crt_csv_file(f_path, headings, delim=delim)
        except Exception:
            logging.error(traceback.format_exc().rstrip())

            print(f'\n{FB}Error encountered...{FR}\n')
            raise  # Re-raise exception
        else:
            # Create CsvFileMd instance
            csv_obj = CsvFileMd(f_path, delim)

            return csv_obj
    else:
        return None


def view_csv(csv_obj: CsvFileMd) -> None:
    '''
    Display CSV file contents.

    Column and row indices are added to the output content to aid with
    CSV editing operations.

    Parameters
    ----------------
    csv_obj : CsvFileMd
        CSV file metadata object.
    '''
    with open(csv_obj.path, 'r', encoding=ENC, newline='') as f:
        reader = csv.reader(f, delimiter=csv_obj.delimiter)

        reader_gen = (row for row in reader)  # Convert reader to generator

        col_names = next(reader_gen)  # Get column names
        # Determine max length of column info to use in output string
        max_len = max([
            len(f'{col_name} ({col_i}): ')
            for col_i, col_name in enumerate(col_names)
        ])

        output = ''  # Create output string
        for row_i, row in enumerate(reader_gen, start=1):
            row_hdr_len = len(f'Row index {row_i}')
            output += f"Row index {row_i}\n{'-' * row_hdr_len}"
            for col_i, col_name in enumerate(col_names):
                output += (
                    f"\n{col_name + ' (' + str(col_i) + '): ':{max_len}}"
                    f'{row[col_i]}'
                )
            output += '\n\n'

        pydoc.pager(output.rstrip())  # Send final string to paginator


def view_hdgs(csv_obj: CsvFileMd) -> None:
    '''
    Display CSV file headings.

    Column indices are added to the output content to aid with
    CSV editing operations.

    Parameters
    ----------------
    csv_obj : CsvFileMd
        CSV file metadata object.
    '''
    with open(csv_obj.path, 'r', encoding=ENC, newline='') as f:
        reader = csv.reader(f, delimiter=csv_obj.delimiter)

        col_names = list(reader)[0]

        output = 'Headings\n-------'
        for col_i, col_name in enumerate(col_names):
            output += f"\n{col_name + ' (' + str(col_i) + ')'}"

        pydoc.pager(output)


def add_row(csv_obj: CsvFileMd) -> CsvFileMd:
    '''
    Add a row to a CSV file.

    Assumes that the CSV file in question's first row is a headings row.

    Parameters
    ----------------
    csv_obj : CsvFileMd
        CSV file metadata object.

    Returns
    ----------------
    CsvFileMd
        CSV file metadata object.
    '''
    with open(csv_obj.path, 'a+', encoding=ENC, newline='') as f:
        f.seek(0)  # Change stream position to beginning of file
        # Obtain list of column names
        columns = f.readline().rstrip().split(csv_obj.delimiter)

        writer = csv.writer(f, delimiter=csv_obj.delimiter)

        # Construct row to add
        while True:
            row = []
            print()
            for column in columns:
                cell = input(f'Enter {FB}{column}{FR} value: ').strip()
                row.append(cell)
            writer.writerow(row)

            # Allow for entry of another row
            prompt = '\nAdd another row? (y or n): '
            choice = input(prompt).lower().strip()

            # Validate choice
            while choice != 'y' and choice != 'n':
                print('\nInvalid choice. Try again or enter q to quit.')
                choice = input(prompt).lower().strip()
                if choice == 'q':
                    sys.exit(0)

            if choice == 'n':
                break

    return csv_obj


def delete_row(csv_obj: CsvFileMd) -> CsvFileMd:
    '''
    Delete a row from a CSV file.

    Parameters
    ----------------
    csv_obj : CsvFileMd
        CSV file metadata object.

    Returns
    ----------------
    CsvFileMd
        CSV file metadata object.
    '''
    # Get index of row to remove
    print()
    prompt = 'Enter index of row to remove: '
    row_idx = set_idx(prompt, csv_obj.num_of_rows)

    # Create temporary file
    temp_file = NamedTemporaryFile('w+t', newline='', delete=False)
    # Open file path as csv_file and temp_file
    with open(csv_obj.path, 'r', encoding=ENC,
              newline='') as csv_file, temp_file:
        # Create Reader object from csv_file
        reader = csv.reader(csv_file, delimiter=csv_obj.delimiter)
        # Create Writer object from temp_file
        writer = csv.writer(temp_file, delimiter=csv_obj.delimiter)

        # Loop through each row in reader
        for i, row in enumerate(reader):
            if i == row_idx:
                continue
            writer.writerow(row)

    # Replace original file with temporary file
    shutil.move(temp_file.name, csv_obj.path)

    return csv_obj


def add_column(csv_obj: CsvFileMd) -> CsvFileMd:
    '''
    Add a column to a CSV file.

    Parameters
    ----------------
    csv_obj : CsvFileMd
        CSV file metadata object.

    Returns
    ----------------
    CsvFileMd
        CSV file metadata object.
    '''
    # Get name of column to add
    col_name = input('\nEnter name of column to add: ').strip()

    temp_file = NamedTemporaryFile('w+t', newline='', delete=False)
    with open(csv_obj.path, 'r', encoding=ENC,
              newline='') as csv_file, temp_file:
        reader = list(csv.reader(csv_file, delimiter=csv_obj.delimiter))
        writer = csv.writer(temp_file, delimiter=csv_obj.delimiter)

        reader[0].append(col_name)  # Add new column name to first row

        # Obtain default value for new column
        def_col_val = input('Enter default column value: ').strip()

        # Add default value for new column to each row
        for row in reader[1:]:
            row.append(def_col_val)

        for row in reader:
            writer.writerow(row)

    shutil.move(temp_file.name, csv_obj.path)

    return csv_obj


def delete_column(csv_obj: CsvFileMd) -> CsvFileMd:
    '''
    Delete a column from a CSV file.

    Parameters
    ----------------
    csv_obj : CsvFileMd
        CSV file metadata object. Defaults to None.

    Returns
    ----------------
    CsvFileMd
        CSV file metadata object.
    '''
    # Get index of column to remove
    print()
    prompt = 'Enter index of column to remove: '
    col_idx = set_idx(prompt, csv_obj.num_of_cols)

    temp_file = NamedTemporaryFile('w+t', newline='', delete=False)
    with open(csv_obj.path, 'r', encoding=ENC,
              newline='') as csv_file, temp_file:
        reader = list(csv.reader(csv_file, delimiter=csv_obj.delimiter))
        writer = csv.writer(temp_file, delimiter=csv_obj.delimiter)

        for row in reader:
            del row[col_idx]
            writer.writerow(row)

    shutil.move(temp_file.name, csv_obj.path)

    return csv_obj


def update_cell(csv_obj: CsvFileMd) -> CsvFileMd:
    '''
    Update a cell in a CSV file.

    Parameters
    ----------------
    csv_obj : CsvFileMd
        CSV file metadata object.

    Returns
    ----------------
    CsvFileMd
        CSV file metadata object.
    '''
    # Get index of row to update
    prompt = 'Enter index of row to update: '
    row_idx = set_idx(prompt, csv_obj.num_of_rows)

    # Get index of column to update
    prompt = 'Enter index of column to update: '
    col_idx = set_idx(prompt, csv_obj.num_of_cols)

    # Get new cell value
    new_val = input('Enter new cell value: ').strip()

    temp_file = NamedTemporaryFile('w+t', newline='', delete=False)
    with open(csv_obj.path, 'r', encoding=ENC,
              newline='') as csv_file, temp_file:
        columns = csv_file.readline().rstrip().split(csv_obj.delimiter)
        csv_file.seek(0)

        reader = csv.reader(csv_file, delimiter=csv_obj.delimiter)
        writer = csv.writer(temp_file, delimiter=csv_obj.delimiter)

        for i_row, row in enumerate(reader):
            # If row matches the row of interest
            if i_row == int(row_idx):
                new_row = []
                for i_col, column in enumerate(columns):
                    # If cell matches the column of interest
                    if i_col == int(col_idx):
                        new_row.append(new_val)
                    else:
                        new_row.append(row[i_col])
                writer.writerow(new_row)
            else:
                writer.writerow(row)

    shutil.move(temp_file.name, csv_obj.path)

    return csv_obj


def menu(csv_obj: CsvFileMd | None = None) -> None:
    '''
    Display main menu and process user choice.

    Parameters
    ----------------
    csv_obj : CsvFileMd | None (optional)
        CSV file metadata object. Defaults to None.
    '''
    # Check if current file is set
    if csv_obj:
        # Define current file string
        curr_file = f'Current file: {FI}{csv_obj.path}{FR}\n'
        # Create menu string variable
        menu_str = f'{MENU_PRE}{curr_file}{MENU_POST}'
    else:
        menu_str = f'{MENU_PRE}{MENU_POST}'

    print(menu_str)  # Display menu options

    # Store answer
    menu_answer = input('\nEnter menu selection: ').lower().strip()

    # Evaluate user menu selection
    match menu_answer:
        case 'c':
            prompt = '\nEnter directory to save CSV file to: '
            f_dir = set_dir(prompt)

            prompt = 'Enter save as CSV file name: '
            f_name = input(prompt).strip()
            if not f_name.endswith('.csv'):
                f_name = f'{f_name}.csv'
            f_name = clean_name(NAME_PAT, f_name)

            prompt = 'Enter delimiter to use in CSV file: '
            delim = set_delimiter(prompt)

            path = os.path.join(f_dir, f_name)
            csv_obj = crt_csv_obj(path, delim)

            if csv_obj is None:
                menu()
            else:
                menu(csv_obj)
        case 'v':
            if csv_obj is None:
                prompt = '\nEnter path of CSV file: '
                f_path = set_file(prompt)

                prompt = 'Enter CSV file delimiter: '
                delim = set_delimiter(prompt)

                csv_obj = CsvFileMd(f_path, delim)

            if csv_obj.num_of_rows > 1:
                view_csv(csv_obj)

                menu(csv_obj)
            elif csv_obj.num_of_rows == 1:
                view_hdgs(csv_obj)

                menu(csv_obj)
            else:
                print(INV_MSG)

                menu()
        case 'ar':
            if csv_obj is None:
                prompt = '\nEnter path of CSV file: '
                f_path = set_file(prompt)

                prompt = 'Enter CSV file delimiter: '
                delim = set_delimiter(prompt)

                csv_obj = CsvFileMd(f_path, delim)

            if csv_obj.num_of_rows >= 1:
                csv_obj = add_row(csv_obj)
                csv_obj.calc_dims()

                menu(csv_obj)
            else:
                print(INV_MSG)

                menu()
        case 'dr':
            if csv_obj is None:
                prompt = '\nEnter path of CSV file: '
                f_path = set_file(prompt)

                prompt = 'Enter CSV file delimiter: '
                delim = set_delimiter(prompt)

                csv_obj = CsvFileMd(f_path, delim)

            if csv_obj.num_of_rows > 1:
                csv_obj = delete_row(csv_obj)
                csv_obj.calc_dims()

                menu(csv_obj)
            elif csv_obj.num_of_rows == 1:
                print(NO_DATA_MSG)

                menu(csv_obj)
            else:
                print(INV_MSG)

                menu()
        case 'ac':
            if csv_obj is None:
                prompt = '\nEnter path of CSV file: '
                f_path = set_file(prompt)

                prompt = 'Enter CSV file delimiter: '
                delim = set_delimiter(prompt)

                csv_obj = CsvFileMd(f_path, delim)

            if csv_obj.num_of_rows >= 1:
                csv_obj = add_column(csv_obj)
                csv_obj.calc_dims()

                menu(csv_obj)
            else:
                print(INV_MSG)

                menu()
        case 'dc':
            if csv_obj is None:
                prompt = '\nEnter path of CSV file: '
                f_path = set_file(prompt)

                prompt = 'Enter CSV file delimiter: '
                delim = set_delimiter(prompt)

                csv_obj = CsvFileMd(f_path, delim)

            if csv_obj.num_of_rows >= 1:
                csv_obj = delete_column(csv_obj)
                csv_obj.calc_dims()

                menu(csv_obj)
            else:
                print(INV_MSG)

                menu()
        case 'u':
            if csv_obj is None:
                prompt = '\nEnter path of CSV file: '
                f_path = set_file(prompt)

                prompt = 'Enter CSV file delimiter: '
                delim = set_delimiter(prompt)

                csv_obj = CsvFileMd(f_path, delim)

            if csv_obj.num_of_rows >= 1:
                print()
                csv_obj = update_cell(csv_obj)

                menu(csv_obj)
            else:
                print(INV_MSG)

                menu()
        case 's':
            prompt = '\nEnter path of CSV file: '
            f_path = set_file(prompt)

            prompt = 'Enter CSV file delimiter: '
            delim = set_delimiter(prompt)

            csv_obj = CsvFileMd(f_path, delim)

            if csv_obj.num_of_rows >= 1:
                menu(csv_obj)
            else:
                print(INV_MSG)

                menu()
        case 'q':
            pass
        case _:
            print(f'\n{FB}Please enter a valid menu option.{FR}')

            menu(csv_obj)


def main() -> None:
    '''Define starting point for execution of the program.'''
    _, _ = parse_args()

    signal(SIGINT, hndl_sigint)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    menu()  # Display menu

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
