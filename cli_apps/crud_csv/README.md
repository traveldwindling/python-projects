# CRUD CSV

Perform CRUD operations on CSV files.

## Sample Output

```console

*************

CRUD CSV
~~~~~~~~
 c) Create CSV file
 v) View CSV file
ar) Add a row
dr) Delete a row
ac) Add a column
dc) Delete a column
 u) Update a cell
 s) Set current file
 q) Quit

Enter menu selection: 
```