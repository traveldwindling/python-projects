'''
A collection of extract, transform, and load functions.

Functions:

    * clean_sql_stmt() - Clean a SQL statement.
    * sl_crt_conn() - Connect to a SQLite database file.
    * sl_exec_qry() - Execute a SQLite SQL query.
    * get_db_md() - Get database metadata.
    * crt_n_type_map() - Create a column name to pandas data type mapping.
    * exec_txt_qry() - Execute a text-based SQL query.
    * imp_files_pd() - Return a generator iterator of pandas DataFrames
                       created from a directory of files.

Requires
----------------
pandas
    Powerful data structures for data analysis, time series, and statistics
pyarrow
    Python library for Apache Arrow
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.
'''
###########
# Modules #
###########
# Enable abstract base class for classes that provide
# the .__iter__() and .__next__() methods
from collections.abc import Iterator
# Enable returning a possibly empty list of path names that match pathname,
# which must be a string containing a path specification
from glob import glob
# Enable a portable way of using operating system dependent functionality
import os
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable a SQL interface compliant with the DB-API 2.0 specification
import sqlite3
from typing import Any  # Enable special kind of type

import pandas as pd  # Enable data structures and data analysis tools
import sqlalchemy as sa  # Enable database abstraction library


#############
# Functions #
#############
def clean_sql_stmt(stmt: str) -> str:
    '''
    Clean a SQL statement.

    Several transformations are performed to reduce the input string
    into a single-line, clean SQL statement.

    Parameters
    ----------------
    stmt : str
        SQL statement to clean.

    Returns
    ----------------
    str
        Cleaned SQL statement.
    '''
    transforms = [
        # Remove semicolons
        (';', ''),
        # Remove comments
        (r'/\*[^+]([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/', ''),
        (r'--[^+].*', ''),
        # Replace line breaks and spaces with single space
        (r'\s+', ' ')
    ]

    for pattern, replacement in transforms:
        stmt = re.sub(pattern, replacement, stmt)

    return stmt.strip()


def sl_crt_conn(f_path: str) -> sqlite3.Connection:
    '''
    Connect to a SQLite database file.

    Parameters
    ----------------
    f_path : str
        SQLite database file path.

    Returns
    ----------------
    sqlite3.Connection
        sqlite3 connection object.
    '''
    conn = sqlite3.connect(f_path)

    return conn


def sl_exec_qry(conn: sqlite3.Connection, query: str,
                param_subs: dict | None = None) -> list[tuple] | None:
    '''
    Execute a SQLite SQL query.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    query : str
        A SQL query.
    param_subs : dict | None (optional)
        Parameter substitution name/value pairs, where the parameter name
        is the dictionary key, and the value is the parameter value.
        Defaults to None.

    Returns
    ----------------
    list | None
        List of result rows or None.

    Examples
    ----------------
    # Enable a SQL interface compliant with the DB-API 2.0 specification
    import sqlite3

    # Create database connection
    conn = sqlite3.connect('/home/comcomly/chinook.sqlite')

    query = (
        'SELECT '
        '* '
        'FROM '
        'Album'
    )
    result = sl_exec_qry(conn, query)

    # Select rows by playlist ID or name criteria from Playlist table
    query = (
        'SELECT '
        'PlaylistId, '
        'Name '
        'FROM '
        'Playlist '
        "WHERE "
        "PlaylistId = :PlaylistId "
        "OR Name = :Name"
    )
    param_subs = {
        'PlaylistId': 12,
        'Name': 'Classical'
    }
    result = sl_exec_qry(conn, query, param_subs=param_subs)
    '''
    if param_subs is None:
        param_subs = {}

    cur = conn.cursor()

    with conn:
        result = cur.execute(query, param_subs).fetchall()

    cur.close()

    return result


def get_db_md(engine: sa.engine.Engine, schema: str = None,
              views: bool = False) \
        -> tuple[sa.sql.schema.MetaData, dict[str, list[str]]]:
    '''
    Get database metadata.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    schema : str (optional)
        Database schema. Defaults to None.
    views : bool (optional)
        Whether to also reflect views or not. Defaults to False.

    Returns
    ----------------
    tuple
        A collection of Table objects and their associated schema constructs,
        and a dictionary of database metadata, where the keys are the
        table names and the values are a list of table column names.
    '''
    # Create MetaData container
    md = sa.MetaData(schema=schema)
    # Load all available table definitions from the database
    md.reflect(bind=engine, views=views)

    db_md = {}
    for tbl_name in sorted(md.tables.keys()):
        db_md[tbl_name] = sorted(md.tables[tbl_name].c.keys())

    return md, db_md


def crt_n_type_map(tbl_obj: sa.sql.schema.Table) -> dict[str, str]:
    '''
    Create a column name to pandas data type mapping.

    Parameters
    ----------------
    tbl_obj : sa.sql.schema.Table
        SQLAlchemy Table object to create mapping from.

    Returns
    ----------------
    dict
        Dictionary where the keys are the column names and the values
        are the pandas data types.
    '''
    name_type_map = {}

    for col_obj in tbl_obj.c:
        # Define and run type tests
        bool_type = (isinstance(col_obj.type.as_generic(), sa.types.Boolean))
        dt_type = (
            isinstance(col_obj.type.as_generic(), sa.types.Date)
            or isinstance(col_obj.type.as_generic(), sa.types.DateTime)
        )
        float_type = (
            isinstance(col_obj.type.as_generic(), sa.types.Double)
            or isinstance(col_obj.type.as_generic(), sa.types.Float)
            or isinstance(col_obj.type.as_generic(), sa.types.Numeric)
        )
        int_type = (
            isinstance(col_obj.type.as_generic(), sa.types.BigInteger)
            or isinstance(col_obj.type.as_generic(), sa.types.Integer)
            or isinstance(col_obj.type.as_generic(), sa.types.SmallInteger)
        )
        obj_type = (
            isinstance(col_obj.type.as_generic(), sa.types.Enum)
            or isinstance(col_obj.type.as_generic(), sa.types.LargeBinary)
            or isinstance(col_obj.type.as_generic(), sa.types.MatchType)
            or isinstance(col_obj.type.as_generic(), sa.types.PickleType)
            or isinstance(col_obj.type.as_generic(), sa.types.SchemaType)
        )
        str_type = (
            isinstance(col_obj.type.as_generic(), sa.types.Interval)
            or isinstance(col_obj.type.as_generic(), sa.types.String)
            or isinstance(col_obj.type.as_generic(), sa.types.Text)
            or isinstance(col_obj.type.as_generic(), sa.types.Time)
            or isinstance(col_obj.type.as_generic(), sa.types.Unicode)
            or isinstance(col_obj.type.as_generic(), sa.types.UnicodeText)
            or isinstance(col_obj.type.as_generic(), sa.types.Uuid)
        )

        # Map generic types to pandas types
        if bool_type:
            name_type_map[col_obj.name] = 'boolean'
        elif dt_type:
            name_type_map[col_obj.name] = 'datetime64[ns]'
        elif float_type:
            name_type_map[col_obj.name] = 'float64'
        elif int_type:
            name_type_map[col_obj.name] = 'Int64'
        elif obj_type:
            name_type_map[col_obj.name] = 'object'
        elif str_type:
            name_type_map[col_obj.name] = 'string[pyarrow]'

    return name_type_map


def exec_txt_qry(engine: sa.engine.Engine, query: str,
                 data: list[dict] | None = None,
                 param_subs: dict | None = None) \
        -> sa.engine.cursor.CursorResult:
    '''
    Execute a text-based SQL query.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    query : str
        A text-based SQL query.
    data : list | None (optional)
        List of dictionaries where each dictionary represents a row of
        data to insert. The dictionary keys are the column names,
        and the values are the column values. Defaults to None.
    param_subs : dict | None (optional)
        Parameter substitution name/value pairs, where the parameter name
        is the dictionary key, and the value is the parameter value.
        Defaults to None.

    Returns
    ----------------
    sa.engine.cursor.CursorResult
        SQLAlchemy cursor result.

    Examples
    ----------------
    import sqlalchemy as sa  # Enable database abstraction library

    # Create URL object
    url = sa.URL.create('sqlite', database='/home/comcomly/chinook.sqlite')
    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Select random subset of rows from Track table
    query = (
        'SELECT '
        '* '
        'FROM '
        'Track '
        'ORDER BY RANDOM() '
        'LIMIT 10'
    )
    result = exec_txt_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    # Insert new genres into Genre table
    query = (
        'INSERT INTO '
        'Genre '
        '(GenreId, Name) '
        "VALUES "
        "(:GenreId, :Name)"
    )
    data = [
        {
            'GenreId': '26',
            'Name': 'Chiptune'
        },
        {
            'GenreId': '27',
            'Name': 'Trip Hop'
        }
    ]
    result = exec_txt_qry(engine, query, data=data)

    # Select rows by playlist ID or name criteria from Playlist table
    query = (
        'SELECT '
        'PlaylistId, '
        'Name '
        'FROM '
        'Playlist '
        "WHERE "
        "PlaylistId = :PlaylistId "
        "OR Name = :Name"
    )
    param_subs = {
        'PlaylistId': 12,
        'Name': 'Classical'
    }
    result = exec_txt_qry(engine, query, param_subs=param_subs)
    cols, rows = list(result.keys()), result.all()
    '''
    with engine.begin() as conn:
        if data:
            result = conn.execute(sa.text(query), data)
        else:
            result = conn.execute(sa.text(query), param_subs)

    return result


def imp_files_pd(dir_path: str, ext: str,
                 rdr_kwargs: dict[str, Any] | None = None) \
        -> Iterator[pd.core.frame.DataFrame]:
    '''
    Return a generator iterator of pandas DataFrames created from
    a directory of files.

    Parameters
    ----------------
    dir_path : str
        Path to directory of files to import.
    ext : str
        Extension of directory files (e.g., parquet, csv, ods).
    rdr_kwargs: dict | None (optional)
        Dictionary of keyword arguments to pass to reading function.
        Defaults to None.

    Returns
    ----------------
    Iterator
        Iterator of pandas DataFrames.
    '''
    # Obtain file paths
    file_paths = sorted(glob(os.path.join(dir_path, f'*.{ext}')))

    # Set reader function based on file extension
    match ext:
        case 'parquet':
            rdr_fn = pd.read_parquet
        case 'csv':
            rdr_fn = pd.read_csv
        case _:
            rdr_fn = pd.read_excel

    dfs = (
        rdr_fn(file_path, **rdr_kwargs)
        if rdr_kwargs
        else
        rdr_fn(file_path)
        for file_path in file_paths
    )

    return dfs
