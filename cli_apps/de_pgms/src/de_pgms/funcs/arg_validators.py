'''
A collection of functions that validate various inputs.

Functions:

    * val_dialect_arg() - Validate a database dialect.
    * val_file_arg() - Validate a file.
    * val_dir_arg() - Validate a directory.
    * val_file_dir_arg() - Validate a file or directory.
    * val_delim_arg() - Validate a file delimiter.
'''
import argparse  # Enable program argument parsing
# Enable a portable way of using operating system dependent functionality
import os

from de_pgms.config import ConstantsNamespace as CNS


def val_dialect_arg(dialect: str) -> str:
    '''
    Validate a database dialect.

    Parameters
    ----------------
    dialect : str
        Dialect to be validated.

    Returns
    ----------------
    str
        Validated dialect.
    '''
    if dialect in CNS.DIALECTS:
        return dialect
    else:
        err_msg = (
            f'\n\n{CNS.FB}{dialect}{CNS.FR} is not a supported dialect. '
            'Supported dialects are:\n'
            f'{CNS.DIALECTS}'
        )
        raise argparse.ArgumentTypeError(err_msg)


def val_file_arg(file_to_val: str) -> str:
    '''
    Validate a file.

    Parameters
    ----------------
    file_to_val : str
        File to be validated.

    Returns
    ----------------
    str
        Validated file.
    '''
    if os.path.isfile(file_to_val):
        return file_to_val
    else:
        err_msg = f'{CNS.FB}{file_to_val}{CNS.FR} is not a valid file path.'
        raise argparse.ArgumentTypeError(err_msg)


def val_dir_arg(dir_to_val: str) -> str:
    '''
    Validate a directory.

    Parameters
    ----------------
    dir_to_val : str
        Directory to be validated.

    Returns
    ----------------
    str
        Validated directory.
    '''
    if os.path.isdir(dir_to_val):
        return dir_to_val
    else:
        err_msg = (
            f'{CNS.FB}{dir_to_val}{CNS.FR} is not a valid directory path.'
        )
        raise argparse.ArgumentTypeError(err_msg)


def val_file_dir_arg(obj_to_val: str) -> str:
    '''
    Validate a file or directory.

    Parameters
    ----------------
    obj_to_val : str
        Filesystem object to be validated.

    Returns
    ----------------
    str
        Validated file or directory.
    '''
    if os.path.isfile(obj_to_val) or os.path.isdir(obj_to_val):
        return obj_to_val
    else:
        err_msg = (
            f'{CNS.FB}{obj_to_val}{CNS.FR} is not a valid file or '
            'directory path.'
        )
        raise argparse.ArgumentTypeError(err_msg)


def val_delim_arg(delimiter: str) -> str:
    '''
    Validate a file delimiter.

    Parameters
    ----------------
    delimiter : str
        Delimiter to use. Must be a single symbolic character.

    Returns
    ----------------
    str
        Validated delimiter.
    '''
    if bool(CNS.DELIM_PAT.search(delimiter)):
        return delimiter
    else:
        err_msg = (
            f'{CNS.FB}{delimiter}{CNS.FR} is not a single symbolic character.'
        )
        raise argparse.ArgumentTypeError(err_msg)
