'''
A collection of I/O-oriented functions.

Functions:

    * crt_dir() - Create a directory if it does not exist.
    * wipe_dir() - Wipe a directory's contents.
    * rd_file() - Read a text file.
    * wr_csv() - Write a CSV file.
    * rd_csv() - Read a CSV file.
'''
###########
# Modules #
###########
# Enable classes to read and write tabular data in CSV format
import csv
# Enable functions and classes that implement an event logging system
# Enable a portable way of using operating system dependent functionality
import os
# Enable a number of high-level operations on files and collections of files
import shutil


#############
# Functions #
#############
def crt_dir(dir_to_crt: str) -> None:
    '''
    Create a directory if it does not exist.

    Parameters
    ----------------
    dir_to_crt : str
        Directory to create.
    '''
    if not os.path.isdir(dir_to_crt):
        os.makedirs(dir_to_crt)


def wipe_dir(dir_path: str) -> None:
    '''
    Wipe a directory's contents.

    Parameters
    ----------------
    dir_path : str
        Directory path to wipe.
    '''
    with os.scandir(dir_path) as objs:
        for obj in objs:
            if obj.is_file() or obj.is_symlink():
                os.unlink(obj.path)
            else:
                shutil.rmtree(obj.path)


def rd_file(f_path: str, encoding: str = 'utf-8') -> str:
    '''
    Read a text file.

    Parameters
    ----------------
    f_path : str
        Path of the text file to read.
    encoding : str (optional)
        Encoding of the text file to read. Defaults to utf-8.

    Returns
    ----------------
    str
        The text of the read file as a single string.
    '''
    with open(f_path, 'r', encoding=encoding, newline='') as f:
        data = f.read()

    return data


def wr_csv(f_path: str, data: list[tuple], encoding: str = 'utf-8',
           delimiter: str = ',', append: bool = False) -> None:
    '''
    Write a CSV file.

    Parameters
    ----------------
    f_path : str
        Path of the CSV file to write.
    data : list
        Data to write to the CSV file.
    encoding : str (optional)
        Encoding of the CSV file to write. Defaults to utf-8.
    delimiter : str (optional)
        Delimiter to use in the CSV file to read. Defaults to a ,.
    append : bool (optional)
        Whether to append data or not. Defaults to False.
    '''
    if append:
        with open(f_path, 'a', encoding=encoding, newline='') as f:
            f_csv = csv.writer(f, delimiter=delimiter)
            f_csv.writerows(data)
    else:
        with open(f_path, 'w', encoding=encoding, newline='') as f:
            f_csv = csv.writer(f, delimiter=delimiter)
            f_csv.writerows(data)


def rd_csv(f_path: str, encoding: str = 'utf-8', delimiter: str = ',',
           headings: bool = True) -> tuple[tuple, list[tuple]] | list[tuple]:
    '''
    Read a CSV file.

    Parameters
    ----------------
    f_path : str
        Path of the CSV file to read.
    encoding : str (optional)
        Encoding of the CSV file to read. Defaults to utf-8.
    delimiter : str (optional)
        Delimiter to use in the CSV file to read. Defaults to a ,.
    headings : bool (optional)
        Whether the CSV file to read contains a headings line
        or not. Defaults to True.

    Returns
    ----------------
    tuple | list
        Either a tuple of the headings tuple and rows list, or the rows list.
    '''
    if headings:
        with open(f_path, 'r', encoding=encoding, newline='') as f:
            f_csv = csv.reader(f, delimiter=delimiter)
            headings = tuple(next(f_csv))
            rows = [tuple(row) for row in f_csv]

        return headings, rows
    else:
        with open(f_path, 'r', encoding=encoding, newline='') as f:
            f_csv = csv.reader(f, delimiter=delimiter)
            rows = [tuple(row) for row in f_csv]

        return rows
