'''
A collection of functions that check various conditions.

Functions:

    * db_qry_ck() - Run database/query dictionary check.
    * drv_ck() - Run driver/authorization check.
'''
import argparse  # Enable program argument parsing
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback

from de_pgms.cli import logging
from de_pgms.config import ConstantsNamespace as CNS


def db_qry_ck(args: argparse.Namespace) -> None:
    '''
    Run database/query dictionary check.

    Parameters
    ----------------
    args : Namespace
        An argparse namespace class.
    '''
    if not args.database and not args.query:
        logging.error('Database/query dictionary check failed')
        err_msg = (
            f'\nSpecify the {CNS.FB}--database{CNS.FR} and/or '
            f'{CNS.FB}--query{CNS.FR} option(s) before proceeding again.'
        )
        print(err_msg)

        logging.debug(
            f'Exiting "{args.subcommand}" subprogram with error code 1'
        )
        sys.exit(1)


def drv_ck(args: argparse.Namespace) -> str:
    '''
    Run driver check.

    Parameters
    ----------------
    args : Namespace
        An argparse namespace class.

    Returns
    ----------------
    str
        The driver import command to run.
    '''
    # Set conditional import commands/messages
    match args.dialect:
        case 'mariadb' | 'mysql':
            pkg = 'PyMySQL'
            cmd = CNS.PKGS_DRVS['PyMySQL']
        case 'mssql':
            pkg = 'pyodbc'
            cmd = CNS.PKGS_DRVS['pyodbc']
        case 'oracle':
            pkg = 'oracledb'
            cmd = CNS.PKGS_DRVS['oracledb']
        case 'postgresql':
            pkg = 'pg8000'
            cmd = CNS.PKGS_DRVS['pg8000']

    try:
        exec(cmd)
    except ImportError:
        logging.error(traceback.format_exc().rstrip())
        import_msg = (
            f'\nInstall the {CNS.FB}{pkg}{CNS.FR} package before proceeding.'
        )
        print(import_msg)

        logging.debug(
            f'Exiting "{args.subcommand}" subprogram with error code 1'
        )
        sys.exit(1)

    return cmd
