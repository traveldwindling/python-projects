'''\
Run a SQL query.

This program aims to provide a database-agnostic way to run a SQL query
and extract the result set to a Parquet file. A database name and/or
query mapping is required to run the program.

Functions:

    * main() - Define starting point for execution of the subprogram.

Requires
----------------
pandas
    Powerful data structures for data analysis, time series, and statistics
pyarrow
    Python library for Apache Arrow
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import datetime as dt  # Enable classes for manipulating dates and times
# Enable storing and accessing of passwords safely
import keyring as kr
# Enable a portable way of using operating system dependent functionality
import os
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable a standard interface to extract, format, and print stack traces
import traceback

import pandas as pd  # Enable data structures and data analysis tools
import pyarrow as pa  # Enable Python library for Apache Arrow
import sqlalchemy as sa  # Enable database abstraction library

from de_pgms.cli import logging
from de_pgms.config import ConstantsNamespace as CNS
from de_pgms.funcs.checkers import db_qry_ck, drv_ck
from de_pgms.funcs.etlfns import exec_txt_qry


#############
# Functions #
#############
def main(args: argparse.Namespace) -> None:
    '''Define starting point for execution of the subprogram.'''
    logging.debug(f'Starting "{args.subcommand}" subprogram')

    # Confirm whether output should be Parquet files or not
    prq_o = not args.csv_output
    # Confirm whether output should be CSV files or not
    csv_o = False if prq_o else True

    db_qry_ck(args)

    if args.dialect != 'sqlite':
        cmd = drv_ck(args)
        exec(cmd, globals())
        drivername = f'{args.dialect}+{CNS.DIALS_DRVS[args.dialect]}'
    else:
        drivername = args.dialect

    if args.credentials:
        logging.debug('Obtaining database credentials')
        db_srv_name, db_username = args.credentials.split(',')
        db_creds = kr.get_credential(db_srv_name, db_username)
        username, password = db_creds.username, db_creds.password

    if args.oracle_client_libs:
        logging.debug(
            'Loading Oracle Client libraries and '
            'enabling Thick Mode'
        )
        oracledb.init_oracle_client(lib_dir=args.oracle_client_libs)

    logging.debug('Creating engine')
    # Create URL object
    url = sa.URL.create(
        drivername=drivername,
        username=(username if args.credentials else None),
        password=(password if args.credentials else None),
        host=args.host,
        port=args.port,
        database=args.database,
        query=args.query
    )
    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Create connection context to verify connection
    logging.debug('Testing engine connection')
    try:
        with engine.connect():
            pass
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nEngine connection test failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise

    log_qry = re.sub(r'(\n|\r|\r\n)', ' ', args.cnt_qry)
    logging.debug('Determining number of rows')
    logging.debug(f'Running query: "{log_qry}"')
    try:
        start = dt.datetime.now()
        result = exec_txt_qry(engine, args.cnt_qry)
        end = dt.datetime.now()
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nNumber of rows query failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise
    else:
        secs_elapsed = (end - start).total_seconds()
        msg = (
            'Number of rows query execution time: '
            f'"{dt.timedelta(seconds=secs_elapsed)}"'
        )
        logging.info(msg)

    logging.debug('Extracting number of rows')
    num_of_rows = result.all()[0][0]
    logging.info(f'Number of rows: "{num_of_rows:,}"')

    log_stmt = re.sub(r'(\n|\r|\r\n)', ' ', args.sql_qry)
    logging.debug(f'Running SQL query: "{log_stmt}"')
    try:
        start = dt.datetime.now()
        result = exec_txt_qry(engine, args.sql_qry)
        end = dt.datetime.now()
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nSQL query execution failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise
    else:
        secs_elapsed = (end - start).total_seconds()
        msg = (
            'SQL query execution time: '
            f'"{dt.timedelta(seconds=secs_elapsed)}"'
        )
        logging.info(msg)

    logging.debug('Extracting query result set')
    start = dt.datetime.now()
    rslt_set = result.all()
    end = dt.datetime.now()

    secs_elapsed = (end - start).total_seconds()
    msg = (
        'Result set extraction time: '
        f'"{dt.timedelta(seconds=secs_elapsed)}"'
    )
    logging.info(msg)

    logging.debug('Converting result set to a DataFrame')
    df = pd.DataFrame(rslt_set, columns=args.col_conv_map.keys())

    logging.debug('Converting DataFrame column data types')
    try:
        df = df.astype(args.col_conv_map)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            '\nDataFrame column data type conversion '
            f'failed: {CNS.FB}{exc}{CNS.FR}\n'
        )
        print(err_msg)
        raise

    o_path = (
        os.path.join(args.o_dir, f'{CNS.D_STAMP}_{args.sql_qry_name}.parquet')
        if prq_o
        else
        os.path.join(args.o_dir, f'{CNS.D_STAMP}_{args.sql_qry_name}.csv')
    )

    logging.debug('Exporting result set')
    try:
        start = dt.datetime.now()
        if prq_o:
            df.to_parquet(o_path)
        else:
            df.to_csv(o_path, sep=args.delimiter, index=False,
                      encoding=CNS.ENC)
        end = dt.datetime.now()
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nBatch result export failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise
    else:
        secs_elapsed = (end - start).total_seconds()
        msg = (
            'Result set write time: '
            f'"{dt.timedelta(seconds=secs_elapsed)}"'
        )
        logging.info(msg)

    logging.debug('Determining destination number of rows')
    if csv_o:
        try:
            ser = pd.read_csv(o_path, sep=args.delimiter,
                              encoding=CNS.ENC, usecols=[0])
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                '\nImporting output CSV file failed: '
                f'{CNS.FB}{exc}{CNS.FR}\n'
            )
            print(err_msg)
            raise
        else:
            exp_nrows = ser.shape[0]
    else:
        try:
            col = (
                pa.parquet
                  .read_schema(o_path, memory_map=True)
                  .names[0]
            )
            exp_nrows = pd.read_parquet(o_path, columns=[col]).shape[0]
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                '\nImporting output Parquet file failed: '
                f'{CNS.FB}{exc}{CNS.FR}\n'
            )
            print(err_msg)
            raise

    logging.info(f'Source data number of rows: "{num_of_rows:,}"')
    logging.info(f'Destination number of rows: "{exp_nrows:,}"')

    logging.debug(f'Ending "{args.subcommand}" subprogram')
