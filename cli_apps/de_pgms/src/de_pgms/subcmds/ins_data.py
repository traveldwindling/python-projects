'''\
Insert data into a table.

This program aims to provide a database-agnostic way to insert data into
a database table in batches from a Parquet file(s). A database name and/or
query mapping is required to run the program.

Functions:

    * ins_data_bats() - Insert data into a database table in batches from a
                        Parquet file(s).
    * main() - Define starting point for execution of the subprogram.

Requires
----------------
pandas
    Powerful data structures for data analysis, time series, and statistics
pyarrow
    Python library for Apache Arrow
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import datetime as dt  # Enable classes for manipulating dates and times
# Enable storing and accessing of passwords safely
import keyring as kr
# Enable a portable way of using operating system dependent functionality
import os
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback

import pandas as pd  # Enable data structures and data analysis tools
import pyarrow as pa  # Enable Python library for Apache Arrow
import pyarrow.parquet as pq
import sqlalchemy as sa  # Enable database abstraction library

from de_pgms.cli import logging
from de_pgms.config import ConstantsNamespace as CNS
from de_pgms.funcs.checkers import db_qry_ck, drv_ck
from de_pgms.funcs.etlfns import exec_txt_qry, get_db_md, imp_files_pd


#############
# Functions #
#############
def ins_data_bats(data_src: str, engine: sa.engine.Engine, num_of_rows: int,
                  tbl_obj: sa.sql.schema.Table, bat_size: int = 0,
                  csv_input: bool = None, delimiter: str = ',',
                  encoding: str = 'utf-8', col_conv_map: dict[str, str] = None,
                  bat_num: int = 0) -> int:
    '''
    Insert data into a database table from a Parquet file(s).

    Parameters
    ----------------
    data_src : str
        Parquet file path or directory path of Parquet files for the
        source data.
    engine : sa.engine.Engine
        SQLAlchemy engine for the database to insert data into.
    num_of_rows : int
        Number of rows in data source.
    tbl_obj : sa.sql.schema.Table
        Database table object to insert data into.
    bat_size: int (optional)
        Batch size (number of rows) to use when reading in a Parquet file.
        Defaults to 0.
    csv_input : bool (optional)
        Whether data source is CSV or not. Defaults to None.
    delimiter : str (optional)
        Delimiter to use in source data CSV file. Defaults to a ,.
    encoding : str (optional)
        Encoding to use for source data CSV file. Defaults to utf-8.
    col_conv_map : dict (optional)
        Dictionary of string column name keys to string pandas data
        type values. Defaults to None.
    bat_num : int (optional)
        The batch number to start at. Defaults to 0.

    Returns
    ----------------
    int
        Number of rows inserted.
    '''
    if os.path.isfile(data_src):
        if csv_input:
            logging.debug('Reading source data CSV file')
            try:
                # Assumes a headers row
                ds_iter = pd.read_csv(data_src, sep=delimiter,
                                      encoding=encoding, chunksize=bat_size,
                                      header=None, skiprows=1)
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())
                err_msg = (
                    '\nCSV read file operation failed: '
                    f'{CNS.FB}{exc}{CNS.FR}\n'
                )
                print(err_msg)
                raise
        else:
            logging.debug('Reading source data Parquet file')
            try:
                ds_iter = (
                    pq.ParquetFile(data_src)
                      .iter_batches(batch_size=bat_size)
                )
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())
                err_msg = (
                    '\nParquet read file operation failed: '
                    f'{CNS.FB}{exc}{CNS.FR}\n'
                )
                print(err_msg)
                raise
            else:
                ds_iter = (
                    rec_bat.to_pandas().replace({float('nan'): None})
                    for rec_bat in ds_iter
                )
    else:
        if csv_input:
            logging.debug('Reading source data CSV files')
            try:
                rdr_kwargs = {'sep': delimiter, 'encoding': encoding}
                ds_iter = imp_files_pd(data_src, 'csv', rdr_kwargs)
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())
                err_msg = (
                    '\nCSV read files operation failed: '
                    f'{CNS.FB}{exc}{CNS.FR}\n'
                )
                print(err_msg)
                raise
        else:
            logging.debug('Reading source data Parquet files')
            try:
                ds_iter = imp_files_pd(data_src, 'parquet')
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())
                err_msg = (
                    '\nParquet read files operation failed: '
                    f'{CNS.FB}{exc}{CNS.FR}\n'
                )
                print(err_msg)
                raise
            else:
                ds_iter = (
                    df.replace({float('nan'): None})
                    for df in ds_iter
                )

    # Adjust iterator to start at desired batch
    if bat_num:
        for num in range(bat_num):
            next(ds_iter)

    cols = tbl_obj.c.keys()  # Define database table column names

    ins_ctr = 0  # Set insert counter

    # Insert data into table
    for i, df in enumerate(ds_iter):
        if csv_input:
            logging.debug(
                'Setting DataFrame column names and performing '
                'data type conversions'
            )
            df.columns = col_conv_map.keys()
            df = df.astype(col_conv_map).replace({float('nan'): None})

        logging.debug(f'Converting batch "{i}" to list of dictionaries')
        rslt_set = [
            {col: value for col, value in zip(cols, row)}
            for row in df.to_numpy().tolist()
        ]

        logging.debug('Inserting batch results')
        if i == 0:
            logging.debug(f'Running statement: "{tbl_obj.insert()}"')

        attempts = 0
        while attempts < CNS.MAX_ATTEMPTS:
            try:
                start = dt.datetime.now()
                with engine.begin() as conn:
                    _ = conn.execute(tbl_obj.insert(), rslt_set)
                end = dt.datetime.now()
            except Exception as exc:
                attempts += 1
                if attempts == CNS.MAX_ATTEMPTS:
                    logging.error(traceback.format_exc().rstrip())
                    err_msg = f'\nBatch insert failed: {CNS.FB}{exc}{CNS.FR}\n'
                    print(err_msg)
                    raise
                else:
                    err_msg = 'Batch insert failed. Trying again.'
                    logging.error(err_msg)
                    continue
            else:
                secs_elapsed = (end - start).total_seconds()
                msg = (
                    'Batch insert execution time: '
                    f'"{dt.timedelta(seconds=secs_elapsed)}"'
                )
                logging.info(msg)
                ins_ctr += len(rslt_set)
                break

    return ins_ctr


def main(args: argparse.Namespace) -> None:
    '''Define starting point for execution of the subprogram.'''
    logging.debug(f'Starting "{args.subcommand}" subprogram')

    db_qry_ck(args)

    dir_input = os.path.isdir(args.data_src)
    if not dir_input:
        if not args.bat_sz:
            err_msg = 'Batch size not provided for file data source'
            logging.error(err_msg)

            err_msg = (
                f'\nA batch size using the {CNS.FB}--bat-size{CNS.FR} option '
                'needs to be specified when using a file data source.'
            )
            print(err_msg)

            logging.debug('Exiting program with error code 1')
            sys.exit(1)

    if args.csv_input:
        if not args.col_conv_map:
            err_msg = 'Column conversion map not provided for CSV data source'
            logging.error(err_msg)

            err_msg = (
                '\nA column conversion map using the '
                f'{CNS.FB}--col-conv-map{CNS.FR} option needs to be '
                'specified when using a CSV data source.'
            )
            print(err_msg)

            logging.debug('Exiting program with error code 1')
            sys.exit(1)

    if args.dialect != 'sqlite':
        cmd = drv_ck(args)
        exec(cmd, globals())
        drivername = f'{args.dialect}+{CNS.DIALS_DRVS[args.dialect]}'
    else:
        drivername = args.dialect

    if args.credentials:
        logging.debug('Obtaining database credentials')
        db_srv_name, db_username = args.credentials.split(',')
        db_creds = kr.get_credential(db_srv_name, db_username)
        username, password = db_creds.username, db_creds.password

    if args.oracle_client_libs:
        logging.debug(
            'Loading Oracle Client libraries and '
            'enabling Thick Mode'
        )
        oracledb.init_oracle_client(lib_dir=args.oracle_client_libs)

    logging.debug('Creating engine')
    # Create URL object
    url = sa.URL.create(
        drivername=drivername,
        username=(username if args.credentials else None),
        password=(password if args.credentials else None),
        host=args.host,
        port=args.port,
        database=args.database,
        query=args.query

    )
    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Create connection context to verify connection
    logging.debug('Testing engine connection')
    try:
        with engine.connect():
            pass
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nEngine connection test failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise

    # Set database schema and cleaned table name
    if len(args.db_tbl.split('.')) > 1:
        db_sch = args.db_tbl.split('.')[0]
    else:
        db_sch = None
    no_dqtes_db_tbl = args.db_tbl.replace('"', '')

    logging.debug('Obtaining database metadata')
    md, db_md = get_db_md(engine, schema=db_sch, views=True)

    if args.display_metadata:
        logging.debug('Displaying database metadata')
        hdr = f'{args.database} Objects'
        hdr_len = len(hdr)
        print(f"\n{CNS.FB}{hdr}{CNS.FR}\n{'~' * hdr_len}")
        for tbl_key in sorted(md.tables.keys()):
            print(tbl_key)

        logging.debug(f'Ending "{args.subcommand}" subprogram')
        return

    logging.debug('Checking that database table exists')
    if no_dqtes_db_tbl in db_md.keys():
        tbl_obj = md.tables[no_dqtes_db_tbl]
    else:
        logging.error('Database table does not exist')

        err_msg = (
            f'\n{CNS.FB}{args.db_tbl}{CNS.FR} does not exist in the database.'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    if '"' in args.db_tbl:
        logging.info(f"Database table name '{args.db_tbl}'")
    else:
        logging.info(f'Database table name "{args.db_tbl}"')

    if args.bat_sz:
        logging.info(f'Batch size "{args.bat_sz:,}"')

    logging.debug('Determining number of data source rows')
    if os.path.isfile(args.data_src):
        if args.csv_input:
            try:
                ser = pd.read_csv(args.data_src, sep=args.delimiter,
                                  encoding=CNS.ENC, usecols=[0])
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())
                err_msg = (
                    '\nImporting CSV file failed: '
                    f'{CNS.FB}{exc}{CNS.FR}\n'
                )
                print(err_msg)
                raise
            else:
                num_of_rows = ser.shape[0]
        else:
            try:
                col = (
                    pa.parquet
                    .read_schema(args.data_src, memory_map=True)
                    .names[0]
                )
                num_of_rows = (
                    pd.read_parquet(args.data_src, columns=[col]).shape[0]
                )
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())
                err_msg = (
                    '\nImporting Parquet file failed: '
                    f'{CNS.FB}{exc}{CNS.FR}\n'
                )
                print(err_msg)
                raise
    else:
        if args.csv_input:
            ext = 'csv'
            rdr_kwargs = {
                'delimiter': args.delimiter,
                'encoding': CNS.ENC
            }
            err_msg_ext = 'CSV'
        else:
            ext = 'parquet'
            rdr_kwargs = None
            err_msg_ext = ext

        try:
            dfs = imp_files_pd(args.data_src, ext, rdr_kwargs)
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                f'\nImporting {err_msg_ext} files failed: '
                f'{CNS.FB}{exc}{CNS.FR}\n'
            )
            print(err_msg)
            raise
        else:
            sers = (df.iloc[:, 0] for df in dfs)
            num_of_rows = pd.concat(sers).shape[0]

    if not args.append:
        stmt = sa.delete(tbl_obj)
        logging.debug('Wipe database table contents')
        try:
            start = dt.datetime.now()
            with engine.begin() as conn:
                _ = conn.execute(stmt)
            end = dt.datetime.now()
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                f'\nDatabase table wipe failed: {CNS.FB}{exc}{CNS.FR}\n'
            )
            print(err_msg)
            raise
        else:
            secs_elapsed = (end - start).total_seconds()
            msg = (
                'Database table wipe execution time: '
                f'"{dt.timedelta(seconds=secs_elapsed)}"'
            )
            logging.info(msg)

    logging.debug('Inserting data')
    start = dt.datetime.now()
    ins_ctr = ins_data_bats(args.data_src, engine, num_of_rows, tbl_obj,
                            bat_size=args.bat_sz, csv_input=args.csv_input,
                            delimiter=args.delimiter, encoding=CNS.ENC,
                            col_conv_map=args.col_conv_map,
                            bat_num=args.bat_num)
    end = dt.datetime.now()

    secs_elapsed = (end - start).total_seconds()
    msg = (
        'Data insert total execution time: '
        f'"{dt.timedelta(seconds=secs_elapsed)}"'
    )
    logging.info(msg)
    logging.info(f'Number of rows inserted: "{ins_ctr:,}"')

    num_rows_qry = (
        'SELECT '
        'COUNT(*) AS num_of_rows '
        'FROM '
        + args.db_tbl
    )
    logging.debug('Determining destination number of rows')
    logging.debug(f'Running query: "{num_rows_qry}"')
    try:
        result = exec_txt_qry(engine, num_rows_qry)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            '\nDestination number of rows query failed: '
            f'{CNS.FB}{exc}{CNS.FR}\n'
        )
        print(err_msg)
        raise
    else:
        logging.info(f'Source data number of rows: "{num_of_rows:,}"')
        logging.info(f'Number of rows inserted: "{ins_ctr:,}"')
        msg = f'Destination data number of rows: "{result.all()[0][0]:,}"'
        logging.info(msg)

    logging.debug(f'Ending "{args.subcommand}" subprogram')
