'''\
Batch extract a table.

This program aims to provide a database-agnostic way to extract a database
table in batches to Parquet files. A database name and/or query mapping
is required to run the program.

By default, all columns are extracted, but specific columns and additional
WHERE clause filtering strings can be specified.

Functions:

    * extr_dbt_bats() - Extract a database table in batches to a directory of
                        Parquet files.
    * main() - Define starting point for execution of the subprogram.

Requires
----------------
pandas
    Powerful data structures for data analysis, time series, and statistics
pyarrow
    Python library for Apache Arrow
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import datetime as dt  # Enable classes for manipulating dates and times
# Enable storing and accessing of passwords safely
import keyring as kr
# Enable a portable way of using operating system dependent functionality
import os
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback

import pandas as pd  # Enable data structures and data analysis tools
import sqlalchemy as sa  # Enable database abstraction library

from de_pgms.cli import logging
from de_pgms.config import ConstantsNamespace as CNS
from de_pgms.funcs.checkers import db_qry_ck, drv_ck
from de_pgms.funcs.etlfns import (crt_n_type_map, exec_txt_qry, get_db_md,
                                  imp_files_pd)
from de_pgms.funcs.iofns import crt_dir, wipe_dir


#############
# Functions #
#############
def extr_dbt_bats(engine: sa.engine.Engine, bat_size: int, num_of_rows: int,
                  sel_qry: str, o_dir: str,
                  col_conv_map: dict[str, str] | None = None,
                  csv_o: bool = False, delimiter: str = ',',
                  encoding: str = 'utf-8', bat_num: int = 0) -> int:
    '''
    Extract a database table in batches to a directory of Parquet files.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    bat_size: int
        Batch size (number of rows) to utilize for each SELECT query.
    num_of_rows : int
        Number of rows in database table to extract.
    sel_qry : str
        SELECT query to run.
    o_dir : str
        Output directory to save Parquet files to.
    col_conv_map : dict | None (optional)
        Column conversion dictionary, where the keys are column names, and
        the values are pandas data types. Defaults to None.
    csv_o : bool (optional)
        Whether or not to use CSV output files. Defaults to False.
    delimiter : str (optional)
        Delimiter to use in output CSV files. Defaults to a ,.
    encoding : str (optional)
        Encoding to use for output CSV files. Defaults to utf-8.
    bat_num : int (optional)
        The batch number to start at. Defaults to 0.

    Returns
    ----------------
    int
        Number of rows exported.
    '''
    prq_o = not csv_o

    # Determine the number of batches to run
    # and the size of any remaining batch
    num_of_bats = num_of_rows // bat_size
    rem_bat_size = num_of_rows % bat_size

    # Determine total number of batches
    tot_num_bats = num_of_bats + 1 if rem_bat_size else num_of_bats

    # Ensure batch size is less than the number of database table rows
    if num_of_bats < 1:
        err_msg = (
            f'Provided batch size ({bat_size:,}) exceeds the '
            f'size of the database table ({num_of_rows:,})'
        )
        logging.error(err_msg)

        err_msg = (
            f'\nProvided batch size ({CNS.FB}{bat_size:,}{CNS.FR}) exceeds '
            'the size of the database table '
            f'({CNS.FB}{num_of_rows:,}{CNS.FR}).'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    # Ensure batch number is valid
    if bat_num >= tot_num_bats:
        err_msg = (
            f'Batch number "{bat_num}" exceeds the highest batch '
            f'number, "{tot_num_bats - 1}"'
        )
        logging.error(err_msg)

        err_msg = (
            f'\nInvalid batch number provided, {CNS.FB}{bat_num}{CNS.FR}. '
            f'The max batch number is {CNS.FB}{tot_num_bats - 1}{CNS.FR}.'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    # Initialize offset rows value
    offset_rows = bat_num * bat_size if bat_num else 0
    exp_ctr = 0  # Set export counter

    # Write data to files in set batch sizes
    for i in range(bat_num, tot_num_bats):
        msg = f'Extracting batch "{i}"'
        if rem_bat_size and i == tot_num_bats - 1:
            msg += ' (remainder)'
        logging.debug(msg)

        if rem_bat_size and i == tot_num_bats - 1:
            query = (
                sel_qry.replace('N_ROWS', str(rem_bat_size))
                       .replace('OS_ROWS', str(offset_rows))
            )
        else:
            query = (
                sel_qry.replace('N_ROWS', str(bat_size))
                       .replace('OS_ROWS', str(offset_rows))
            )

        attempts = 0
        while attempts < CNS.MAX_ATTEMPTS:
            logging.debug(f'Running query: "{query}"')
            try:
                start = dt.datetime.now()
                result = exec_txt_qry(engine, query)
                end = dt.datetime.now()
            except Exception as exc:
                attempts += 1
                if attempts == CNS.MAX_ATTEMPTS:
                    logging.error(traceback.format_exc().rstrip())
                    err_msg = f'\nBatch query failed: {CNS.FB}{exc}{CNS.FR}\n'
                    print(err_msg)
                    raise
                else:
                    err_msg = 'Batch query failed. Trying again.'
                    logging.error(err_msg)
                    continue
            else:
                secs_elapsed = (end - start).total_seconds()
                msg = (
                    'Batch query execution time: '
                    f'"{dt.timedelta(seconds=secs_elapsed)}"'
                )
                logging.info(msg)

            logging.debug('Extracting batch results')
            try:
                start = dt.datetime.now()
                cols, rslt_set = tuple(result.keys()), result.all()
                end = dt.datetime.now()
            except Exception as exc:
                attempts += 1
                if attempts == CNS.MAX_ATTEMPTS:
                    logging.error(traceback.format_exc().rstrip())
                    err_msg = (
                        f'\nBatch extraction failed: {CNS.FB}{exc}{CNS.FR}\n'
                    )
                    print(err_msg)
                    raise
                else:
                    err_msg = 'Batch extraction failed. Trying again.'
                    logging.error(err_msg)
                    continue
            else:
                secs_elapsed = (end - start).total_seconds()
                msg = (
                    'Batch extraction time: '
                    f'"{dt.timedelta(seconds=secs_elapsed)}"'
                )
                logging.info(msg)
                break

        logging.debug('Converting batch results to a DataFrame')
        cols = col_conv_map.keys()
        df = pd.DataFrame(rslt_set, columns=cols)

        logging.debug('Converting DataFrame column data types')
        try:
            df = df.astype(col_conv_map)
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                '\nDataFrame column data type conversion '
                f'failed: {CNS.FB}{exc}{CNS.FR}\n'
            )
            print(err_msg)
            raise

        o_path = (
            os.path.join(o_dir, f'bat.{i}.parquet')
            if prq_o
            else
            os.path.join(o_dir, f'bat.{i}.csv')
        )

        logging.debug('Exporting batch results')
        try:
            start = dt.datetime.now()
            if prq_o:
                df.to_parquet(o_path)
            else:
                df.to_csv(o_path, sep=delimiter, index=False,
                          encoding=encoding)
            end = dt.datetime.now()
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = f'\nBatch result export failed: {CNS.FB}{exc}{CNS.FR}\n'
            print(err_msg)
            raise
        else:
            secs_elapsed = (end - start).total_seconds()
            msg = (
                'Batch write time: '
                f'"{dt.timedelta(seconds=secs_elapsed)}"'
            )
            logging.info(msg)
            exp_ctr += df.shape[0]

        # Increment offset rows value to obtain next batch
        offset_rows += bat_size

    return exp_ctr


def main(args: argparse.Namespace) -> None:
    '''Define starting point for execution of the subprogram.'''
    logging.debug(f'Starting "{args.subcommand}" subprogram')

    # Determine output type
    prq_o = not args.csv_output
    csv_o = False if prq_o else True

    db_qry_ck(args)

    if args.dialect != 'sqlite':
        cmd = drv_ck(args)
        exec(cmd, globals())
        drivername = f'{args.dialect}+{CNS.DIALS_DRVS[args.dialect]}'
    else:
        drivername = args.dialect

    if args.credentials:
        logging.debug('Obtaining database credentials')
        db_srv_name, db_username = args.credentials.split(',')
        db_creds = kr.get_credential(db_srv_name, db_username)
        username, password = db_creds.username, db_creds.password

    if args.oracle_client_libs:
        logging.debug(
            'Loading Oracle Client libraries and '
            'enabling Thick Mode'
        )
        oracledb.init_oracle_client(lib_dir=args.oracle_client_libs)

    logging.debug('Creating engine')
    # Create URL object
    url = sa.URL.create(
        drivername=drivername,
        username=(username if args.credentials else None),
        password=(password if args.credentials else None),
        host=args.host,
        port=args.port,
        database=args.database,
        query=args.query

    )
    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Create connection context to verify connection
    logging.debug('Testing engine connection')
    try:
        with engine.connect():
            pass
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nEngine connection test failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise

    # Set database schema and cleaned table name
    if len(args.db_tbl.split('.')) > 1:
        db_sch = args.db_tbl.split('.')[0]
    else:
        db_sch = None
    no_dqtes_db_tbl = args.db_tbl.replace('"', '')

    logging.debug('Obtaining database metadata')
    md, db_md = get_db_md(engine, schema=db_sch, views=True)

    if args.display_metadata:
        logging.debug('Displaying database metadata')
        hdr = f'{args.database} Objects'
        hdr_len = len(hdr)
        print(f"\n{CNS.FB}{hdr}{CNS.FR}\n{'~' * hdr_len}")
        for tbl_key in sorted(md.tables.keys()):
            print(tbl_key)

        logging.debug(f'Ending "{args.subcommand}" subprogram')
        return

    logging.debug('Checking that database table exists')
    if no_dqtes_db_tbl not in db_md.keys():
        logging.error('Database table does not exist')

        err_msg = (
            f'\n{CNS.FB}{args.db_tbl}{CNS.FR} does not exist in the database.'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    if '"' in args.db_tbl:
        logging.info(f"Database table name '{args.db_tbl}'")
    else:
        logging.info(f'Database table name "{args.db_tbl}"')
    logging.info(f'Batch size "{args.bat_sz:,}"')

    # Create number of rows query
    num_rows_qry = (
        'SELECT '
        'COUNT(*) AS num_of_rows '
        'FROM '
        + args.db_tbl
    )
    num_rows_qry += f' {args.filter}' if args.filter else ''

    logging.debug('Determining number of database table rows')
    logging.debug(f'Running query: "{num_rows_qry}"')
    try:
        start = dt.datetime.now()
        result = exec_txt_qry(engine, num_rows_qry)
        end = dt.datetime.now()
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nNumber of rows query failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise
    else:
        secs_elapsed = (end - start).total_seconds()
        msg = (
            'Number of database table rows query execution time: '
            f'"{dt.timedelta(seconds=secs_elapsed)}"'
        )
        logging.info(msg)

    logging.debug('Extracting number of rows')
    num_of_rows = result.all()[0][0]
    logging.info(f'Number of database table rows: "{num_of_rows:,}"')

    if args.col_conv_map:
        col_conv_map = args.col_conv_map
    else:
        logging.debug('Creating Table object from database table')
        tbl_obj = md.tables[no_dqtes_db_tbl]

        logging.debug('Creating column name to data type conversion map')
        col_conv_map = crt_n_type_map(tbl_obj)

    if args.select_columns and not args.col_conv_map:
        # Strip column names
        fltr_cols = args.select_columns.split(',')
        fltr_cols = [col.strip() for col in fltr_cols]

        logging.debug(
            'Filtering automatically generated column conversion map'
        )
        col_conv_map = {
            col: d_type
            for col, d_type in col_conv_map.items()
            if col in fltr_cols
        }

    # Create SELECT query
    sel_qry = 'SELECT '
    sel_qry += f'{args.select_columns} ' if args.select_columns else '* '
    sel_qry += 'FROM '
    sel_qry += f'{args.db_tbl} '
    sel_qry += f'{args.filter} ' if args.filter else ''
    sel_qry += 'ORDER BY '
    sel_qry += f'{args.ob_cols} '
    sel_qry += (
        'OFFSET OS_ROWS ROWS FETCH NEXT N_ROWS ROWS ONLY'
        if args.dialect == 'oracle'
        else
        'LIMIT N_ROWS OFFSET OS_ROWS'
    )

    o_dir = os.path.join(args.o_root_dir, f'{CNS.D_STAMP}_{no_dqtes_db_tbl}')
    try:
        # If output directory does not exist, create it
        crt_dir(o_dir)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            f'\nOutput directory creation failed: {CNS.FB}{exc}{CNS.FR}\n'
        )
        print(err_msg)
        raise

    if not args.bat_num:
        try:
            wipe_dir(o_dir)  # Wipe output directory
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                f'\nOutput directory wipe failed: {CNS.FB}{exc}{CNS.FR}\n'
            )
            print(err_msg)
            raise

    logging.debug('Extracting database table')
    start = dt.datetime.now()
    exp_ctr = extr_dbt_bats(engine, args.bat_sz, num_of_rows, sel_qry,
                            o_dir, col_conv_map=col_conv_map,
                            csv_o=csv_o, delimiter=args.delimiter,
                            encoding=CNS.ENC, bat_num=args.bat_num)
    end = dt.datetime.now()

    secs_elapsed = (end - start).total_seconds()
    msg = (
        'Database table total extraction time: '
        f'"{dt.timedelta(seconds=secs_elapsed)}"'
    )
    logging.info(msg)
    logging.info(f'Number of rows extracted: "{exp_ctr:,}"')

    logging.debug('Determining destination number of rows')
    if csv_o:
        ext = 'csv'
        rdr_kwargs = {
            'delimiter': args.delimiter,
            'encoding': CNS.ENC
        }
        err_msg_ext = 'CSV'
    else:
        ext = 'parquet'
        rdr_kwargs = None
        err_msg_ext = ext

    try:
        dfs = imp_files_pd(o_dir, ext, rdr_kwargs)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            f'\nImporting output {err_msg_ext} files failed: '
            f'{CNS.FB}{exc}{CNS.FR}\n'
        )
        print(err_msg)
        raise
    else:
        sers = (df.iloc[:, 0] for df in dfs)
        exp_nrows = pd.concat(sers).shape[0]

    logging.info(f'Source data number of rows: "{num_of_rows:,}"')
    logging.info(f'Number of rows extracted: "{exp_ctr:,}"')
    logging.info(f'Destination number of rows: "{exp_nrows:,}"')

    logging.debug(f'Ending "{args.subcommand}" subprogram')
