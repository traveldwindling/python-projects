'''\
Batch refresh a table from another table.

This program aims to provide a database-agnostic way to batch refresh one
database table from another database table. A database name and/or
query mapping is required to run the program.

By default, all columns are selected, but specific columns and additional
WHERE clause filtering strings can be specified.

Classes:

    * DatabaseArgs - Model a database's arguments.

Functions:

    * bat_rfsh_tbl() - Batch refresh a table in a destination database from a
                       table in a source database.
    * parse_cfg() - Parse configuration file entries.
    * main() - Define starting point for execution of the subprogram.

Requires
----------------
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import datetime as dt  # Enable classes for manipulating dates and times
# Enable storing and accessing of passwords safely
import keyring as kr
# Enable access to some variables/functions used/maintained by the interpreter
import sys
import tomllib  # Enable an interface for parsing TOML
# Enable a standard interface to extract, format, and print stack traces
import traceback

import sqlalchemy as sa  # Enable database abstraction library

from de_pgms.cli import logging
from de_pgms.config import ConstantsNamespace as CNS
from de_pgms.funcs.arg_validators import val_dialect_arg
from de_pgms.funcs.checkers import drv_ck
from de_pgms.funcs.etlfns import exec_txt_qry, get_db_md


###########
# Classes #
###########
class DatabaseArgs:
    '''
    Model a database's arguments.

    Methods:

        * __init__() - Initialize the class with stateful data.
        * __repr__() - Return a string containing a printable representation
                       of an  object.
        * __str__() - Return a string containing a nicely printable
                      representation of an object.
    '''

    def __init__(self, dialect: str = 'sqlite', database: str = '',
                 query: dict[str: str | tuple[str]] | None = None,
                 table: str | None = None, credentials: str | None = None,
                 host: str | None = None, port: int | None = None) -> None:
        '''
        Initialize the class with stateful data.

        Parameters
        ----------------
        dialect : str (optional)
            The name of the database backend. Defaults to sqlite.
        database : str (optional)
            The database name. Defaults to an empty string.
        query : dict (optional)
            A dictionary of string keys to string values to be passed
            to the dialect and/or the DBAPI upon connect. Defaults to None.
        table : str (optional)
            The database table to operate on. Defaults to None.
        credentials : str (optional)
            The service name and username for the database credentials
            stored in the system key store, separated by a comma
            (e.g., 'db_creds,guido'). Defaults to None.
        host : str (optional)
            The name of the host. Defaults to None.
        port : int (optional)
            The port number. Defaults to None.
        '''
        self.dialect = dialect
        self.database = database
        self.query = query
        self.table = table
        self.credentials = credentials
        self.host = host
        self.port = port

    def __repr__(self) -> str:
        '''
        Return a string containing a printable representation of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'dialect={self.dialect!r}, '
            f'database={self.database!r}, '
            f'query={self.query!r}, '
            f'table={self.table!r}, '
            f'credentials={self.credentials!r}, '
            f'host={self.host!r}, '
            f'port={self.port!r})'
        )

    def __str__(self) -> str:
        '''
        Return a string containing a nicely printable representation
        of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'{self.dialect}, '
            f'{self.database}, '
            f'{self.query}, '
            f'{self.table}, '
            f'{self.credentials}, '
            f'{self.host}, '
            f'{self.port})'
        )


#############
# Functions #
#############
def bat_rfsh_tbl(engine_src: sa.engine.Engine, engine_dest: sa.engine.Engine,
                 bat_size: int, num_of_rows: int,
                 dest_tbl: sa.sql.schema.Table, sel_qry: str | None = None,
                 bat_num: int = 0) -> int:
    '''
    Batch refresh a table in a destination database from a table
    in a source database.

    Parameters
    ----------------
    engine_src : sa.engine.Engine | str
        SQLAlchemy engine for the source database.
    engine_dest : sa.engine.Engine
        SQLAlchemy engine for the destination database.
    bat_size: int
        Batch size (number of rows) to utilize for each SELECT query.
    num_of_rows : int
        Number of rows in data source to extract.
    dest_tbl : sa.sql.schema.Table
        Destination database table object to refresh.
    sel_qry : str (optional)
        SELECT query to run for source database. Defaults to None.
    bat_num : int (optional)
        The batch number to start at. Defaults to 0.

    Returns
    ----------------
    int
        Number of rows inserted.
    '''
    # Determine the number of batches to run
    # and the size of any remaining batch
    num_of_bats = num_of_rows // bat_size
    rem_bat_size = num_of_rows % bat_size

    # Determine total number of batches
    tot_num_bats = num_of_bats + 1 if rem_bat_size else num_of_bats

    # Ensure batch size is less than the number of database object rows
    if num_of_bats < 1:
        err_msg = (
            f'Provided batch size ({bat_size:,}) exceeds the '
            f'size of the table ({num_of_rows:,})'
        )
        logging.error(err_msg)

        err_msg = (
            f'\nProvided batch size ({CNS.FB}{bat_size:,}{CNS.FR}) exceeds '
            f'the size of the table ({CNS.FB}{num_of_rows:,}{CNS.FR}).'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    # Ensure batch number is valid
    if bat_num >= tot_num_bats:
        err_msg = (
            f'Batch number "{bat_num}" exceeds the highest batch '
            f'number, "{tot_num_bats - 1}"'
        )
        logging.error(err_msg)

        err_msg = (
            f'\nInvalid batch number provided, {CNS.FB}{bat_num}{CNS.FR}. '
            f'The max batch number is {CNS.FB}{tot_num_bats - 1}{CNS.FR}.'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    cols = dest_tbl.c.keys()  # Define destination table column names

    # Initialize offset rows value
    offset_rows = bat_num * bat_size if bat_num else 0
    ins_ctr = 0  # Set insert counter

    # Insert data into destination table in set batch sizes
    for i in range(bat_num, tot_num_bats):
        msg = f'Extracting batch "{i}"'
        if rem_bat_size and i == tot_num_bats - 1:
            msg += ' (remainder)'
        logging.debug(msg)

        if rem_bat_size and i == tot_num_bats - 1:
            query = (
                sel_qry.replace('N_ROWS', str(rem_bat_size))
                       .replace('OS_ROWS', str(offset_rows))
            )
        else:
            query = (
                sel_qry.replace('N_ROWS', str(bat_size))
                       .replace('OS_ROWS', str(offset_rows))
            )

        attempts = 0
        while attempts < CNS.MAX_ATTEMPTS:
            logging.debug(f'Running query: "{query}"')
            try:
                start = dt.datetime.now()
                result = exec_txt_qry(engine_src, query)
                end = dt.datetime.now()
            except Exception as exc:
                attempts += 1
                if attempts == CNS.MAX_ATTEMPTS:
                    logging.error(traceback.format_exc().rstrip())
                    err_msg = (
                        f'\nBatch query failed: {CNS.FB}{exc}{CNS.FR}\n'
                    )
                    print(err_msg)
                    raise
                else:
                    err_msg = 'Batch query failed. Trying again.'
                    logging.error(err_msg)
                    continue
            else:
                secs_elapsed = (end - start).total_seconds()
                msg = (
                    'Batch query execution time: '
                    f'"{dt.timedelta(seconds=secs_elapsed)}"'
                )
                logging.info(msg)

            logging.debug('Extracting batch results')
            try:
                start = dt.datetime.now()
                rslt_set = result.all()
                end = dt.datetime.now()
            except Exception as exc:
                attempts += 1
                if attempts == CNS.MAX_ATTEMPTS:
                    logging.error(traceback.format_exc().rstrip())
                    err_msg = (
                        '\nBatch extraction failed: '
                        f'{CNS.FB}{exc}{CNS.FR}\n'
                    )
                    print(err_msg)
                    raise
                else:
                    err_msg = 'Batch extraction failed. Trying again.'
                    logging.error(err_msg)
                    continue
            else:
                secs_elapsed = (end - start).total_seconds()
                msg = (
                    'Batch extraction time: '
                    f'"{dt.timedelta(seconds=secs_elapsed)}"'
                )
                logging.info(msg)
                break

        logging.debug('Converting batch results to list of dictionaries')
        rslt_set = [
            {col: value for col, value in zip(cols, row)}
            for row in rslt_set
        ]

        logging.debug('Inserting batch results')
        if i == 0:
            logging.debug(f'Running statement: "{dest_tbl.insert()}"')

        attempts = 0
        while attempts < CNS.MAX_ATTEMPTS:
            try:
                start = dt.datetime.now()
                with engine_dest.begin() as conn:
                    _ = conn.execute(dest_tbl.insert(), rslt_set)
                end = dt.datetime.now()
            except Exception as exc:
                attempts += 1
                if attempts == CNS.MAX_ATTEMPTS:
                    logging.error(traceback.format_exc().rstrip())
                    err_msg = f'\nBatch insert failed: {CNS.FB}{exc}{CNS.FR}\n'
                    print(err_msg)
                    raise
                else:
                    err_msg = 'Batch insert failed. Trying again.'
                    logging.error(err_msg)
                    continue
            else:
                secs_elapsed = (end - start).total_seconds()
                msg = (
                    'Batch insert execution time: '
                    f'"{dt.timedelta(seconds=secs_elapsed)}"'
                )
                logging.info(msg)
                ins_ctr += len(rslt_set)
                break

        # Increment offset rows value to obtain next batch
        offset_rows += bat_size

    return ins_ctr


def parse_cfg(args: argparse.Namespace) \
        -> tuple[DatabaseArgs, DatabaseArgs]:
    '''
    Parse configuration file entries.

    Parameters
    ----------------
    args : argparse.Namespace
        An argparse namespace class.

    Returns
    ----------------
    tuple | DatabaseArgs
        Tuple of DatabaseArgs objects for the source and destination
        databases.
    '''
    try:
        with open(args.cfg_file, 'rb') as f:
            cfg = tomllib.load(f)
    except Exception as exc:
        err_msg = f'\nConfiguration file read failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise

    src_cfg = cfg['databases']['source']
    src_args = DatabaseArgs()

    dest_cfg = cfg['databases']['destination']
    if 'ob_cols' not in dest_cfg.keys():
        items = list(dest_cfg.items())
        items.insert(2, ('ob_cols', ''))
        dest_cfg = dict(items)
    dest_args = DatabaseArgs()

    for kv_src, kv_dest in zip(src_cfg.items(), dest_cfg.items()):
        match kv_dest[0]:
            case 'dialect':
                src_args.dialect = val_dialect_arg(kv_src[1])
                dest_args.dialect = val_dialect_arg(kv_dest[1])
            case 'table':
                src_args.table = str(kv_src[1])
                dest_args.table = str(kv_dest[1])
            case 'ob_cols':
                src_args.ob_cols = str(kv_src[1])
                dest_args.ob_cols = str(kv_dest[1])
            case 'credentials':
                src_args.credentials = str(kv_src[1])
                dest_args.credentials = str(kv_dest[1])
            case 'database':
                src_args.database = str(kv_src[1])
                dest_args.database = str(kv_dest[1])
            case 'host':
                src_args.host = str(kv_src[1])
                dest_args.host = str(kv_dest[1])
            case 'port':
                try:
                    src_args.port = int(kv_src[1])
                except Exception:
                    err_msg = (
                        f'{CNS.FB}{kv_src[1]}{CNS.FR} is not '
                        'a valid integer.'
                    )
                    raise argparse.ArgumentTypeError(err_msg)

                try:
                    dest_args.port = int(kv_dest[1])
                except Exception:
                    err_msg = (
                        f'{CNS.FB}{kv_dest[1]}{CNS.FR} is not '
                        'a valid integer.'
                    )
                    raise argparse.ArgumentTypeError(err_msg)
            case 'query':
                try:
                    src_args.query = dict(kv_src[1])
                except Exception:
                    err_msg = (
                        f'{CNS.FB}{kv_src[1]}{CNS.FR} is not '
                        'a valid dictionary.'
                    )
                    raise argparse.ArgumentTypeError(err_msg)

                try:
                    dest_args.query = dict(kv_dest[1])
                except Exception:
                    err_msg = (
                        f'{CNS.FB}{kv_dest[1]}{CNS.FR} is not '
                        'a valid dictionary.'
                    )
                    raise argparse.ArgumentTypeError(err_msg)
            case _:
                err_msg = (
                    '\nConfiguration file provided unsupported key: '
                    f'{CNS.FB}{kv_dest[0]}{CNS.FR}'
                )
                print(err_msg)
                sys.exit(1)
    else:
        return src_args, dest_args


def main(args: argparse.Namespace) -> None:
    '''Define starting point for execution of the subprogram.'''
    logging.debug(f'Starting "{args.subcommand}" subprogram')

    src_args, dest_args = parse_cfg(args)

    cnd = (
        (not src_args.database and not src_args.query)
        or (not dest_args.database and not dest_args.query)
    )
    if cnd:
        logging.error('Database/query dictionary check failed')

        err_msg = (
            f'\nSpecify the {CNS.FB}--database{CNS.FR} and/or '
            f'{CNS.FB}--query{CNS.FR} option(s) before proceeding again.'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    if src_args.dialect != 'sqlite':
        cmd = drv_ck(src_args)
        exec(cmd, globals())
        drivername_src = (
            f'{src_args.dialect}+{CNS.DIALS_DRVS[src_args.dialect]}'
        )
    else:
        drivername_src = src_args.dialect

    if dest_args.dialect != 'sqlite':
        cmd = drv_ck(dest_args)
        exec(cmd, globals())
        drivername_dest = (
            f'{dest_args.dialect}+{CNS.DIALS_DRVS[dest_args.dialect]}'
        )
    else:
        drivername_dest = dest_args.dialect

    if src_args.credentials:
        logging.debug('Obtaining source database credentials')
        db_srv_name, db_username = src_args.credentials.split(',')
        db_creds = kr.get_credential(db_srv_name, db_username)
        username_src, password_src = db_creds.username, db_creds.password
    if dest_args.credentials:
        logging.debug('Obtaining destination database credentials')
        db_srv_name, db_username = dest_args.credentials.split(',')
        db_creds = kr.get_credential(db_srv_name, db_username)
        username_dest, password_dest = db_creds.username, db_creds.password

    if args.oracle_client_libs:
        logging.debug(
            'Loading Oracle Client libraries and '
            'enabling Thick Mode'
        )
        oracledb.init_oracle_client(lib_dir=args.oracle_client_libs)

    logging.debug('Creating source engine')
    url_src = sa.URL.create(
        drivername=drivername_src,
        username=(username_src if src_args.credentials else None),
        password=(password_src if src_args.credentials else None),
        host=(src_args.host if src_args.host else None),
        port=(src_args.port if src_args.port else None),
        database=(src_args.database if src_args.database else None),
        query=(src_args.query if src_args.query else {})
    )
    # Create a new engine instance
    engine_src = sa.create_engine(url_src, pool_pre_ping=True)

    logging.debug('Testing source engine connection')
    try:
        with engine_src.connect():
            pass
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            '\nSource engine connection test failed: '
            f'{CNS.FB}{exc}{CNS.FR}\n'
        )
        print(err_msg)
        raise

    # Set source database schema
    if len(src_args.table.split('.')) > 1:
        db_sch_src = src_args.table.split('.')[0]
    else:
        db_sch_src = None
    logging.debug('Obtaining source database metadata')
    md_src, db_md_src = get_db_md(engine_src, schema=db_sch_src,
                                  views=True)

    if '"' in src_args.table:
        logging.info(f"Source database table name '{src_args.table}'")
    else:
        logging.info(f'Source database table name "{src_args.table}"')

    logging.debug('Creating destination engine')
    url_dest = sa.URL.create(
        drivername=drivername_dest,
        username=(username_dest if dest_args.credentials else None),
        password=(password_dest if dest_args.credentials else None),
        host=(dest_args.host if dest_args.host else None),
        port=(dest_args.port if dest_args.port else None),
        database=(dest_args.database if dest_args.database else None),
        query=(dest_args.query if dest_args.query else {})
    )
    engine_dest = sa.create_engine(url_dest, pool_pre_ping=True)

    logging.debug('Testing destination engine connection')
    try:
        with engine_dest.connect():
            pass
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            '\nDestination engine connection test failed: '
            f'{CNS.FB}{exc}{CNS.FR}\n'
        )
        print(err_msg)
        raise

    # Set destination database schema and cleaned object name
    if len(dest_args.table.split('.')) > 1:
        db_sch_dest = dest_args.table.split('.')[0]
    else:
        db_sch_dest = None
    no_dqtes_db_obj = dest_args.table.replace('"', '')

    logging.debug('Obtaining destination database metadata')
    md_dest, db_md_dest = get_db_md(engine_dest, schema=db_sch_dest,
                                    views=True)

    if args.display_metadata:
        logging.debug('Displaying database metadata')

        hdr = f'{src_args.database} Objects'
        hdr_len = len(hdr)
        print(f"\n{CNS.FB}{hdr}{CNS.FR}\n{'~' * hdr_len}")
        for tbl_key in sorted(md_src.tables.keys()):
            print(tbl_key)

        hdr = f'{dest_args.database} Objects'
        hdr_len = len(hdr)
        print(f"\n{CNS.FB}{hdr}{CNS.FR}\n{'~' * hdr_len}")
        for tbl_key in sorted(md_dest.tables.keys()):
            print(tbl_key)

        logging.debug(f'Ending "{args.subcommand}" subprogram')
        return

    logging.debug('Checking that destination table exists')
    if dest_args.table in db_md_dest.keys():
        dest_tbl = md_dest.tables[no_dqtes_db_obj]
    else:
        logging.error('Destination table does not exist')

        err_msg = (
            f'\n{CNS.FB}{dest_args.table}{CNS.FR} does not exist '
            'in the database.'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1')
        sys.exit(1)

    if '"' in dest_args.table:
        logging.info(f"Destination table name '{dest_args.table}'")
    else:
        logging.info(f'Destination table name "{dest_args.table}"')
    logging.info(f'Batch size "{args.bat_sz:,}"')

    # Create number of rows query
    num_rows_qry = (
        'SELECT '
        'COUNT(*) AS num_of_rows '
        'FROM '
        + src_args.table
    )
    num_rows_qry += f' {args.filter}' if args.filter else ''

    logging.debug('Determining number of source database table rows')
    logging.debug(f'Running query: "{num_rows_qry}"')
    try:
        start = dt.datetime.now()
        result = exec_txt_qry(engine_src, num_rows_qry)
        end = dt.datetime.now()
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nNumber of rows query failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise
    else:
        secs_elapsed = (end - start).total_seconds()
        msg = (
            'Number of table rows query execution time: '
            f'"{dt.timedelta(seconds=secs_elapsed)}"'
        )
        logging.info(msg)

    logging.debug('Extracting number of rows')
    num_of_rows = result.all()[0][0]

    # Create SELECT query
    sel_qry = 'SELECT '
    sel_qry += f'{args.columns} ' if args.columns else '* '
    sel_qry += 'FROM '
    sel_qry += f'{src_args.table} '
    sel_qry += f'{args.filter} ' if args.filter else ''
    sel_qry += 'ORDER BY '
    sel_qry += f'{src_args.ob_cols} '
    sel_qry += (
        'OFFSET OS_ROWS ROWS FETCH NEXT N_ROWS ROWS ONLY'
        if src_args.dialect == 'oracle'
        else
        'LIMIT N_ROWS OFFSET OS_ROWS'
    )

    logging.info(f'Number of data source rows: "{num_of_rows:,}"')

    if not args.bat_num:
        stmt = sa.delete(dest_tbl)
        logging.debug('Wipe destination table contents')
        try:
            start = dt.datetime.now()
            with engine_dest.begin() as conn:
                _ = conn.execute(stmt)
            end = dt.datetime.now()
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                f'\nDestination table wipe failed: {CNS.FB}{exc}{CNS.FR}\n'
            )
            print(err_msg)
            raise
        else:
            secs_elapsed = (end - start).total_seconds()
            msg = (
                'Destination table wipe execution time: '
                f'"{dt.timedelta(seconds=secs_elapsed)}"'
            )
            logging.info(msg)

    logging.debug('Refreshing destination table with updated data')
    start = dt.datetime.now()
    ins_ctr = bat_rfsh_tbl(engine_src, engine_dest, args.bat_sz, num_of_rows,
                           dest_tbl, sel_qry=sel_qry, bat_num=args.bat_num)
    end = dt.datetime.now()

    secs_elapsed = (end - start).total_seconds()
    msg = (
        'Table refresh total execution time: '
        f'"{dt.timedelta(seconds=secs_elapsed)}"'
    )
    logging.info(msg)
    logging.info(f'Number of rows inserted: "{ins_ctr:,}"')

    num_rows_qry = (
        'SELECT '
        'COUNT(*) AS num_of_rows '
        'FROM '
        + dest_args.table
    )
    logging.debug('Determining destination data number of rows')
    logging.debug(f'Running query: "{num_rows_qry}"')
    try:
        result = exec_txt_qry(engine_dest, num_rows_qry)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            '\nDestination data number of rows query failed: '
            f'{CNS.FB}{exc}{CNS.FR}\n'
        )
        print(err_msg)
        raise
    else:
        logging.info(f'Source data number of rows: "{num_of_rows:,}"')
        logging.info(f'Number of rows inserted: "{ins_ctr:,}"')
        msg = f'Destination data number of rows: "{result.all()[0][0]:,}"'
        logging.info(msg)

    logging.debug(f'Ending "{args.subcommand}" subprogram')
