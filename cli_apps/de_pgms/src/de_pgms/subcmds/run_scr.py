'''\
Run a SQL script.

This program aims to provide a database-agnostic way to run a SQL script via
Python. A database name and/or query mapping is required to run the program.

If a SQL statement produces a result set and an output directory
is specified, the result set is written to a CSV file. CSV files are named
with a date stamp and the SQL statement's position in the SQL script.

Functions:

    * main() - Define starting point for execution of the subprogram.

Requires
----------------
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import datetime as dt  # Enable classes for manipulating dates and times
# Enable storing and accessing of passwords safely
import keyring as kr
# Enable a portable way of using operating system dependent functionality
import os
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable a standard interface to extract, format, and print stack traces
import traceback

import sqlalchemy as sa  # Enable database abstraction library

from de_pgms.cli import logging
from de_pgms.config import ConstantsNamespace as CNS
from de_pgms.funcs.checkers import db_qry_ck, drv_ck
from de_pgms.funcs.etlfns import clean_sql_stmt, exec_txt_qry
from de_pgms.funcs.iofns import crt_dir, rd_file, wr_csv


#############
# Functions #
#############
def main(args: argparse.Namespace) -> None:
    '''Define starting point for execution of the subprogram.'''
    logging.debug(f'Starting "{args.subcommand}" subprogram')

    db_qry_ck(args)

    if args.dialect != 'sqlite':
        cmd = drv_ck(args)
        exec(cmd, globals())
        drivername = f'{args.dialect}+{CNS.DIALS_DRVS[args.dialect]}'
    else:
        drivername = args.dialect

    if args.credentials:
        logging.debug('Obtaining database credentials')
        db_srv_name, db_username = args.credentials.split(',')
        db_creds = kr.get_credential(db_srv_name, db_username)
        username, password = db_creds.username, db_creds.password

    if args.oracle_client_libs:
        logging.debug(
            'Loading Oracle Client libraries and '
            'enabling Thick Mode'
        )
        oracledb.init_oracle_client(lib_dir=args.oracle_client_libs)

    logging.debug('Creating engine')
    # Create URL object
    url = sa.URL.create(
        drivername=drivername,
        username=(username if args.credentials else None),
        password=(password if args.credentials else None),
        host=args.host,
        port=args.port,
        database=args.database,
        query=args.query
    )
    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Create connection context to verify connection
    logging.debug('Testing engine connection')
    try:
        with engine.connect():
            pass
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nEngine connection test failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise

    logging.debug('Reading SQL script file')
    try:
        sql_scr_txt = rd_file(args.sql_scr)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = f'\nSQL script read failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise

    logging.debug('Parsing SQL text to create statements')
    sql_stmts = re.split(CNS.SEMI_PAT, sql_scr_txt)

    if args.clean:
        # Clean SQL statements
        sql_stmts = [
            clean_sql_stmt(stmt)
            for stmt in sql_stmts
        ]

    # Filter SQL statements
    sql_stmts = [
        sql_stmt
        for sql_stmt in sql_stmts
        if sql_stmt not in ('', '\n', '\r', '\r\n')
    ]

    # Display statements and exit, if desired
    if args.display_statements:
        logging.debug('Displaying SQL statements')
        for idx, stmt in enumerate(sql_stmts, start=1):
            sec_len = len(f'SQL statement {idx}') + 4
            stmt_msg = (
                f'\n{CNS.SEC_CHAR * sec_len}\n'
                f'{CNS.SEC_CHAR} SQL statement {idx} {CNS.SEC_CHAR}\n'
                f'{CNS.SEC_CHAR * sec_len}\n'
                + stmt.strip()
            )
            print(stmt_msg)

        logging.debug(f'Ending "{args.subcommand}" subprogram')
        return

    if args.output_dir:
        try:
            # If output directory does not exist, create it
            crt_dir(args.output_dir)
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                f'\nOutput directory creation failed: {CNS.FB}{exc}{CNS.FR}\n'
            )
            print(err_msg)
            raise

    # Create error message lists
    stmt_exec_msgs = []
    wr_msgs = []

    for idx, stmt in enumerate(sql_stmts, start=1):
        log_stmt = re.sub(r'(\n|\r|\r\n)', ' ', stmt)
        logging.debug(f'Executing SQL statement "{idx}": "{log_stmt}"')
        try:
            start = dt.datetime.now()
            result = exec_txt_qry(engine, stmt)
            end = dt.datetime.now()
        except Exception:
            logging.error(traceback.format_exc().rstrip())
            stmt_exec_msgs.append(f'- {CNS.FB}{idx}{CNS.FR}')
            continue
        else:
            secs_elapsed = (end - start).total_seconds()
            msg = (
                'SQL statement execution time: '
                f'"{dt.timedelta(seconds=secs_elapsed)}"'
            )
            logging.info(msg)

        logging.debug('Checking for result set')
        try:
            result.first()
        except sa.exc.ResourceClosedError as exc:
            logging.warning(exc)
            continue
        except Exception:
            logging.error(traceback.format_exc().rstrip())
            stmt_exec_msgs.append(f'- {CNS.FB}{idx}{CNS.FR}')
            continue

        try:
            logging.debug(
                f'Re-executing SQL statement "{idx}" to get result set'
            )
            result = exec_txt_qry(engine, stmt)
        except Exception:
            logging.error(traceback.format_exc().rstrip())
            stmt_exec_msgs.append(f'- {CNS.FB}{idx}{CNS.FR}')
            continue
        else:
            if args.output_dir:
                # Extract column names and rows
                logging.debug('Extracting statement results')
                start = dt.datetime.now()
                cols, rslt_set = tuple(result.keys()), result.all()
                end = dt.datetime.now()

                secs_elapsed = (end - start).total_seconds()
                msg = (
                    'Result set extraction time: '
                    f'"{dt.timedelta(seconds=secs_elapsed)}"'
                )
                logging.info(msg)

                if rslt_set:
                    msg = f'Result set size: "{len(rslt_set):,}"'
                    logging.info(msg)

                    # Add column names to result set
                    rslt_set.insert(0, cols)

                    # Set path to save output results to
                    o_path = os.path.join(
                        args.output_dir,
                        f'{CNS.D_STAMP}_{idx}.csv'
                    )

                    logging.debug('Exporting statement results')
                    try:
                        start = dt.datetime.now()
                        wr_csv(o_path, rslt_set, delimiter=args.delimiter)
                        end = dt.datetime.now()
                    except Exception:
                        logging.error(traceback.format_exc().rstrip())
                        wr_msgs.append(f'- {CNS.FB}{idx}{CNS.FR}')
                    else:
                        secs_elapsed = (end - start).total_seconds()
                        msg = (
                            'Result set write time: '
                            f'"{dt.timedelta(seconds=secs_elapsed)}"'
                        )
                        logging.info(msg)
                else:
                    logging.info(CNS.NO_DATA_RSLT_MSG)
            else:
                warn_msg = (
                    'Cannot write result set to file, as no output '
                    'directory was specified'
                )
                logging.warning(warn_msg)

    if stmt_exec_msgs:
        stmt_exec_msgs.insert(0, CNS.STMT_EXEC_ERR_MSGS)
        stmt_exec_msg = '\n'.join(stmt_exec_msgs)
        print(stmt_exec_msg)

    if wr_msgs:
        wr_msgs.insert(0, CNS.WR_ERR_MSGS)
        wr_msg = '\n'.join(wr_msgs)
        print(wr_msg)

    if stmt_exec_msgs or wr_msgs:
        print(CNS.ERR_MSGS_END)

    logging.debug(f'Ending "{args.subcommand}" subprogram')
