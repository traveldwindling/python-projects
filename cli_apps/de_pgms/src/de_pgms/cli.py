'''\
A CLI program to accomplish data engineering tasks.

A log is saved to 'ex_log_path'.

Functions:

    * crt_parsers() - Create parsers.
    * add_extr_tbl_sp() - Add extract table subparser.
    * add_ins_data_sp() - Add insert data subparser.
    * add_refresh_tbl_sp() - Add refresh table subparser.
    * add_run_qry_sp() - Add run query subparser.
    * add_run_scr_sp() - Add run script subparser.
    * main() - Define starting point for execution of the program.

Requires
----------------
arg_validators
    A collection of functions that validate various inputs.
config
    Configuration file for program settings.
extr_tbl
    Batch extract a table.
ins_data
    Insert data into a table.
pgmspt
    A collection of program support functions.
refresh_tbl
    Batch refresh a table from another table.
run_qry
    Run a SQL query.
run_scr
    Run a SQL query.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
# Enable a lightweight data interchange format inspired by
# JavaScript object literal syntax
import json
# Enable functions and classes that implement an event logging system
import logging
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version as string
from platform import python_version
from signal import signal, SIGINT  # Enable use of signal handlers

from de_pgms.config import ConstantsNamespace as CNS
from de_pgms.funcs.arg_validators import (val_delim_arg, val_dialect_arg,
                                          val_dir_arg, val_file_arg,
                                          val_file_dir_arg)
from de_pgms.funcs.pgmspt import hndl_sigint, init_logging
from de_pgms.subcmds import extr_tbl, ins_data, refresh_tbl, run_qry, run_scr


def crt_parsers() -> tuple[argparse.ArgumentParser,
                           argparse.ArgumentParser,
                           argparse._SubParsersAction]:
    '''
    Create parsers.

    Returns
    ----------------
    tuple
        The common parser, main parser, and subparsers objects.
    '''
    # Create common parser for arguments shared by all subparsers
    common = argparse.ArgumentParser(add_help=False)

    # Add custom log location option
    log_help = 'Log directory path.'
    common.add_argument('-g', '--log-directory', type=str, help=log_help)

    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', CNS.LOG_PATH))
    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    sp_desc = f'''\
The {CNS.PKG_NAME} program offers subcommands that accomplish various
data engineering tasks. A subcommand must be chosen in order to use the
program.

A default install of {CNS.PKG_NAME} does not include any Python database
drivers. If a subcommand requires such a driver and it is not available,
the program prompts for the installation of the appropriate Python package
at runtime.

The following RDMS drivers are supported:

- MariaDB/MySQL -> pymysql
- Microsoft SQL Server -> pyodbc
- Oracle Database -> oracledb
- PostgreSQL -> pg8000

Brief descriptions of each subcommand follow. Display the help of a subcommand
for detailed usage instructions.\
'''
    sp_help = '''\
extr-tbl
    Batch extract a table.

    This subcommand provides a database-agnostic way to extract a database
    table in batches to Parquet files. A database name and/or query mapping
    is required to run the program.

    By default, all columns are extracted, but specific columns and additional
    WHERE clause filtering strings can be specified.

ins-data
    Insert data into a table.

    This program aims to provide a database-agnostic way to insert data into
    a database table in batches from a Parquet file(s). A database name and/or
    query mapping is required to run the program.

refresh-tbl
    Batch refresh a table from another table.

    This subcommand provides a database-agnostic way to batch refresh one
    database table from another database table. A database name and/or
    query mapping is required to run the program.

    By default, all columns are selected, but specific columns and additional
    WHERE clause filtering strings can be specified.

run-qry
    Run a SQL query.

    This subcommand provides a database-agnostic way to run a SQL query
    and extract the result set to a Parquet file. A database name and/or
    query mapping is required to run the program.

run-scr
    Run a SQL script.

    This subcommand provides a database-agnostic way to run a SQL script.
    A database name and/or query mapping is required to run the program.

    If a SQL statement produces a result set and an output directory
    is specified, the result set is written to a CSV file. CSV files are named
    with a date stamp and the SQL statement's position in the SQL script.\
'''
    # Create special action object
    subparsers = parser.add_subparsers(
        description=sp_desc,
        help=sp_help,
        dest='subcommand',
        required=True
    )

    return common, parser, subparsers


def add_extr_tbl_sp(common: argparse.ArgumentParser,
                    subparsers: argparse._SubParsersAction) \
        -> tuple[argparse._SubParsersAction, argparse.ArgumentParser]:
    '''
    Add extract table subparser.

    Parameters
    ----------------
    common : argparse.ArgumentParser
        The common parser object.
    subparsers : argparse._SubParsersAction
        The subparsers object.

    Returns
    ----------------
    tuple
        The subparsers and extract table subparser objects.
    '''
    et_epilog = f'''\
Example Commands
----------------

SQLite
~~~~~~
{CNS.PKG_NAME} extr-tbl \\
    -n 'exp_id, date_time, prototype, runtime' \\
    -s '/home/goji/abullah.sqlite' \\
    -f "WHERE prototype = 'bora'" \\
    'sqlite' \\
    'prototypes' \\
    'rowid' \\
    1000000 \\
    '/home/goji/proto_data/'

PostgreSQL
~~~~~~~~~~
{CNS.PKG_NAME} extr-tbl \\
    -c 'system,gesicht' \\
    -s 'europol' \\
    -t 'localhost' \\
    -p 5432 \\
    'postgresql' \\
    'gesicht.closed_cases' \\
    'ctid' \\
    10000 \\
    '/home/gesicht/cc_data/'\
'''
    et_subparser = subparsers.add_parser(
        'extr-tbl',
        epilog=et_epilog,
        formatter_class=argparse.RawTextHelpFormatter,
        parents=[common]
    )

    # Add dialect argument
    dial_help = 'The name of the database backend.'
    et_subparser.add_argument('dialect', type=val_dialect_arg, help=dial_help)

    # Add database table argument
    db_tbl_help = (
        'The name of the database table to extract. This name is '
        'used in the SELECT query, as is. Also, the casing of this name '
        'should match what is reflected from the database by SQLAlchemy. '
        "This can be verified by using the program's -m (--display-metadata) "
        'option.'
    )
    et_subparser.add_argument('db_tbl', type=str, help=db_tbl_help)

    # Add ORDER BY columns argument
    ob_cols_help = 'Columns SQL string to include in the ORDER BY clause.'
    et_subparser.add_argument('ob_cols', type=str, help=ob_cols_help)

    # Add batch size argument
    bat_sz_help = 'The number of rows to extract in each batch.'
    et_subparser.add_argument('bat_sz', type=int, help=bat_sz_help)

    # Add output root directory argument
    o_root_help = 'The directory to save the output to.'
    et_subparser.add_argument('o_root_dir', type=val_dir_arg, help=o_root_help)

    # Add options
    bn_help = 'The batch number to start at.'
    et_subparser.add_argument('-b', '--bat-num', type=int, help=bn_help,
                              default=0)

    ccm_help = (
        'A dictionary of string column name keys to string '
        'pandas data type values. Provide as a JSON object.'
    )
    et_subparser.add_argument('-e', '--col-conv-map', type=json.loads,
                              help=ccm_help)

    crd_help = (
        'The service name and username for the database credentials '
        'stored in the system key store, separated by a comma '
        "(e.g., 'db_creds,guido')."
    )
    et_subparser.add_argument('-c', '--credentials', type=str, help=crd_help)

    csv_help = 'Save output to CSV files.'
    et_subparser.add_argument('-o', '--csv-output', help=csv_help,
                              action='store_true')

    db_name_help = 'The database name.'
    et_subparser.add_argument('-s', '--database', type=str, help=db_name_help)

    delim_help = 'Delimiter to use in output CSV files. Defaults to a ,.'
    et_subparser.add_argument('-d', '--delimiter', type=val_delim_arg,
                              help=delim_help, default=',')

    dm_help = 'Display SQLAlchemy metadata container keys.'
    et_subparser.add_argument('-m', '--display-metadata', help=dm_help,
                              action='store_true')

    fltr_help = 'SQL filter string to append to the SELECT query.'
    et_subparser.add_argument('-f', '--filter', type=str, help=fltr_help)

    host_help = 'The name of the host.'
    et_subparser.add_argument('-t', '--host', type=str, help=host_help)

    o_clt_lib_help = (
        'The directory that contains the Oracle client libraries.'
    )
    et_subparser.add_argument('-l', '--oracle-client-libs',
                              type=val_dir_arg, help=o_clt_lib_help)

    port_help = 'The port number.'
    et_subparser.add_argument('-p', '--port', type=int, help=port_help)

    qry_help = (
        'A dictionary of string keys to string values to be passed '
        'to the dialect and/or the DBAPI upon connect. Provide as a '
        'JSON object.'
    )
    et_subparser.add_argument('-q', '--query', type=json.loads, help=qry_help,
                              default={})

    sel_cols_help = 'Columns SQL string to include in the SELECT query.'
    et_subparser.add_argument('-n', '--select-columns', type=str,
                              help=sel_cols_help)

    return subparsers, et_subparser


def add_ins_data_sp(common: argparse.ArgumentParser,
                    subparsers: argparse._SubParsersAction) \
        -> tuple[argparse._SubParsersAction, argparse.ArgumentParser]:
    '''
    Add insert data subparser.

    Parameters
    ----------------
    common : argparse.ArgumentParser
        The common parser object.
    subparsers : argparse._SubParsersAction
        The subparsers object.

    Returns
    ----------------
    tuple
        The subparsers and insert data subparser objects.
    '''
    id_epilog = f'''\
Example Commands
----------------

SQLite
~~~~~~
{CNS.PKG_NAME} ins-data \\
    -s '/home/goji/abullah.sqlite' \\
    -z 500 \\
    'sqlite' \\
    'prototypes' \\
    '/home/goji/proto_data/prototypes.parquet'

PostgreSQL
~~~~~~~~~~
{CNS.PKG_NAME} ins-data \\
    -c 'system,gesicht' \\
    -s 'europol' \\
    -t 'localhost' \\
    -p 5432 \\
    'postgresql' \\
    'gesicht.closed_cases' \\
    '/home/gesicht/cc_data/'\
'''
    id_subparser = subparsers.add_parser(
        'ins-data',
        epilog=id_epilog,
        formatter_class=argparse.RawTextHelpFormatter,
        parents=[common]
    )

    # Add dialect argument
    dial_help = 'The name of the database backend.'
    id_subparser.add_argument('dialect', type=val_dialect_arg, help=dial_help)

    # Add database table argument
    db_tbl_help = (
        'The name of the database table to insert data into. The casing '
        'of this name should match what is reflected from the database '
        "by SQLAlchemy. This can be verified by using the program's -m "
        '(--display-metadata) option.'
    )
    id_subparser.add_argument('db_tbl', type=str, help=db_tbl_help)

    # Add data source path argument
    ds_help = (
        'Path to file or directory of files with data '
        'to insert into database table.'
    )
    id_subparser.add_argument('data_src', type=val_file_dir_arg, help=ds_help)

    # Add options
    apnd_help = 'Append data to database table.'
    id_subparser.add_argument('-a', '--append', help=apnd_help,
                              action='store_true')

    bn_help = (
        'The batch number to start at. For a file data source, this '
        'corresponds to a chunk. For a directory data source, '
        'this corresponds to a file.'
    )
    id_subparser.add_argument('-b', '--bat-num', type=int, help=bn_help,
                              default=0)

    bat_sz_help = (
        'The number of rows to insert in each batch when the '
        'data source is a file. For a directory data source, the '
        'batch size is determined by the contents of the file.'
    )
    id_subparser.add_argument('-z', '--bat-sz', type=int, help=bat_sz_help,
                              default=0)

    ccm_help = (
        'A dictionary of string column name keys to string '
        'pandas data type values. Provide as a JSON object. '
        'String column name keys should match column names '
        'in CSV source data.'
    )
    id_subparser.add_argument('-n', '--col-conv-map', type=json.loads,
                              help=ccm_help)

    crd_help = (
        'The service name and username for the database credentials '
        'stored in the system key store, separated by a comma '
        "(e.g., 'db_creds,guido')."
    )
    id_subparser.add_argument('-c', '--credentials', type=str, help=crd_help)

    csv_help = (
        'Path to CSV file or directory of CSV files to insert into '
        'database table.'
    )
    id_subparser.add_argument('-i', '--csv-input', help=csv_help,
                              action='store_true')

    db_name_help = 'The database name.'
    id_subparser.add_argument('-s', '--database', type=str, help=db_name_help)

    delim_help = 'Delimiter to use for CSV file(s). Defaults to a ,.'
    id_subparser.add_argument('-d', '--delimiter', type=val_delim_arg,
                              help=delim_help, default=',')

    dm_help = 'Display SQLAlchemy metadata container keys.'
    id_subparser.add_argument('-m', '--display-metadata', help=dm_help,
                              action='store_true')

    host_help = 'The name of the host.'
    id_subparser.add_argument('-t', '--host', type=str, help=host_help)

    o_clt_lib_help = (
        'The directory that contains the Oracle client libraries.'
    )
    id_subparser.add_argument('-l', '--oracle-client-libs',
                              type=val_dir_arg, help=o_clt_lib_help)

    port_help = 'The port number.'
    id_subparser.add_argument('-p', '--port', type=int, help=port_help)

    qry_help = (
        'A dictionary of string keys to string values to be passed '
        'to the dialect and/or the DBAPI upon connect. Provide as a '
        'JSON object.'
    )
    id_subparser.add_argument('-q', '--query', type=json.loads, help=qry_help,
                              default={})

    return subparsers, id_subparser


def add_refresh_tbl_sp(common: argparse.ArgumentParser,
                       subparsers: argparse._SubParsersAction) \
        -> tuple[argparse._SubParsersAction, argparse.ArgumentParser]:
    '''
    Add refresh table subparser.

    Parameters
    ----------------
    common : argparse.ArgumentParser
        The common parser object.
    subparsers : argparse._SubParsersAction
        The subparsers object.

    Returns
    ----------------
    tuple
        The subparsers and refresh table subparser objects.
    '''
    rt_epilog = '''
Configuration File Key/Value Pairs
----------------------------------
dialect
The name of the database backend.

table
The database table to operate on. This name is used in the SELECT query, as is.
Also, the casing of this name should match what is reflected from the
database by SQLAlchemy. This can be verified by using the program's
-m (--display-metadata) option.

ob_cols
Columns SQL string for the source database table to include in the
ORDER BY clause.

credentials
The service name and username for the database credentials stored in the
system key store, separated by a comma (e.g., 'db_creds,guido').

database
The database name.

host
The name of the host.

port
The port number the database is being served on.

query
A dictionary of string keys to string values to be passed to the dialect
and/or the DBAPI upon connect.

Example Configuration Files
---------------------------

SQLite
~~~~~~
[databases]

[databases.source]
dialect = "sqlite"
table = "prototypes"
ob_cols = "rowid"
credentials = ""
database = "/home/tenma/atom.sqlite"
host = ""
port = 0  # If not used, default value should be 0
query = {}

[databases.destination]
dialect = "sqlite"
table = "prototypes"
credentials = ""
database = "/home/tenma/pluto.sqlite"
host = ""
port = 0  # If not used, default value should be 0
query = {}

PostgreSQL
~~~~~~~~~~
[databases]

[databases.source]
dialect = "postgresql"
table = "gesicht.closed_cases"
ob_cols = "ctid"
credentials = "system,gesicht"
database = "europol"
host = "localhost"
port = 5432  # If not used, default value should be 0
query = {}

[databases.destination]
dialect = "postgresql"
table = "ozhanomizu.closed_cases"
credentials = "system,ozhanomizu"
database = "ministry_of_science"
host = "localhost"
port = 5432  # If not used, default value should be 0
query = {}\
'''
    rt_subparser = subparsers.add_parser(
        'refresh-tbl',
        epilog=rt_epilog,
        formatter_class=argparse.RawTextHelpFormatter,
        parents=[common]
    )

    # Add batch size argument
    bat_sz_help = 'The number of rows to extract in each batch.'
    rt_subparser.add_argument('bat_sz', type=int, help=bat_sz_help)

    # Add configuration file argument
    cfg_help = 'Path to file that specifies required arguments in TOML format.'
    rt_subparser.add_argument('cfg_file', type=val_file_arg, help=cfg_help)

    # Add options
    bn_help = 'The batch number to start at.'
    rt_subparser.add_argument('-b', '--bat-num', type=int, help=bn_help,
                              default=0)

    cols_help = 'Columns SQL string to include in the SELECT query.'
    rt_subparser.add_argument('-n', '--columns', type=str, help=cols_help)

    dm_help = 'Display SQLAlchemy metadata container keys.'
    rt_subparser.add_argument('-m', '--display-metadata', help=dm_help,
                              action='store_true')

    fltr_help = 'SQL filter string to append to the SELECT query.'
    rt_subparser.add_argument('-f', '--filter', type=str, help=fltr_help)

    o_clt_lib_help = (
        'The directory that contains the Oracle client libraries.'
    )
    rt_subparser.add_argument('-l', '--oracle-client-libs',
                              type=val_dir_arg, help=o_clt_lib_help)

    return subparsers, rt_subparser


def add_run_qry_sp(common: argparse.ArgumentParser,
                   subparsers: argparse._SubParsersAction) \
        -> tuple[argparse._SubParsersAction, argparse.ArgumentParser]:
    '''
    Add run query subparser.

    Parameters
    ----------------
    common : argparse.ArgumentParser
        The common parser object.
    subparsers : argparse._SubParsersAction
        The subparsers object.

    Returns
    ----------------
    tuple
        The subparsers and run query subparser objects.
    '''
    rq_epilog = f'''\
Example Commands
----------------

SQLite
~~~~~~
{CNS.PKG_NAME} run-qry \\
    -s '/home/comcomly/chinook.sqlite' \\
    'sqlite' \\
    "WITH src AS ( SELECT DISTINCT Artist.Name AS artist_name, Album.Title AS album_name, Track.Name AS track_name FROM Artist INNER JOIN Album ON Artist.ArtistId = Album.ArtistId INNER JOIN Track ON Album.AlbumId = Track.AlbumId ORDER BY artist_name ASC, album_name ASC, track_name ASC ) SELECT COUNT(*) AS num_of_rows FROM src" \\
    "SELECT DISTINCT Artist.Name AS artist_name, Album.Title AS album_name, Track.Name AS track_name FROM Artist INNER JOIN Album ON Artist.ArtistId = Album.ArtistId INNER JOIN Track ON Album.AlbumId = Track.AlbumId ORDER BY artist_name ASC, album_name ASC, track_name ASC" \\
    '{{"artist_name": "string[pyarrow]", "album_name": "string[pyarrow]", "track_name": "string[pyarrow]"}}' \\
    'tracks' \\
    '/home/comcomly/data/'

PostgreSQL
~~~~~~~~~~
{CNS.PKG_NAME} run-qry \\
    -c 'system,comcomly' \\
    -s 'chinook' \\
    -t 'localhost' \\
    -p 5432 \\
    'postgresql' \\
    "SELECT COUNT(*) FROM InvoiceLine" \\
    "SELECT * FROM InvoiceLine" \\
    '{{"invoice_line_id": "string[pyarrow]", "invoice_id": "string[pyarrow]", "track_id": "string[pyarrow]", "unit_price": "float64", "quantity": "Int64"}}' \\
    'invoice_line' \\
    '/home/comcomly/data/'\
'''
    rq_subparser = subparsers.add_parser(
        'run-qry',
        epilog=rq_epilog,
        formatter_class=argparse.RawTextHelpFormatter,
        parents=[common]
    )

    # Add dialect argument
    dial_help = 'The name of the database backend.'
    rq_subparser.add_argument('dialect', type=val_dialect_arg, help=dial_help)

    # Add count query argument
    cnt_qry_help = (
        'The SQL query to run to determine the total '
        'number of rows in the result set.'
    )
    rq_subparser.add_argument('cnt_qry', type=str, help=cnt_qry_help)

    # Add SQL query argument
    sql_qry_help = (
        'The SQL query to run. Column names in the output are determined '
        'by the keys specified in the col_conv_map argument.'
    )
    rq_subparser.add_argument('sql_qry', type=str, help=sql_qry_help)

    # Add column name/pandas data type mapping argument
    ccm_help = (
        'A dictionary of string column name keys to string '
        'pandas data type values. Provide as a JSON object.'
    )
    rq_subparser.add_argument('col_conv_map', type=json.loads, help=ccm_help)

    # Add query name argument
    qry_name_help = (
        'A name for the SQL query.'
    )
    rq_subparser.add_argument('sql_qry_name', type=str, help=qry_name_help)

    # Add output directory argument
    o_dir_help = 'The directory to save the output to.'
    rq_subparser.add_argument('o_dir', type=val_dir_arg, help=o_dir_help)

    # Add options
    crd_help = (
        'The service name and username for the database credentials '
        'stored in the system key store, separated by a comma '
        "(e.g., 'db_creds,guido')."
    )
    rq_subparser.add_argument('-c', '--credentials', type=str, help=crd_help)

    csv_help = 'Save output to CSV files.'
    rq_subparser.add_argument('-o', '--csv-output', help=csv_help,
                              action='store_true')

    db_name_help = 'The database name.'
    rq_subparser.add_argument('-s', '--database', type=str, help=db_name_help)

    delim_help = 'Delimiter to use in output CSV files. Defaults to a ,.'
    rq_subparser.add_argument('-d', '--delimiter', type=val_delim_arg,
                              help=delim_help, default=',')

    host_help = 'The name of the host.'
    rq_subparser.add_argument('-t', '--host', type=str, help=host_help)

    o_clt_lib_help = (
        'The directory that contains the Oracle client libraries.'
    )
    rq_subparser.add_argument('-l', '--oracle-client-libs', type=val_dir_arg,
                              help=o_clt_lib_help)

    port_help = 'The port number the database is being served on.'
    rq_subparser.add_argument('-p', '--port', type=int, help=port_help)

    qry_help = (
        'A dictionary of string keys to string values to be passed '
        'to the dialect and/or the DBAPI upon connect. Provide as a '
        'JSON object.'
    )
    rq_subparser.add_argument('-q', '--query', type=json.loads, help=qry_help,
                              default={})

    return subparsers, rq_subparser


def add_run_scr_sp(common: argparse.ArgumentParser,
                   subparsers: argparse._SubParsersAction) \
        -> tuple[argparse._SubParsersAction, argparse.ArgumentParser]:
    '''
    Add run script subparser.

    Parameters
    ----------------
    common : argparse.ArgumentParser
        The common parser object.
    subparsers : argparse._SubParsersAction
        The subparsers object.

    Returns
    ----------------
    tuple
        The subparsers and run script subparser objects.
    '''
    rs_epilog = f'''\
Example Commands
----------------

SQLite
~~~~~~
{CNS.PKG_NAME} run-scr \\
    -s '/home/goji/abullah.sqlite' \\
    -o '/home/goji/output/' \\
    'sqlite' \\
    '/home/goji/bora.sql'

PostgreSQL
~~~~~~~~~~
{CNS.PKG_NAME} run-scr \\
    -c 'system,gesicht' \\
    -s 'europol' \\
    -t 'localhost' \\
    -p 5432 \\
    'postgresql' \\
    'pluto.sql'\
'''
    rs_subparser = subparsers.add_parser(
        'run-scr',
        epilog=rs_epilog,
        formatter_class=argparse.RawTextHelpFormatter,
        parents=[common]
    )

    # Add dialect argument
    dial_help = 'The name of the database backend.'
    rs_subparser.add_argument('dialect', type=val_dialect_arg, help=dial_help)

    # Add SQL script argument
    sql_scr_help = 'The path to the SQL script to execute.'
    rs_subparser.add_argument('sql_scr', type=val_file_arg, help=sql_scr_help)

    # Add options
    cln_help = 'Clean SQL statements before execution.'
    rs_subparser.add_argument('-n', '--clean', help=cln_help,
                              action='store_true')

    crd_help = (
        'The service name and username for the database credentials '
        'stored in the system key store, separated by a comma '
        "(e.g., 'db_creds,guido')."
    )
    rs_subparser.add_argument('-c', '--credentials', type=str, help=crd_help)

    db_name_help = 'The database name.'
    rs_subparser.add_argument('-s', '--database', type=str, help=db_name_help)

    delim_help = 'Delimiter to use in output CSV file. Defaults to a ,.'
    rs_subparser.add_argument('-d', '--delimiter', type=val_delim_arg,
                              help=delim_help, default=',')

    ds_help = 'Display statements instead of executing them.'
    rs_subparser.add_argument('-a', '--display-statements', help=ds_help,
                              action='store_true')

    host_help = 'The name of the host.'
    rs_subparser.add_argument('-t', '--host', type=str, help=host_help)

    o_clt_lib_help = (
        'The directory that contains the Oracle client libraries.'
    )
    rs_subparser.add_argument('-l', '--oracle-client-libs', type=val_dir_arg,
                              help=o_clt_lib_help)

    o_dir_help = 'The directory to save output files to.'
    rs_subparser.add_argument('-o', '--output-dir', type=val_dir_arg,
                              help=o_dir_help)

    port_help = 'The port number the database is being served on.'
    rs_subparser.add_argument('-p', '--port', type=int, help=port_help)

    qry_help = (
        'A dictionary of string keys to string values to be passed '
        'to the dialect and/or the DBAPI upon connect. Provide as a '
        'JSON object.'
    )
    rs_subparser.add_argument('-q', '--query', type=json.loads, help=qry_help,
                              default={})

    return subparsers, rs_subparser


def main() -> None:
    '''Define starting point for execution of the program.'''
    common, parser, subparsers = crt_parsers()
    subparsers, et_subparser = add_extr_tbl_sp(common, subparsers)
    subparsers, id_subparser = add_ins_data_sp(common, subparsers)
    subparsers, rt_subparser = add_refresh_tbl_sp(common, subparsers)
    subparsers, rq_subparser = add_run_qry_sp(common, subparsers)
    subparsers, rs_subparser = add_run_scr_sp(common, subparsers)
    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    signal(SIGINT, hndl_sigint)

    log_path = (
        os.path.join(args.log_directory, f'{CNS.PKG_NAME}.log')
        if args.log_directory
        else
        CNS.LOG_PATH
    )
    try:
        init_logging(log_path)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {CNS.FB}{exc}{CNS.FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    match args.subcommand:
        case 'extr-tbl':
            extr_tbl.main(args)
        case 'ins-data':
            ins_data.main(args)
        case 'refresh-tbl':
            refresh_tbl.main(args)
        case 'run-qry':
            run_qry.main(args)
        case 'run-scr':
            run_scr.main(args)
        case _:
            pass

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()
