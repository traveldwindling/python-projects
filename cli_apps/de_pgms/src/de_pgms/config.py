'''
Configuration file for program settings.

Classes:

    * ConstantsNamespace() - Define constant values for program.
'''
import datetime as dt  # Enable classes for manipulating dates and times
# Enable a portable way of using operating system dependent functionality
import os
# Enable regular expression matching operations similar to those found in Perl
import re


class ConstantsNamespace():
    '''Define constant values for program.'''

    # Define formatting variables
    FB = '\033[1m'
    FR = '\033[0m'

    D_FORMAT = '%Y-%m-%d'  # Set date format
    # Create date stamp using specified date format
    D_STAMP = dt.datetime.now().strftime(D_FORMAT)

    HOME = os.path.expanduser('~')  # Obtain user home directory

    # Obtain package name
    PKG_NAME = __name__.split('.')[0].replace('_', '-')

    LOG_ROOT = os.path.join(HOME, 'logs')  # Set log root
    LOG_DIR = os.path.join(LOG_ROOT, PKG_NAME)  # Set log directory
    LOG_PATH = os.path.join(LOG_DIR, f'{PKG_NAME}.log')  # Set log path

    # Define supported dialects
    DIALECTS = [
        'mariadb',
        'mysql',
        'mssql',
        'oracle',
        'postgresql',
        'sqlite'
    ]
    # Create dialect/driver mapping
    DIALS_DRVS = {
        'mariadb': 'pymysql',
        'mysql': 'pymysql',
        'mssql': 'pyodbc',
        'oracle': 'oracledb',
        'postgresql': 'pg8000'
    }
    # Create package/driver mapping
    PKGS_DRVS = {
        'oracledb': 'import oracledb',
        'pg8000': 'import pg8000',
        'PyMySQL': 'import pymysql',
        'pyodbc': 'import pyodbc'
    }

    # Create delimiter regular expression object
    DELIM_PAT = re.compile(r'^[^a-zA-Z0-9]{1}$')
    ENC = 'utf-8'  # Set CSV file encoding

    # Define number of maximum attempts for exception producing batched actions
    MAX_ATTEMPTS = 3

    # Define regular expression for splitting SQL statements on a semicolon
    SEMI_PAT = r";(?=[^']*(?:(?:'[^']*){2})*$)"
    # Define SQL statement display section decoration character
    SEC_CHAR = '~'

    # Define no data result message
    NO_DATA_RSLT_MSG = 'No data in returned result'

    # Define SQL statement execution error messages
    STMT_EXEC_ERR_MSGS = (
        '\nThe following statements encountered execution errors:\n'
    )
    # Define write error messages
    WR_ERR_MSGS = (
        "\nThe following statements' results were not written to disk:\n"
    )
    # Define error ending message
    ERR_MSGS_END = (
        f"\nRefer to the log for additional information:\n{FB}'{LOG_PATH}'{FR}"
    )
