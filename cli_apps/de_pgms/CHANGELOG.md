# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.3] - 2025-02-05

### Fixes

- Ensure column conversion map is always available and utilized, regardless of output type, for `extr-tbl` subcommand.
- Ensure column conversion map is always utilized for `run-qry` subcommand.

## [0.2.2] - 2025-02-02

### Additions

- Add column conversion map option to `extr-tbl` subcommand to enable greater data type flexibility, when needed.

### Changes

- Expand column conversion map option description for `ins-data` subcommand.

## [0.2.1] - 2025-01-30

### Fixes

- Add column conversion map option to the `ins-data` subcommand. This map is required when using CSV input data and improves reliability of `INSERT` behavior.

## [0.2.0] - 2025-01-28

### Additions

- Add `ins-data` subcommand.
- Add database table check to `extr-tbl` subcommand.

### Changes

- Simplify `refresh-tbl` subcommand by removing CSV source and destination table creation options, and reducing redundancies with other subcommands.
- Improve destination data number of rows check for CSV output in `run-qry` subcommand.
- Update CLI interface to reflect subcommand changes.

## [0.1.3] - 2025-01-26

### Changes

- Update inline documentation.

### Fixes

- Ensure CSV encoding is used for CSV data source in `refresh_tbl` module.

## [0.1.2] - 2025-01-25

### Changes

- Add Parquet support to `imp_files_pd()` function.
- Remove unneeded `list_dir()` function.
- Remove unneeded imports from `extr_tbl` module.
- Refactor `extr_tbl` module output files row count implementation.

## [0.1.1] - 2025-01-22

### Changes

- Refactor `drv_ck()` function to return driver import command.
- Update inline documentation regarding third-party drivers. Clarify that a driver installation prompt is only done when necessary.

### Fixes

- Correct subprograms to execute driver import command in their namespace.