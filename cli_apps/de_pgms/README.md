# de-pgms

A CLI program to accomplish data engineering tasks.

Run the following commands for program documentation:

- `de-pgms -h`
- `de-pgms extr-tbl -h`
- `de-pgms ins-data -h`
- `de-pgms refresh-tbl -h`
- `de-pgms run-qry -h`
- `de-pgms run-scr -h`

## Optional Dependencies

`de-pgms` can be installed with all supported Python database drivers:

`(de-pgms) $ python3 -m pip install 'de_pgms/dist/de_pgms-X.X.X.tar.gz[drivers]'`

`(de-pgms) $ python3 -m pip install 'de_pgms/dist/de_pgms-X.X.X-py3-none-any.whl[drivers]'`

`pipx install 'de_pgms[drivers]'`