#!/usr/bin/env python3
'''\
Create a SQLite table from a CSV file or directory of CSV files.

A log is saved to 'ex_log_path'.

Functions:

    * val_file_dir_arg() - Validate a file or directory.
    * val_delim_arg() - Validate a file delimiter.
    * sl_crt_conn() - Connect to a SQLite database file.
    * sl_exec_qry() - Execute a SQLite SQL query.
    * list_dir() - Return lists of directory object names and absolute paths.
    * rd_csv() - Read a CSV file.
    * parse_args() - Parse program arguments.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * main() - Define starting point for execution of the program.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
# Enable classes to read and write tabular data in CSV format
import csv
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version as string
from platform import python_version
# Enable regular expression matching operations similar to those found in Perl
import re
from signal import signal, SIGINT  # Enable use of signal handlers
# Enable a SQL interface compliant with the DB-API 2.0 specification
import sqlite3
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type

#############
# Variables #
#############
# Define formatting variables
FB = '\033[1m'
FR = '\033[0m'

# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

# Create delimiter regular expression object
DELIM_PAT = re.compile(r'^[^a-zA-Z0-9]{1}$')
ENC = 'utf-8'  # Set CSV file encoding


#############
# Functions #
#############
# Validation Functions Start #
def val_file_dir_arg(obj_to_val: str) -> str:
    '''
    Validate a file or directory.

    Parameters
    ----------------
    obj_to_val : str
        Filesystem object to be validated.

    Returns
    ----------------
    str
        Validated file or directory.
    '''
    if os.path.isfile(obj_to_val) or os.path.isdir(obj_to_val):
        return obj_to_val
    else:
        err_msg = (
            f'{FB}{obj_to_val}{FR} is not a valid file or directory path.'
        )
        raise argparse.ArgumentTypeError(err_msg)


def val_delim_arg(delimiter: str) -> str:
    '''
    Validate a file delimiter.

    Parameters
    ----------------
    delimiter : str
        Delimiter to use. Must be a single symbolic character.

    Returns
    ----------------
    str
        Validated delimiter.
    '''
    if bool(DELIM_PAT.search(delimiter)):
        return delimiter
    else:
        err_msg = f'{FB}{delimiter}{FR} is not a single symbolic character.'
        raise argparse.ArgumentTypeError(err_msg)
# Validation Functions End #


def sl_crt_conn(f_path: str) -> sqlite3.Connection:
    '''
    Connect to a SQLite database file.

    Parameters
    ----------------
    f_path : str
        SQLite database file path.

    Returns
    ----------------
    sqlite3.Connection
        sqlite3 connection object.
    '''
    conn = sqlite3.connect(f_path)

    return conn


def sl_exec_qry(conn: sqlite3.Connection, query: str,
                param_subs: dict | None = None) -> list[tuple] | None:
    '''
    Execute a SQLite SQL query.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    query : str
        A SQL query.
    param_subs : dict | None (optional)
        Parameter substitution name/value pairs, where the parameter name
        is the dictionary key, and the value is the parameter value.
        Defaults to None.

    Returns
    ----------------
    list | None
        List of result rows or None.

    Examples
    ----------------
    # Enable a SQL interface compliant with the DB-API 2.0 specification
    import sqlite3

    # Create database connection
    conn = sqlite3.connect('/home/comcomly/chinook.sqlite')

    query = (
        'SELECT '
        '* '
        'FROM '
        'Album'
    )
    result = sl_exec_qry(conn, query)

    # Select rows by playlist ID or name criteria from Playlist table
    query = (
        'SELECT '
        'PlaylistId, '
        'Name '
        'FROM '
        'Playlist '
        "WHERE "
        "PlaylistId = :PlaylistId "
        "OR Name = :Name"
    )
    param_subs = {
        'PlaylistId': 12,
        'Name': 'Classical'
    }
    result = sl_exec_qry(conn, query, param_subs=param_subs)
    '''
    if param_subs is None:
        param_subs = {}

    cur = conn.cursor()

    with conn:
        result = cur.execute(query, param_subs).fetchall()

    cur.close()

    return result


def list_dir(i_dir: str) -> tuple[list[str], list[str]]:
    '''
    Return lists of directory object names and absolute paths.

    Parameters
    ----------------
    i_dir : str
        Path of directory to get listing from.

    Returns
    ----------------
    tuple
        Lists of directory object names and absolute paths.
    '''
    clg_dir = os.getcwd()  # Obtain current working directory

    os.chdir(i_dir)  # Change to provided directory

    obj_names = os.listdir(i_dir)  # Get object names
    # Get object absolute paths
    obj_abs_paths = [os.path.abspath(obj) for obj in obj_names]
    os.chdir(clg_dir)  # Change back to calling directory

    return obj_names, obj_abs_paths


def rd_csv(f_path: str, encoding: str = 'utf-8', delimiter: str = '|',
           headings: bool = True) -> tuple[tuple, list[tuple]] | list[tuple]:
    '''
    Read a CSV file.

    Parameters
    ----------------
    f_path : str
        Path of the CSV file to read.
    encoding : str (optional)
        Encoding of the CSV file to read. Defaults to utf-8.
    delimiter : str (optional)
        Delimiter to use in the CSV file to read. Defaults to a |.
    headings : bool (optional)
        Whether the CSV file to read contains a headings line
        or not. Defaults to True.

    Returns
    ----------------
    tuple | list
        Either a tuple of the headings tuple and rows list, or the rows list.
    '''
    if headings:
        with open(f_path, 'r', encoding=encoding, newline='') as f:
            f_csv = csv.reader(f, delimiter=delimiter)
            headings = tuple(next(f_csv))
            rows = [tuple(row) for row in f_csv]

        return headings, rows
    else:
        with open(f_path, 'r', encoding=encoding, newline='') as f:
            f_csv = csv.reader(f, delimiter=delimiter)
            rows = [tuple(row) for row in f_csv]

        return rows


def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Set epilogue to append to argument help
    epilog = ''''''

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        epilog=epilog,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Add SQLite database file path argument
    db_help = 'Path to the SQLite database file.'
    parser.add_argument('db_path', type=str, help=db_help)

    # Add table name argument
    tbl_help = 'Name of table to create.'
    parser.add_argument('tbl_name', type=str, help=tbl_help)

    # Add DDL string argument
    ddl_help = 'DDL to create the table.'
    parser.add_argument('crt_tbl_ddl', type=str, help=ddl_help)

    # Add CSV file/directory path argument
    csv_help = (
        'Path to CSV file or directory of CSV files to insert into '
        'created table. Assumes CSV files with a header row.'
    )
    parser.add_argument('csv_path', type=val_file_dir_arg, help=csv_help)

    # Add options
    delim_help = 'Delimiter to use in source CSV file(s). Defaults to a |.'
    parser.add_argument('-d', '--delimiter', type=val_delim_arg,
                        help=delim_help, default='|')

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def main(conn: sqlite3.Connection) -> None:
    '''
    Define starting point for execution of the program.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    '''
    signal(SIGINT, hndl_sigint)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    logging.debug(f'Creating "{args.tbl_name}" table')
    log_stmt = re.sub(r'(\n|\r|\r\n)', ' ', args.crt_tbl_ddl)
    logging.debug(f'Executing DDL statement: "{log_stmt}"')
    try:
        _ = sl_exec_qry(conn, args.crt_tbl_ddl)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            f'\nTable creation failed: {FB}{exc}{FR}\n'
        )
        print(err_msg)
        raise

    logging.debug('Reading CSV source data')
    # Assumes a headers row
    if os.path.isfile(args.csv_path):
        try:
            _, rows = rd_csv(args.csv_path, encoding=ENC,
                             delimiter=args.delimiter)
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = f'\nCSV read file operation failed: {FB}{exc}{FR}\n'
            print(err_msg)
            raise

        row_len = len(rows[0])
        row_bats = (bat for bat in [rows])
    else:
        _, obj_abs_paths = list_dir(args.csv_path)

        try:
            _, rows = rd_csv(obj_abs_paths[0], encoding=ENC,
                             delimiter=args.delimiter)
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = f'\nCSV read file operation failed: {FB}{exc}{FR}\n'
            print(err_msg)
            raise

        row_len = len(rows[0])

        try:
            row_bats = (
                rd_csv(obj_abs_path, encoding=ENC,
                       delimiter=args.delimiter)[1]
                for obj_abs_path in obj_abs_paths
            )
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = f'\nCSV read files operation failed: {FB}{exc}{FR}\n'
            print(err_msg)
            raise

    # Create placeholder for VALUES clause
    ph = ('?, ' * row_len).rstrip(', ')

    stmt = (
        'INSERT INTO '
        f'{args.tbl_name} '
        'VALUES '
        f'({ph})'
    )
    logging.debug(f'Preparing to run insert SQL: "{stmt}"')
    ins_ctr = 0  # Set insert counter
    for idx, bat in enumerate(row_bats):
        logging.debug(f'Inserting batch {idx}')
        cur = conn.cursor()
        try:
            with conn:
                cur.executemany(stmt, bat)
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                f'\nBatch {idx} insert failed: {FB}{exc}{FR}\n'
            )
            print(err_msg)
            raise
        else:
            ins_ctr += len(bat)
        finally:
            cur.close

    logging.info(f'Source data number of rows: "{ins_ctr:,}"')
    # Create number of rows query
    num_rows_qry = (
        'SELECT '
        'COUNT(*) AS num_of_rows '
        'FROM '
        + args.tbl_name
    )
    logging.debug('Determining destination data number of rows')
    logging.debug(f'Running query: "{num_rows_qry}"')
    try:
        result = sl_exec_qry(conn, num_rows_qry)
    except Exception as exc:
        logging.error(traceback.format_exc().rstrip())
        err_msg = (
            f'\nDestination data number of rows query failed: {FB}{exc}{FR}\n'
        )
        print(err_msg)
        raise
    else:
        logging.info(f'Destination data number of rows: "{result[0][0]:,}"')

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    _, args = parse_args()

    try:
        conn = sl_crt_conn(args.db_path)
        main(conn)  # Start program
    finally:
        conn.close()
