# Create SQLite Table

Create a SQLite table from a CSV file or directory of CSV files.

Run the following command for program documentation:

`crt_sqlite_tbl.py -h`