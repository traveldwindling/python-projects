# SQLAlchemy Initialization

Initialize an interactive SQLAlchemy session.

Run the following command for program documentation:

`sql_alc_init.py -h`