#!/usr/bin/env python3
'''\
Initialize an interactive SQLAlchemy session.

This program is meant to be used with the Python interpreter's -i option.

A log is saved to 'ex_log_path'.

Functions:

    * val_dialect_arg() - Validate a database dialect.
    * parse_args() - Parse program arguments.
    * init_logging() - Initialize logging.
    * get_creds() - Get username and password credentials.
    * crt_eng() -  Create a SQLAlchemy engine instance.
    * get_db_md() - Get database metadata.
    * dsp_db_tbls() - Display database table names.
    * dsp_tbl_cols() - Display a table's columns.
    * exec_txt_qry() - Execute a text-based SQL query.
    * exec_obj_qry() - Execute an object-based SQL query.
    * exp_tbl_csv() - Export a SQLAlchemy Table object's contents
                      to a CSV file.
    * exp_rslt_csv() - Export a SQLAlchemy result set to a CSV file.
    * exp_rslt_html() - Export a SQLAlchemy result set to an HTML file.
    * main() - Define starting point for execution of the program.

Requires
----------------
pandas
    Powerful data structures for data analysis, time series, and statistics
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
# Enable classes to read and write tabular data in CSV format
import csv
from getpass import getpass  # Enable portable password input
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version
from platform import python_version
import pydoc  # Enable automatic documentation generation from Python modules
# Enable access to some variables used/maintained by the interpreter and to
# functions that strongly interact with the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback

# Define formatting variables
FB = '\033[1m'
FR = '\033[0m'

# Create mapping of required package name to import statement
REQUIRED_PKGS = {
    'pandas': 'import pandas as pd',
    'SQLAlchemy': 'import sqlalchemy as sa'
}
missing_pkgs = []  # Create missing packages list
for pkg_name, imp_stmt in REQUIRED_PKGS.items():
    try:
        exec(imp_stmt)  # Try to import library
    except ImportError:
        missing_pkgs.append(pkg_name)  # Capture missing package
# Exit if packages are missing
if missing_pkgs:
    import_msg = (
        '\nInstall the following packages before proceeding:'
        f"\n{FB}{' '.join(missing_pkgs)}{FR}"
    )
    print(import_msg)
    sys.exit(1)  # Exit with error status

#############
# Variables #
#############
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

# Define supported dialects
DIALECTS = [
    'mariadb',
    'mysql',
    'mssql',
    'oracle',
    'postgresql',
    'sqlite'
]

# Define documentation links dictionary
DOC_LINKS = {
    'docs': 'https://docs.sqlalchemy.org/index.html',
    'tutorial': 'https://docs.sqlalchemy.org/tutorial/index.html',
    'core': 'https://docs.sqlalchemy.org/core/index.html',
    'orm': 'https://docs.sqlalchemy.org/orm/index.html',
    'engines': 'https://docs.sqlalchemy.org/core/engines.html',
    'connections': 'https://docs.sqlalchemy.org/core/connections.html',
    'reflection': 'https://docs.sqlalchemy.org/core/reflection.html',
    'operators': 'https://docs.sqlalchemy.org/core/operators.html',
    'types': 'https://docs.sqlalchemy.org/core/type_basics.html',
    'parameters': ('https://docs.sqlalchemy.org/tutorial/'
                   'dbapi_transactions.html#sending-parameters'),
    'glossary': 'https://docs.sqlalchemy.org/glossary.html'
}

# Create required drivers dictionary
REQUIRED_DRVS = {
    'oracledb': 'import oracledb',  # Oracle Database
    'pg8000': 'import pg8000',  # PostgreSQL
    'PyMySQL': 'import pymysql',  # MariaDB/MySQL
    'pyodbc': 'import pyodbc'  # MS SQL Server
}


#############
# Functions #
#############
def val_dialect_arg(dialect: str) -> str:
    '''
    Validate a database dialect.

    Parameters
    ----------------
    dialect : str
        Dialect to be validated.

    Returns
    ----------------
    str
        Validated dialect.
    '''
    if dialect in DIALECTS:
        return dialect
    else:
        err_msg = (
            f'\n\n{FB}{dialect}{FR} is not a supported dialect. '
            'Supported dialects are:\n'
            f'{DIALECTS}'
        )
        raise argparse.ArgumentTypeError(err_msg)


def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Set epilogue to append to argument help
    epilog = f'''
Examples
--------
SQLite
~~~~~~
python3 -i -- '{__file__}' 'sqlite'

try:
    engine = crt_eng(
        'sqlite',
        database='/home/comcomly/chinook.sqlite'
    )
except Exception:
    logging.error(traceback.format_exc().rstrip())

    err_msg = (
        'Engine connection operation failed with the '
        'following exception:\\n\\n'
        + traceback.format_exc()
    )
    pydoc.pager(err_msg)

md, db_md = get_db_md(engine)  # Get database metadata

PostgreSQL
~~~~~~~~~~
python3 -i -- '{__file__}' 'postgresql'

username, password = get_creds()  # Get database credentials

try:
    engine = crt_eng(
        'postgresql+pg8000',
        username=username,
        password=password,
        host='127.0.0.1',
        port=5432,
        database='chinook'
    )
except Exception:
    logging.error(traceback.format_exc().rstrip())

    err_msg = (
        'Engine connection operation failed with the '
        'following exception:\\n\\n'
        + traceback.format_exc()
    )
    pydoc.pager(err_msg)

md, db_md = get_db_md(engine, schema='public', views=True)\
'''

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        epilog=epilog,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Add arguments
    dia_help = '''\
Database dialect.

Supported dialects are:

- mariadb
- mysql
- mssql
- oracle
- postgresql
- sqlite\
'''
    parser.add_argument('dialect', type=val_dialect_arg, help=dia_help)

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def get_creds() -> tuple[str, str]:
    '''
    Get username and password credentials.

    Returns
    ----------------
    tuple
        Username and password.
    '''
    username = input('\nEnter your username: ').rstrip()
    password = getpass('Enter your password: ').rstrip()

    return username, password


def crt_eng(drivername: str, username: str | None = None,
            password: str | None = None, host: str | None = None,
            port: int | None = None, database: str | None = None,
            query: dict[str, str | tuple[str]] = {}) -> sa.engine.Engine:
    '''
    Create a SQLAlchemy engine instance.

    Parameters
    ----------------
    drivername : str
        Database backend and driver name.
    username : str (optional)
        Database username. Defaults to None.
    password : str (optional)
        Database user password. Defaults to None.
    host : str (optional)
        Hostname for host serving the database. Defaults to None.
    port : int (optional)
        Database port number. Defaults to None.
    database : str (optional)
        Database name. Defaults to None.
    query : dict (optional)
        A mapping representing the query string.
        Defaults to an empty dictionary.

    Returns
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    '''
    # Create URL object
    url = sa.URL.create(
        drivername=drivername,
        username=username,
        password=password,
        host=host,
        port=port,
        database=database,
        query=query
    )

    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Create connection context to verify connection
    with engine.connect():
        pass

    return engine


def get_db_md(engine: sa.engine.Engine, schema: str = None,
              views: bool = False) \
        -> tuple[sa.sql.schema.MetaData, dict[str, list[str]]]:
    '''
    Get database metadata.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    schema : str (optional)
        Database schema. Defaults to None.
    views : bool (optional)
        Whether to also reflect views or not. Defaults to False.

    Returns
    ----------------
    tuple
        A collection of Table objects and their associated schema constructs,
        and a dictionary of database metadata, where the keys are the
        table names and the values are a list of table column names.
    '''
    # Create MetaData container
    md = sa.MetaData(schema=schema)
    # Load all available table definitions from the database
    md.reflect(bind=engine, views=views)

    db_md = {}
    for tbl_name in sorted(md.tables.keys()):
        db_md[tbl_name] = sorted(md.tables[tbl_name].c.keys())

    return md, db_md


def dsp_db_tbls(md: sa.sql.schema.MetaData) -> None:
    '''
    Display database table names.

    Parameters
    ----------------
    md : sa.sql.schema.MetaData
        A collection of Table objects and their associated schema constructs.
    '''
    for table in md.sorted_tables:
        print(table)


def dsp_tbl_cols(tbl_obj: sa.sql.schema.Table) -> None:
    '''
    Display a table's columns.

    Parameters
    ----------------
    tbl_obj : sa.sql.schema.Table
        Instance of a Table object.
    '''
    for column in tbl_obj.c:
        print(repr(column))


def exec_txt_qry(engine: sa.engine.Engine, query: str,
                 data: list[dict] | None = None,
                 param_subs: dict | None = None) \
        -> sa.engine.cursor.CursorResult | None:
    '''
    Execute a text-based SQL query.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    query : str
        A text-based SQL query.
    data : list | None (optional)
        List of dictionaries where each dictionary represents a row of
        data to insert. The dictionary keys are the column names,
        and the values are the column values. Defaults to None.
    param_subs : dict | None (optional)
        Parameter substitution name/value pairs, where the parameter name
        is the dictionary key, and the value is the parameter value.
        Defaults to None.

    Returns
    ----------------
    sa.engine.cursor.CursorResult | None
        SQLAlchemy cursor result or None.

    Examples
    ----------------
    import sqlalchemy as sa  # Enable database abstraction library

    # Create URL object
    url = sa.URL.create('sqlite', database='/home/comcomly/chinook.sqlite')
    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Select random subset of rows from Track table
    query = (
        'SELECT '
        '* '
        'FROM '
        'Track '
        'ORDER BY RANDOM() '
        'LIMIT 10'
    )
    result = exec_txt_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    # Insert new genres into Genre table
    query = (
        'INSERT INTO '
        'Genre '
        '(GenreId, Name) '
        "VALUES "
        "(:GenreId, :Name)"
    )
    data = [
        {
            'GenreId': '26',
            'Name': 'Chiptune'
        },
        {
            'GenreId': '27',
            'Name': 'Trip Hop'
        }
    ]
    result = exec_txt_qry(engine, query, data=data)

    # Select rows by playlist ID or name criteria from Playlist table
    query = (
        'SELECT '
        'PlaylistId, '
        'Name '
        'FROM '
        'Playlist '
        "WHERE "
        "PlaylistId = :PlaylistId "
        "OR Name = :Name"
    )
    param_subs = {
        'PlaylistId': 12,
        'Name': 'Classical'
    }
    result = exec_txt_qry(engine, query, param_subs=param_subs)
    cols, rows = list(result.keys()), result.all()
    '''
    try:
        with engine.begin() as conn:
            if data:
                result = conn.execute(sa.text(query), data)
            else:
                result = conn.execute(sa.text(query), param_subs)
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'Text-based query operation failed '
            'with the following exception:\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)

        return None
    else:
        return result


def exec_obj_qry(engine: sa.engine.Engine,
                 query: sa.sql.dml.Insert | sa.sql.selectable.Select
                 | sa.sql.dml.Update | sa.sql.dml.Delete,
                 data: list[dict] | None = None) \
        -> sa.engine.cursor.CursorResult | None:
    '''
    Execute an object-based SQL query.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    query : sa.sql.dml.Insert | sa.sql.selectable.Select
            | sa.sql.dml.Update | sa.sql.dml.Delete
        An object-based SQL query.
    data : list | None (optional)
        List of dictionaries where each dictionary represents a row of
        data to insert. The dictionary keys are the column names,
        and the values are the column values. Defaults to None.

    Returns
    ----------------
    sa.engine.cursor.CursorResult | None
        SQLAlchemy cursor result or None.

    Examples
    ----------------
    import sqlalchemy as sa

    # Create URL object
    url = sa.URL.create(
        drivername='sqlite',
        database='/home/comcomly/chinook.sqlite'
    )
    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    md, _ = get_db_md(engine)  # Get database metadata

    album = md.tables['Album']  # Create table object for Album table
    # Select specific columns from Album table
    query = (
        sa.select(album.c.AlbumId, album.c.Title)
          .select_from(album)
    )
    result = exec_obj_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    # Select specific rows from Album table
    query = (
        sa.select('*')
          .select_from(album)
          .where(album.c.ArtistId == 8)
    )
    result = exec_obj_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    track = md.tables['Track']
    # Select random subset of rows from Track table
    query = (
        sa.select('*')
          .select_from(track)
          .order_by(sa.func.random())
          .limit(10)
    )
    result = exec_obj_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    genre = md.tables['Genre']
    # Insert new rows into Genre table
    query = sa.insert(genre)
    data = [
        {'GenreId': '29', 'Name': 'Jazz Rock'},
        {'GenreId': '30', 'Name': 'Swing'}
    ]
    result = exec_obj_qry(engine, query, data=data)
    '''
    try:
        with engine.begin() as conn:
            result = conn.execute(query, data)
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'Object-based query operation failed '
            'with the following exception:\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)

        return None
    else:
        return result


def exp_tbl_csv(f_path: str, engine: sa.engine.Engine,
                tbl: sa.sql.schema.Table,
                enc: str = 'utf-8', delim: str = '|') -> None:
    '''
    Export a SQLAlchemy Table object's contents to a CSV file.

    Parameters
    ----------------
    f_path : str
        Output file path to export the CSV file to (e.g., '/tmp/table.csv').
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    tbl : sa.sql.schema.Table
        SQLAlchemy Table object whose content to export.
    enc : str (optional)
        Encoding to use for CSV file. Defaults to utf-8.
    delim : str (optional)
        Delimiter to use in CSV file. Defaults to a |.
    '''
    if not f_path.endswith('.csv'):
        f_path = f'{f_path}.csv'

    try:
        with engine.begin() as conn:
            result = conn.execute(sa.select(tbl))
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'Select operation failed with the '
            'following exception:\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)

        return None  # End function execution

    try:
        with open(f_path, 'w', encoding=enc, newline='') as f:
            writer = csv.writer(f, delimiter=delim)
            writer.writerow(tbl.c.keys())
            writer.writerows(result.all())
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'Table export operation to CSV failed with the '
            'following exception:\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)


def exp_rslt_csv(f_path: str, cols: list[str], rslt_set: list[tuple],
                 enc: str = 'utf-8', delim: str = '|') -> None:
    '''
    Export a SQLAlchemy result set to a CSV file.

    Parameters
    ----------------
    f_path : str
        Output file path to export the CSV file to (e.g., '/tmp/results.csv').
    cols : list
        List of column names.
    rslt_set : list
        SQLAlchemy result set where each tuple in the list is a row of data.
    enc : str (optional)
        Encoding to use for CSV file. Defaults to utf-8.
    delim : str (optional)
        Delimiter to use in CSV file. Defaults to a |.
    '''
    try:
        with open(f_path, 'w', encoding=enc, newline='') as f:
            writer = csv.writer(f, delimiter=delim)
            writer.writerow(cols)
            writer.writerows(rslt_set)
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'Result set export operation to CSV failed with the '
            'following exception:\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)


def exp_rslt_html(f_path: str, cols: list[str], rslt_set: list[tuple],
                  enc: str = 'utf-8') -> None:
    '''
    Export a SQLAlchemy result set to an HTML file.

    Parameters
    ----------------
    f_path : str
        Output file path to export the HTML file to
        (e.g., '/tmp/results.html').
    cols : list
        List of column names.
    rslt_set : list
        SQLAlchemy result set where each tuple in the list is a row of data.
    enc : str (optional)
        Encoding to use for HTML file. Defaults to utf-8.
    '''
    df = pd.DataFrame(rslt_set, columns=cols)

    html_tbl = df.to_html(index=False, justify='justify-all')

    try:
        with open(f_path, 'w', encoding=enc, newline='') as f:
            f.write(html_tbl)
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'Result set export operation to HTML failed with the '
            'following exception:\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)


def main() -> None:
    '''Define starting point for execution of the program.'''
    _, args = parse_args()

    # Set conditional import commands/messages
    if args.dialect != 'sqlite':
        if args.dialect == 'mariadb' or args.dialect == 'mysql':
            cmd = REQUIRED_DRVS['PyMySQL']
            import_msg = (
                f'\nInstall {FB}PyMySQL{FR} before '
                'running the program again.'
            )
        elif args.dialect == 'mssql':
            cmd = REQUIRED_DRVS['pyodbc']
            import_msg = (
                f'\nInstall {FB}pyodbc{FR} before '
                'running the program again.'
            )
        elif args.dialect == 'oracle':
            cmd = REQUIRED_DRVS['oracledb']
            import_msg = (
                f'\nInstall {FB}oracledb{FR} before '
                'running the program again.'
            )
        elif args.dialect == 'postgresql':
            cmd = REQUIRED_DRVS['pg8000']
            import_msg = (
                f'\nInstall {FB}pg8000{FR} before '
                'running the program again.'
            )
        try:
            exec(cmd, globals())
        except ImportError:
            print(import_msg)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
