# SQLAlchemy CRUD

Perform CRUD operations on SQLAlchemy-supported databases.

## Examples

### Initialization

#### SQLite

```console

Initialization Start
1: postgresql | pg8000
2: mysql | pymysql
3: mariadb | pymysql
4: sqlite | sqlite3
5: oracle | cx_oracle
6: mssql | pyodbc

Select a database dialect/API pair: 4

Docs: https://docs.sqlalchemy.org/core/engines.html#database-urls
Enter post-:/// portion of database URL: /home/comcomly/chinook.sqlite
Initialization End

********************

SQLAlchemy CRUD
Database: /home/comcomly/chinook.sqlite
~~~~~~~~~~~~~~~
vn) View table names
vt) View a table
 j) Inner join two tables
ar) Add a row
dr) Delete a row
 u) Update a cell
 s) Set current table
 q) Quit

Enter menu selection: 
```

#### PostgreSQL

```console

Initialization Start
1: postgresql | pg8000
2: mysql | pymysql
3: mariadb | pymysql
4: sqlite | sqlite3
5: oracle | cx_oracle
6: mssql | pyodbc

Select a database dialect/API pair: 1

Docs: https://docs.sqlalchemy.org/core/engines.html#database-urls
Enter post-:// portion of database URL: tumulth:wetiko@localhost/chinook
Initialization End

********************

SQLAlchemy CRUD
Database: chinook
~~~~~~~~~~~~~~~
vn) View table names
vt) View a table
 j) Inner join two tables
ar) Add a row
dr) Delete a row
 u) Update a cell
 s) Set current table
 q) Quit

Enter menu selection: 
```

### Inner Join Two Tables

```console

********************

SQLAlchemy CRUD
Database: /home/comcomly/chinook.sqlite
~~~~~~~~~~~~~~~
vn) View table names
vt) View a table
 j) Inner join two tables
ar) Add a row
dr) Delete a row
 u) Update a cell
 s) Set current table
 q) Quit

Enter menu selection: j

Enter table name: artists

Enter SELECT clause SQL content: artists.Name, albums.Title

Enter second table name: albums

Enter ON clause SQL condition: artists.ArtistId = albums.ArtistId
```

### Add a Row

```console

********************

SQLAlchemy CRUD
Database: /home/comcomly/chinook.sqlite
Current table: artists
~~~~~~~~~~~~~~~
vn) View table names
vt) View a table
 j) Inner join two tables
ar) Add a row
dr) Delete a row
 u) Update a cell
 s) Set current table
 q) Quit

Enter menu selection: ar

Enter ArtistId value of type INTEGER: 276
Enter Name value of type NVARCHAR(120): Seatbelts
```

### Delete a Row

```console

********************

SQLAlchemy CRUD
Database: /home/comcomly/chinook.sqlite
Current table: tracks
~~~~~~~~~~~~~~~
vn) View table names
vt) View a table
 j) Inner join two tables
ar) Add a row
dr) Delete a row
 u) Update a cell
 s) Set current table
 q) Quit

Enter menu selection: dr

Docs: https://docs.sqlalchemy.org/tutorial/dbapi_transactions.html#sending-parameters

Enter name:value parameter pairs 
to use in WHERE clause (s to stop): AlbumId:347

Enter name:value parameter pairs 
to use in WHERE clause (s to stop): Bytes:3305164

Enter name:value parameter pairs 
to use in WHERE clause (s to stop): s

Enter WHERE clause SQL condition: AlbumId = :AlbumId AND Bytes = :Bytes
```

### Update a Cell

```console

********************

SQLAlchemy CRUD
Database: /home/comcomly/chinook.sqlite
Current table: genres
~~~~~~~~~~~~~~~
vn) View table names
vt) View a table
 j) Inner join two tables
ar) Add a row
dr) Delete a row
 u) Update a cell
 s) Set current table
 q) Quit

Enter menu selection: u

Docs: https://docs.sqlalchemy.org/tutorial/dbapi_transactions.html#sending-parameters

Enter name:value parameter pairs to 
use in SET and WHERE clauses (s to stop): Name:Fantasy

Enter name:value parameter pairs to 
use in SET and WHERE clauses (s to stop): GenreId:20

Enter name:value parameter pairs to 
use in SET and WHERE clauses (s to stop): s

Enter SET clause SQL content: Name = :Name

Enter WHERE clause SQL condition: GenreId = :GenreId
```