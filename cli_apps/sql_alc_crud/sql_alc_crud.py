#!/usr/bin/env python3
'''\
Perform CRUD operations on SQLAlchemy-supported databases.

A log is saved to 'ex_log_path'.

Functions:

    * val_dial_dbapi() - Validate a database dialect/API pair.
    * val_tbl_name() - Validate whether a name is a valid database table name.
    * val_tbl_col_name() - Validate whether a table name/column name pair
      (e.g., tbl_name.col_name) is valid.
    * val_o_path() - Validate an absolute path to save an output file to.
    * set_dial_dbapi() - Set a database dialect and API.
    * set_tbl_name() - Set a database table name.
    * set_params() - Set parameter pairs.
    * set_tbl_col_names() - Set table name/column pairs string.
    * set_o_path() - Set an output file path.
    * parse_args() - Parse program arguments.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * get_tbl_inst() - Create a database table instance.
    * view_tbl() - View a database table.
    * join_tbls() - Inner join two tables.
    * add_row() - Add a table row.
    * delete_row() - Delete a table row.
    * update_cell() - Update a table cell.
    * exp_tbl_csv() - Export a SQLAlchemy Table object's contents
                      to a CSV file.
    * menu() - Display main menu and process user choice.
    * main() - Define starting point for execution of the program.

Requires
----------------
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
# Enable classes to read and write tabular data in CSV format
import csv
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable a class that represents concrete paths of the system's path flavor
from pathlib import Path
# Enable returning Python version
from platform import python_version
import pydoc  # Enable automatic documentation generation from Python modules
from signal import SIGINT, signal  # Enable use of signal handlers
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type

# Define formatting variables
FB = '\033[1m'
FI = '\033[3m'
FR = '\033[0m'

try:
    # Python SQL Toolkit and Object Relational Mapper
    import sqlalchemy as sa
except ImportError:
    import_msg = (
        f'\nInstall {FB}SQLAlchemy{FR} before '
        'running the program again.'
    )
    print(import_msg)
    sys.exit(1)  # Exit script with error status

#############
# Variables #
#############
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

# Define dialect/dbapi pairs
DIALECTS_DBAPIS = {
    '1': 'postgresql | pg8000',
    '2': 'mysql | pymysql',
    '3': 'mariadb | pymysql',
    '4': 'sqlite | sqlite3',
    '5': 'oracle | oracledb',
    '6': 'mssql | pyodbc'
}

# Create required drivers dictionary
REQUIRED_DRVS = {
    'oracledb': 'import oracledb',
    'pg8000': 'import pg8000',
    'PyMySQL': 'import pymysql',
    'pyodbc': 'import pyodbc'
}

# Define documentation URL dictionary
params_url = (
    'https://docs.sqlalchemy.org/tutorial/'
    'dbapi_transactions.html#sending-parameters'
)
engines_url = (
    'https://docs.sqlalchemy.org/core/engines.html'
    '#database-urls'
)
DOC_URLS = {
    'engine': f'{engines_url}',
    'params': f'{params_url}'
}

# Define menu characters and title
SEP_CHAR, UND_CHAR = '*', '~'
MENU_TI = 'SQLAlchemy CRUD'

# Calculate menu length and generate decoration strings
MT_LEN = len(MENU_TI)
SEP_LEN = MT_LEN + 5
UND_STR, SEP_STR = (UND_CHAR * MT_LEN), (SEP_CHAR * SEP_LEN)

# Define menu string
MENU_PRE = f'''
{SEP_STR}

{MENU_TI}
'''

MENU_POST = f'''\
{UND_STR}
vn) View Table Names
vt) View a Table
 j) Inner Join Two Tables
ar) Add a Row
dr) Delete a Row
 u) Update a Cell
 e) Export a Table
 s) Set Current Table
 q) Quit\
'''


#############
# Functions #
#############
# Validation Functions Start #
def val_dial_dbapi(dialect_dbapi: str) -> bool:
    '''
    Validate a database dialect/API pair.

    Parameters
    ----------------
    dialect : str
        Database dialect to be validated.
    dbapi : str
        Database API to be validated for the provided dialect.

    Returns
    ----------------
    bool
        Whether provided dialect/database API pair is valid or not.
    '''
    return bool(dialect_dbapi in DIALECTS_DBAPIS)


def val_tbl_name(tbl_name: str, md: sa.sql.schema.MetaData) -> bool:
    '''
    Validate whether a name is a valid database table name.

    Parameters
    ----------------
    tbl_name : str
        Table name to be validated.
    md : sa.sql.schema.MetaData
        A collection of Table objects and their associated schema constructs.

    Returns
    ----------------
    bool
        Whether provided dialect/database API pair is valid or not.
    '''
    return bool(tbl_name in md.tbl_names)


def val_tbl_col_name(tbl_col_name: str, md: sa.sql.schema.MetaData) -> bool:
    '''
    Validate whether a table name/column name pair
    (e.g., tbl_name.col_name) is valid.

    Parameters
    ----------------
    tbl_col_name : str
        Table name/column pair name to be validated.
    md : sa.sql.schema.MetaData
        A collection of Table objects and their associated schema constructs.

    Returns
    ----------------
    bool
        Whether provided table name/column name pair is valid or not.
    '''
    try:
        tbl_name, col_name = tbl_col_name.split('.')
    except ValueError:
        logging.error(traceback.format_exc().rstrip())
        return False
    else:
        return bool(col_name in md.tbl_clm_names[tbl_name])


def val_o_path(o_path: str) -> bool:
    '''
    Validate an absolute path to save an output file to.

    Parameters
    ----------------
    o_path : str
        Path to save output file to.

    Returns
    ----------------
    bool
        Whether provided output path is valid or not.
    '''
    o_path = Path(o_path)

    if os.path.isabs(o_path):
        return os.path.isdir(os.path.dirname(o_path))
    else:
        return False
# Validation Functions End #


# Set Functions Start #
def set_dial_dbapi(prompt: str) -> str:
    '''
    Set a database dialect and API.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        Database dialect and API choice.
    '''
    while True:
        choice = input(prompt).strip()

        if choice == 'q':
            sys.exit(0)
        elif val_dial_dbapi(choice):
            return choice
        else:
            print('\nInvalid choice. Try again or enter q to quit.\n')


def set_tbl_name(prompt: str, md: sa.sql.schema.MetaData) -> str:
    '''
    Set a database table name.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.
    md : sa.sql.schema.MetaData
        A collection of Table objects and their associated schema constructs.

    Returns
    ----------------
    str
        Database table name.
    '''
    while True:
        tbl_name = input(prompt).strip()

        if tbl_name == 'q':
            sys.exit(0)
        elif val_tbl_name(tbl_name, md):
            return tbl_name
        else:
            print('\nInvalid table name. Try again or enter q to quit.\n')


def set_params(prompt: str) -> dict[str, str]:
    '''
    Set parameter pairs.

    Bound parameters are requested as name:value pairs.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    dict
        Name/value parameter pairs, where the keys are the names and the
        values are the values.
    '''
    parameters = {}
    while True:
        try:
            param_nv = input(prompt).strip()

            if param_nv != 's':
                param_name, param_value = param_nv.split(':', maxsplit=1)
            else:
                break
        except Exception:
            logging.error(traceback.format_exc().rstrip())

            err_msg = '\nInvalid parameter pair. Try again.'
            print(err_msg)
        else:
            parameters[param_name] = param_value

    return parameters


def set_tbl_col_names(prompt: str,
                      md: sa.sql.schema.MetaData) -> tuple[str, list[str]]:
    '''
    Set table name/column pairs string.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.
    md : sa.sql.schema.MetaData
        A collection of Table objects and their associated schema constructs.

    Returns
    ----------------
    tuple
        Table name/column pairs string (e.g., 'artists.Name, albums.Title'),
        and list of table column names
        (e.g., ['artists.Name', 'albums.Title']).
    '''
    while True:
        tbl_col_names = input(prompt).strip()
        tcn_list = [
            tbl_col_name.strip()
            for tbl_col_name in tbl_col_names.split(',')
        ]

        # Determine invalid pairs
        inv_pairs = [
            tbl_col_name
            for tbl_col_name in tcn_list
            if not val_tbl_col_name(tbl_col_name, md)
        ]

        if not inv_pairs:
            return tbl_col_names, tcn_list
        elif tbl_col_names == 'q':
            sys.exit(0)
        else:
            err_msg = (
                '\nThe following table name/column name pairs are invalid:'
                + f'\n{inv_pairs}\n'
            )
            print(err_msg)


def set_o_path(prompt: str) -> str:
    '''
    Set an output file path.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        An output file path.
    '''
    while True:
        o_path = input(prompt).strip()

        if o_path == 'q':
            sys.exit(0)
        elif val_o_path(o_path):
            return o_path
        else:
            print('\nInvalid file path. Try again or enter q to quit.\n')
# Set Functions End #


def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def get_tbl_inst(md: sa.sql.schema.MetaData,
                 tbl_name: str) -> sa.sql.schema.Table:
    '''
    Get a database table instance from a MetaData container.

    Parameters
    ----------------
    md : sa.sql.schema.MetaData
        A collection of Table objects and their associated schema constructs.
    tbl_name : str
        Database table name.

    Returns
    ----------------
    sa.sql.schema.Table
        Instance of a Table object.
    '''
    # Get Table instance for requested table
    tbl_obj = md.tables[tbl_name]
    # Add column names/types dictionary as instance attribute
    tbl_obj.col_nsts = {
        column.name: column.type
        for column in tbl_obj.c
    }

    return tbl_obj


def view_tbl(engine: sa.engine.Engine,
             tbl_obj: sa.sql.schema.Table) -> None:
    '''
    View a database table.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    tbl_obj : sa.sql.schema.Table
        Instance of a Table object.
    '''
    # Create connection context
    with engine.begin() as conn:
        query = sa.select(tbl_obj)  # Define query
        result = conn.execute(query)  # Obtain query results

        # Determine max length of column info to use in output string
        max_len = max([
            len(f'{col_name}: ')
            for col_name in tbl_obj.col_nsts.keys()
        ])
        output = ''  # Create output string

        for row in result:
            for col_i, col_name in enumerate(tbl_obj.col_nsts.keys()):
                output += (
                    f"\n{col_name + ': ':{max_len}}"
                    f'{str(row[col_i])}'
                )
            output += '\n'

        pydoc.pager(output.rstrip())  # Send final string to paginator


def join_tbls(engine: sa.engine.Engine, md: sa.sql.schema.MetaData,
              tbl_obj: sa.sql.schema.Table) -> sa.sql.schema.Table:
    '''
    Inner join two tables.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    md : sa.sql.schema.MetaData
        A collection of Table objects and their associated schema constructs.
    tbl_obj : sa.sql.schema.Table
        Instance of a Table object.

    Returns
    ----------------
    sa.sql.schema.Table
        Instance of a Table object.
    '''
    # Create connection context
    with engine.begin() as conn:
        print()
        prompt = 'Enter SELECT clause SQL content: '
        tbl_col_names, tcn_list = set_tbl_col_names(prompt, md)

        prompt = '\nEnter second table name: '
        sec_tbl_name = set_tbl_name(prompt, md)
        sec_tbl_obj = get_tbl_inst(md, sec_tbl_name)

        # Set ON condition
        prompt = '\nEnter ON clause SQL condition: '
        on_cnd = input(prompt).strip()

        query = (
            'SELECT '
            f'{tbl_col_names} '
            'FROM '
            f'{tbl_obj.name} '
            f'INNER JOIN {sec_tbl_obj.name} '
            f'ON {on_cnd}'
        )

        try:
            result = conn.execute(sa.text(query))
        except Exception:
            logging.error(traceback.format_exc().rstrip())

            err_msg = (
                'Join operation failed with the '
                'following exception:\n\n'
                + traceback.format_exc()
            )
            pydoc.pager(err_msg)
        else:
            max_len = max([
                len(f'{tbl_col_name}: ')
                for tbl_col_name in tcn_list
            ])
            output = ''

            for row in result:
                for tcn_i, tbl_col_name in enumerate(tcn_list):
                    output += (
                        f"\n{tbl_col_name + ': ':{max_len}}"
                        f'{str(row[tcn_i])}'
                    )
                output += '\n'

            pydoc.pager(output.rstrip())

    return tbl_obj


def add_row(engine: sa.engine.Engine,
            tbl_obj: sa.sql.schema.Table) -> sa.sql.schema.Table:
    '''
    Add a table row.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    tbl_obj : sa.sql.schema.Table
        Instance of a Table object.

    Returns
    ----------------
    sa.sql.schema.Table
        Instance of a Table object.
    '''
    with engine.begin() as conn:
        # Construct row to add
        while True:
            row = {}
            print()
            for col_name in tbl_obj.col_nsts.keys():
                prompt = (
                    f'Enter {FB}{col_name}{FR} value of type '
                    f'{tbl_obj.col_nsts[col_name]}: '
                )
                cell = input(prompt).strip()
                row[col_name] = cell
            try:
                conn.execute(sa.insert(tbl_obj), [row])
            except Exception:
                logging.error(traceback.format_exc().rstrip())

                err_msg = (
                    'Insert operation failed with the '
                    'following exception:\n\n'
                    + traceback.format_exc()
                )
                pydoc.pager(err_msg)

                break
            else:
                scs_msg = 'Insert operation successful.'
                pydoc.pager(scs_msg)

                # Allow for entry of another row
                prompt = '\nAdd another row? (y or n): '
                choice = input(prompt).lower().strip()

                # Validate choice
                while choice != 'y' and choice != 'n':
                    print('\nInvalid choice. Try again or enter q to quit.')
                    choice = input(prompt).lower().strip()
                    if choice == 'q':
                        sys.exit(0)

                if choice == 'n':
                    break

    return tbl_obj


def delete_row(engine: sa.engine.Engine,
               tbl_obj: sa.sql.schema.Table) -> sa.sql.schema.Table:
    '''
    Delete a table row.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    tbl_obj : sa.sql.schema.Table
        Instance of a Table object.

    Returns
    ----------------
    sa.sql.schema.Table
        Instance of a Table object.
    '''
    with engine.begin() as conn:
        print(f"\nDocs: {engine.docs['params']}")
        prompt = (
            f'\nEnter {FI}name:value{FR} parameter pair \n'
            'to use in WHERE clause (s to stop): '
        )
        parameters = set_params(prompt)  # Obtain query parameter pairs

        if parameters:
            query = (
                'DELETE FROM '
                f'{tbl_obj.name} '
                "WHERE "
            )

            # Set WHERE condition
            prompt = '\nEnter WHERE clause SQL condition: '
            where_cond = input(prompt).strip()

            query += where_cond  # Add WHERE condition to query

            try:
                conn.execute(sa.text(query), parameters)
            except Exception:
                logging.error(traceback.format_exc().rstrip())

                err_msg = (
                    'Delete operation failed with the '
                    'following exception:\n\n'
                    + traceback.format_exc()
                )
                pydoc.pager(err_msg)
            else:
                scs_msg = 'Delete operation successful.'
                pydoc.pager(scs_msg)
        else:
            no_params_msg = 'No parameters provided. No action has been taken.'
            pydoc.pager(no_params_msg)

    return tbl_obj


def update_cell(engine: sa.engine.Engine,
                tbl_obj: sa.sql.schema.Table) -> sa.sql.schema.Table:
    '''
    Update a table cell.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    tbl_obj : sa.sql.schema.Table
        Instance of a Table object.

    Returns
    ----------------
    sa.sql.schema.Table
        Instance of a Table object.
    '''
    with engine.begin() as conn:
        print(f"\nDocs: {engine.docs['params']}")
        prompt = (
            f'\nEnter {FI}name:value{FR} parameter pair to \n'
            'use in SET or WHERE clause (s to stop): '
        )
        parameters = set_params(prompt)

        if parameters:
            # Set SET content
            prompt = '\nEnter SET clause SQL content: '
            set_cnt = input(prompt).strip()

            # Set WHERE condition
            prompt = '\nEnter WHERE clause SQL condition: '
            where_cond = input(prompt).strip()

            query = (
                'UPDATE '
                f'{tbl_obj.name} '
                "SET "
                f"{set_cnt} "
                "WHERE "
                f"{where_cond}"
            )

            try:
                conn.execute(sa.text(query), parameters)
            except Exception:
                logging.error(traceback.format_exc().rstrip())

                err_msg = (
                    'Update operation failed with the '
                    'following exception:\n\n'
                    + traceback.format_exc()
                )
                pydoc.pager(err_msg)
            else:
                scs_msg = 'Update operation successful.'
                pydoc.pager(scs_msg)
        else:
            no_params_msg = 'No parameters provided. No action has been taken.'
            pydoc.pager(no_params_msg)

    return tbl_obj


def exp_tbl_csv(f_path: str, engine: sa.engine.Engine,
                tbl: sa.sql.schema.Table,
                enc: str = 'utf-8', delim: str = '|') -> None:
    '''
    Export a SQLAlchemy Table object's contents to a CSV file.

    Parameters
    ----------------
    f_path : str
        Output file path to export the CSV file to (e.g., '/tmp/table.csv').
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    tbl : sa.sql.schema.Table
        SQLAlchemy Table object whose content to export.
    enc : str (optional)
        Encoding to use for CSV file. Defaults to utf-8.
    delim : str (optional)
        Delimiter to use in CSV file. Defaults to a |.
    '''
    if not f_path.endswith('.csv'):
        f_path = f'{f_path}.csv'

    try:
        with engine.begin() as conn:
            result = conn.execute(sa.select(tbl))
    except Exception as exc:
        print(exc)
        return None

    try:
        with open(f_path, 'w', encoding=enc, newline='') as f:
            writer = csv.writer(f, delimiter=delim)
            writer.writerow(tbl.c.keys())
            writer.writerows(result.all())
    except Exception as exc:
        print(exc)


def menu(engine: sa.engine.Engine, md: sa.sql.schema.MetaData,
         tbl_obj: sa.sql.schema.Table | None = None) -> None:
    '''
    Display main menu and process user choice.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    md : sa.sql.schema.MetaData
        A collection of Table objects and their associated schema constructs.
    tbl_obj : sa.sql.schema.Table | None (optional)
        Instance of a Table object. Defaults to None.
    '''
    # Define database menu string
    menu_db = f'Database: {FI}{engine.url.database}{FR}\n'
    # Check if table is set
    if tbl_obj is not None:
        # Define current table string
        curr_tbl = f'Current table: {FI}{tbl_obj.name}{FR}\n'
        # Create menu string variable
        menu_str = f'{MENU_PRE}{menu_db}{curr_tbl}{MENU_POST}'
    else:
        menu_str = f'{MENU_PRE}{menu_db}{MENU_POST}'

    print(menu_str)  # Display menu options

    # Store answer
    menu_answer = input('\nEnter menu selection: ').lower().strip()

    # Evaluate user menu selection
    match menu_answer:
        case 'vn':
            # Create output string
            output = (
                'Database Table Names\n'
                '--------------------\n'
            )
            for tbl_name in md.tbl_names:
                output += f'{tbl_name}\n'
            pydoc.pager(output)  # Send output string to system pager

            menu(engine, md, tbl_obj)
        case 'vt':
            if tbl_obj is None:
                print()
                prompt = 'Enter table name: '
                tbl_name = set_tbl_name(prompt, md)
                tbl_obj = get_tbl_inst(md, tbl_name)

            view_tbl(engine, tbl_obj)

            menu(engine, md, tbl_obj)
        case 'j':
            if tbl_obj is None:
                print()
                prompt = 'Enter table name: '
                tbl_name = set_tbl_name(prompt, md)
                tbl_obj = get_tbl_inst(md, tbl_name)

            join_tbls(engine, md, tbl_obj)

            menu(engine, md, tbl_obj)
        case 'ar':
            if tbl_obj is None:
                print()
                prompt = 'Enter table name: '
                tbl_name = set_tbl_name(prompt, md)
                tbl_obj = get_tbl_inst(md, tbl_name)

            tbl_obj = add_row(engine, tbl_obj)

            menu(engine, md, tbl_obj)
        case 'dr':
            if tbl_obj is None:
                print()
                prompt = 'Enter table name: '
                tbl_name = set_tbl_name(prompt, md)
                tbl_obj = get_tbl_inst(md, tbl_name)

            tbl_obj = delete_row(engine, tbl_obj)

            menu(engine, md, tbl_obj)
        case 'u':
            if tbl_obj is None:
                print()
                prompt = 'Enter table name: '
                tbl_name = set_tbl_name(prompt, md)
                tbl_obj = get_tbl_inst(md, tbl_name)

            tbl_obj = update_cell(engine, tbl_obj)

            menu(engine, md, tbl_obj)
        case 'e':
            if tbl_obj is None:
                print()
                prompt = 'Enter table name: '
                tbl_name = set_tbl_name(prompt, md)
                tbl_obj = get_tbl_inst(md, tbl_name)

            print()
            prompt = 'Enter output absolute file path: '
            o_path = set_o_path(prompt)

            exp_tbl_csv(o_path, engine, tbl_obj)

            menu(engine, md, tbl_obj)
        case 's':
            print()
            prompt = 'Enter table name: '
            tbl_name = set_tbl_name(prompt, md)
            tbl_obj = get_tbl_inst(md, tbl_name)

            menu(engine, md, tbl_obj)
        case 'q':
            pass
        case _:
            print(f'\n{FB}Please enter a valid menu option.{FR}')

            menu(engine, md, tbl_obj)


def main() -> None:
    '''Define starting point for execution of the program.'''
    _, _ = parse_args()

    # Obtain initialization data
    print(f'\n{FB}Initialization Start{FR}')
    prompt = ''
    for opt, dialect_dbapi in DIALECTS_DBAPIS.items():
        prompt += f'{opt}: {dialect_dbapi}\n'
    prompt += '\nSelect a database dialect/API pair: '
    choice = set_dial_dbapi(prompt)
    # Create dialect and database API values from chosen pair
    dialect, dbapi = DIALECTS_DBAPIS[choice].split(' | ')

    # If not using sqlite dialect
    if choice != '4':
        # Set conditional import commands/messages
        if choice == '1':
            cmd = REQUIRED_DRVS['pg8000']
            import_msg = (
                f'\nInstall {FB}pg8000{FR} before '
                'running the program again.'
            )
        elif choice == '2' or choice == '3':
            cmd = REQUIRED_DRVS['PyMySQL']
            import_msg = (
                f'\nInstall {FB}PyMySQL{FR} before '
                'running the program again.'
            )
        elif choice == '5':
            cmd = REQUIRED_DRVS['oracledb']
            import_msg = (
                f'\nInstall {FB}oracledb{FR} before '
                'running the program again.'
            )
        elif choice == '6':
            cmd = REQUIRED_DRVS['pyodbc']
            import_msg = (
                f'\nInstall {FB}pyodbc{FR} before '
                'running the program again.'
            )
        try:
            exec(cmd, globals())
        except ImportError:
            print(import_msg)
            sys.exit(1)

    # Obtain database URL string
    if dialect == 'sqlite':
        prompt = (
            f"\nDocs: {DOC_URLS['engine']}"
            '\nEnter post-:/// portion of database URL: '
        )
    else:
        prompt = (
            f"\nDocs: {DOC_URLS['engine']}"
            '\nEnter post-:// portion of database URL: '
        )
    db_loc = input(prompt).strip()

    signal(SIGINT, hndl_sigint)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    try:
        # Create a new engine instance
        if dialect == 'sqlite':
            engine = sa.create_engine(f'sqlite:///{db_loc}',
                                      pool_pre_ping=True)
        else:
            engine = sa.create_engine(f'{dialect}+{dbapi}://{db_loc}',
                                      pool_pre_ping=True)
        # Create connection context to verify connection
        with engine.connect():
            pass
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'Database connection failed.\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)

        logging.debug('Exiting program with error code 1.')
        sys.exit(1)

    # Add documentation dictionary as engine attribute
    engine.docs = DOC_URLS

    # Create MetaData container
    md = sa.MetaData()
    # Load all available table definitions from the database
    md.reflect(bind=engine)

    # Obtain database table names
    md.tbl_names = [tbl_name for tbl_name in md.tables.keys()]

    # Create dictionary of table names and their columns' names
    md.tbl_clm_names = {}
    for tbl_name in md.tables.keys():
        tbl_obj = get_tbl_inst(md, tbl_name)
        md.tbl_clm_names[tbl_obj.name] = tuple(tbl_obj.col_nsts.keys())
    print(f'{FB}Initialization End{FR}')

    menu(engine, md)  # Display menu

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
