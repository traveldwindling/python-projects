# Collect Products

Maintain collection of products and their prices.

## Sample Output

```console

Starting headless browser...

***********************

Product Collection
~~~~~~~~~~~~~~~~~~
1) Add new product
2) View products
3) Update prices
4) Remove product
5) View price change log
6) Export tables to CSV
q) Quit

Enter menu selection: 
```

## Required Packages/Modules

### Debian

`# apt install sqlite3`

### Fedora

`# dnf install sqlite`

### Common

`geckodriver` is a proxy for using W3C WebDriver compatible clients to interact with Gecko-based browsers.

The latest release (e.g., `geckodriver-v0.33.0-linux64.tar.gz`) for geckodriver can
be obtained here:

<https://github.com/mozilla/geckodriver/releases>

Ensure that geckodriver is executable (`chmod a+x geckodriver`) and that the
directory it is saved to is located in your system's `PATH`.

## Variables to Set

- `ALLOW_URLS` (optional) - Site base URLs that are allowed to bypass `urllib` validation.
- `LCLE` - System locale.
- `O_DIR` - Output directory for content.