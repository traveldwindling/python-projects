#!/usr/bin/env python3
'''
Maintain collection of products and their prices.

The program maintains a collection of products and a log of their
price changes. The collection and log are stored as tables in a
SQLite database.

When entering a product, you must provide a name, product web page URL, and
unique CSS selector for an HTML element that contains the product's price.
The web page URL and unique CSS selector are validated upon entry via urllib
and selenium, respectively.

urllib validation does not work for all sites (i.e., a web page may load fine
in a web browser, but return an error code when accessed via urllib,
or simply hang). For these cases, you can add the site's base URL
(e.g., https://example.org) to ALLOW_URLS to bypass validation.

Selenium-based browser activity is performed via a headless instance of the
Firefox web browser. To view the browser as it performs its functions, enable
debug mode via the -d (--debug) option.

In order for Selenium to control Firefox, geckodriver must be available. The
latest release (e.g., geckodriver-v0.33.0-linux64.tar.gz) for geckodriver can
be obtained here:

https://github.com/mozilla/geckodriver/releases

Ensure that geckodriver is executable (chmod a+x geckodriver) and that the
directory it is saved to is located in your system's PATH.

A log is saved to 'ex_log_path'.

Usage: 'ex_script_path.py'

Classes:
    * Product - Model a product of interest.

Functions:

    * val_name() - Validate a name.
    * val_web_page() - Validate a web page.
    * val_url() - Validate a URL.
    * val_css_sel() - Validate a CSS selector.
    * set_unique_name() - Set a name.
    * set_url() - Set a URL.
    * set_css_sel - Set a CSS selector.
    * set_extant_name() - Set an extant name.
    * parse_args() - Parse program arguments.
    * run_init_cks() - Run initialization checks.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * create_drv() - Create a WebDriver object.
    * create_conn() - Connect to a SQLite database file.
    * exec_query() - Execute a SQL query.
    * get_tbl_names() - Get SQLite database table names.
    * init_db() - Initialize a SQLite database file.
    * write_prod() - Write a Product object's attributes to the database.
    * create_ish_msg() - Create a potential issue message.
    * test_attempts - Test if number of attempts reached the maximum limit.
    * add_prods() - Add products to the database.
    * get_tbl_md() - Get table metadata.
    * view_tbl() - View products in the system pager.
    * log_price_chg() - Log price changes.
    * view_price_chgs() - View product price changes.
    * update_prices() - Check for price changes for each product and
                        update their prices in the database.
    * view_prod_log() - View product log in the system pager.
    * delete_row() - Delete a row from a SQLite database.
    * exp_tbls_csv() - Export SQLite tables to CSV files.
    * menu() - Display main menu and process user choice.
    * main() - Define starting point for execution of the program.

Requires
----------------
geckodriver
    Proxy for using W3C WebDriver compatible clients to interact with
    Gecko-based browsers.
selenium
    a suite of tools for automating web browsers
sqlite3
    A command line interface for SQLite version 3

Variables to set:
----------------
ALLOW_URLS (optional)
    Site base URLs that are allowed to bypass urllib validation.
LCLE
    System locale.
O_DIR
    Output directory for content.

Options:
----------------
-d, --debug
    Enable debug mode.

    Turns off Firefox's headless mode, which is
    useful when you need to verify the browser's actions
    or update a troublesome product price CSS selector.
'''
###########
# Modules #
###########
# Enable classes to read and write tabular data in CSV format
import csv
# Enable a single object containing all the information
# from a date object and a time object
from datetime import datetime as dt
# Open access to the POSIX locale database and functionality
import locale
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version as string
from platform import python_version
import pydoc  # Enable automatic documentation generation from Python modules
# Enable string replacement via regular expressions
import re
from shlex import split  # Enable splitting a string using shell-like syntax
from shutil import which  # Enable returning the path to an executable
from signal import SIGINT, signal  # Enable use of signal handlers
# Enable a SQL interface compliant with the DB-API 2.0 specification
import sqlite3
# Enable spawning of new processes, connecting to their input/output/error
# pipes, and obtaining of their return codes
import subprocess as sp
# Enable access to some variables used/maintained by the interpreter and to
# functions that strongly interact with the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type
from urllib.request import urlopen  # Enable opening of URLs

# Define formatting variables
FB = '\033[1m'
FR = '\033[0m'

try:
    # Enable web browser automation
    from selenium.webdriver.common.by import By
    from selenium.common.exceptions \
        import NoSuchElementException, TimeoutException
    from selenium import webdriver
except ImportError:
    import_msg = (
        f'\nInstall {FB}selenium{FR} before '
        'running the program again.'
    )
    print(import_msg)
    sys.exit(1)

#############
# Variables #
#############
HOME = os.path.expanduser('~')  # Obtain user home directory
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

# Define output directory
O_DIR = ''

# Define products database file path
DB_PATH = os.path.join(O_DIR, 'products.sqlite')

# Define products table headings
PROD_HDGS = ('name', 'url', 'price_selector', 'price')
# Define log table headings
LOG_HDGS = ('date', 'name', 'price')

# Define regular expressions for product price filtering
PRICE_PATS = (r'\.\d{2}', r'[^\d]')

# Set date format and create date stamp
D_FORMAT = '%Y-%m-%d'
D_STAMP = dt.now().strftime(D_FORMAT)
# Determine header length for log viewing function
LOG_HDR_LEN = len(D_STAMP)

LCLE = ''  # Define locale
locale.setlocale(locale.LC_ALL, LCLE)  # Set locale

ENC = 'utf-8'  # Define text encoding
DELIM = '|'  # Define output files' delimiter

# Define base URLs that can bypass validation
ALLOW_URLS = []

# Define number of seconds to implicitly wait for an element to be found
IMP_WAIT_TIME = 15
# Define number of seconds to wait for a page load to complete
PAGE_LOAD_TO = 15
# Define number of maximum attempts for exception producing actions
MAX_ATTEMPTS = 5

TER_EMU = 'gnome-terminal'  # Define terminal emulator

# Define URL regular expression
URL_REGEX = r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.'
URL_REGEX += r'[a-zA-Z0-9()]{1,18}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)'
# Create URL regular expression object
URL_PAT = re.compile(URL_REGEX)

# Define queries dictionary
QUERIES = {
    'sel_tbls': (
        'SELECT '
        'name '
        'FROM '
        'sqlite_master '
        "WHERE "
        "type='table'"
    ),
    'crt_prods_tbl': (
        'CREATE TABLE products ('
        'name TEXT NOT NULL, '
        'url TEXT NOT NULL, '
        'price_selector TEXT NOT NULL, '
        'price REAL NOT NULL, '
        'PRIMARY KEY (name)'
        ')'
    ),
    'crt_logs_tbl': (
        'CREATE TABLE logs ('
        'date TEXT NOT NULL, '
        'name TEXT NOT NULL, '
        'price REAL NOT NULL'
        ')'
    ),
    'val_name': (
        'SELECT '
        'name '
        'FROM '
        'products '
        "WHERE "
        "name = :name"
    ),
    'write_prod': (
        'INSERT INTO '
        'products '
        "VALUES "
        "(:name, :url, :price_selector, :price)"
    ),
    'write_logs': (
        'INSERT INTO '
        'logs '
        "VALUES "
        "(:date, :name, :price)"
    ),
    'sel_prods': (
        'SELECT '
        'name, url, price_selector, price '
        'FROM '
        'products'
    ),
    'sel_logs': (
        'SELECT '
        'date, name, price '
        'FROM '
        'logs'
    ),
    'up_url': (
        'UPDATE '
        'products '
        "SET "
        "url = :url "
        "WHERE "
        "name = :name"
    ),
    'up_sel': (
        'UPDATE '
        'products '
        "SET "
        "price_selector = :price_selector "
        "WHERE "
        "name = :name"
    ),
    'up_price': (
        'UPDATE '
        'products '
        "SET "
        "price = :price "
        "WHERE "
        "name = :name"
    ),
    'remove_prod': (
        'DELETE FROM '
        'products '
        "WHERE "
        "name = :name"
    )
}

# Define operation descriptions that should not produce a user output message
NO_MSG = (
    'Column name extraction',
    'Select product table operation',
    'Select logs table operation',
    'Select tables operation',
    'Select table operation',
    'product table update',
    'logs table update'
)

# Define menu characters and title
SEP_CHAR, UND_CHAR = '*', '~'
MENU_TI = 'Product Collection'

# Calculate menu length and generate decoration strings
MT_LEN = len(MENU_TI)
SEP_LEN = MT_LEN + 5
UND_STR, SEP_STR = (UND_CHAR * MT_LEN), (SEP_CHAR * SEP_LEN)

# Create menu string variable
MENU_STR = f'''
{SEP_STR}

{MENU_TI}
{UND_STR}
1) Add new product
2) View products
3) Update prices
4) Remove product
5) View price change log
6) Export tables to CSV
7) Start sqlite3 session
q) Quit\
'''

# Create required variables dictionary
REQ_VARS = {
    'LCLE': LCLE,
    'O_DIR': O_DIR
}

# Define required commands
REQ_CMDS = (
    'geckodriver',
    'sqlite3'
)


###########
# Classes #
###########
class Product:
    '''
    Model a product of interest.

    Methods:

        * __init__() - Initialize the class with stateful data.
        * __repr__() - Return a string containing a printable representation
                       of an  object.
        * set_price() - Set a product's price.
    '''

    def __init__(self, name: str, url: str, price_sel: str) -> None:
        '''
        Initialize the class with stateful data.

        Parameters
        ----------------
        name : str
            Product name.
        url : str
            Product page URL.
        price_sel: str
            Unique product price CSS selector.
        '''
        self.name = name
        self.url = url
        self.price_sel = price_sel
        self.price = None

    def __repr__(self) -> str:
        '''
        Return a string containing a printable representation of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'name={self.name!r}, '
            f'url={self.url!r}, '
            f'price_sel={self.price_sel!r}, '
            f'price={self.price!r})'
        )

    def set_price(self,
                  browser: webdriver.firefox.webdriver.WebDriver) -> None:
        '''
        Set a Product object's price.

        Parameters
        ----------------
        browser : webdriver.firefox.webdriver.WebDriver
            Selenium WebDriver object.
        '''
        err_msg = create_ish_msg(self.url)

        attempts = 0
        while attempts < MAX_ATTEMPTS:
            try:
                browser.get(self.url)  # Navigate to product page
            except TimeoutException:
                logging.error(traceback.format_exc().rstrip())
                print(err_msg)

                attempts += 1
                test_attempts(attempts, MAX_ATTEMPTS)
            else:
                break

        attempts = 0
        while attempts < MAX_ATTEMPTS:
            try:
                # Obtain price element
                price_elem = browser.find_element(By.CSS_SELECTOR,
                                                  self.price_sel)
            except NoSuchElementException:
                logging.error(traceback.format_exc().rstrip())
                print(err_msg)

                browser.get(self.url)

                attempts += 1
                test_attempts(attempts, MAX_ATTEMPTS)
            else:
                break

        # Remove any decimal values from product price
        prod_price_fltr = re.sub(PRICE_PATS[0], '', price_elem.text)
        # Remove any non-digits
        prod_price_fltr = re.sub(PRICE_PATS[1], '', prod_price_fltr)

        self.price = prod_price_fltr  # Set product price as attribute


#############
# Functions #
#############
# Validation Functions Start #
def val_name(name: str, conn: sqlite3.Connection) -> bool:
    '''
    Validate a name.

    Parameters
    ----------------
    name : str
        Name to be validated.
    conn : sqlite3.Connection
        sqlite3 connection object.

    Returns
    ----------------
    bool
        Whether the name is valid or not.
    '''
    cur = conn.cursor()

    try:
        with conn:
            result = cur.execute(QUERIES['val_name'],
                                 {'name': name}).fetchall()
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'Name validation operation failed with the '
            'following exception:\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)

    cur.close()

    if result:
        return False
    else:
        return True


def val_web_page(web_page: str) -> bool:
    '''
    Validate a web page.

    Parameters
    ----------------
    web_page : str
        Web page to be validated.

    Returns
    ----------------
    bool
        Whether the web page is valid or not.
    '''
    allow_url = any(base_url in web_page for base_url in ALLOW_URLS)
    if allow_url:
        return True
    else:
        try:
            urlopen(web_page)
        except Exception:
            logging.error(traceback.format_exc().rstrip())
            return False
        else:
            return True


def val_url(url_pat: re.Pattern, url: str) -> bool:
    '''
    Validate a URL.

    Parameters
    ----------------
    url_pat : re.Pattern
        URL regular expression object to validate against.
    url : str
        URL to be validated.

    Returns
    ----------------
    bool
        Whether URL is a valid data source.
    '''
    return bool(url_pat.search(url))


def val_css_sel(css_sel: str,
                browser: webdriver.firefox.webdriver.WebDriver) -> bool:
    '''
    Validate a CSS selector.

    Also, confirms that the targeted element contains content.

    Parameters
    ----------------
    css_sel : str
        CSS selector to be validated.
    browser : webdriver.firefox.webdriver.WebDriver
        Selenium WebDriver object.

    Returns
    ----------------
    bool
        Whether the CSS selector is valid or not.
    '''
    try:
        elem = browser.find_element(By.CSS_SELECTOR, css_sel)
    except Exception:
        logging.error(traceback.format_exc().rstrip())
        return False
    else:
        if elem.text:
            return True
        else:
            return False
# Validation Functions End #


# Set Functions Start #
def set_unique_name(prompt: str, conn: sqlite3.Connection) -> str:
    '''
    Set a unique name.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.
    conn : sqlite3.Connection
        sqlite3 connection object.

    Returns
    ----------------
    str
        A unique name.
    '''
    while True:
        name = input(prompt).lower().strip()

        if name == 'q':
            sys.exit(0)
        elif val_name(name, conn):
            return name
        else:
            print('\nInvalid name. Try again or enter q to quit.\n')


def set_url(prompt: str) -> str:
    '''
    Set a URL.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        A URL.
    '''
    while True:
        url = input(prompt).strip()

        if url == 'q':
            sys.exit(0)
        elif val_web_page(url) and val_url(URL_PAT, url):
            return url
        else:
            print('\nInvalid URL. Try again or enter q to quit.\n')


def set_css_sel(prompt: str,
                browser: webdriver.firefox.webdriver.WebDriver) -> str:
    '''
    Set a CSS selector.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.
    browser : webdriver.firefox.webdriver.WebDriver
        Selenium WebDriver object.

    Returns
    ----------------
    str
        A CSS selector.
    '''
    while True:
        css_sel = input(prompt).strip()

        if css_sel == 'q':
            sys.exit(0)
        elif val_css_sel(css_sel, browser):
            return css_sel
        else:
            print('\nInvalid CSS selector. Try again or enter q to quit.\n')


def set_extant_name(prompt: str, conn: sqlite3.Connection) -> str:
    '''
    Set an extant name.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.
    conn : sqlite3.Connection
        sqlite3 connection object.

    Returns
    ----------------
    str
        An extant name.
    '''
    while True:
        name = input(prompt).lower().strip()

        if name == 'q':
            sys.exit(0)
        elif not val_name(name, conn):
            return name
        else:
            print('\nInvalid name. Try again or enter q to quit.\n')
# Set Functions End #


def parse_args() -> tuple[str, bool] | None:
    '''Parse program arguments.'''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_script_path.py', os.path.realpath(__file__))
                      .replace('ex_log_path', LOG_PATH))
    # Check for unsupported arguments
    if len(sys.argv) > 2:
        print(fltr_ds)
        sys.exit(1)
    # Evaluate arguments
    elif len(sys.argv) == 2:
        if sys.argv[1] in ('-h', '--help'):
            print(fltr_ds)
            sys.exit(0)
        elif sys.argv[1] in ('-d', '--debug'):
            start_msg = '\nStarting browser...'
            debug = True
            return start_msg, debug
        else:
            print(fltr_ds)
            sys.exit(1)


def run_init_cks() -> None:
    '''Run initialization checks.'''
    # Verify terminal emulator is installed and command is being run
    # from a desktop environment
    cmd = (
        f'bash -c "command -v {TER_EMU}" && '
        'test "${XDG_CURRENT_DESKTOP}"'
    )
    result = sp.run(cmd, shell=True, capture_output=True)
    if result.returncode != 0:
        err_msg = (
            '\ngnome-terminal and a desktop environment must '
            'be available to use this program.'
        )
        print(err_msg)
        sys.exit(1)

    mis_cmds = []
    # Collect required commands that are missing
    for cmd in REQ_CMDS:
        if not which(cmd):
            mis_cmds.append(cmd)
    # Display missing commands message and exit
    if mis_cmds:
        err_msg = (
            '\nInstall the following command(s) before proceeding:'
            f'\n{FB}{" ".join(mis_cmds)}{FR}'
        )
        print(err_msg)
        sys.exit(1)

    unset_vars = []  # Create unset variables list
    # Check for unset variables
    for req_var_name, req_var_obj in REQ_VARS.items():
        seq_cnd = (
            isinstance(req_var_obj, str)
            or isinstance(req_var_obj, list)
            or isinstance(req_var_obj, tuple)
        )
        if seq_cnd:
            if not req_var_obj:
                unset_vars.append(req_var_name)
        elif isinstance(req_var_obj, dict):
            if not all(req_var_obj.values()):
                unset_vars.append(req_var_name)
    # Display missing variables message and exit
    if unset_vars:
        err_msg = (
            '\nSet the following variable(s) before '
            'running the program again:\n'
            f"{FB}{' '.join(unset_vars)}{FR}"
        )
        print(err_msg)
        sys.exit(1)


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def create_drv(debug: bool = False) -> webdriver.firefox.webdriver.WebDriver:
    '''
    Create a WebDriver object.

    Parameters
    ----------------
    debug : bool (optional)
        Whether to enable debug options or not. Defaults to False.

    Returns
    ----------------
    webdriver.firefox.webdriver.WebDriver
        Selenium WebDriver object.
    '''
    try:
        opts = webdriver.FirefoxOptions()  # Create Options object
        if not debug:
            opts.add_argument('-headless')  # Enable headless mode

        # Create WebDriver object with Options object
        browser = webdriver.Firefox(options=opts)
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            'WebDriver object creation failed with the '
            'following exception:\n\n'
            + traceback.format_exc()
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1.')
        sys.exit(1)
    else:
        browser.implicitly_wait(IMP_WAIT_TIME)
        browser.set_page_load_timeout(PAGE_LOAD_TO)

        return browser


def create_conn(db_path: str) -> sqlite3.Connection:
    '''
    Connect to a SQLite database file.

    Parameters
    ----------------
    db_path : str
        SQLite database file path.

    Returns
    ----------------
    sqlite3.Connection
        sqlite3 connection object.
    '''
    try:
        conn = sqlite3.connect(db_path)
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            '\nConnection operation failed with the '
            'following exception:\n\n'
        )
        print(err_msg)

        raise
    else:
        return conn


def exec_query(conn: sqlite3.Connection, query: str, op_desc: str,
               param_subs: dict | None = None) -> list[tuple] | None:
    '''
    Execute a SQL query.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    query : str
        A SQL query.
    op_desc : str
        Operation description to use in output messages.
    param_subs : dict | None (optional)
        Parameter substitution name/value pairs, where the parameter name
        is the dictionary key, and the value is the parameter value.
        Defaults to None.

    Returns
    ----------------
    list | None
        List of result rows or None.
    '''
    if param_subs is None:
        param_subs = {}

    cur = conn.cursor()
    result = None

    try:
        with conn:
            result = cur.execute(query, param_subs).fetchall()
    except Exception:
        logging.error(traceback.format_exc().rstrip())

        err_msg = (
            f'{op_desc} failed with the following exception:\n\n'
            + traceback.format_exc()
        )
        pydoc.pager(err_msg)
    else:
        if op_desc not in NO_MSG:
            scs_msg = f'{op_desc} successful.'
            pydoc.pager(scs_msg)

    cur.close()

    return result


def get_tbl_names(conn: sqlite3.Connection) -> list[str]:
    '''
    Get SQLite database table names.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.

    Returns
    ----------------
    list
        List of database table names.
    '''
    op_desc = 'Select tables operation'
    tbl_tuples = exec_query(conn, QUERIES['sel_tbls'], op_desc)

    tbl_names = [
        tbl_name
        for tbl_tuple in tbl_tuples
        for tbl_name in tbl_tuple
    ]

    return tbl_names


def init_db(conn: sqlite3.Connection, tbl_names: list[str]) -> None:
    '''
    Initialize a SQLite database file.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    tbl_names : list
        List of database table names.
    '''
    if 'products' not in tbl_names:
        op_desc = 'products table creation'
        exec_query(conn, QUERIES['crt_prods_tbl'], op_desc)

    if 'logs' not in tbl_names:
        op_desc = 'logs table creation'
        exec_query(conn, QUERIES['crt_logs_tbl'], op_desc)


def write_prod(conn: sqlite3.Connection, prod: Product) -> None:
    '''
    Write a Product object's attributes to the database.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    prod : Product
        A Product object.
    '''
    op_desc = 'product table addition'
    param_values = {
        'name': prod.name,
        'url': prod.url,
        'price_selector': prod.price_sel,
        'price': prod.price
    }
    exec_query(conn, QUERIES['write_prod'], op_desc, param_values)

    log_price_chg(conn, prod, 'logs table addition')


def create_ish_msg(page_url: str) -> str:
    '''
    Create a potential issue message.

    Parameters
    ----------------
    page_url : str
        Page URL.

    Returns
    ----------------
    str
        Potential issue message.
    '''
    ish_msg = (
        '\nPotential issue encountered.\n'
        f'Reloading {FB}{page_url}{FR}.'
    )
    return ish_msg


def test_attempts(attempts: int, max_attempts: int) -> None:
    '''
    Test if number of attempts reached the maximum limit.

    Parameters
    ----------------
    attempts : int
        Number of attempts.
    max_attempts : int
        Maximum number of attempts limit.
    '''
    log_path = logging.getLoggerClass().root.handlers[0].baseFilename

    if attempts == max_attempts:
        err_msg = (
            '\nMaximum attempts limit reached.\n'
            f'View {FB}{log_path}{FR} for more information.'
        )
        print(err_msg)

        logging.debug('Exiting program with error code 1.')
        sys.exit(1)


def add_prods(conn: sqlite3.Connection,
              browser: webdriver.firefox.webdriver.WebDriver) -> None:
    '''
    Add products to the database.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    browser : webdriver.firefox.webdriver.WebDriver
        Selenium WebDriver object.
    '''
    print()
    while True:
        # Capture product data
        prompt = 'Enter product name: '
        prod_name = set_unique_name(prompt, conn)

        prompt = 'Enter product page URL: '
        prod_url = set_url(prompt)
        err_msg = create_ish_msg(prod_url)
        attempts = 0
        while attempts < MAX_ATTEMPTS:
            try:
                browser.get(prod_url)
            except TimeoutException:
                logging.error(traceback.format_exc().rstrip())
                print(err_msg)

                attempts += 1
                test_attempts(attempts, MAX_ATTEMPTS)
            else:
                break

        prompt = 'Enter product price CSS selector: '
        prod_sel = set_css_sel(prompt, browser)

        # Create product object
        prod = Product(prod_name, prod_url, prod_sel)

        # Obtain product price and add as attribute
        prod.set_price(browser)

        # Check that product price is numerical
        try:
            int(prod.price)
        except Exception:
            logging.error(traceback.format_exc().rstrip())

            price_msg = (
                'Product price not properly extracted.'
                '\nTry adding the product again.'
            )
            pydoc.pager(price_msg)
        else:
            write_prod(conn, prod)  # Write product to database

        # Allow for entry of another product
        prompt = '\nAdd another product? (y or n): '
        choice = input(prompt).lower().strip()

        # Validate choice
        while choice != 'y' and choice != 'n':
            print('\nInvalid choice. Try again or enter q to quit.')
            choice = input(prompt).lower().strip()
            if choice == 'q':
                sys.exit(0)

        if choice == 'y':
            print()
        elif choice == 'n':
            break


def get_tbl_md(conn: sqlite3.Connection,
               tbl_name: str) -> dict[str, list[str]]:
    '''
    Get table metadata.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    tbl_name : str
        Table name.

    Returns
    ----------------
    dict
        Dictionary of table metadata, where the key is the table name
        and the value is a list of column names.
    '''
    query = (
        'PRAGMA '
        f'table_info({tbl_name})'
    )
    op_desc = 'Column name extraction'
    result = exec_query(conn, query, op_desc)  # Obtain table column names

    tbl_md = {tbl_name: []}
    for row in result:
        tbl_md[tbl_name].append(row[1])

    return tbl_md


def view_tbl(conn: sqlite3.Connection, tbl_md: dict[str, list[str]],
             query: str, op_desc: str) -> None:
    '''
    View products in the system pager.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    tbl_md : dict
        Dictionary of table metadata, where the key is the table name
        and the value is a list of column names.
    query : str
        A SQL query.
    op_desc : str
        Operation description to use in output messages.
    '''
    tbl_name = [key for key in tbl_md.keys()][0]
    result = exec_query(conn, query, op_desc)

    # Determine max length of column info to use in output string
    max_len = max([
        len(f'{col_name}: ')
        for col_name in tbl_md[tbl_name]
    ])
    output = ''  # Create output string

    for row in result:
        for col_i, col_name in enumerate(tbl_md[tbl_name]):
            output += (
                f"\n{col_name + ': ':{max_len}}"
                f'{str(row[col_i])}'
            )
        output += '\n'

    pydoc.pager(output.rstrip())  # Send final string to paginator


def log_price_chg(conn: sqlite3.Connection, prod: Product,
                  op_desc: str) -> None:
    '''
    Log price changes.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    prod : Product
        A Product object.
    op_desc : str
        Operation description to use in output messages.
    '''
    param_values = {
        'date': D_STAMP,
        'name': prod.name,
        'price': prod.price
    }
    exec_query(conn, QUERIES['write_logs'], op_desc, param_values)


def view_price_chgs(pr_chgs: dict[str, dict[str, str | float]]) -> str:
    '''
    View product price changes.

    Displays the new price, the old price, and their percent difference.

    Parameters
    ----------------
    pr_chgs : dict
        Dictionary of product price change information.

    Returns
    ----------------
    str
        Price change output information.
    '''
    output = 'Price changes were detected:\n\n'
    for prd_name, prs_dict in pr_chgs.items():
        output += f'Product: {prd_name}\n'
        for key, value in prs_dict.items():
            if key != '% Change':
                value = locale.currency(int(value), grouping=True)
                output += f'{key}: {value}\n'
            else:
                output += f'{key}: {value:.0%}\n'
        output += '\n'

    return output


def update_prices(conn: sqlite3.Connection,
                  browser: webdriver.firefox.webdriver.WebDriver) -> None:
    '''
    Check for price changes for each product and update their prices in
    the database.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    browser : webdriver.firefox.webdriver.WebDriver
        Selenium WebDriver object.
    '''
    print('\nUpdating product prices...')

    op_desc = 'Select product table operation'
    result = exec_query(conn, QUERIES['sel_prods'], op_desc)
    # Create list of result rows, where each row is an anonymous dictionary
    # For the inner dictionary, keys are the table headings and values are
    # the heading values for that row
    result = [
        {heading: value for heading, value in zip(PROD_HDGS, list(row))}
        for row in result
    ]

    pr_chgs = {}  # Create price changes dictionary

    for row in result:
        # Confirm product page URL still valid
        if not val_web_page(row['url']):
            val_msg = (
                f"\nCurrent product page URL for {FB}{row['name']}{FR} "
                'is no longer valid.\n'
            )
            print(val_msg)

            prompt = 'Enter product page URL: '
            row['url'] = set_url(prompt)

            op_desc = 'product table URL update'
            param_values = {
                'name': row['name'],
                'url': row['url']
            }
            exec_query(conn, QUERIES['up_url'], op_desc, param_values)

        # Confirm product price CSS selector still valid
        err_msg = create_ish_msg(row['url'])
        attempts = 0
        while attempts < MAX_ATTEMPTS:
            try:
                browser.get(row['url'])
            except TimeoutException:
                logging.error(traceback.format_exc().rstrip())
                print(err_msg)

                attempts += 1
                test_attempts(attempts, MAX_ATTEMPTS)
            else:
                break
        if not val_css_sel(row['price_selector'], browser):
            val_msg = (
                f'\nCurrent product price CSS selector for '
                f"{FB}{row['name']}{FR} is no longer valid."
                f"\n{row['url']}\n"
            )
            print(val_msg)

            prompt = 'Enter product price CSS selector: '
            row['price_selector'] = set_css_sel(prompt, browser)

            op_desc = 'product table CSS selector update'
            param_values = {
                'name': row['name'],
                'price_selector': row['price_selector']
            }
            exec_query(conn, QUERIES['up_sel'], op_desc, param_values)

        # Create product object using current data
        prod = Product(row['name'], row['url'], row['price_selector'])
        prod.set_price(browser)

        # Check that product price is numerical
        try:
            int(prod.price)
        except Exception:
            logging.error(traceback.format_exc().rstrip())

            price_msg = (
                f'Price for {prod.name} not properly extracted.'
                '\nVerify product information before trying again.'
            )
            pydoc.pager(price_msg)
        else:
            # Detect if there has been a price change
            if prod.price != str(int(row['price'])):
                price_diff = (int(prod.price) - int(row['price'])) \
                    / int(row['price'])
                pr_chgs[row['name']] = {
                    'New Price': prod.price,
                    'Old Price': row['price'],
                    '% Change': price_diff
                }

                op_desc = 'product table update'
                param_values = {
                    'name': prod.name,
                    'price': prod.price
                }
                exec_query(conn, QUERIES['up_price'], op_desc, param_values)

                log_price_chg(conn, prod, 'logs table update')

    # Display price change information/message
    if pr_chgs:
        pydoc.pager(view_price_chgs(pr_chgs).rstrip())
    else:
        pydoc.pager('No price changes were detected.')


def view_prod_log(conn: sqlite3.Connection, tbl_md: dict[str, list[str]],
                  query: str, op_desc: str) -> None:
    '''
    View product log in the system pager.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    tbl_md : dict
        Dictionary of table metadata, where the key is the table name
        and the value is a list of column names.
    query : str
        A SQL query.
    op_desc : str
        Operation description to use in output messages.
    '''
    tbl_name = [key for key in tbl_md.keys()][0]
    result = exec_query(conn, query, op_desc)
    result.reverse()  # Reverse list order so latest entries at top
    result = [
        {heading: value for heading, value in zip(LOG_HDGS, list(row))}
        for row in result
    ]

    # Determine max length of column info to use in output string
    max_len = max([
        len(f'{col_name} ({col_i}): ')
        for col_i, col_name in enumerate(tbl_md[tbl_name])
    ])

    output = ''  # Create output string
    for row in result:
        output += f"{row['date']}\n{'-' * LOG_HDR_LEN}"
        row['price'] = locale.currency(int(row['price']), grouping=True)
        for col_name, value in row.items():
            if col_name != 'date':
                output += (
                    f"\n{col_name + ': ':{max_len}}"
                    f'{value}'
                )
        output += '\n\n'

    pydoc.pager(output.rstrip())  # Send final string to paginator


def delete_row(conn: sqlite3.Connection) -> None:
    '''
    Delete a row from a SQLite database.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    '''
    print()
    # Capture product data
    prompt = 'Enter product name to remove: '
    prod_name = set_extant_name(prompt, conn)

    op_desc = 'Remove product operation'
    param_values = {'name': prod_name}
    exec_query(conn, QUERIES['remove_prod'], op_desc, param_values)


def exp_tbls_csv(conn: sqlite3.Connection) -> None:
    '''
    Export SQLite tables to CSV files.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    '''
    # Get names of database tables
    tbl_names = get_tbl_names(conn)

    for tbl_name in tbl_names:
        tbl_md = get_tbl_md(conn, tbl_name)  # Get table metadata

        query = (
            'SELECT '
            '* '
            'FROM '
            f'{tbl_name}'
        )
        op_desc = 'Select table operation'
        result = exec_query(conn, query, op_desc)  # Get table data

        f_path = os.path.join(O_DIR, f'{tbl_name}.csv')
        try:
            with open(f_path, 'w', encoding=ENC, newline='') as f:
                writer = csv.writer(f, delimiter=DELIM)
                writer.writerow(tbl_md[tbl_name])  # Write headings row
                for row in result:
                    writer.writerow(row)
        except Exception:
            logging.error(traceback.format_exc().rstrip())

            err_msg = (
                'Export failed with the following exception:\n\n'
                + traceback.format_exc()
            )
            pydoc.pager(err_msg)
        else:
            scs_msg = f'{tbl_name} table export successful.'
            pydoc.pager(scs_msg)


def menu(conn: sqlite3.Connection,
         browser: webdriver.firefox.webdriver.WebDriver) -> None:
    '''
    Display main menu and process user choice.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    browser : webdriver.firefox.webdriver.WebDriver
        Selenium WebDriver object.
    '''
    print(MENU_STR)  # Display menu options

    # Store answer
    menu_answer = input('\nEnter menu selection: ').lower().strip()

    # Evaluate user menu selection
    match menu_answer:
        case '1':
            add_prods(conn, browser)

            menu(conn, browser)
        case '2':
            tbl_md = get_tbl_md(conn, 'products')

            op_desc = 'Select product table operation'
            view_tbl(conn, tbl_md, QUERIES['sel_prods'], op_desc)

            menu(conn, browser)
        case '3':
            update_prices(conn, browser)

            menu(conn, browser)
        case '4':
            delete_row(conn)

            menu(conn, browser)
        case '5':
            tbl_md = get_tbl_md(conn, 'logs')

            op_desc = 'Select logs table operation'
            view_prod_log(conn, tbl_md, QUERIES['sel_logs'], op_desc)

            menu(conn, browser)
        case '6':
            exp_tbls_csv(conn)

            menu(conn, browser)
        case '7':
            cmd = (
                f'{TER_EMU} '
                f"--working-directory='{O_DIR}' "
                "-- bash -c 'sqlite3 products.sqlite; exec bash'"
            )
            sp.run(split(cmd), capture_output=True)

            menu(conn, browser)
        case 'q':
            pass
        case _:
            print(f'\n{FB}Please enter a valid menu option.{FR}')

            menu(conn, browser)


def main(conn: sqlite3.Connection,
         browser: webdriver.firefox.webdriver.WebDriver) -> None:
    '''
    Define starting point for execution of the program.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    browser : webdriver.firefox.webdriver.WebDriver
        Selenium WebDriver object.
    '''
    signal(SIGINT, hndl_sigint)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    # Check to see if products database file has the correct tables
    tbl_names = get_tbl_names(conn)

    tbls_test = 'products' in tbl_names and 'logs' in tbl_names
    if not tbls_test:
        print(f'\n{FB}Initialization Start{FR}')
        init_db(conn, tbl_names)
        add_prods(conn, browser)
        print(f'{FB}Initialization End{FR}')

    menu(conn, browser)  # Display menu

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    start_msg = '\nStarting headless browser...'  # Set program start message
    debug = False  # Set debug mode flag

    returned = parse_args()
    if returned is not None:
        start_msg, debug = returned[0], returned[1]

    run_init_cks()

    # Create output directory if it does not exist
    if not os.path.isdir(O_DIR):
        os.makedirs(O_DIR)

    try:
        conn = create_conn(DB_PATH)  # Create database connection

        print(start_msg)
        browser = create_drv(debug)  # Create WebDriver object

        main(conn, browser)  # Start program
    finally:
        conn.close()
        browser.quit()
