# Extract PDF Text

Interactively extract text from PDF files.

## Sample Output

```console

*******************

PDF Extraction
~~~~~~~~~~~~~~
1) Extract All Text
2) Extract Specific Text
3) Batch Extract Text
s) Set Output Directory
q) Quit

Enter menu selection: 
```

## Required Packages/Modules

### Debian

`# apt install ocrmypdf`

### Fedora

`# dnf install ocrmypdf`