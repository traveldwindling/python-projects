#!/usr/bin/env python3
'''\
Interactively extract text from PDF files.

When all text is extracted, the content is written as a single string to a
.txt file. For machine-generated PDFs, text can be written with special/escaped
characters preserved or not. If no text is extracted, optical character
recognition (OCR) is used via the Tesseract engine.

Specific text can be extracted by providing a regular expression. In this case,
the written output is a .json file, where the names are the PDF file page
numbers, and the values are arrays of matches on the respective page for the
provided regular expression. This option is not available for scanned PDFs.

A log is saved to 'ex_log_path'.

Classes:

    * PdfFile - Model a PDF file.

Functions:

    * val_dir() - Validate a directory.
    * val_file() - Validate a file.
    * val_regex() - Validate a regular expression.
    * set_dir() - Set a directory path.
    * set_file() - Set a file path.
    * set_regex() - Set a regular expression.
    * set_sel() - Set a binary selection.
    * parse_args() - Parse program arguments.
    * run_init_cks() - Run initialization checks.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging_cl() - Initialize logging with a custom logger.
    * list_dir() - Return lists of directory object names and absolute paths.
    * extr_all_txt() - Extract all text from a PDF file.
    * extr_all_ocr() - Extract all text from a PDF file using
      optical character recognition (OCR).
    * extr_pat() - Extract all text from a PDF file that matches a pattern.
    * wr_output() - Write PDF text to a file.
    * menu() - Display main menu and process user choice.
    * main() - Define starting point for execution of the program.

Requires
----------------
pdfplumber
    Plumb a PDF for detailed information about each char, rectangle, and line.
ocrmypdf
    add an OCR text layer to PDF files
tesseract
    command-line OCR engine\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import json  # Enable JSON API
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version as string
from platform import python_version
import pydoc  # Enable automatic documentation generation from Python modules
# Enable regular expression matching operations similar to those found in Perl
import re
from shlex import split  # Enable splitting a string using shell-like syntax
from shutil import which  # Enable returning the path to an executable
from signal import SIGINT, signal  # Enable use of signal handlers
# Enable spawning of new processes, connecting to their input/output/error
# pipes, and obtaining of their return codes
import subprocess as sp
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type

# Define formatting variables
FB = '\033[1m'
FI = '\033[3m'
FR = '\033[0m'

try:
    # Enable plumbing a PDF for detailed information about each
    # text character, rectangle, and line
    import pdfplumber

    # Enable extracting information from PDF documents
    from pdfminer import pdfparser
except ImportError:
    import_msg = f'\nInstall {FB}pdfplumber{FR} before proceeding.'
    print(import_msg)
    sys.exit(1)  # Exit with error status

#############
# Variables #
#############
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

ENC = 'utf-8'  # Define text encoding

O_FMT_CHOICES = {'r': True, 'n': False}  # Define output format choices

# Define not PDF messages
NOT_PDF_MSG = 'Ensure that entered file is a PDF file and try again.'
NOT_PDFS_MSG = 'The following files were not processed:\n'
NOT_PDFS_MSG_END = '\nEnsure that they are PDF files.'

# Define OCR used messages
OCR_MSG = 'OCR was required to extract text. Results may vary.'
OCR_MSGS = 'OCR was required for the following files:\n'
OCR_MSGS_END = '\nResults may vary.'

# Define no text extracted messages
NO_TXT_MSG = 'No text extracted. Check PDF type.'
NO_TXT_MSGS = 'No content was extracted for the following files:\n'
NO_TXT_MSGS_END = '\nCheck their PDF file types.'

# Define write error messages
WR_ERR_MSGS = "The following files' output was not written to disk\n"
WR_ERR_MSGS_END = (
    f"\nRefer to the log for additional information:\n{FB}'{LOG_PATH}'{FR}"
)

PAGER = 'less -Mr'  # Define pager command

# Define menu characters and title
SEP_CHAR, UND_CHAR = '*', '~'
MENU_TI = 'PDF Extraction'

# Calculate menu length and generate decoration strings
MT_LEN = len(MENU_TI)
SEP_LEN = MT_LEN + 5
UND_STR, SEP_STR = (UND_CHAR * MT_LEN), (SEP_CHAR * SEP_LEN)

# Define menu string
MENU_PRE = f'''
{SEP_STR}

{MENU_TI}
'''

MENU_POST = f'''\
{UND_STR}
1) Extract All Text
2) Extract Specific Text
3) Batch Extract Text
s) Set Output Directory
q) Quit\
'''

# Define required commands
REQ_CMDS = (
    'ocrmypdf',
    'tesseract'
)


###########
# Classes #
###########
class PdfFile:
    '''
    Model a PDF file.

    Methods:

        * __init__() - Initialize the class with stateful data.
        * __repr__() - Return a string containing a printable representation
                       of an  object.
    '''

    def __init__(self, f_path: str) -> None:
        '''
        Initialize the class with stateful data.

        Parameters
        ----------------
        f_path : str
            Path to PDF file.
        '''
        self.path = f_path
        self.name = os.path.basename(os.path.splitext(f_path)[0])
        self.content = ''
        self.matches = {}

    def __repr__(self) -> str:
        '''
        Return a string containing a printable representation of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'path={self.path!r}, '
            f'name={self.name!r}, '
            f'content={self.content!r}, '
            f'matches={self.matches!r})'
        )


#############
# Functions #
#############
# Validation Functions Start #
def val_dir(dir_to_val: str) -> bool:
    '''
    Validate a directory.

    Parameters
    ----------------
    dir_to_val : str
        Directory to validate.

    Returns
    ----------------
    bool
        Whether provided directory path is valid or not.
    '''
    return os.path.isdir(dir_to_val)


def val_file(file_to_val: str) -> bool:
    '''
    Validate a file.

    Parameters
    ----------------
    file_to_val : str
        File to be validated.

    Returns
    ----------------
    bool
        Whether provided file path is valid or not.
    '''
    return os.path.isfile(file_to_val)


def val_regex(pat_to_val: str) -> bool:
    '''
    Validate a regular expression.

    Parameters
    ----------------
    pat_to_val : str
        Regular expression pattern.

    Returns
    ----------------
    bool
        Whether provided regular expression pattern is valid or not.
    '''
    try:
        re.compile(pat_to_val)
        return True
    except re.error:
        logger.error(traceback.format_exc().rstrip())
        return False
# Validation Functions End #


# Set Functions Start #
def set_dir(prompt: str) -> str:
    '''
    Set a directory path.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        A directory path.
    '''
    while True:
        dir_to_val = input(prompt).strip().rstrip('/')

        if dir_to_val == 'q':
            sys.exit(0)
        elif val_dir(dir_to_val):
            return dir_to_val
        else:
            print('\nInvalid directory. Try again or enter q to quit.\n')


def set_file(prompt: str) -> str:
    '''
    Set a file path.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        A file path.
    '''
    while True:
        file_to_val = input(prompt).strip()

        if file_to_val == 'q':
            sys.exit(0)
        elif val_file(file_to_val):
            return file_to_val
        else:
            print('\nInvalid file. Try again or enter q to quit.\n')


def set_regex(prompt: str) -> str:
    '''
    Set a regular expression.

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        A regular expression.
    '''
    while True:
        pat_to_val = input(prompt).strip()

        if pat_to_val == 'q':
            sys.exit(0)
        elif val_regex(pat_to_val):
            return pat_to_val
        else:
            err_msg = '\nInvalid regular expression. Try again '
            err_msg += 'or enter q to quit.\n'
            print(err_msg)


def set_sel(prompt: str) -> str:
    '''
    Set a binary selection.

    Designed for prompts with two parenthesized, single character choices
    (e.g., Choice one (o) or choice two (t): ).

    Parameters
    ----------------
    prompt : str
        Prompt text to use when gathering input.

    Returns
    ----------------
    str
        The selected choice.
    '''
    # Extract binary selection choices from prompt
    sel_1 = prompt[prompt.index('(') + 1:prompt.index('(') + 2]
    sel_2 = prompt[prompt.rindex(')') - 1:prompt.rindex(')')]

    while True:
        choice_to_val = input(prompt).lower().strip()

        if choice_to_val == 'q':
            sys.exit(0)
        elif choice_to_val in (sel_1, sel_2):
            return choice_to_val
        else:
            print('\nInvalid choice. Try again or enter q to quit.\n')
# Set Functions End #


def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def run_init_cks() -> None:
    '''Run initialization checks.'''
    mis_cmds = []
    # Collect required commands that are missing
    for cmd in REQ_CMDS:
        if not which(cmd):
            mis_cmds.append(cmd)
    # Display missing commands message and exit
    if mis_cmds:
        err_msg = (
            '\nInstall the following command(s) before proceeding:'
            f'\n{FB}{" ".join(mis_cmds)}{FR}'
        )
        print(err_msg)
        sys.exit(1)


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging_cl(log_path: str) -> logging.Logger:
    '''
    Initialize logging with a custom logger.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.

    Returns
    ----------------
    logging.Logger
        A custom logger.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    logger = logging.getLogger(__name__)  # Create a custom logger
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s- %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel('DEBUG')

    return logger


def list_dir(i_dir: str) -> tuple[list[str], list[str]]:
    '''
    Return lists of directory object names and absolute paths.

    Parameters
    ----------------
    i_dir : str
        Path of directory to get listing from.

    Returns
    ----------------
    tuple
        Lists of directory object names and absolute paths.
    '''
    clg_dir = os.getcwd()  # Obtain current working directory

    try:
        os.chdir(i_dir)  # Change to provided directory
    except Exception as exc:
        logger.error(traceback.format_exc().rstrip())
        print(f'\nInvalid directory provided.\nError: {exc}')

        logger.debug('Exiting program with error code 1.')
        sys.exit(1)
    else:
        obj_names = os.listdir(i_dir)  # Get object names
        # Get object absolute paths
        obj_abs_paths = [os.path.abspath(obj) for obj in obj_names]
        os.chdir(clg_dir)  # Change back to calling directory

        return obj_names, obj_abs_paths


def extr_all_txt(f_obj: PdfFile) -> PdfFile:
    '''
    Extract all text from a PDF file.

    Parameters
    ----------------
    f_obj : PdfFile
        PDF file object.

    Returns
    ----------------
    PdfFile
        PDF file object.
    '''
    with pdfplumber.open(f_obj.path) as pdf:
        for page in pdf.pages:
            f_obj.content += page.extract_text()

    return f_obj


def extr_all_ocr(f_obj: PdfFile, o_dir: str) -> None:
    '''
    Extract all text from a PDF file using optical character recognition (OCR).

    Parameters
    ----------------
    f_obj : PdfFile
        PDF file object.

    o_dir : str
        Directory to save output to.
    '''
    cmd = (
        f"ocrmypdf --sidecar '{o_dir}/{f_obj.name}.txt' "
        f"'{f_obj.path}' -"
    )
    sp.run(split(cmd), capture_output=True)


def extr_pat(f_obj: PdfFile, pat: str) -> PdfFile:
    '''
    Extract all text from a PDF file that matches a pattern.

    Parameters
    ----------------
    f_obj : PdfFile
        PDF file object.

    pat : str
        Regular expression pattern to match.

    Returns
    ----------------
    PdfFile
        PDF file object.
    '''
    pat = re.compile(pat)  # Compile regular expression pattern
    with pdfplumber.open(f_obj.path) as pdf:
        for idx, page in enumerate(pdf.pages):
            cnt = page.extract_text()  # Extract page text
            f_obj.content += cnt  # Add page text to content attribute
            page_matches = re.findall(pat, cnt)  # Find all matches for page
            # Record page matches for respective page
            f_obj.matches[idx + 1] = page_matches

    return f_obj


def wr_output(f_obj: PdfFile, o_dir: str, raw: bool = False) -> None:
    '''
    Write PDF text to a file.

    If all text is extracted, a text file is created. If specific text is
    extracted, a JSON file is created.

    Parameters
    ----------------
    f_obj : PdfFile
        PDF file object.

    o_dir : str
        Directory to write output file to.

    raw : bool (optional)
        Whether to write out raw string or not. Defaults to False.
    '''
    if f_obj.matches:
        with open(os.path.join(o_dir, f'{f_obj.name}.json'), 'w',
                  encoding=ENC, newline='') as f:
            json.dump(f_obj.matches, f)
    else:
        with open(os.path.join(o_dir, f'{f_obj.name}.txt'), 'w',
                  encoding=ENC, newline='') as f:
            if raw:
                f.write(repr(f_obj.content))
            else:
                f.write(f_obj.content)


def menu(o_dir: str) -> None:
    '''
    Display main menu and process user choice.

    Parameters
    ----------------
    o_dir : str
        Directory to save output to.
    '''
    # Define current output directory string
    curr_o_dir = f'Current output directory: {FI}{o_dir}{FR}\n'
    # Create menu string variable
    menu_str = f'{MENU_PRE}{curr_o_dir}{MENU_POST}'

    print(menu_str)  # Display menu options

    # Store answer
    menu_answer = input('\nEnter menu selection: ').lower().strip()

    # Evaluate user menu selection
    match menu_answer:
        case '1':
            print()
            # Set and validate file path
            prompt = 'Enter PDF file path: '
            i_file = set_file(prompt)

            # Create PdfFile instance with provided path
            f_obj = PdfFile(i_file)

            logger.debug(f'Processing "{i_file}"')
            try:
                f_obj = extr_all_txt(f_obj)
            except pdfparser.PDFSyntaxError:
                logger.error(traceback.format_exc().rstrip())
                # Send not a PDF message to pager
                pydoc.pipepager(NOT_PDF_MSG, cmd=PAGER)
            else:
                if f_obj.content:
                    prompt = 'Write out raw text (r) or normal text (n)? '
                    o_fmt_choice = set_sel(prompt)
                    try:
                        wr_output(f_obj, o_dir,
                                  raw=O_FMT_CHOICES[o_fmt_choice])
                    except Exception:
                        logger.error(traceback.format_exc().rstrip())

                        err_msg = (
                            'PDF text output write operation failed '
                            'with the following exception:\n\n'
                            + traceback.format_exc()
                        )
                        pydoc.pager(err_msg)
                else:
                    logger.debug('Extracting text with OCR')
                    extr_all_ocr(f_obj, o_dir)

                    # Send OCR used message to pager
                    pydoc.pipepager(OCR_MSG, cmd=PAGER)

            menu(o_dir)
        case '2':
            print()
            prompt = 'Enter PDF file path: '
            i_file = set_file(prompt)

            f_obj = PdfFile(i_file)

            # Set and validate regular expression
            prompt = 'Enter regular expression pattern to search for: '
            pat = set_regex(prompt)

            logger.debug(f'Processing "{i_file}"')
            try:
                f_obj = extr_pat(f_obj, pat)
            except pdfparser.PDFSyntaxError:
                logger.error(traceback.format_exc().rstrip())
                pydoc.pipepager(NOT_PDF_MSG, cmd=PAGER)
            else:
                if f_obj.content:
                    try:
                        wr_output(f_obj, o_dir)
                    except Exception:
                        logger.error(traceback.format_exc().rstrip())

                        err_msg = (
                            'PDF text output write operation failed '
                            'with the following exception:\n\n'
                            + traceback.format_exc()
                        )
                        pydoc.pager(err_msg)
                else:
                    pydoc.pipepager(NO_TXT_MSG, cmd=PAGER)

            menu(o_dir)
        case '3':
            print()
            # Set and validate input directory
            prompt = 'Enter directory path with PDFs to process: '
            i_bat_dir = set_dir(prompt)

            # Set and validate extraction mode
            while True:
                prompt = 'Extract all text (a) or specific text (s)? '
                extr_choice = input(prompt).lower().strip()

                if extr_choice in ('a', 's'):
                    break
                elif extr_choice == 'q':
                    sys.exit(0)
                else:
                    print('\nInvalid choice. Try again or enter q to quit.\n')

            # Obtain object names/paths from input directory
            obj_names, obj_paths = list_dir(i_bat_dir)

            # Create message lists
            not_pdf_msgs = []
            ocr_msgs = []
            no_txt_msgs = []
            wr_err_msgs = []

            # Extract text based on user choice
            if extr_choice == 'a':
                prompt = 'Write out raw text (r) or normal text (n)? '
                o_fmt_choice = set_sel(prompt)

                for obj_path in obj_paths:
                    if obj_path.endswith('.pdf'):
                        logger.debug(f'Processing "{obj_path}"')
                        f_obj = PdfFile(obj_path)
                        try:
                            f_obj = extr_all_txt(f_obj)
                        except pdfparser.PDFSyntaxError:
                            logger.error(traceback.format_exc().rstrip())
                            not_pdf_msgs.append(f'- {FB}{f_obj.name}{FR}')
                        else:
                            if f_obj.content:
                                try:
                                    wr_output(f_obj, o_dir,
                                              raw=O_FMT_CHOICES[o_fmt_choice])
                                except Exception:
                                    logger.error(
                                        traceback.format_exc().rstrip()
                                    )
                                    wr_err_msgs.append(
                                        f'- {FB}{f_obj.name}{FR}'
                                    )
                            else:
                                logger.debug('Extracting text with OCR')
                                extr_all_ocr(f_obj, o_dir)

                                ocr_msgs.append(f'- {FB}{f_obj.name}{FR}')
            elif extr_choice == 's':
                prompt = 'Enter regular expression pattern to search for: '
                pat = set_regex(prompt)

                for obj_path in obj_paths:
                    if obj_path.endswith('.pdf'):
                        logger.debug(f'Processing "{obj_path}"')
                        f_obj = PdfFile(obj_path)
                        try:
                            f_obj = extr_pat(f_obj, pat)
                        except pdfparser.PDFSyntaxError:
                            logger.error(traceback.format_exc().rstrip())
                            not_pdf_msgs.append(f'- {FB}{f_obj.name}{FR}')
                        else:
                            if f_obj.content:
                                try:
                                    wr_output(f_obj, o_dir)
                                except Exception:
                                    logger.error(
                                        traceback.format_exc().rstrip()
                                    )
                                    wr_err_msgs.append(
                                        f'- {FB}{f_obj.name}{FR}'
                                    )
                            else:
                                no_txt_msgs.append(f'- {FB}{f_obj.name}{FR}')

            # Display applicable messages via pager
            if not_pdf_msgs:
                not_pdf_msgs.insert(0, NOT_PDFS_MSG)
                not_pdf_msgs.append(NOT_PDFS_MSG_END)
                pydoc.pipepager('\n'.join(not_pdf_msgs), cmd=PAGER)
            if ocr_msgs:
                ocr_msgs.insert(0, OCR_MSGS)
                ocr_msgs.append(OCR_MSGS_END)
                pydoc.pipepager('\n'.join(ocr_msgs), cmd=PAGER)
            if no_txt_msgs:
                no_txt_msgs.insert(0, NO_TXT_MSGS)
                no_txt_msgs.append(NO_TXT_MSGS_END)
                pydoc.pipepager('\n'.join(no_txt_msgs), cmd=PAGER)
            if wr_err_msgs:
                wr_err_msgs.insert(0, WR_ERR_MSGS)
                wr_err_msgs.append(WR_ERR_MSGS_END)
                pydoc.pipepager('\n'.join(wr_err_msgs), cmd=PAGER)

            menu(o_dir)
        case 's':
            print()
            prompt = 'Enter directory path to save output files to: '
            o_dir = set_dir(prompt)

            menu(o_dir)
        case 'q':
            pass
        case _:
            print(f'\n{FB}Please enter a valid menu option.{FR}')

            menu(o_dir)


def main() -> None:
    '''Define starting point for execution of the program.'''
    _, _ = parse_args()

    run_init_cks()

    signal(SIGINT, hndl_sigint)

    try:
        logger = init_logging_cl(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise
    else:
        globals()['logger'] = logger

    logger.debug('Starting program')

    logger.debug(f'Python version: {python_version()}')

    # Obtain initialization data
    print(f'\n{FB}Initialization Start{FR}')
    # Set and validate output directory
    prompt = 'Enter directory to save output to: '
    o_dir = set_dir(prompt)
    print(f'{FB}Initialization End{FR}')

    menu(o_dir)  # Display menu

    logger.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
