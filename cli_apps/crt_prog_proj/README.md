# Create Programming Project

Create a programming project.

The following types of projects are supported:

- Bash script
- Python script

Each project type is created from a template stored in a templates directory.

Dictionaries are used to set the following attributes for each project:

- **Project type:** A general name for the type of project (e.g., `Python Script`).
- **Project extension:** The main file extension type for the project type (e.g., `py`).
- **Project template:** The template filesystem object on which to base the new project.
- **Project directory:** The output directory for the newly created project.

## Sample Output

```console

*******************************

Create Programming Project
~~~~~~~~~~~~~~~~~~~~~~~~~~
1) Bash Script
2) Python Script
q) Quit

Enter menu selection: 1

Enter script name: Init Atom
Enter script purpose: Initialize Atom.

Bash Script project successfully created.

Execute the following command(s) to begin working with the project:
chmod 700 '/home/tenma/init_atom.bash' &&
    vim '/home/tenma/init_atom.bash'
```

## Variables to Set

- `BASH_SCR_MAP` - Bash script configuration dictionary.
- `BASH_SCR_URLS` (optional) - URLs to open for Bash script projects.
- `PROJ_ED` - Project editor to open the new project in.
- `PY_SCR_MAP` - Python script configuration dictionary.
- `PY_SCR_URLS` (optional) - URLs to open for Python script projects.