#!/usr/bin/env python3
'''\
Create a programming project.

The following types of projects are supported:

- Bash script
- Python script

Each project type is created from a template stored in a templates directory.

Dictionaries are used to set the following attributes for each project:

- Project type: A general name for the type of project (e.g., Python Script).
- Project extension: The main file extension type for the project type
  (e.g., py).
- Project template: The template filesystem object on which to base the new
  project.
- Project template purpose placeholder: The text that should be replaced in
  the project template by the entered project purpose.
- Project directory: The output directory for the newly created project.

A log is saved to 'ex_log_path'.

Functions:

    * parse_args() - Parse program arguments.
    * run_init_cks() - Run initialization checks.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * get_scr_info() - Get script information.
    * clean_name() - Create a clean object name.
    * rd_file() - Read a text file.
    * wr_file() - Write a text file.
    * crt_scr() - Create a script.
    * main() - Define starting point for execution of the program.

Variables to set:
----------------
BASH_SCR_MAP
    Bash script configuration dictionary.
BASH_SCR_URLS (optional)
    URLs to open for Bash script projects.
PROJ_ED
    Project editor to open the new project in.
PY_SCR_MAP
    Python script configuration dictionary.
PY_SCR_URLS (optional)
    URLs to open for Python script projects.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
from collections import namedtuple  # Enable creation of new tuple subclasses
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
from platform import system  # Enable returning the system/OS name
# Enable regular expression matching operations similar to those found in Perl
import re
from signal import signal, SIGINT  # Enable use of signal handlers
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type
# Enable a high-level interface to allow displaying Web-based
# documents to users
import webbrowser as wb

#############
# Variables #
#############
# Define formatting variables
FB = '\033[1m'
FR = '\033[0m'

HOME = os.path.expanduser('~')  # Obtain user home directory
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

WIN = (system() == 'Windows')  # Determine if running Windows

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path

NAME_REGEX = r'[^a-zA-Z0-9]'  # Define name regular expression
# Create name regular expression object
NAME_PAT = re.compile(NAME_REGEX)

SCR_PERMS = '700'  # Define new script numeric permissions

# Set Bash script URLs
BASH_SCR_URLS = []
# Set Python script URLs
PY_SCR_URLS = []

PROJ_ED = ''  # Define editor to open the new project in

# Set Bash script configuration
BASH_SCR_MAP = {
    'proj_type': '',
    'proj_ext': '',
    'proj_tpl': '',
    'proj_purp_ph': '',
    'proj_dir': ''
}
# Set Python script configuration
PY_SCR_MAP = {
    'proj_type': '',
    'proj_ext': '',
    'proj_tpl': '',
    'proj_purp_ph': '',
    'proj_dir': ''
}

# Create Project tuple subclass
Project = namedtuple(
    'Project',
    ['proj_type', 'proj_ext', 'proj_tpl', 'proj_purp_ph', 'proj_dir']
)
# Create project prototype instance
proj_proto = Project(None, None, None, None, None)
# Create project instances
bash_scr = proj_proto._replace(**BASH_SCR_MAP)
py_scr = proj_proto._replace(**PY_SCR_MAP)

# Define menu characters and title
SEP_CHAR, UND_CHAR = '*', '~'
MENU_TI = 'Create Programming Project'

# Calculate menu length and generate decoration strings
MT_LEN = len(MENU_TI)
SEP_LEN = MT_LEN + 5
UND_STR, SEP_STR = (UND_CHAR * MT_LEN), (SEP_CHAR * SEP_LEN)

# Create menu string variable
MENU_STR = f'''
{SEP_STR}

{MENU_TI}
{UND_STR}
1) Bash Script
2) Python Script
q) Quit\
'''

# Create required variables dictionary
REQ_VARS = {
    'BASH_SCR_MAP': BASH_SCR_MAP,
    'PROJ_ED': PROJ_ED,
    'PY_SCR_MAP': PY_SCR_MAP,
}


#############
# Functions #
#############
def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def run_init_cks() -> None:
    '''Run initialization checks.'''
    unset_vars = []  # Create unset variables list
    # Check for unset variables
    for req_var_name, req_var_obj in REQ_VARS.items():
        seq_cnd = (
            isinstance(req_var_obj, str)
            or isinstance(req_var_obj, list)
            or isinstance(req_var_obj, tuple)
        )
        if seq_cnd:
            if not req_var_obj:
                unset_vars.append(req_var_name)
        elif isinstance(req_var_obj, dict):
            if not all(req_var_obj.values()):
                unset_vars.append(req_var_name)
    # Display missing variables message and exit
    if unset_vars:
        err_msg = (
            '\nSet the following variable(s) before '
            'running the program again:\n'
            f"{FB}{' '.join(unset_vars)}{FR}"
        )
        print(err_msg)
        sys.exit(1)


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def get_scr_info() -> tuple[str, str]:
    '''
    Get script information.

    Returns
    ----------------
    tuple
        Script name and purpose.
    '''
    scr_name = input('\nEnter script name: ').strip()
    scr_purpose = input('Enter script purpose: ').strip()
    return scr_name, scr_purpose


def clean_name(name_pat: re.Pattern, obj_name: str) -> str:
    '''
    Create a clean object name.

    Parameters
    ----------------
    name_pat : re.Pattern
        Name regular expression object that denotes which characters should be
        replaced with an underscore.
    obj_name : str
        String to clean and use as a filename.

    Returns
    ----------------
    str
        Cleaned string to use as a filename.
    '''
    # Replace most non-alphanumeric characters with underscores
    obj_name = re.sub(name_pat, '_', obj_name)
    # Squeeze underscores, lowercase name, strip leading/ending underscores
    obj_name = re.sub(r'([_])(\1+)', r'\1', obj_name.lower()).strip('_')

    return obj_name


def rd_file(f_path: str, encoding: str = 'utf-8') -> str:
    '''
    Read a text file.

    Parameters
    ----------------
    f_path : str
        Path of the text file to read.
    encoding : str (optional)
        Encoding of the text file to read. Defaults to utf-8.

    Returns
    ----------------
    str
        The text of the read file as a single string.
    '''
    with open(f_path, 'r', encoding=encoding, newline='') as f:
        data = f.read()

    return data


def wr_file(f_path: str, data: str, encoding: str = 'utf-8',
            append: bool = False) -> None:
    '''
    Write a text file.

    Parameters
    ----------------
    f_path : str
        Path of the text file to write.
    data : str
        Data to write to the text file.
    encoding : str (optional)
        Encoding of the text file to write. Defaults to utf-8.
    append : bool (optional)
        Whether to append text or not. Defaults to False.
    '''
    if append:
        with open(f_path, 'a', encoding=encoding, newline='') as f:
            f.write(data)
    else:
        with open(f_path, 'w', encoding=encoding, newline='') as f:
            f.write(data)


def crt_scr(proj_inst: Project, replcs: dict[str, str], scr_path: str) -> None:
    '''
    Create a script.

    Content of a script template is read into a string, placeholder
    replacements are completed, and the final script content is written
    to the script output path.

    Parameters
    ----------------
    proj_inst : Project
        Instance of Project class.
    replcs : dict
        Dictionary of terms to replace, where the key is the old term
        and the value is the new term.
    scr_path : str
        Script output path.
    '''
    scr_text = rd_file(proj_inst.proj_tpl)
    for old_term, new_term in replcs.items():
        scr_text = scr_text.replace(old_term, new_term)
    wr_file(scr_path, scr_text)


def menu() -> None:
    '''
    Display main menu and process user choice.
    '''
    logging.debug('Displaying menu')
    print(MENU_STR)

    logging.debug('Collecting menu selection')
    menu_answer = input('\nEnter menu selection: ').lower().strip()

    logging.debug('Evaluating menu selection')
    match menu_answer:
        case '1':
            logging.debug('Getting script information')
            scr_name, scr_purpose = get_scr_info()

            logging.debug('Cleaning script name')
            scr_name = clean_name(NAME_PAT, scr_name)
            scr = f'{scr_name}.{bash_scr.proj_ext}'

            logging.debug(f'Creating "{scr}" script')
            replcs = {bash_scr.proj_purp_ph: scr_purpose}
            scr_path = os.path.join(bash_scr.proj_dir, scr)
            try:
                crt_scr(bash_scr, replcs, scr_path)
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())

                err_msg = f'\nThe following error occurred: {exc}'
                print(err_msg)

                logging.debug('Exiting program with error code 1')
                sys.exit(1)

            if BASH_SCR_URLS:
                logging.debug('Opening project URLs')
                for url in BASH_SCR_URLS:
                    wb.open(url)

            logging.debug('Displaying project message')
            proj_msg = (
                f'\n{FB}{bash_scr.proj_type}{FR} project '
                'successfully created.\n\n'
                'Execute the following command(s) to begin working with '
                'the project:\n'
            )
            if WIN:
                cmd = f"{FB}{PROJ_ED} '{scr_path}'{FR}"
            else:
                cmd = (
                    f"{FB}chmod {SCR_PERMS} '{scr_path}' &&\n"
                    f"    {PROJ_ED} '{scr_path}'{FR}"
                )
            print(f'{proj_msg}{cmd}')
        case '2':
            logging.debug('Getting script information')
            scr_name, scr_purpose = get_scr_info()

            logging.debug('Cleaning script name')
            scr_name = clean_name(NAME_PAT, scr_name)
            scr = f'{scr_name}.{py_scr.proj_ext}'

            logging.debug(f'Creating "{scr}" script')
            replcs = {py_scr.proj_purp_ph: scr_purpose}
            scr_path = os.path.join(py_scr.proj_dir, scr)
            try:
                crt_scr(py_scr, replcs, scr_path)
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())

                err_msg = f'\nThe following error occurred: {exc}'
                print(err_msg)

                logging.debug('Exiting program with error code 1')
                sys.exit(1)

            if PY_SCR_URLS:
                logging.debug('Opening project URLs')
                for url in PY_SCR_URLS:
                    wb.open(url)

            logging.debug('Displaying project message')
            proj_msg = (
                f'\n{FB}{py_scr.proj_type}{FR} project '
                'successfully created.\n\n'
                'Execute the following command(s) to begin working with '
                'the project:\n'
            )
            if WIN:
                cmd = f"{FB}{PROJ_ED} '{scr_path}'{FR}"
            else:
                cmd = (
                    f"{FB}chmod {SCR_PERMS} '{scr_path}' &&\n"
                    f"    {PROJ_ED} '{scr_path}'{FR}"
                )
            print(f'{proj_msg}{cmd}')
        case 'q':
            pass
        case _:
            print(f'\n{FB}Please enter a valid menu option.{FR}')

            menu()


def main() -> None:
    '''Define starting point for execution of the program.'''
    _, _ = parse_args()

    run_init_cks()

    signal(SIGINT, hndl_sigint)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    menu()  # Display menu

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
