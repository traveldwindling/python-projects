#!/usr/bin/env python3
'''
A collection of I/O-oriented functions.

Functions:

    * crt_dir() - Create a directory if it does not exist.
    * wr_file() - Write a text file.
    * get_worksheets() - Return a dictionary of worksheets for a workbook.
    * init_logging() - Initialize logging.

Requires
----------------
odfpy
    Python API and tools to manipulate OpenDocument files
openpyxl
    A Python library to read/write Excel 2010 xlsx/xlsm files
pandas
    Powerful data structures for data analysis, time series, and statistics
'''
###########
# Modules #
###########
# Enable functions and classes that implement an event logging system
import logging
# Enable a portable way of using operating system dependent functionality
import os
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler

import pandas as pd  # Enable data structures and data analysis tools


#############
# Functions #
#############
def crt_dir(dir_to_crt: str) -> None:
    '''
    Create a directory if it does not exist.

    Parameters
    ----------------
    dir_to_crt : str
        Directory to create.
    '''
    if not os.path.isdir(dir_to_crt):
        os.makedirs(dir_to_crt)


def wr_file(f_path: str, data: str, encoding: str = 'utf-8',
            append: bool = False) -> None:
    '''
    Write a text file.

    Parameters
    ----------------
    f_path : str
        Path of the text file to write.
    data : str
        Data to write to the text file.
    encoding : str (optional)
        Encoding of the text file to write. Defaults to utf-8.
    append : bool (optional)
        Whether to append text or not. Defaults to False.
    '''
    if append:
        with open(f_path, 'a', encoding=encoding, newline='') as f:
            f.write(data)
    else:
        with open(f_path, 'w', encoding=encoding, newline='') as f:
            f.write(data)


def get_worksheets(f_path: str) -> dict[int, str]:
    '''
    Return a dictionary of worksheets for a workbook. The key is
    the worksheet numerical index and the value is the worksheet name.

    Parameters
    ----------------
    f_path : str
        Path of the workbook.

    Returns
    ----------------
    dict
        Dictionary of worksheet indices and names.
    '''
    wb = pd.ExcelFile(f_path)
    worksheets = dict(enumerate(wb.sheet_names))

    return worksheets


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )
