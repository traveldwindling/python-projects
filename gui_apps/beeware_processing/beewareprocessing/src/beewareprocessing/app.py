"""
A demonstration of CSV, OpenDocument, and Excel file processing with BeeWare.

Classes:
    * BeeWareProcessing - Model a BeeWare application.

Functions:

    * std_col_names() - Standardize DataFrame column names.
    * write_summ_info() - Write pandas DataFrame summary information.
    * main() - Define starting point for execution of the program.
"""
###########
# Modules #
###########
# Enable a single object containing all the information
# from a date object and a time object
from datetime import datetime as dt
# Enable functions and classes that implement an event logging system
import logging
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version as string
from platform import python_version
import re  # Enable regular expression matching operations
from io import StringIO  # Enable text stream using an in-memory text buffer
# Enable a standard interface to extract, format, and print stack traces
import traceback
import warnings  # Enable warning control

# Enable creating static, animated, and interactive visualizations
import matplotlib as mpl
from matplotlib.figure import Figure
import pandas as pd  # Enable data structures and data analysis tools
import matplotlib.pyplot as plt  # Enable state-based interface to matplotlib
import toga
from toga.style import Pack
from toga.style.pack import COLUMN

from beewareprocessing.modules.iofns import (
    crt_dir,
    get_worksheets,
    init_logging,
    wr_file
)

#############
# Variables #
#############
HOME = os.path.expanduser('~')  # Obtain user home directory

ROOT_DIR = os.path.join(HOME, 'Documents', 'beeware_processing')
LOG_PATH = os.path.join(ROOT_DIR, 'logs', 'beeware_processing.log')
LOG_DIR = os.path.dirname(os.path.abspath(LOG_PATH))
O_DIR = os.path.join(ROOT_DIR, 'output')

DATE_FORMAT = '%Y-%m-%d'
DATE_STAMP = dt.now().strftime(DATE_FORMAT)
YEAR_STAMP = DATE_STAMP[:4]
MO_DAY_STAMP = DATE_STAMP[5:]

# Define color constants
GOLD = '#ffd343'
BLUE = '#1f4260'
WHITE = '#fcfcfc'
BLACK = '#0e1117'

# Define interface colors
DIV_BG_CLR = GOLD

APP_DESC_BOX_BG_CLR = BLUE
APP_DESC_PNL_TXT_CLR = WHITE

CSV_BOX_BG_CLR = BLACK
CSV_DELIM_LBL_TXT_CLR = WHITE
CSV_UP_LBL_TXT_CLR = WHITE

WB_BOX_BG_CLR = BLACK
WS_REFS_LBL_TXT_CLR = WHITE
WB_UP_LBL_TXT_CLR = WHITE

DF_O_BOX_BG_CLR = BLACK
DF_O_PNL_TXT_CLR = WHITE

PLT_BOX_BG_CLR = BLACK

MAIN_BOX_BG_CLR = BLACK


###########
# Classes #
###########
class BeeWareProcessing(toga.App):
    '''
    Model a BeeWare application.

    Parent Classes:

        * toga.App - The top-level representation of an application.

    Methods:

        * set_csv_delim() - Set the CSV delimiter character.
        * set_ws_refs() - Set the workbook worksheet references.
        * set_f_info() - Set selected file information.
        * proc_data() - Process data.
        * write_plot() - Write a plot.
        * proc_csv() - Process a CSV file.
        * proc_wb() - Process an OpenDocument/Excel workbook file.
        * csv_to_df() - Read in a CSV file to a pandas DataFrame.
        * wb_to_dfs() - Read in an OpenDocument/Excel file to
                        pandas DataFrames.
        * csv_dlg_act() - CSV file dialog box actions.
        * wb_dlg_act() - OpenDocument/Excel workbook file dialog box actions.
        * startup() - Construct and show the Toga application.
    '''
    def set_csv_delim(self, widget: toga.widgets.selection.Selection) -> None:
        '''
        Set the CSV delimiter character.

        Parameters
        ----------------
        widget : toga.widgets.selection.Selection
            Toga selection widget.
        '''
        self.csv_delim = widget.value

    def set_ws_refs(self, widget: toga.widgets.textinput.TextInput) -> None:
        '''
        Set the workbook worksheet references.

        Parameters
        ----------------
        widget : toga.widgets.textinput.TextInput
            Toga text input widget.
        '''
        self.ws_refs = widget.value

    def set_f_info(self, f_path: str) -> None:
        '''
        Set selected file information.

        Parameters
        ----------------
        f_path : str
            Path of the selected file.
        '''
        self.f_path = str(f_path)
        self.f_name, self.f_ext = \
            os.path.splitext(os.path.basename(self.f_path))

    def proc_data(self, df: pd.core.frame.DataFrame) \
            -> tuple[pd.core.frame.DataFrame, str, Figure]:
        '''
        Process data.

        Parameters
        ----------------
        df : pd.core.frame.DataFrame
            pandas DataFrame to process.

        Returns
        ----------------
        tuple
            The processed pandas DataFrame, the DataFrame summary information,
            and the plot of the number of unique values per column.
        '''
        # Clean column names
        proc_df = std_col_names(df)

        # Get random DataFrame rows
        if proc_df.shape[0] > 25:
            proc_df = proc_df.sample(25).reset_index(drop=True)

        # Create a text stream using an in-memory text buffer
        buf = StringIO()
        # Write info output to the text stream object
        proc_df.info(buf=buf)
        df_info = buf.getvalue()

        # Obtain number of unique values per column
        uniq_cnts_dict = {
            col_name: proc_df[col_name].nunique()
            for col_name in proc_df.columns
        }
        uniq_cnts_ser = pd.Series(uniq_cnts_dict.values(),
                                  index=uniq_cnts_dict.keys())

        # Create figure and Axes
        fig, ax = plt.subplots(layout='constrained')
        uniq_cnts_ser.sort_values(ascending=True).plot(kind='barh', ax=ax)

        ax.set_title('Number of Unique Values By Column')
        ax.set_xlabel('Number of Unique Values')
        ax.set_ylabel('Column Name')

        return proc_df, df_info, fig

    async def write_plot(self, sub_o_dir: str, name: str, fig: Figure) -> None:
        '''
        Write a plot.

        Parameters
        ----------------
        sub_o_dir : str
            Path to write plot to.
        name : str
            Name of image file to write.
        fig : Figure
            Figure to write to file.
        '''
        o_path = os.path.join(
            sub_o_dir,
            f'{DATE_STAMP}-plot_{name}.png'
        )
        logging.debug(f'Saving plot to "{o_path}"')
        fig.savefig(o_path, bbox_inches='tight')
        plt.close()

    async def proc_csv(self, df: pd.core.frame.DataFrame) -> None:
        '''
        Process a CSV file.

        Parameters
        ----------------
        df : pd.core.frame.DataFrame
            pandas DataFrame created from CSV file to process.
        '''
        logging.debug('Processing DataFrame')
        proc_df, df_info, fig = self.proc_data(df)

        sub_o_dir = os.path.join(O_DIR, self.f_name, YEAR_STAMP, MO_DAY_STAMP)
        logging.debug(f'Creating output subdirectory: "{sub_o_dir}"')
        crt_dir(sub_o_dir)

        o_path = os.path.join(
            sub_o_dir,
            f'{DATE_STAMP}-data_{self.f_name}{self.f_ext}'
        )
        logging.debug(f'Saving processed DataFrame to "{o_path}"')
        proc_df.to_csv(o_path, index=False)

        # Save DataFrame summary information
        await write_summ_info(sub_o_dir, self.f_name, df_info)

        # Save plot to output directory
        await self.write_plot(sub_o_dir, self.f_name, fig)

    async def proc_wb(self, dfs: dict[pd.core.frame.DataFrame]) -> None:
        '''
        Process an OpenDocument/Excel workbook file.

        Parameters
        ----------------
        dfs : dict
            A dictionary of pandas DataFrames created from an
            OpenDocument/Excel file to process.
        '''
        sub_o_dir = os.path.join(
            O_DIR,
            self.f_name,
            YEAR_STAMP,
            MO_DAY_STAMP
        )
        logging.debug(f'Creating output subdirectory: "{sub_o_dir}"')
        crt_dir(sub_o_dir)

        logging.debug('Getting mapping of worksheet indices to names')
        worksheets = get_worksheets(self.f_path)

        logging.debug('Determining output worksheet names')
        o_ws_names = [
            worksheets[ref]
            if isinstance(ref, int)
            else
            ref
            for ref in self.ws_refs_coll
        ]

        proc_dfs = {}
        for ws_name, df in zip(o_ws_names, dfs.values()):
            logging.debug(f'Processing "{ws_name}" DataFrame')
            proc_df, df_info, fig = self.proc_data(df)

            proc_dfs[ws_name] = proc_df

            # Save DataFrame summary information
            await write_summ_info(sub_o_dir, ws_name, df_info)

            # Save plot to output directory
            await self.write_plot(sub_o_dir, ws_name, fig)

        o_path = os.path.join(
            sub_o_dir,
            f'{DATE_STAMP}-data_{self.f_name}{self.f_ext}'
        )
        logging.debug(
            f'Saving workbook to "{o_path}"'
        )
        with pd.ExcelWriter(o_path) as w:
            for ws_name, df in proc_dfs.items():
                logging.debug(f'Saving "{ws_name}" DataFrame to workbook')
                df.to_excel(w, index=False, sheet_name=ws_name)

    async def csv_to_df(self) -> None:
        '''Read in a CSV file to a pandas DataFrame.'''
        logging.debug(f'Reading "{self.f_path}"')
        try:
            df = pd.read_csv(self.f_path, delimiter=self.csv_delim)
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                'There was an error during the reading of the '
                f'input CSV file:\n\n{exc}'
            )
            await self.main_window.info_dialog(
                'Read CSV File Error',
                err_msg
            )
        else:
            try:
                await self.proc_csv(df)
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())
                await self.main_window.info_dialog(
                    'Processing Errors',
                    f'There was an error during CSV file processing:\n\n{exc}'
                )
            else:
                await self.main_window.info_dialog(
                    'Processing Complete',
                    f'Processing of "{self.f_name}{self.f_ext}" is complete.'
                )

    async def wb_to_dfs(self) -> None:
        '''Read in an OpenDocument/Excel file to pandas DataFrames.'''
        logging.debug(f'Reading "{self.f_path}"')
        try:
            dfs = pd.read_excel(self.f_path, sheet_name=self.ws_refs_coll)
        except Exception as exc:
            logging.error(traceback.format_exc().rstrip())
            err_msg = (
                'There was an error during the reading of the '
                f'input workbook file:\n\n{exc}'
            )
            await self.main_window.info_dialog(
                'Read Workbook File Error',
                err_msg
            )
        else:
            try:
                await self.proc_wb(dfs)
            except Exception as exc:
                logging.error(traceback.format_exc().rstrip())
                err_msg = (
                    'There was an error during workbook file processing:'
                    f'\n\n{exc}'
                )
                await self.main_window.info_dialog(
                    'Processing Errors',
                    err_msg
                )
            else:
                await self.main_window.info_dialog(
                    'Processing Complete',
                    f'Processing of "{self.f_name}{self.f_ext}" is complete.'
                )

    async def csv_dlg_act(self, widget: toga.widgets.button.Button) -> None:
        '''
        CSV file dialog box actions.

        Parameters
        ----------------
        widget : toga.widgets.button.Button
            Toga button widget.
        '''
        logging.debug('Opening CSV file')
        f_path = await self.main_window.open_file_dialog(
            'Open CSV File',
            file_types=['csv']
        )

        if f_path is None:
            logging.debug('No file selected')
            await self.main_window.info_dialog(
                'No Opened File',
                'No file selected.'
            )
        else:
            self.set_f_info(f_path)
            await self.csv_to_df()

    async def wb_dlg_act(self, widget: toga.widgets.button.Button) -> None:
        '''
        OpenDocument/Excel workbook file dialog box actions.

        Parameters
        ----------------
        widget : toga.widgets.button.Button
            Toga button widget.
        '''
        logging.debug('Opening workbook file')
        f_path = await self.main_window.open_file_dialog('Open Workbook File')

        if f_path is None:
            logging.debug('No file selected')
            await self.main_window.info_dialog(
                'No Opened File',
                'No file selected.'
            )
        else:
            self.set_f_info(f_path)

            logging.debug(
                'Converting worksheet string into list of worksheet references'
            )
            temp_ws_refs = self.ws_refs.split(',')
            self.ws_refs_coll = []
            for ref in temp_ws_refs:
                try:
                    self.ws_refs_coll.append(int(ref))
                except Exception:
                    self.ws_refs_coll.append(ref)

            await self.wb_to_dfs()

    def startup(self) -> None:
        '''Construct and show the Toga application.'''
        logging.debug('Creating and displaying Toga application')

        # Application description
        app_desc = (
            'This application allows you to select a CSV, OpenDocument, '
            'or Excel file to process.\n\n'
            f'Output Directory:\n{O_DIR}\n\n'
            f'Log:\n{LOG_PATH}'
        )
        app_desc_panel = toga.MultilineTextInput(
            value=app_desc,
            readonly=True,
            style=Pack(
                background_color=APP_DESC_BOX_BG_CLR,
                color=APP_DESC_PNL_TXT_CLR,
                flex=1,
                height=150,
                padding=10,
            )
        )
        app_desc_box = toga.Box(
            style=Pack(background_color=APP_DESC_BOX_BG_CLR)
        )
        app_desc_box.add(app_desc_panel)

        # CSV Delimiter
        csv_delim_lbl = toga.Label(
            'Select CSV delimiter',
            style=Pack(
                background_color=CSV_BOX_BG_CLR,
                color=CSV_DELIM_LBL_TXT_CLR,
                flex=1,
                padding=(6, 0, 0, 0),
                text_align='center',
            )
        )
        csv_delim_sel = toga.Selection(
            items=[',', '|'],
            on_change=self.set_csv_delim,
            style=(Pack(flex=1)),
        )
        # Set default CSV delimiter as instance attribute
        self.csv_delim = csv_delim_sel.value
        csv_delim_box = toga.Box(
            style=Pack(
                background_color=APP_DESC_BOX_BG_CLR,
            )
        )
        csv_delim_box = toga.Box(
            children=[
                csv_delim_lbl,
                csv_delim_sel,
            ],
            style=Pack(
                background_color=CSV_BOX_BG_CLR,
                padding=10,
            )
        )

        # CSV Upload
        csv_up_lbl = toga.Label(
            'Select CSV file',
            style=Pack(
                background_color=CSV_BOX_BG_CLR,
                color=CSV_UP_LBL_TXT_CLR,
                flex=1,
                padding=(6, 0, 0, 0),
                text_align='center',
            )
        )
        csv_up_btn = toga.Button(
            'Upload',
            on_press=self.csv_dlg_act,
            style=Pack(flex=1),
        )
        csv_up_box = toga.Box(
            children=[
                csv_up_lbl,
                csv_up_btn,
            ],
            style=Pack(
                background_color=CSV_BOX_BG_CLR,
                padding=(0, 10, 10, 10),
            )
        )

        # Divider
        div_box = toga.Box(
            style=Pack(
                background_color=DIV_BG_CLR,
                height=2,
                padding=(0, 60, 10, 60),
            )
        )

        # OpenDocument/Excel Worksheet References
        lbl_txt = (
            'Enter worksheet indices/names\n'
            '(e.g., 0,Sheet3,5)'
        )
        ws_refs_lbl = toga.Label(
            lbl_txt,
            style=Pack(
                background_color=WB_BOX_BG_CLR,
                color=WS_REFS_LBL_TXT_CLR,
                flex=1,
                text_align='center',
            )
        )
        ws_refs_i = toga.TextInput(
            on_change=self.set_ws_refs,
            style=Pack(flex=1, padding_top=4),
            value=0
        )
        # Set default OpenDocument/Excel worksheet references
        # as instance attribute
        self.ws_refs = ws_refs_i.value
        ws_ref_box = toga.Box(
            children=[
                ws_refs_lbl,
                ws_refs_i,
            ],
            style=Pack(
                background_color=WB_BOX_BG_CLR,
                padding=(0, 10, 10, 10),
            )
        )

        # OpenDocument/Excel Workbook Upload
        wb_up_lbl = toga.Label(
            'Select OpenDocument/Excel file',
            style=Pack(
                background_color=WB_BOX_BG_CLR,
                color=WB_UP_LBL_TXT_CLR,
                flex=1,
                padding=(6, 10, 0, 0),
                text_align='center',
            )
        )
        wb_up_btn = toga.Button(
            'Upload',
            on_press=self.wb_dlg_act,
            style=Pack(flex=1),
        )
        wb_up_box = toga.Box(
            children=[
                wb_up_lbl,
                wb_up_btn,
            ],
            style=Pack(
                background_color=WB_BOX_BG_CLR,
                padding=(0, 10, 10, 10),
            )
        )

        # Define and populate generic container
        # for main application widgets
        main_box = toga.Box(
            children=[
                app_desc_box,
                csv_delim_box,
                csv_up_box,
                div_box,
                ws_ref_box,
                wb_up_box,
            ],
            style=Pack(
                background_color=MAIN_BOX_BG_CLR,
                direction=COLUMN,
            )
        )

        # Define the main window of the application
        self.main_window = toga.MainWindow(title=self.formal_name)
        self.main_window.content = main_box
        self.main_window.show()


#############
# Functions #
#############
def std_col_names(df: pd.core.frame.DataFrame) -> pd.core.frame.DataFrame:
    '''
    Standardize DataFrame column names.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.

    Returns
    ----------------
    pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    # Obtain DataFrame column names as a list
    og_columns = df.columns.to_list()
    # Create shallow copy of og_columns list to standardize
    std_columns = og_columns.copy()
    # Create dictionary of regular expression patterns
    # and the values to replace matches with
    patterns = {
        # Turn unwanted characters to a space
        re.compile(r'\n{2,}'): r'\n',
        re.compile(r'\n'): ' ',
        # Squeeze multiple spaces to a single space
        re.compile(' {2,}'): ' ',
        # Get rid of spaces around separator characters
        re.compile(r' *- *'): '-',
        re.compile(r' *_ *'): '_',
    }

    for i in range(len(std_columns)):
        for pattern, replacement in patterns.items():
            # Identify a match
            if pattern.search(std_columns[i]):
                # Replace a match with the associated replacement
                std_columns[i] = re.sub(pattern, replacement, std_columns[i])

    for i in range(len(std_columns)):
        # Replace column name with sanitized value
        std_columns[i] = std_columns[i].lower().strip().replace(' ', '_')

    # Create dictionary of original/standardized column names
    col_names = {
        og_column: std_column
        for og_column, std_column in zip(og_columns, std_columns)
    }

    return df.rename(columns=col_names)  # Rename DataFrame columns


async def write_summ_info(sub_o_dir: str, name: str, df_info: str) -> None:
    '''
    Write pandas DataFrame summary information.

    Parameters
    ----------------
    sub_o_dir : str
        Path to write summary information to.
    name : str
        Name of text file to write.
    df_info : str
        pandas DataFrame information to write to file.
    '''
    o_path = os.path.join(
        sub_o_dir,
        f'{DATE_STAMP}-summ_{name}.txt'
    )
    logging.debug(f'Saving DataFrame summary information to "{o_path}"')
    wr_file(o_path, df_info)


def main() -> BeeWareProcessing:
    '''
    Define starting point for execution of the program.

    Returns
    ----------------
    BeeWareProcessing
        An instance of the BeeWareProcessing class.
    '''
    # Ignore deprecation warnings
    warnings.filterwarnings('ignore', category=DeprecationWarning)
    # Ignore resource warnings
    warnings.filterwarnings('ignore', category=ResourceWarning)

    # Set Matplotlib to use non-interactive backend
    mpl.use('agg')

    try:
        init_logging(LOG_PATH)  # Initialize logging
    except Exception as exc:
        err_msg = '\nLog directory creation failed with '
        err_msg += f'the following error: {exc}\n'
        print(err_msg)

        raise

    logging.debug(f'Python version: {python_version()}')

    # Disable matplotlib font manager logger
    logging.getLogger('matplotlib.font_manager').disabled = True

    logging.debug(f'Creating output directory: "{O_DIR}"')
    crt_dir(O_DIR)

    logging.debug('Creating application instance')
    return BeeWareProcessing()
