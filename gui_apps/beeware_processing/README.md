# BeeWare Processing

A demonstration of CSV, OpenDocument, and Excel file processing with BeeWare.

The BeeWare application expects a CSV, OpenDocument, or Excel file for processing. For a CSV file, a `,` or `|` are supported delimiters. For an OpenDocument/Excel file, a single worksheet or multiple worksheets can be processed. When entering multiple worksheets, either the worksheet index or name can be provided in a comma-separated list (e.g., `0,Sheet3,5`).

A summary of the file's data, twenty five random data rows, and a plot are written out to files in the output directory. For an OpenDocument/Excel file with multiple worksheets to process, a summary and plot file are created for *each* worksheet.

Assuming the processing of an example `bees.csv` file, the output directory would be structured as follows:

```console
$ tree "${HOME}/Documents/beeware_processing/output/"
/home/brutus/Documents/beeware_processing/output
└── bees
    └── 2024
        └── 02-20
            ├── 2024-02-20-data_bees.csv
            ├── 2024-02-20-plot_bees.png
            └── 2024-02-20-summ_bees.txt

4 directories, 3 files
```

## Required Packages/Modules

### Debian

```console
# apt install \
    build-essential \
    gir1.2-webkit2-4.0 \
    git \
    libcairo2-dev \
    libcanberra-gtk3-module \
    libgirepository1.0-dev \
    pkg-config \
    python3-dev \
    python3-venv
```

### Fedora

```console
# dnf install \
    cairo-gobject-devel \
    git \
    gobject-introspection-devel \
    libcanberra-gtk3 \
    pkg-config \
    python3-devel \
    rpm-build \
    webkit2gtk3
```

## Usage

Confirm that you are in the project directory:

```console
$ pwd
/home/brutus/beeware_processing/beewareprocessing
```

Run the application in developer mode:

`briefcase dev`

Create a scaffold for an application installer:

`briefcase create`

Compile/build an application:

`briefcase build`

Start the application using the packaged version of the application code:

`briefcase run`

Compile/build an application installer:

`briefcase package`

Install the built package:

**Debian**  
`# dpkg -i ex_pkg.deb`

**Fedora**  
`# rpm -i ex_pkg.rpm`

## Resources

- [BeeWare Project](https://beeware.org/project/)
    - [Tutorial](https://docs.beeware.org/en/latest/)
- [Briefcase](https://pypi.org/project/briefcase/)
    - [Briefcase Documentation](https://briefcase.readthedocs.io/en/stable/)
        - [Reference](https://briefcase.readthedocs.io/en/stable/reference/index.html)
            - [Command Reference](https://briefcase.readthedocs.io/en/stable/reference/commands/index.html)
            - [Platform Support](https://briefcase.readthedocs.io/en/stable/reference/platforms/index.html)
            - [Project Configuration Options](https://briefcase.readthedocs.io/en/stable/reference/configuration.html)
- [Toga](https://pypi.org/project/toga/)
    - [Examples](https://github.com/beeware/toga/tree/main/examples)
    - [Toga Documentation](https://toga.readthedocs.io/en/stable/)
        - [Reference](https://toga.readthedocs.io/en/latest/reference/index.html)