'''
An experimental demonstration of attended CSV file processing with PyScript.

Functions:

    * add_event() - Add an event listener.
    * std_col_names() - Standardize DataFrame column names.
    * save_to_file() - Save a string to a file.
    * cap_info_o() - Capture pandas DataFrame.info() method's output.
    * create_o_msg() - Create an output message.
    * process_df() - Process a DataFrame's content.
    * proc_arb_file() - Process an arbitrarily selected file.
    * proc_pred_file() - Process a predefined file.
    * main() - Define starting point for execution of the program.
'''
###########
# Modules #
###########
from typing import Callable  # Enable typing of callback functions
import re  # Enable regular expression matching operations
from io import StringIO  # Enable text stream using an in-memory text buffer
# Enable a standard interface to extract, format, and print stack traces
import traceback

# Enable importing of JavaScript objects in the globalThis global scope
from js import Blob, URL
# Enable creation of a JsProxy from a PyProxy
from pyodide.ffi import create_proxy, JsProxy
# Enable objects to be displayed and DOM element creation/manipulation
from pyscript import display, document
import pandas as pd  # Enable data structures and data analysis tools
# Enable objects to be displayed and DOM element creation/manipulation
import matplotlib.pyplot as plt  # Enable state-based interface to matplotlib

#############
# Variables #
#############
# Define input/output filenames
I_NAME = 'input.csv'
O_NAME = 'output.csv'

NUM_OF_DECS = 2  # Define number of decimal places for pandas output floats


#############
# Functions #
#############
def add_event(func: Callable, sel: str, evt_type: str) -> None:
    '''
    Add an event listener.

    Sets up a function that will be called whenever the specified event
    is delivered to the target.

    Parameters
    ----------------
    func : Callable
        Function to call when event is delivered to target.
    sel: str
        CSS ID of HTML element to target.
    evt_type : str
        Event to listen for.
    '''
    # Create proxy so that browser can call the callback function
    event = create_proxy(func)
    # Set the listener to the callback function
    elem = document.getElementById(sel)
    elem.addEventListener(evt_type, event)


def std_col_names(df: pd.core.frame.DataFrame) -> pd.core.frame.DataFrame:
    '''
    Standardize DataFrame column names.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.

    Returns
    ----------------
    pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    # Obtain DataFrame column names as a list
    og_columns = df.columns.to_list()
    # Create shallow copy of og_columns list to standardize
    std_columns = og_columns.copy()
    # Create dictionary of regular expression patterns
    # and the values to replace matches with
    patterns = {
        # Turn unwanted characters to a space
        re.compile(r'\n{2,}'): r'\n',
        re.compile(r'\n'): ' ',
        # Squeeze multiple spaces to a single space
        re.compile(' {2,}'): ' ',
        # Get rid of spaces around separator characters
        re.compile(r' *- *'): '-',
        re.compile(r' *_ *'): '_',
    }

    for i in range(len(std_columns)):
        for pattern, replacement in patterns.items():
            # Identify a match
            if pattern.search(std_columns[i]):
                # Replace a match with the associated replacement
                std_columns[i] = re.sub(pattern, replacement, std_columns[i])

    for i in range(len(std_columns)):
        # Replace column name with sanitized value
        std_columns[i] = std_columns[i].lower().strip().replace(' ', '_')

    # Create dictionary of original/standardized column names
    col_names = {
        og_column: std_column
        for og_column, std_column in zip(og_columns, std_columns)
    }

    return df.rename(columns=col_names)  # Rename DataFrame columns


async def save_to_file(output: str, o_name: str) -> None:
    '''
    Save a string to a file.

    Parameters
    ----------------
    output : str
        String to write to output file.
    o_name : str
        Name of output file.
    '''
    tag = document.createElement('a')
    blob = Blob.new([output], {type: 'text/plain'})
    tag.href = URL.createObjectURL(blob)
    tag.download = o_name
    tag.click()


def cap_info_o(df: pd.core.frame.DataFrame, html: bool = False) -> str:
    '''
    Capture pandas DataFrame.info() method's output.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        DataFrame to process.
    html: bool (optional)
        Whether to return HTML text or not. Defaults to False.

    Returns
    ----------------
    str
        DataFrame.info() method's output.
    '''
    buf = StringIO()
    df.info(buf=buf)

    if html:
        lines = buf.getvalue().strip().split('\n')
        header = lines[0:3]
        footer = lines[-2:]
        body = lines[3:-2]

        col_names = body[0].split()
        df_dict = {col_name: [] for col_name in col_names}

        for line in body[2:]:
            for col_name, elem in zip(df_dict.keys(), line.split()):
                df_dict[col_name].append(elem)

        info = (
            f"<p>{header[0].replace('<', '').replace('>', '')}<br />"
            f'{header[1]}<br />'
            f'{header[2]}</p>'
            '<div id="table-wrapper">'
            + pd.DataFrame(df_dict).to_html(border=0, index=False,
                                            justify='inherit')
            + '</div>'
            f'<p>{footer[0]}<br />'
            f'{footer[1]}</p>'
        )
    else:
        info = buf.getvalue()

    return info


def create_o_msg(info_html: str, df: pd.core.frame.DataFrame,
                 f_name: str | None) -> str:
    '''
    Create an HTML output message.

    Parameters
    ----------------
    info_html : str
        pandas DataFrame summary information as HTML.
    df : pd.core.frame.DataFrame
        pandas DataFrame to create output message for.
    f_name : str | None
        Either None if predefined file is to be processed, or name of
        arbitrarily selected file to process.

    Returns
    ----------------
    str
        Output string.
    '''
    if f_name is None:
        f_name = I_NAME

    output = (
        f'<p><code>{f_name}</code> processing complete.</p>'
        f'<h3>Data Summary</h3>{info_html}'
        '<h3>Data Statistics</h3>'
        '<div id="table-wrapper">'
        + df.describe().to_html(border=0, justify='inherit')
        + '</div>'
        '<h3>Data Content</h3>'
        '<div id="table-wrapper">'
        + df.to_html(border=0, justify='inherit')
        + '</div>'
        '<h3>Data Plot</h3>'
    )

    return output


async def process_df(df: pd.core.frame.DataFrame, delim: str,
                     f_name: str | None = None) \
        -> pd.core.frame.DataFrame | None:
    '''
    Process a DataFrame's content.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        DataFrame to process.
    delim : str
        Delimiter to use in output CSV file.
    f_name : str | None (optional)
        Arbitrary CSV file name. Defaults to None.

    Returns
    ----------------
    pd.core.frame.DataFrame | None
        pandas DataFrame or None.
    '''
    # Clean column names
    df = std_col_names(df)

    # Get random DataFrame rows
    if df.shape[0] > 25:
        processed_df = df.sample(25).reset_index(drop=True)
    else:
        processed_df = df

    # Save new DataFrame to a file
    output = processed_df.to_csv(sep=delim, index=False)
    await save_to_file(output, O_NAME)

    # Obtain DataFrame.info()'s output as an HTML string
    info_html = cap_info_o(processed_df, html=True)
    # Construct and insert output HTML
    output = create_o_msg(info_html, processed_df, f_name)
    document.getElementById('py-dsp').innerHTML = output

    # Obtain number of unique values per column
    uniq_cnts_dict = {
        col_name: processed_df[col_name].nunique()
        for col_name in processed_df.columns
    }
    uniq_cnts_ser = pd.Series(uniq_cnts_dict.values(),
                              index=uniq_cnts_dict.keys())

    # Create figure and Axes
    fig, ax = plt.subplots(layout='constrained')
    uniq_cnts_ser.sort_values(ascending=False).plot(kind='bar', ax=ax)

    ax.set_title('Number of Unique Values By Column')
    ax.set_xlabel('Column Name')
    ax.set_ylabel('Number of Unique Values')

    display(fig, target='py-dsp')


################
# Events Start #
################
async def proc_arb_file(event: JsProxy) -> None:
    '''
    Process an arbitrarily selected file.

    Parameters
    ----------------
    event : JsProxy
        Event to handle.
    '''
    delim = document.getElementById('arb-delim').value

    file_list = event.target.files.to_py()

    for f in file_list:
        f_name = f.name
        try:
            data = await f.text()
            df = pd.read_csv(StringIO(data), delimiter=delim)
        except Exception as exc:
            err_msg = (
                f'<p><span id="exc-msg"><code>{f_name}</code> import failed '
                f'with the following exception:</span></p><p>{exc}</p>'
                f'<p>{traceback.format_exc()}</p>'
            )
            document.getElementById('py-dsp').innerHTML = err_msg
        else:
            await process_df(df, delim, f_name)


async def proc_pred_file(event: JsProxy) -> None:
    '''
    Process a predefined file.

    Parameters
    ----------------
    event : JsProxy
        Event to handle.
    '''
    delim = document.getElementById('pred-delim').value
    try:
        df = pd.read_csv(I_NAME, delimiter=delim)
    except Exception as exc:
        err_msg = (
            f'<p><span id="exc-msg"><code>{I_NAME}</code> import failed '
            f'with the following exception:</span></p><p>{exc}</p>'
            f'<p>{traceback.format_exc()}</p>'
        )
        document.getElementById('py-dsp').innerHTML = err_msg
    else:
        await process_df(df, delim)
##############
# Events End #
##############


def main() -> None:
    '''Define starting point for execution of the program.'''
    add_event(proc_arb_file, 'select-file', 'change')

    elem = document.getElementById('app-status')
    elem.innerHTML = 'Ready'
    elem.style.color = 'green'


###########
# Program #
###########
if __name__ == '__main__':
    pd.set_option('display.float_format', lambda x: f'{x:.{NUM_OF_DECS}f}')

    main()
