# PyScript Processing

An experimental demonstration of CSV file processing with _PyScript_.

The single-page application (SPA) expects a CSV file for processing with either a `,` or `|` as the file delimiter. After choosing a delimiter, you can either select an arbitrary file from the file system to process, or process an extant file at `./assets/files/input.csv` (relative to the app's root directory).

A processing complete message, a summary of the file's data, twenty five random data rows, and a plot are shown in the _Output_ section. The randomly selected rows are also saved to an `output.csv` file in the browser's default save location.

Pyodide Version: `0.25.0`  
PyScript Version: `0.4.5`

## Usage

A web server is required to use the SPA. Python's `http.server` module can be utilized for local usage like so:

```console
$ pwd
/home/monty/py_proc
$ ls
app  README.md
$ python3 -m http.server -d 'app/'
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

## `input.csv`

A default `input.csv` is provided in the `./assets/files/` directory. It contains a random selection of 100 rows from the `tracks` table of the [Chinook Database](https://github.com/lerocha/chinook-database), which is licensed under the [Chinook Database](https://github.com/lerocha/chinook-database/blob/master/LICENSE.md) license.

## Web App Favicon

`favicon.ico` is [Python logo 01.svg](https://commons.wikimedia.org/wiki/File:Python_logo_01.svg) by [Dnu72](https://commons.wikimedia.org/wiki/User:Dnu72) and is licensed under a [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.

## Resources

- [Pyodide](https://github.com/pyodide/pyodide)
    - [Docs](https://pyodide.org/en/stable/index.html)
        - [Dealing with the file system](https://pyodide.org/en/stable/usage/file-system.html)
        - [Frequently Asked Questions](https://pyodide.org/en/stable/usage/faq.html)
        - [Loading packages](https://pyodide.org/en/stable/usage/loading-packages.html)
        - [Packages built in Pyodide](https://pyodide.org/en/stable/usage/packages-in-pyodide.html)
    - [Pyodide: Bringing the scientific Python stack to the browser](https://hacks.mozilla.org/2019/04/pyodide-bringing-the-scientific-python-stack-to-the-browser/)
- [PyScript](https://github.com/pyscript/pyscript)
    - [Home](https://pyscript.net/)
    - [Docs](https://pyscript.github.io/docs/)
    - [Anaconda Blog](https://www.anaconda.com/blog)
    - [Anaconda PyScript Community](https://community.anaconda.cloud/c/tech-topics/pyscript/41)
    - [Keynote - Peter Wang | PyCon US 2022](https://youtu.be/qKfkCY7cmBQ)
- [Emscripten](https://github.com/emscripten-core/emscripten)
    - [Docs](https://emscripten.org/docs/index.html)
    - [The LLVM Compiler Infrastructure Project](https://llvm.org/)
        - [Docs](https://llvm.org/docs/)
- [WebAssembly](https://webassembly.org/)
    - [Docs](https://developer.mozilla.org/en-US/docs/WebAssembly)