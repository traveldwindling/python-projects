# Django Demo Site

A demo site to showcase the Django web framework.

## Usage

Django includes a [lightweight development web server](https://docs.djangoproject.com/en/5.0/ref/django-admin/#runserver) that can be used to view the site:

```console
$ pwd
/home/djangopony/django_demo_site/demo_site
$ ls
art  db.sqlite3  demo_site  manage.py  pages  public  static  templates
$ python3 manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
January 13, 2024 - 00:20:15
Django version 5.0.1, using settings 'demo_site.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

The Django admin can be accessed at:

<http://127.0.0.1:8000/admin/login/>

Username: `djangopony`  
Password: `The web framework for perfectionists with deadlines`

## Web App Favicon

`favicon.ico` is [brand django.svg](https://iconduck.com/icons/154952/brand-django) by [Paweł Kuna](https://iconduck.com/designers/pawel-kuna) and is licensed under a MIT license.

## `normalize.css`

[normalize.css](https://github.com/necolas/normalize.css) by [necolas](https://github.com/necolas) is licensed under a [MIT](https://github.com/necolas/normalize.css/blob/master/LICENSE.md) license.