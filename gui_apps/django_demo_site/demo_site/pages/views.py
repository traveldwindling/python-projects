'''
Define views for the pages app.

Functions:

    * index() - Return index page response.
    * about() - Return About page response.
    * custom_404() - Return 404 page response.
'''
from django.http import Http404, HttpRequest, HttpResponse
from django.shortcuts import render


def index(request: HttpRequest) -> HttpResponse:
    '''
    Return index page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    page_title = 'A Django Demo Site'

    context = {'page_title': page_title}

    return render(request, 'pages/index.html', context)


def about(request: HttpRequest) -> HttpResponse:
    '''
    Return About page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    page_title = 'About'

    context = {'page_title': page_title}

    return render(request, 'pages/about.html', context)


def custom_404(request: HttpRequest, exception: Http404) -> HttpResponse:
    '''
    Return 404 page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.
    exception : Http404
        Django Http404 object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    page_title = 'Not Found'

    context = {'page_title': page_title}

    return render(request, 'pages/404.html', context, status=404)
