from django.urls import path

from . import views

app_name = 'pages'  # Set app name

# Define URL patterns
urlpatterns = [
    # /
    path('', views.index, name='home'),
    # /about/
    path('about/', views.about, name='about'),
]
