document.addEventListener("DOMContentLoaded", () => {
  function easterEgg() {
    selCount++;
    if (selCount == 10) {
      alert("The Birth & Death of JavaScript")
    }
  }

  let selCount = 0;
  const pageTitle = document.getElementById("subtitle");

  pageTitle.addEventListener("click", easterEgg);
});