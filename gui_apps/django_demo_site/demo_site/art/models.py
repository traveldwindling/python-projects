'''
Define models for the art app.

Classes:
    * TimestampedModel - Generate creation and modification date fields
                         for objects.
    * Artist - Define Artist model.
    * Artwork - Define Artwork model.
    * ArtworkRecommendation - Define ArtworkRecommendation model.
'''
from typing import Any  # Enable special kind of type

from django.db import models
from django.urls import reverse


class TimestampedModel(models.Model):
    '''
    Generate creation and modification date fields for objects.

    Parent Classes:

        * models.Model - Create a new instance of a model.

    Classes:

        * Meta - Give model metadata.
    '''

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        '''Give model metadata.'''

        abstract = True


class Artist(TimestampedModel):
    '''
    Define Artist model.

    Parent Classes:

        * TimestampedModel - Generate creation and modification date fields
                             for objects.

    Classes:

        * Meta - Give model metadata.

    Methods:

        * __repr__() - Return a string containing a printable representation
                       of an object.
        * __str__() - Return a string containing a nicely printable
                      representation of an object.
        * get_absolute_url() - Return the URL to access a particular instance
                               of the model.
        * save() - Override .save() method with customizations before
                   saving data to the database.
    '''

    first_name = models.CharField(
        'First Name',
        max_length=100,
        help_text='Enter artist first name'
    )
    last_name = models.CharField(
        'Last Name',
        max_length=100,
        help_text='Enter artist last name'
    )
    date_of_birth = models.DateField(
        'Date of Birth',
        help_text='Enter artist date of birth'
    )
    country_of_origin = models.CharField(
        'Country of Origin',
        max_length=200,
        help_text='Enter artist country of origin'
    )
    info_url = models.URLField(
        'Information URL',
        help_text='Enter artist information URL'
    )
    slug = models.SlugField(
        'URL Slug',
        help_text='Enter artist URL slug'
    )
    description = models.TextField(
        'Description',
        blank=True,
        help_text='Enter optional artist description'
    )

    class Meta:
        '''Give model metadata.'''

        db_table = 'artists'
        ordering = ['last_name']

    def __repr__(self) -> str:
        '''
        Return a string containing a printable representation of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'first_name={self.first_name!r}, '
            f'last_name={self.last_name!r}, '
            f'date_of_birth={self.date_of_birth!r}, '
            f'country_of_origin={self.country_of_origin!r}, '
            f'info_url={self.info_url!r}, '
            f'slug={self.slug!r}, '
            f'description={self.description!r})'
        )

    def __str__(self) -> str:
        '''
        Return a string containing a nicely printable representation
        of an object.
        '''
        return f'{self.last_name}, {self.first_name}'

    def get_absolute_url(self) -> str:
        '''Return the URL to access a particular instance of the model.'''
        return reverse('art:artist_detail', args=[self.slug])

    def save(self, *args: Any, **kwargs: Any) -> None:
        '''
        Override .save() method with customizations before
        saving data to the database.
        '''
        self.first_name = self.first_name.title()
        self.last_name = self.last_name.title()
        self.country_of_origin = self.country_of_origin.title()
        super().save(*args, **kwargs)


class Artwork(TimestampedModel):
    '''
    Define Artwork model.

    Parent Classes:

        * TimestampedModel - Generate creation and modification date fields
                             for objects.

    Classes:

        * Meta - Give model metadata.

    Methods:

        * __repr__() - Return a string containing a printable representation
                       of an object.
        * __str__() - Return a string containing a nicely printable
                      representation of an object.
        * get_absolute_url() - Return the URL to access a particular instance
                               of the model.
        * save() - Override .save() method with customizations before
                   saving data to the database.
    '''

    artist = models.ForeignKey(
        Artist,
        on_delete=models.CASCADE
    )
    title = models.CharField(
        'Title',
        max_length=200,
        help_text='Enter artwork title'
    )
    pub_date = models.DateField(
        'Publication Date',
        help_text='Enter artwork publication date'
    )
    image = models.ImageField(
        'Image',
        help_text='Add artwork image',
        upload_to='images/'
    )
    info_url = models.URLField(
        'Information URL',
        help_text='Enter artwork information URL'
    )
    slug = models.SlugField(
        'URL Slug',
        help_text='Enter artwork URL slug'
    )
    description = models.TextField(
        'Description',
        blank=True,
        help_text='Enter optional artwork description'
    )

    class Meta:
        '''Give model metadata.'''

        db_table = 'artwork'
        ordering = ['title']

    def __repr__(self) -> str:
        '''
        Return a string containing a printable representation of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'artist={self.artist!r}, '
            f'title={self.title!r}, '
            f'pub_date={self.pub_date!r}, '
            f'image={self.image!r}, '
            f'info_url={self.info_url!r}, '
            f'slug={self.slug!r}, '
            f'description={self.description!r})'
        )

    def __str__(self) -> str:
        '''
        Return a string containing a nicely printable representation
        of an object.
        '''
        return f'{self.title}'

    def get_absolute_url(self) -> str:
        '''Return the URL to access a particular instance of the model.'''
        return reverse('art:artwork_detail', args=[self.artist.slug,
                                                   self.slug])

    def save(self, *args: Any, **kwargs: Any) -> None:
        '''
        Override .save() method with customizations before
        saving data to the database.
        '''
        self.title = self.title.title()
        super().save(*args, **kwargs)


class ArtworkRecommendation(TimestampedModel):
    '''
    Define ArtworkRecommendation model.

    Parent Classes:

        * TimestampedModel - Generate creation and modification date fields
                             for objects.

    Classes:

        * Meta - Give model metadata.

    Methods:

        * __repr__() - Return a string containing a printable representation
                       of an object.
        * __str__() - Return a string containing a nicely printable
                      representation of an object.
        * save() - Override .save() method with customizations before
                   saving data to the database.
    '''

    title = models.CharField(
        'Title',
        max_length=200,
        help_text='Enter artwork title',
    )
    first_name = models.CharField(
        'First Name',
        max_length=100,
        help_text='Enter artist first name'
    )
    last_name = models.CharField(
        'Last Name',
        max_length=100,
        help_text='Enter artist last name'
    )

    class Meta:
        '''Give model metadata.'''

        db_table = 'artwork_recs'
        ordering = ['-created_at']

    def __repr__(self) -> str:
        '''
        Return a string containing a printable representation of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'first_name={self.first_name!r}, '
            f'last_name={self.last_name!r}, '
            f'title={self.title!r}'
        )

    def __str__(self) -> str:
        '''
        Return a string containing a nicely printable representation
        of an object.
        '''
        return f'{self.title}'

    def save(self, *args: Any, **kwargs: Any) -> None:
        '''
        Override .save() method with customizations before
        saving data to the database.
        '''
        self.title = self.title.title()
        self.first_name = self.first_name.title()
        self.last_name = self.last_name.title()
        super().save(*args, **kwargs)
