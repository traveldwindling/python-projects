'''
Define forms for the art app.

Classes:
    * RecArtworkForm - Define RecArtworkForm model.
'''
from django.forms import ModelForm, TextInput, ValidationError

from .models import Artwork, ArtworkRecommendation


class RecArtworkForm(ModelForm):
    '''
    Define RecArtworkForm model.

    Parent Classes:

        * ModelForm - Helper class that lets you create a Form class
                      from a Django model.

    Classes:

        * Meta - Give model metadata.

    Methods:

        * __repr__() - Return a string containing a printable representation
                       of an object.
        * __str__() - Return a string containing a nicely printable
                      representation of an object.
        * clean() - Confirm entered artwork does not already exist
                    in the collection.
    '''

    class Meta:
        '''Give form metadata.'''

        model = ArtworkRecommendation
        fields = ['title', 'first_name', 'last_name']
        widgets = {
            'title': TextInput(
                attrs={'placeholder': 'Enter artwork title...'}
            ),
            'first_name': TextInput(
                attrs={'placeholder': 'Enter artist first name...'}
            ),
            'last_name': TextInput(
                attrs={'placeholder': 'Enter artist last name...'}
            ),
        }

    def __repr__(self) -> str:
        '''
        Return a string containing a printable representation of an object.
        '''
        cls = type(self)
        return (
            f'{cls.__name__}('
            f'first_name={self.first_name!r}, '
            f'last_name={self.last_name!r}, '
            f'title={self.title!r}'
        )

    def __str__(self) -> str:
        '''
        Return a string containing a nicely printable representation
        of an object.
        '''
        return f'{self.title}'

    def clean(self) -> None:
        '''Confirm entered artwork does not already exist in the collection.'''
        cleaned_data = super().clean()

        title = cleaned_data.get('title').title()
        first_name = cleaned_data.get('first_name').title()
        last_name = cleaned_data.get('last_name').title()

        match = Artwork.objects.filter(
            title=title,
            artist__first_name=first_name,
            artist__last_name=last_name,
        )
        if match.exists():
            err_msg = (
                f'{title} by {first_name} {last_name} is '
                'already present in the collection.'
            )
            raise ValidationError(err_msg)
