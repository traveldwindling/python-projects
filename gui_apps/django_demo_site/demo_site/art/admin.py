'''
Define admin customizations.

Classes:
    * ArtistAdmin - Customize the admin interface for the Artist model.
    * ArtworkAdmin - Customize the admin interface for the Artwork model.
    * ArtworkRecommendationAdmin - Customize the admin interface for the
                                   ArtworkRecommendation model.
'''
from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse

from .models import Artist, Artwork, ArtworkRecommendation


class ArtistAdmin(admin.ModelAdmin):
    '''
    Customize the admin interface for the Artist model.

    Parent Classes:

        * admin.ModelAdmin - The representation of a model in the
                             admin interface.

    Methods:

        * crt_info_url() - Return artist information URL.
    '''

    # Define fields to display on change page
    list_display = ['last_name', 'first_name', 'crt_info_url']
    # Define fields to filter by
    list_filter = ['country_of_origin']
    # Define fields to search by
    search_fields = ['last_name', 'first_name']
    # Define field sectioning and layout
    fieldsets = [
        (
            'Required Fields',
            {
                'description': 'Required artist information',
                'fields': ['first_name', 'last_name', 'date_of_birth',
                           'country_of_origin', 'info_url', 'slug']
            }
        ),
        (
            'Optional Fields',
            {
                'description': 'Optional artist information',
                'classes': ['collapse'],
                'fields': ['description']
            }
        )
    ]

    @admin.display(description='Information URL')
    def crt_info_url(self, obj: Artist) -> str:
        '''
        Return artist information URL.

        Parameters
        ----------------
        obj : Artist
            An Artist object.

        Returns
        ----------------
        str
            The artist information URL.
        '''
        return format_html('<a href="{url}">{url}</a>', url=obj.info_url)


class ArtworkAdmin(admin.ModelAdmin):
    '''
    Customize the admin interface for the Artwork model.

    Parent Classes:

        * admin.ModelAdmin - The representation of a model in the
                             admin interface.

    Methods:

        * crt_artist_url() - Return artist change page URL.
        * crt_info_url() - Return artwork information URL.
    '''

    list_display = ['title', 'crt_artist_url', 'crt_info_url']
    list_filter = ['artist']
    search_fields = ['title', 'artist__last_name', 'artist__first_name']
    fieldsets = [
        (
            'Required Fields',
            {
                'description': 'Required artwork information',
                'fields': ['artist', 'title', 'pub_date',
                           'image', 'info_url', 'slug']
            }
        ),
        (
            'Optional Fields',
            {
                'description': 'Optional artwork information',
                'classes': ['collapse'],
                'fields': ['description']
            }
        )
    ]

    @admin.display(description='Artist')
    def crt_artist_url(self, obj: Artwork) -> str:
        '''
        Return artist change page URL.

        Parameters
        ----------------
        obj : Artwork
            An Artwork object.

        Returns
        ----------------
        str
            The artist change page URL.
        '''
        url = reverse("admin:art_artist_change", args=[obj.artist.id])
        artist_name = f'{obj.artist.last_name}, {obj.artist.first_name}'
        return format_html('<a href="{}">{}</a>', url, artist_name)

    @admin.display(description='Information URL')
    def crt_info_url(self, obj: Artwork) -> str:
        '''
        Return artwork information URL.

        Parameters
        ----------------
        obj : Artwork
            An Artwork object.

        Returns
        ----------------
        str
            The artwork information URL.
        '''
        return format_html('<a href="{url}">{url}</a>', url=obj.info_url)


class ArtworkRecommendationAdmin(admin.ModelAdmin):
    '''
    Customize the admin interface for the ArtworkRecommendation model.

    Parent Classes:

        * admin.ModelAdmin - The representation of a model in the
                             admin interface.
    '''

    list_display = ['title', 'last_name', 'first_name', 'created_at']
    list_filter = ['title']
    search_fields = ['title', 'last_name', 'first_name']
    fieldsets = [
        (
            'Required Fields',
            {
                'description': 'Required artwork information',
                'fields': ['title', 'last_name', 'first_name']
            }
        ),
    ]


# Register models and customizations
admin.site.register(Artist, ArtistAdmin)
admin.site.register(Artwork, ArtworkAdmin)
admin.site.register(ArtworkRecommendation, ArtworkRecommendationAdmin)
