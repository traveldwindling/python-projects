'''
Define feeds for the art app.

Classes:
    * ArtFeed - Generate art feed.
'''
# Enable returning a callable object that fetches attr from its operand
from operator import attrgetter
# Enable an iterator that sequentially returns elements
# until all iterators are exhausted
from itertools import chain
# Enable a single object containing all the information
# from a date object and a time object
from datetime import datetime

from django.contrib.syndication.views import Feed

from .models import Artist, Artwork


class ArtFeed(Feed):
    '''
    Generate art feed.

    Parent Classes:

        * Feed - A Python class that represents a syndication feed.

    Methods:

        * items() - Return Artist and Artwork objects
                    by creation date in descending order.
        * item_title() - Return Artist name or Artwork title.
        * item_description() - Return Artist or Artwork description,
                               or no description message.
        * item_pubdate() - Return Artist or Artwork creation date.
    '''

    title = 'Art Feed'
    link = '/art/'
    description = 'Recent art artists and artwork.'

    def items(self) -> list[Artist | Artwork]:
        '''
        Return Artist and Artwork objects
        by creation date in descending order.

        Returns
        ----------------
        list
            Artist and Artwork objects.
        '''
        artists = Artist.objects.all()
        artwork = Artwork.objects.all()

        return sorted(list(chain(artists, artwork)),
                      key=attrgetter('created_at'),
                      reverse=True)

    def item_title(self, item: Artist | Artwork) -> str:
        '''
        Return Artist name or Artwork title.

        Parameters
        ----------------
        item : Artist | Artwork
            An Artist or Artwork object.

        Returns
        ----------------
        str
            Artist name or Artwork title.
        '''
        if isinstance(item, Artist):
            return f'{item.last_name}, {item.first_name}'
        elif isinstance(item, Artwork):
            return item.title

    def item_description(self, item: Artist | Artwork) -> str:
        '''
        Return Artist or Artwork description,
        or no description message.

        Parameters
        ----------------
        item : Artist | Artwork
            An Artist or Artwork object.

        Returns
        ----------------
        str
            Description text.
        '''
        if item.description:
            return item.description
        else:
            return 'No description available.'

    def item_pubdate(self, item: Artist | Artwork) -> datetime:
        '''
        Return Artist or Artwork creation date.

        Parameters
        ----------------
        item : Artist | Artwork
            An Artist or Artwork object.

        Returns
        ----------------
        datetime
            Creation date.
        '''
        return item.created_at
