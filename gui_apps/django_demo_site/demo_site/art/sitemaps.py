'''
Define sitemaps for the art app.

Classes:
    * ArtSitemap - Generate sitemap.
'''
# Enable returning a callable object that fetches attr from its operand
from operator import attrgetter
# Enable an iterator that sequentially returns elements
# until all iterators are exhausted
from itertools import chain
# Enable a single object containing all the information
# from a date object and a time object
from datetime import datetime

from django.contrib.sitemaps import Sitemap

from .models import Artist, Artwork


class ArtSitemap(Sitemap):
    '''
    Generate sitemap.

    Parent Classes:

        * Sitemap - A Python class that represents a section of entries
                    in your sitemap.

    Methods:

        * items() - Return Artist and Artwork objects
                    by update date in descending order.
        * lastmod() - Return Artist or Artwork update date.
    '''

    changefreq = 'monthly'
    priority = 0.5

    def items(self) -> list[Artist | Artwork]:
        '''
        Return Artist and Artwork objects
        by update date in descending order.

        Returns
        ----------------
        list
            Artist and Artwork objects.
        '''
        artists = Artist.objects.all()
        artwork = Artwork.objects.all()

        return sorted(list(chain(artists, artwork)),
                      key=attrgetter('updated_at'),
                      reverse=True)

    def lastmod(self, item: Artist | Artwork) -> datetime:
        '''
        Return Artist or Artwork update date.

        Parameters
        ----------------
        item : Artist | Artwork
            An Artist or Artwork object.

        Returns
        ----------------
        datetime
            Update date.
        '''
        return item.updated_at
