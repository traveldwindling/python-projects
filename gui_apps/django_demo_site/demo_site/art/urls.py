from django.urls import path
from django.contrib.sitemaps.views import sitemap

from . import views
from .feeds import ArtFeed
from .sitemaps import ArtSitemap

app_name = 'art'  # Set app name

# Define URL patterns
urlpatterns = [
    # /art/
    path('', views.index, name='art_index'),
    # /art/artists/
    path('artists/', views.artists, name='artists'),
    # /art/artwork/
    path('artwork/', views.artwork, name='artwork'),
    # /art/search/
    path('search/', views.search, name='search'),
    # /art/feed/
    path('feed/', ArtFeed(), name="art_feed"),
    # /art/sitemap.xml
    path('sitemap.xml', sitemap,
         {'sitemaps': {'art': ArtSitemap}}, name='art_sitemap'),
    # /art/artwork-recommendation/
    path('artwork-recommendation/', views.artwork_rec,
         name='artwork_rec'),
    # /art/artwork-recommendation-success/
    path('artwork-recommendation-success/',
         views.artwork_rec_succ, name='artwork_rec_succ'),
    # /art/utagawa-hiroshige/
    path('<slug:artist_slug>/', views.artist_detail, name='artist_detail'),
    # /art/utagawa-hiroshige/nihonbashi/
    path('<slug:artist_slug>/<slug:artwork_slug>/', views.artwork_detail,
         name='artwork_detail'),
]
