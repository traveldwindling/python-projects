'''
Define views for the art app.

Functions:

    * index() - Return Art index page response.
    * artists() - Return Artists page response.
    * artwork() - Return Artwork page response.
    * artist_detail() - Return artist detail page response.
    * artwork_detail() - Return artwork detail page response.
    * search() - Return Search page response.
    * artwork_rec_succ() - Return Artwork Recommendation Success page response.
    * artwork_rec() - Return Artwork Recommendation page response.
'''
# Enable an iterator that sequentially returns elements
# until all iterators are exhausted
from itertools import chain
# Enable pseudo-random number generators for various distributions
import random

from django.shortcuts import get_list_or_404, get_object_or_404, render
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.db.models import Q
from django.urls import reverse

from .models import Artist, Artwork, ArtworkRecommendation
from .forms import RecArtworkForm


def index(request: HttpRequest) -> HttpResponse:
    '''
    Return Art index page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    page_title = 'Art'  # Set page title

    # Grab all artwork objects
    artwork_objs = get_list_or_404(Artwork.objects)
    random.shuffle(artwork_objs)  # Shuffle artwork objects

    # If more than five pieces of artwork, grab the first five
    if len(artwork_objs) > 5:
        artwork_objs = artwork_objs[:5]

    context = {
        'page_title': page_title,
        'artwork_objs': artwork_objs
    }

    return render(request, 'art/index.html', context)


def artists(request: HttpRequest) -> HttpResponse:
    '''
    Return Artists page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    page_title = 'Artists'

    artist_objs = get_list_or_404(Artist.objects)

    context = {
        'page_title': page_title,
        'artist_objs': artist_objs
    }

    return render(request, 'art/artists.html', context)


def artwork(request: HttpRequest) -> HttpResponse:
    '''
    Return Artwork page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    page_title = 'Artwork'

    artwork_objs = get_list_or_404(Artwork.objects)

    context = {
        'page_title': page_title,
        'artwork_objs': artwork_objs
    }

    return render(request, 'art/artwork.html', context)


def artist_detail(request: HttpRequest, artist_slug: str) -> HttpResponse:
    '''
    Return artist detail page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.
    artist_slug : str
        Artist URL slug.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    artist = get_object_or_404(Artist, slug=artist_slug)
    artist_artwork = get_list_or_404(Artwork.objects, artist_id=artist.id)

    page_title = f'{artist.first_name} {artist.last_name}'

    context = {
        'artist': artist,
        'artist_artwork': artist_artwork,
        'page_title': page_title
    }

    return render(request, 'art/artist-detail.html', context)


def artwork_detail(request: HttpRequest, artist_slug: str,
                   artwork_slug: str) -> HttpResponse:
    '''
    Return artwork detail page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.
    artist_slug : str
        Artist URL slug.
    artwork_slug : str
        Artwork URL slug.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    artist = get_object_or_404(Artist, slug=artist_slug)
    artwork = get_object_or_404(Artwork, slug=artwork_slug)

    page_title = artwork.title

    context = {
        'artist': artist,
        'artwork': artwork,
        'page_title': page_title
    }

    return render(request, 'art/artwork-detail.html', context)


def search(request: HttpRequest) -> HttpResponse:
    '''
    Return Search page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    query = request.GET.get('q', '')
    if query:
        query_set = (
            Q(first_name__icontains=query)
            | Q(last_name__icontains=query)
        )
        artist_rslts = Artist.objects.filter(query_set).distinct()

        query_set = Q(title__icontains=query)
        artwork_rslts = Artwork.objects.filter(query_set).distinct()

        results = list(chain(artist_rslts, artwork_rslts))
    else:
        results = []

    page_title = 'Search'

    context = {
        'query': query,
        'results': results,
        'page_title': page_title
    }

    return render(request, 'art/search.html', context)


def artwork_rec_succ(request: HttpRequest) -> HttpResponse:
    '''
    Return Artwork Recommendation Success page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    page_title = 'Artwork Recommendation Success'

    succ_msg = 'Your recommendation has been successfully recorded.'
    context = {
        'page_title': page_title,
        'succ_msg': succ_msg
    }

    return render(request, 'art/artwork-recommendation-success.html', context)


def artwork_rec(request: HttpRequest) -> HttpResponse:
    '''
    Return Artwork Recommendation page response.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    HttpResponse
        Django HttpResponse object.
    '''
    page_title = 'Artwork Recommendation'

    if request.method == 'POST':
        form = RecArtworkForm(request.POST)

        if form.is_valid():
            artwork_rec = ArtworkRecommendation()
            artwork_rec.first_name = form.cleaned_data['first_name']
            artwork_rec.last_name = form.cleaned_data['last_name']
            artwork_rec.title = form.cleaned_data['title']
            artwork_rec.save()

            return HttpResponseRedirect(reverse('art:artwork_rec_succ'))
    else:
        form = RecArtworkForm()

    context = {
        'page_title': page_title,
        'form': form
    }

    return render(request, 'art/artwork-recommendation.html', context)
