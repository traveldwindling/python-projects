'''
Define context processors.

Functions:

    * menu_urls() - Return menu URLs.
'''
from django.http import HttpRequest


def menu_urls(request: HttpRequest) -> dict[str: tuple[str, str, str, str]]:
    '''
    Return menu URLs.

    Parameters
    ----------------
    request : HttpRequest
        Django HttpRequest object.

    Returns
    ----------------
    dict
        Dictionary with menu URLs.
    '''
    urls = (
        '/about/',
        '/art/',
        '/art/artists',
        '/art/artwork',
    )
    return {'menu_urls': urls}
