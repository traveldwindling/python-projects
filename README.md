# Python Projects

This repository contains a collection of custom Python modules and apps designed to accomplish a variety of tasks.

The projects have been tested on the following GNU/Linux distributions:

- [Debian](https://www.debian.org/)
- [Fedora](https://getfedora.org/)

<details>
  <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Modules

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Required Packages/Modules</th>
      <th>Required Third-Party Packages</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="modules/dafns/">Data Analysis Functions</a></td>
      <td>A collection of data analysis-oriented functions.</td>
      <td></td>
      <td><em>ipython</em>, <em>matplotlib</em>, <em>numpy</em>, <em>odfpy</em>, <em>openpyxl</em>, <em>pandas</em></td>
    </tr>
    <tr>
      <td><a href="modules/emfns/">Email Functions</a></td>
      <td>A collection of functions to accomplish email-related tasks.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="modules/etlfns/">ETL Functions</a></td>
      <td>A collection of extract, transform, and load functions.</td>
      <td></td>
      <td><em>odfpy</em>, <em>openpyxl</em>, <em>pandas</em>, <em>pyarrow</em>, <em>SQLAlchemy</em></td>
    </tr>
    <tr>
      <td><a href="modules/iofns/">I/O Functions</a></td>
      <td>A collection of I/O-oriented functions.</td>
      <td></td>
      <td><em>odfpy</em>, <em>openpyxl</em>, <em>pandas</em></td>
    </tr>
    <tr>
      <td><a href="modules/mlcls/">Machine Learning Classes</a></td>
      <td>A collection of machine learning-oriented classes.</td>
      <td></td>
      <td><em>imbalanced-learn</em>, <em>numpy</em>, <em>pandas</em>, <em>scikit-learn</em>, <em>scipy</em></td>
    </tr>
    <tr>
      <td><a href="modules/mlfns/">Machine Learning Functions</a></td>
      <td>A collection of machine learning-oriented functions.</td>
      <td><em>dafns</em></td>
      <td><em>matplotlib</em>, <em>pandas</em>, <em>scikit-learn</em>, <em>scipy</em></td>
    </tr>
    <tr>
      <td><a href="modules/pgmspt/">Program Support Functions</a></td>
      <td>A collection of program support functions.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="modules/sysfns/">System Functions</a></td>
      <td>A collection of functions to obtain and manage system attributes.</td>
      <td></td>
      <td><em>keyring</em>, <em>psutils</em></td>
    </tr>
    <tr>
      <td><a href="modules/validators/">Validators</a></td>
      <td>A collection of functions that validate various inputs.</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

## CLI Apps

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Required Packages/Modules</th>
      <th>Required Third-Party Packages</th>
      <th>Variables to Set</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="cli_apps/coll_prods/">Collect Products</a></td>
      <td>A text-based user interface (TUI) program to maintain a collection of products and their prices.</td>
      <td>Debian: <em>sqlite3</em><br />Fedora: <em>sqlite</em></td>
      <td><em>selenium</em></td>
      <td><em>ALLOW_URLS</em>, <em>LCLE</em>, <em>O_DIR</em></td>
    </tr>
    <tr>
      <td><a href="cli_apps/crt_data_proj/">Create Data Project</a></td>
      <td>A command line interface (CLI) program to create a new data project directory tree and default Jupyter Notebook file.</td>
      <td></td>
      <td></td>
      <td><em>NB_SUBDIR</em>, <em>NB_TEMPLATE</em>, <em>PROJ_SUBDIRS</em>, <em>PROJS_DIR</em></td>
    </tr>
    <tr>
      <td><a href="cli_apps/crt_prog_proj/">Create Programming Project</a></td>
      <td>A TUI program to create a new programming project.</td>
      <td></td>
      <td></td>
      <td><em>BASH_SCR_MAP</em>, <em>BASH_SCR_URLS</em>, <em>PROJ_ED</em>, <em>PY_SCR_MAP</em>, <em>PY_SCR_URLS</em></td>
    </tr>
    <tr>
      <td><a href="cli_apps/crt_sqlite_tbl/">Create SQLite Table</a></td>
      <td>A CLI program to create a SQLite table from a CSV file or directory of CSV files.</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="cli_apps/crud_csv/">CRUD CSV</a></td>
      <td>A TUI program to perform Create, Read, Update, and Delete (CRUD) operations on comma-separated values (CSV) files.</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="cli_apps/de_pgms/">Data Engineering Programs</a></td>
      <td>A CLI program to accomplish data engineering tasks.</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="cli_apps/extr_pdf_text/">Extract PDF Text</a></td>
      <td>A TUI program to extract Portable Document Format (PDF) text.</td>
      <td>Debian: <em>ocrmypdf</em><br />Fedora: <em>ocrmypdf</em></td>
      <td><em>pdfplumber</em></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="cli_apps/extr_tb/">Extract Table</a></td>
      <td>A CLI program to extract HyperText Markup Language (HTML) tables.</td>
      <td></td>
      <td><em>bs4</em>, <em>lxml</em>, <em>requests</em></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="cli_apps/sql_alc_crud/">SQLAlchemy CRUD</a></td>
      <td>A TUI program to perform basic CRUD operations on SQLAlchemy-supported databases.</td>
      <td></td>
      <td><em>SQLAlchemy</em></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="cli_apps/sql_alc_init/">SQLAlchemy Initialization</a></td>
      <td>A CLI program to initialize an interactive SQLAlchemy session.</td>
      <td></td>
      <td><em>pandas</em>, <em>SQLAlchemy</em></td>
      <td></td>
    </tr>
  </tbody>
</table>

### Variables to Set Command

Replace `ex_*` placeholders and run:

```console
$ find \
    "${HOME}/python-projects/cli_apps" \
    -name '*.py' \
    -exec sed -i \
    -e 's/LCLE = '\'''\''/LCLE = '\''ex_lcle'\''/g' \
    -e 's/O_DIR = '\'''\''/O_DIR = '\''ex_dir'\''/g' \
    -e 's/NB_SUBDIR = '\'''\''/NB_SUBDIR = '\''ex_dir'\''/g' \
    -e 's/NB_TEMPLATE = '\'''\''/NB_TEMPLATE = '\''ex_tpl'\''/g' \
    -e 's/PROJS_DIR = '\'''\''/PROJS_DIR = '\''ex_dir'\''/g' \
    -e 's/PROJ_ED = '\'''\''/PROJ_ED = '\''ex_ed'\''/g' \
    {} +
```

The variables to set command **does not** include container variables.

## GUI Apps

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Required Packages/Modules</th>
      <th>Required Third-Party Packages</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="gui_apps/beeware_processing/">BeeWare Processing</a></td>
      <td>A demonstration of CSV, OpenDocument, and Excel file processing with BeeWare.</td>
      <td>Refer to <a href="gui_apps/beeware_processing/README.md#required-packagesmodules">project README</a></td>
      <td><em>briefcase</em>, <em>matplotlib</em>, <em>odfpy</em>, <em>openpyxl</em>, <em>pandas</em>, <em>pyarrow</em>, <em>pytest</em>, <em>toga-gtk</em></td>
    </tr>
    <tr>
      <td><a href="gui_apps/django_demo_site/">Django Demo Site</a></td>
      <td>A demo site to showcase the Django web framework.</td>
      <td></td>
      <td><em>Django</em>, <em>pillow</em></td>
    </tr>
    <tr>
      <td><a href="gui_apps/py_proc/">PyScript Processing</a></td>
      <td>An experimental demonstration of CSV file processing with <em>PyScript</em>.</td>
      <td></td>
      <td><em>js</em>, <em>matplotlib</em>, <em>pandas</em>, <em>pyodide</em>, <em>pyscript</em></td>
    </tr>
  </tbody>
</table>

## Notebooks

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Required Packages/Modules</th>
      <th>Required Third-Party Packages</th>
      <th>Variables to Set</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="mpl_exs/">Matplotlib Examples</a></td>
      <td>Jupyter notebook of Matplotlib examples.</td>
      <td></td>
      <td><em>flake8</em>, <em>ipython</em>, <em>jupyterlab</em>, <em>matplotlib</em>, <em>pandas</em>, <em>Pillow</em>, <em>pyarrow</em>, <em>pycodestyle_magic</em></td>
      <td></td>
    </tr>
  </tbody>
</table>

## Templates

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="templates/">Templates</a></td>
      <td>Python configuration/script/package templates.</td>
    </tr>
  </tbody>
</table>

## Creating a Python Development Environment

A [virtual environment](https://docs.python.org/3/tutorial/venv.html) is an excellent way to work with Python projects in an isolated environment without disrupting other Python applications running on your system. How you proceed in creating your virtual environments will depend on your GNU/Linux distribution.

**Debian**

Two packages need to be installed:

1. `python3-pip`
2. `python3-venv`

```console
# apt install \
    python3-pip \
    python3-venv
```

**Fedora**

One package needs to be installed, `python3-pip`.

`# dnf install python3-pip`

### Virtual Environment Creation Example

First, create your virtual environment directory. Here, we name this directory `python_project` and create it in our user's `${HOME}/venvs/` directory:

`python3 -m venv --upgrade-deps "${HOME}/venvs/python_project/"`

The `--upgrade-deps` `venv` option upgrades the virtual environment's core dependencies.

Next, change to the `python_project` directory and activate the virtual environment:

```console
cd "${HOME}/venvs/python_project/" &&
    . 'bin/activate'
```

Afterwards, you should see the name of the activated virtual environment in parentheses prepended to your command prompt:

`(python_project) $`

A virtual environment can be deactivated from any file system location by entering `deactivate`, or by exiting the shell from which you activated the virtual environment.

### Required Third-Party Packages Installation

The Python modules and scripts in this repository may require specific packages in order to function. If a module or script needs packages that are not in [Python's Standard Library](https://docs.python.org/3/library/index.html), it will be accompanied by a `requirements.txt` file, which contains the names and versions of the required packages.

The `requirements.txt` file can be used to install the required packages in your Python environment from the [Python Package Index (PyPI)](https://pypi.org/) like so:

```console
(python_project) $ python3 -m pip install \
    -r "${HOME}/venvs/python_project/requirements.txt"
```

If you make modifications to your Python environment and want others to be able to reliably reproduce it, you can create a `requirements.txt` file using the `pip freeze` command:

`(python_project) $ python3 -m pip freeze > 'requirements.txt'`

### Packages

Additional steps are needed to utilize Python packages in this repository. Depending on the package and your preferences, there are two approaches that can be taken:

- Build and install the package with `build` and `pip`, respectively.
- Install the package with `pipx`.

#### `build` and  `pip`

##### Install a Python Build Frontend

After [creating and activating a virtual environment](#virtual-environment-creation-example) for the project, install `build`:

`(ex_pkg) $ python3 -m pip install build`

##### Editable Install

To be able to make modifications to the package's source code and test the changes, try an [editable install](https://pip.pypa.io/en/stable/topics/local-project-installs/#editable-installs):


`(ex_pkg) $ python3 -m pip install -e ex_pkg_dir`

##### Building Distributions

To use the package, as is, build its [source distribution (sdist)](https://packaging.python.org/en/latest/glossary/#term-Source-Distribution-or-sdist) and [binary distribution (wheel)](https://packaging.python.org/en/latest/glossary/#term-Wheel):

`(ex_pkg) $ python3 -m build ex_pkg_dir`

##### Package Installation

Install either the sdist or wheel:

`(ex_pkg) $ python3 -m pip install ex_pkg_dir/dist/ex_pkg.tar.gz`

`(ex_pkg) $ python3 -m pip install ex_pkg_dir/dist/ex_pkg.whl`

#### `pipx`

If a package is a command-line tool, it may be preferable to install it using [`pipx`](https://pypi.org/project/pipx/):

**Debian**

```console
# apt install pipx &&
    pipx ensurepath
```

**Fedora**

`# dnf install pipx`

**Common**

`pipx install ex_pkg_dir`

## Project Avatar

`logo.png` is [Python icon (black and white).svg](https://commons.wikimedia.org/wiki/File:Python_icon_(black_and_white).svg) by [Waldir](https://commons.wikimedia.org/wiki/User:Waldir) and is licensed under a [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.