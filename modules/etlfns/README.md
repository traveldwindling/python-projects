# ETL Functions

A collection of extract, transform, and load functions.

```python
# Enable collection of extract, transform, and load functions
import etlfns
```

<details>
  <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## `crt_batches()`

Return a generator iterator of batches from a collection.

```python
x_men_97 = [
    'Trust happens when actions meets words.',
    'Blood is blood. Family is a choice.',
    'The offer was made. The door is open.',
    ('Make them mind your weather, sister.\n\n'
     'And them weather your mind.'),
    ('None choose to be born, Bastion. '
     'Thus why we must never begrudge them being.'),
    'The only ones who can break our heart are those kept in it.',
    'Humanity would rather die than have kids like us.'
]
batches = etlfns.crt_batches(x_men_97, 2)
```

## `clean_sql_stmt()`

Clean a SQL statement.

```python
stmt = '''\
-- Destruction leads to a very rough road, but it also breeds creation 
SELECT
    *
FROM
    Track
WHERE
    Composer = 'Red Hot Chili Peppers';\
'''
cln_stmt = etlfns.clean_sql_stmt(stmt)
```

## `sl_crt_conn()`

Connect to a SQLite database file.

```python
f_path = '/home/comcomly/chinook.sqlite'
conn = etlfns.sl_crt_conn(f_path)
```

## `sl_exec_qry()`

Execute a SQLite SQL query.

```python
f_path = '/home/comcomly/chinook.sqlite'
conn = etlfns.sl_crt_conn(f_path)

query = (
    'SELECT '
    '* '
    'FROM '
    'Album;'
)
result = etlfns.sl_exec_qry(conn, query)

# Parameter substitution
query = (
    'SELECT '
    'PlaylistId, '
    'Name '
    'FROM '
    'Playlist '
    "WHERE "
    "PlaylistId = :PlaylistId "
    "OR Name = :Name;"
)
param_subs = {
    'PlaylistId': 12,
    'Name': 'Classical'
}
result = etlfns.sl_exec_qry(conn, query, param_subs=param_subs)
```

## `sl_get_db_md()`

Get SQLite database metadata.

```python
f_path = '/home/comcomly/chinook.sqlite'
conn = etlfns.sl_crt_conn(f_path)

db_md = etlfns.sl_get_db_md(conn)
```

## `sl_view_tbl()`

View SQLite table in the system pager.

```python
f_path = '/home/comcomly/chinook.sqlite'
conn = etlfns.sl_crt_conn(f_path)

db_md = etlfns.sl_get_db_md(conn)
etlfns.sl_view_tbl(conn, 'Track', db_md['Track'])
```

## `sl_exp_tbl_csv()`

Export a SQLite table to a CSV file.

```python
f_path = '/home/comcomly/chinook.sqlite'
conn = etlfns.sl_crt_conn(f_path)

db_md = etlfns.sl_get_db_md(conn)

f_path = '/home/comcomly/data/genre.csv'
etlfns.sl_exp_tbl_csv(conn, 'Genre', db_md['Genre'], f_path)

# Custom delimiter
f_path = '/home/comcomly/data/genre_comma.csv'
etlfns.sl_exp_tbl_csv(conn, 'Genre', db_md['Genre'], f_path, delim=',')
```

## `crt_eng()`

Create a SQLAlchemy engine instance.

```python
f_path = '/home/comcomly/chinook.sqlite'
engine = etlfns.crt_eng('sqlite', database=f_path)
```

## `get_db_md()`

Get database metadata.

```python
f_path = '/home/comcomly/chinook.sqlite'
engine = etlfns.crt_eng('sqlite', database=f_path)

md, db_md = etlfns.get_db_md(engine)
```

## `crt_n_type_map()`

Create a column name to pandas data type mapping.

```python
album = md.tables['Album']

name_type_map = etlfns.crt_n_type_map(album)
```

## `exec_txt_qry()`

Execute a text-based SQL query.

```python
f_path = '/home/comcomly/chinook.sqlite'
engine = etlfns.crt_eng('sqlite', database=f_path)

query = (
    'SELECT '
    '* '
    'FROM '
    'Album;'
)
result = etlfns.exec_txt_qry(engine, query)
```

## `exec_obj_qry()`

Execute an object-based SQL query.

```python
f_path = '/home/comcomly/chinook.sqlite'
engine = etlfns.crt_eng('sqlite', database=f_path)

md, db_md = etlfns.get_db_md(engine)

album = md.tables['Album']
query = (
    sa.select(album.c.AlbumId, album.c.Title)
      .select_from(album)
)
result = etlfns.exec_obj_qry(engine, query)
cols, rows = list(result.keys()), result.all()
```

## `view_tbl()`

View a database table.

```python
f_path = '/home/comcomly/chinook.sqlite'
engine = etlfns.crt_eng('sqlite', database=f_path)

md, db_md = etlfns.get_db_md(engine)

album = md.tables['Artist']
etlfns.view_tbl(engine, album)
```

## `imp_files_pd()`

Return a generator iterator of pandas DataFrames created from a directory of files.

```python
dir_path = '/home/comcomly/data'
dfs = etlfns.imp_files_pd(dir_path, 'csv')

# Keyword Arguments
dir_path = '/home/comcomly/data'
rdr_kwargs = {'sep': '|', 'dtype': 'string[pyarrow]'}
dfs = etlfns.imp_files_pd(dir_path, 'csv', rdr_kwargs=rdr_kwargs)
```

## `exp_pd_dfs()`

Export a generator iterator of pandas DataFrames to a file.

```python
dir_path = '/home/comcomly/data'
dfs = etlfns.imp_files_pd(dir_path, 'csv')

f_path = '/home/comcomly/results.csv'
etlfns.exp_pd_dfs(dfs, f_path)
```

## `exp_rslt_csv()`

Export a result set to a CSV file.

```python
f_path = '/home/comcomly/artist.csv'
etlfns.exp_rslt_csv(f_path, cols, rslt_set)
```

## `exp_rslt_html()`

Export a result set to an HTML file.

```python
f_path = '/home/comcomly/artist.html'
etlfns.exp_rslt_html(f_path, cols, rslt_set)
```

## `exp_rslt_wb()`

Export a result set to a workbook file.

```python
f_path = '/home/comcomly/artist.ods'
etlfns.exp_rslt_wb(f_path, cols, rslt_set)

# Custom sheet name and engine
f_path = '/home/comcomly/artist.xlsx'
etlfns.exp_rslt_wb(f_path, cols, rslt_set,
                   sheet_name='artist', engine='openpyxl')
```