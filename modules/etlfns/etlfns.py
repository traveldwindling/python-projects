'''
A collection of extract, transform, and load functions.

Functions:

    * crt_batches() - Return a generator iterator of batches from a collection.
    * clean_sql_stmt() - Clean a SQL statement.
    * sl_crt_conn() - Connect to a SQLite database file.
    * sl_exec_qry() - Execute a SQLite SQL query.
    * sl_get_db_md() - Get SQLite database metadata.
    * sl_view_tbl() - View SQLite table in the system pager.
    * sl_exp_tbl_csv() - Export a SQLite table to a CSV file.
    * crt_eng() -  Create a SQLAlchemy engine instance.
    * get_db_md() - Get database metadata.
    * crt_n_type_map() - Create a column name to pandas data type mapping.
    * exec_txt_qry() - Execute a text-based SQL query.
    * exec_obj_qry() - Execute an object-based SQL query.
    * view_tbl() - View a database table.
    * imp_files_pd() - Return a generator iterator of pandas DataFrames
                       created from a directory of files.
    * exp_pd_dfs() - Export a generator iterator of pandas DataFrames
                     to a file.
    * exp_rslt_csv() - Export a result set to a CSV file.
    * exp_rslt_html() - Export a result set to an HTML file.
    * exp_rslt_wb() - Export a result set to a workbook file.

Requires
----------------
odfpy
    Python API and tools to manipulate OpenDocument files
openpyxl
    A Python library to read/write Excel 2010 xlsx/xlsm files
pandas
    Powerful data structures for data analysis, time series, and statistics
pyarrow
    Python library for Apache Arrow
SQLAlchemy
    Python SQL Toolkit and Object Relational Mapper.
'''
###########
# Modules #
###########
# Enable abstract base class for classes that provide
# the .__iter__() and .__next__() methods
from collections.abc import Iterator
# Enable classes to read and write tabular data in CSV format
import csv
# Enable returning a possibly empty list of path names that match pathname,
# which must be a string containing a path specification
from glob import glob
# Enable creation of an iterator that returns
# selected elements from the iterable
from itertools import islice
# Enable a portable way of using operating system dependent functionality
import os
# Enable a class that represents concrete paths of the system's path flavor
from pathlib import Path
import pydoc  # Enable automatic documentation generation from Python modules
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable a SQL interface compliant with the DB-API 2.0 specification
import sqlite3
from typing import Any  # Enable special kind of type

import pandas as pd  # Enable data structures and data analysis tools
import sqlalchemy as sa  # Enable database abstraction library


#############
# Functions #
#############
def crt_batches(coll: list | tuple | dict | set,
                bat_size: int) -> Iterator[tuple]:
    '''
    Return a generator iterator of batches from a collection.

    Parameters
    ----------------
    coll : list | tuple | dict | set
        Collection object to make batches from.
    bat_size : int
        Maximum number of items to place in each batch.

    Returns
    ----------------
    Iterator
        Iterator of tuple batches.
    '''
    it = iter(coll)
    while True:
        batch = tuple(islice(it, bat_size))

        if not batch:
            return
        else:
            yield batch


def clean_sql_stmt(stmt: str) -> str:
    '''
    Clean a SQL statement.

    Several transformations are performed to reduce the input string
    into a single-line, clean SQL statement.

    Parameters
    ----------------
    stmt : str
        SQL statement to clean.

    Returns
    ----------------
    str
        Cleaned SQL statement.
    '''
    transforms = [
        # Remove semicolons
        (';', ''),
        # Remove comments
        (r'/\*[^+]([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/', ''),
        (r'--[^+].*', ''),
        # Replace line breaks and spaces with single space
        (r'\s+', ' ')
    ]

    for pattern, replacement in transforms:
        stmt = re.sub(pattern, replacement, stmt)

    return stmt.strip()


def sl_crt_conn(f_path: str) -> sqlite3.Connection:
    '''
    Connect to a SQLite database file.

    Parameters
    ----------------
    f_path : str
        SQLite database file path.

    Returns
    ----------------
    sqlite3.Connection
        sqlite3 connection object.
    '''
    conn = sqlite3.connect(f_path)

    return conn


def sl_exec_qry(conn: sqlite3.Connection, query: str,
                param_subs: dict | None = None) -> list[tuple] | None:
    '''
    Execute a SQLite SQL query.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    query : str
        A SQL query.
    param_subs : dict | None (optional)
        Parameter substitution name/value pairs, where the parameter name
        is the dictionary key, and the value is the parameter value.
        Defaults to None.

    Returns
    ----------------
    list | None
        List of result rows or None.

    Examples
    ----------------
    # Enable a SQL interface compliant with the DB-API 2.0 specification
    import sqlite3

    # Create database connection
    conn = sqlite3.connect('/home/comcomly/chinook.sqlite')

    query = (
        'SELECT '
        '* '
        'FROM '
        'Album'
    )
    result = sl_exec_qry(conn, query)

    # Select rows by playlist ID or name criteria from Playlist table
    query = (
        'SELECT '
        'PlaylistId, '
        'Name '
        'FROM '
        'Playlist '
        "WHERE "
        "PlaylistId = :PlaylistId "
        "OR Name = :Name"
    )
    param_subs = {
        'PlaylistId': 12,
        'Name': 'Classical'
    }
    result = sl_exec_qry(conn, query, param_subs=param_subs)
    '''
    if param_subs is None:
        param_subs = {}

    cur = conn.cursor()

    with conn:
        result = cur.execute(query, param_subs).fetchall()

    cur.close()

    return result


def sl_get_db_md(conn: sqlite3.Connection) -> dict[str, list[str]]:
    '''
    Get SQLite database metadata.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.

    Returns
    ----------------
    dict
        Dictionary of database metadata, where the keys are the table
        names and the values are a list of table column names.
    '''
    query = (
        'SELECT '
        'name '
        'FROM '
        'sqlite_master '
        "WHERE "
        "type='table'"
    )
    cur = conn.cursor()
    with conn:
        tbl_tuples = cur.execute(query).fetchall()
    cur.close()

    tbl_names = [
        tbl_name
        for tbl_tuple in tbl_tuples
        for tbl_name in tbl_tuple
    ]

    db_md = {}
    for tbl_name in tbl_names:
        query = (
            'PRAGMA '
            f'table_info({tbl_name})'
        )
        cur = conn.cursor()
        with conn:
            result = cur.execute(query).fetchall()
        cur.close()

        db_md[tbl_name] = []
        for row in result:
            db_md[tbl_name].append(row[1])

    return db_md


def sl_view_tbl(conn: sqlite3.Connection, tbl_name: str,
                tbl_cols: list[str]) -> None:
    '''
    View SQLite table in the system pager.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    tbl_name : str
        Table name to view.
    tbl_cols : list
        Names of table's columns.
    '''
    query = (
        'SELECT '
        '* '
        'FROM '
        f'{tbl_name}'
    )

    cur = conn.cursor()

    with conn:
        result = cur.execute(query).fetchall()

    cur.close()

    # Determine max length of column info to use in output string
    max_len = max([
        len(f'{col_name}: ')
        for col_name in tbl_cols
    ])
    output = ''  # Create output string

    for row in result:
        for col_i, col_name in enumerate(tbl_cols):
            output += (
                f"\n{col_name + ': ':{max_len}}"
                f'{str(row[col_i])}'
            )
        output += '\n'

    pydoc.pager(output.rstrip())  # Send final string to paginator


def sl_exp_tbl_csv(conn: sqlite3.Connection, tbl_name: str,
                   tbl_cols: list[str], f_path: str,
                   enc: str = 'utf-8', delim: str = '|') -> None:
    '''
    Export a SQLite table to a CSV file.

    Parameters
    ----------------
    conn : sqlite3.Connection
        sqlite3 connection object.
    tbl_name : str
        Table name to view.
    tbl_cols : list
        Names of table's columns.
    f_path : str
        Path to save CSV file to.
    enc : str (optional)
        Encoding of the CSV file to write. Defaults to utf-8.
    delim : str (optional)
        Delimiter to use in the CSV file to read. Defaults to a |.
    '''
    query = (
        'SELECT '
        '* '
        'FROM '
        f'{tbl_name}'
    )

    cur = conn.cursor()

    with conn:
        result = cur.execute(query).fetchall()

    cur.close()

    with open(f_path, 'w', encoding=enc, newline='') as f:
        writer = csv.writer(f, delimiter=delim)
        writer.writerow(tbl_cols)  # Write headings row
        for row in result:
            writer.writerow(row)


def crt_eng(drivername: str, username: str | None = None,
            password: str | None = None, host: str | None = None,
            port: int | None = None, database: str | None = None,
            query: dict[str, str | tuple[str]] = {}) -> sa.engine.Engine:
    '''
    Create a SQLAlchemy engine instance.

    Parameters
    ----------------
    drivername : str
        Database backend and driver name.
    username : str (optional)
        Database username. Defaults to None.
    password : str (optional)
        Database user password. Defaults to None.
    host : str (optional)
        Hostname for host serving the database. Defaults to None.
    port : int (optional)
        Database port number. Defaults to None.
    database : str (optional)
        Database name. Defaults to None.
    query : dict (optional)
        A mapping representing the query string.
        Defaults to an empty dictionary.

    Returns
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    '''
    # Create URL object
    url = sa.URL.create(
        drivername=drivername,
        username=username,
        password=password,
        host=host,
        port=port,
        database=database,
        query=query
    )

    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Create connection context to verify connection
    with engine.connect():
        pass

    return engine


def get_db_md(engine: sa.engine.Engine, schema: str = None,
              views: bool = False) \
        -> tuple[sa.sql.schema.MetaData, dict[str, list[str]]]:
    '''
    Get database metadata.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    schema : str (optional)
        Database schema. Defaults to None.
    views : bool (optional)
        Whether to also reflect views or not. Defaults to False.

    Returns
    ----------------
    tuple
        A collection of Table objects and their associated schema constructs,
        and a dictionary of database metadata, where the keys are the
        table names and the values are a list of table column names.
    '''
    # Create MetaData container
    md = sa.MetaData(schema=schema)
    # Load all available table definitions from the database
    md.reflect(bind=engine, views=views)

    db_md = {}
    for tbl_name in sorted(md.tables.keys()):
        db_md[tbl_name] = sorted(md.tables[tbl_name].c.keys())

    return md, db_md


def crt_n_type_map(tbl_obj: sa.sql.schema.Table) -> dict[str, str]:
    '''
    Create a column name to pandas data type mapping.

    Parameters
    ----------------
    tbl_obj : sa.sql.schema.Table
        SQLAlchemy Table object to create mapping from.

    Returns
    ----------------
    dict
        Dictionary where the keys are the column names and the values
        are the pandas data types.
    '''
    name_type_map = {}

    for col_obj in tbl_obj.c:
        # Define and run type tests
        bool_type = (isinstance(col_obj.type.as_generic(), sa.types.Boolean))
        dt_type = (
            isinstance(col_obj.type.as_generic(), sa.types.Date)
            or isinstance(col_obj.type.as_generic(), sa.types.DateTime)
        )
        float_type = (
            isinstance(col_obj.type.as_generic(), sa.types.Double)
            or isinstance(col_obj.type.as_generic(), sa.types.Float)
            or isinstance(col_obj.type.as_generic(), sa.types.Numeric)
        )
        int_type = (
            isinstance(col_obj.type.as_generic(), sa.types.BigInteger)
            or isinstance(col_obj.type.as_generic(), sa.types.Integer)
            or isinstance(col_obj.type.as_generic(), sa.types.SmallInteger)
        )
        obj_type = (
            isinstance(col_obj.type.as_generic(), sa.types.Enum)
            or isinstance(col_obj.type.as_generic(), sa.types.LargeBinary)
            or isinstance(col_obj.type.as_generic(), sa.types.MatchType)
            or isinstance(col_obj.type.as_generic(), sa.types.PickleType)
            or isinstance(col_obj.type.as_generic(), sa.types.SchemaType)
        )
        str_type = (
            isinstance(col_obj.type.as_generic(), sa.types.Interval)
            or isinstance(col_obj.type.as_generic(), sa.types.String)
            or isinstance(col_obj.type.as_generic(), sa.types.Text)
            or isinstance(col_obj.type.as_generic(), sa.types.Time)
            or isinstance(col_obj.type.as_generic(), sa.types.Unicode)
            or isinstance(col_obj.type.as_generic(), sa.types.UnicodeText)
            or isinstance(col_obj.type.as_generic(), sa.types.Uuid)
        )

        # Map generic types to pandas types
        if bool_type:
            name_type_map[col_obj.name] = 'boolean'
        elif dt_type:
            name_type_map[col_obj.name] = 'datetime64[ns]'
        elif float_type:
            name_type_map[col_obj.name] = 'float64'
        elif int_type:
            name_type_map[col_obj.name] = 'Int64'
        elif obj_type:
            name_type_map[col_obj.name] = 'object'
        elif str_type:
            name_type_map[col_obj.name] = 'string[pyarrow]'

    return name_type_map


def exec_txt_qry(engine: sa.engine.Engine, query: str,
                 data: list[dict] | None = None,
                 param_subs: dict | None = None) \
        -> sa.engine.cursor.CursorResult:
    '''
    Execute a text-based SQL query.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    query : str
        A text-based SQL query.
    data : list | None (optional)
        List of dictionaries where each dictionary represents a row of
        data to insert. The dictionary keys are the column names,
        and the values are the column values. Defaults to None.
    param_subs : dict | None (optional)
        Parameter substitution name/value pairs, where the parameter name
        is the dictionary key, and the value is the parameter value.
        Defaults to None.

    Returns
    ----------------
    sa.engine.cursor.CursorResult
        SQLAlchemy cursor result.

    Examples
    ----------------
    import sqlalchemy as sa  # Enable database abstraction library

    # Create URL object
    url = sa.URL.create('sqlite', database='/home/comcomly/chinook.sqlite')
    # Create a new engine instance
    engine = sa.create_engine(url, pool_pre_ping=True)

    # Select random subset of rows from Track table
    query = (
        'SELECT '
        '* '
        'FROM '
        'Track '
        'ORDER BY RANDOM() '
        'LIMIT 10'
    )
    result = exec_txt_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    # Insert new genres into Genre table
    query = (
        'INSERT INTO '
        'Genre '
        '(GenreId, Name) '
        "VALUES "
        "(:GenreId, :Name)"
    )
    data = [
        {
            'GenreId': '26',
            'Name': 'Chiptune'
        },
        {
            'GenreId': '27',
            'Name': 'Trip Hop'
        }
    ]
    result = exec_txt_qry(engine, query, data=data)

    # Select rows by playlist ID or name criteria from Playlist table
    query = (
        'SELECT '
        'PlaylistId, '
        'Name '
        'FROM '
        'Playlist '
        "WHERE "
        "PlaylistId = :PlaylistId "
        "OR Name = :Name"
    )
    param_subs = {
        'PlaylistId': 12,
        'Name': 'Classical'
    }
    result = exec_txt_qry(engine, query, param_subs=param_subs)
    cols, rows = list(result.keys()), result.all()
    '''
    with engine.begin() as conn:
        if data:
            result = conn.execute(sa.text(query), data)
        else:
            result = conn.execute(sa.text(query), param_subs)

    return result


def exec_obj_qry(engine: sa.engine.Engine,
                 query: sa.sql.dml.Insert | sa.sql.selectable.Select
                 | sa.sql.dml.Update | sa.sql.dml.Delete,
                 data: list[dict] | None = None) \
        -> sa.engine.cursor.CursorResult:
    '''
    Execute an object-based SQL query.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    query : sa.sql.dml.Insert | sa.sql.selectable.Select
            | sa.sql.dml.Update | sa.sql.dml.Delete
        An object-based SQL query.
    data : list | None (optional)
        List of dictionaries where each dictionary represents a row of
        data to insert. The dictionary keys are the column names,
        and the values are the column values. Defaults to None.

    Returns
    ----------------
    sa.engine.cursor.CursorResult
        SQLAlchemy cursor result.

    Examples
    ----------------
    import sqlalchemy as sa

    # Create a new engine instance
    engine = crt_eng('sqlite', database='/home/comcomly/chinook.sqlite')

    md, db_md = get_db_md(engine)

    album = md.tables['Album']  # Create table object for Album table
    # Select specific columns from Album table
    query = (
        sa.select(album.c.AlbumId, album.c.Title)
          .select_from(album)
    )
    result = exec_obj_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    # Select specific rows from Album table
    query = (
        sa.select('*')
          .select_from(album)
          .where(album.c.ArtistId == 8)
    )
    result = exec_obj_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    track = md.tables['Track']
    # Select random subset of rows from Track table
    query = (
        sa.select('*')
          .select_from(track)
          .order_by(sa.func.random())
          .limit(10)
    )
    result = exec_obj_qry(engine, query)
    cols, rows = list(result.keys()), result.all()

    genre = md.tables['Genre']
    # Insert new rows into Genre table
    query = sa.insert(genre)
    data = [
        {'GenreId': '29', 'Name': 'Jazz Rock'},
        {'GenreId': '30', 'Name': 'Swing'}
    ]
    result = exec_obj_qry(engine, query, data=data)
    '''
    with engine.begin() as conn:
        result = conn.execute(query, data)

    return result


def view_tbl(engine: sa.engine.Engine,
             tbl_obj: sa.sql.schema.Table) -> None:
    '''
    View a database table.

    Parameters
    ----------------
    engine : sa.engine.Engine
        Connects a Pool and Dialect together to provide a source of database
        connectivity and behavior.
    tbl_obj : sa.sql.schema.Table
        Instance of a Table object.
    '''
    # Create connection context
    with engine.begin() as conn:
        query = sa.select(tbl_obj)  # Define query
        result = conn.execute(query)  # Obtain query results

        # Determine max length of column info to use in output string
        max_len = max([
            len(f'{col_name}: ')
            for col_name in tbl_obj.c.keys()
        ])
        output = ''  # Create output string

        for row in result:
            for col_i, col_name in enumerate(tbl_obj.c.keys()):
                output += (
                    f"\n{col_name + ': ':{max_len}}"
                    f'{str(row[col_i])}'
                )
            output += '\n'

        pydoc.pager(output.rstrip())  # Send final string to paginator


def imp_files_pd(dir_path: str, ext: str,
                 rdr_kwargs: dict[str, Any] | None = None) \
        -> Iterator[pd.core.frame.DataFrame]:
    '''
    Return a generator iterator of pandas DataFrames created from
    a directory of files.

    Parameters
    ----------------
    dir_path : str
        Path to directory of files to import.
    ext : str
        Extension of directory files (e.g., parquet, csv, ods).
    rdr_kwargs: dict | None (optional)
        Dictionary of keyword arguments to pass to reading function.
        Defaults to None.

    Returns
    ----------------
    Iterator
        Iterator of pandas DataFrames.
    '''
    # Obtain file paths
    file_paths = sorted(glob(os.path.join(dir_path, f'*.{ext}')))

    # Set reader function based on file extension
    match ext:
        case 'parquet':
            rdr_fn = pd.read_parquet
        case 'csv':
            rdr_fn = pd.read_csv
        case _:
            rdr_fn = pd.read_excel

    dfs = (
        rdr_fn(file_path, **rdr_kwargs)
        if rdr_kwargs
        else
        rdr_fn(file_path)
        for file_path in file_paths
    )

    return dfs


def exp_pd_dfs(dfs: Iterator[pd.core.frame.DataFrame], f_path: str,
               wtr_kwargs: dict[str, Any] | None = None) -> None:
    '''
    Export a generator iterator of pandas DataFrames to a file.

    Parameters
    ----------------
    dfs : Iterator
        Iterator of pandas DataFrames.
    f_path : str
        File path to write output file to.
    wtr_kwargs: dict | None (optional)
        Dictionary of keyword arguments to pass to writing function.
        Defaults to None.
    '''
    if os.path.exists(f_path):
        os.unlink(f_path)

    path = Path(f_path)
    ext = path.suffix[1:]
    csv_output = ext == 'csv'

    if not csv_output:
        start_row = 0

    for df in dfs:
        # If file exists, assume a headers row and do not rewrite headers
        hdrs_test = not os.path.exists(f_path)

        if csv_output:
            if wtr_kwargs:
                df.to_csv(f_path, mode='a', index=False,
                          header=hdrs_test, **wtr_kwargs)
            else:
                df.to_csv(f_path, mode='a', index=False, header=hdrs_test)
        else:
            file_exists = os.path.exists(f_path)

            with pd.ExcelWriter(
                f_path,
                engine='openpyxl',
                mode='a' if file_exists else 'w',
                if_sheet_exists='overlay' if file_exists else None
            ) as writer:
                if wtr_kwargs:
                    df.to_excel(writer, index=False, header=hdrs_test,
                                startrow=start_row, **wtr_kwargs)
                else:
                    df.to_excel(writer, index=False, header=hdrs_test,
                                startrow=start_row)

            if start_row == 0:
                start_row += df.shape[0] + 1
            else:
                start_row += df.shape[0]


def exp_rslt_csv(f_path: str, cols: list[str], rslt_set: list[tuple],
                 enc: str = 'utf-8', delim: str = '|') -> None:
    '''
    Export a result set to a CSV file.

    Parameters
    ----------------
    f_path : str
        Output file path to export the CSV file to (e.g., '/tmp/results.csv').
    cols : list
        List of column names.
    rslt_set : list
        Result set where each tuple in the list is a row of data.
    enc : str (optional)
        Encoding to use for CSV file. Defaults to utf-8.
    delim : str (optional)
        Delimiter to use in CSV file. Defaults to a |.
    '''
    df = pd.DataFrame(rslt_set, columns=cols)

    df.to_csv(f_path, index=False, encoding=enc, sep=delim)


def exp_rslt_html(f_path: str, cols: list[str], rslt_set: list[tuple],
                  enc: str = 'utf-8') -> None:
    '''
    Export a result set to an HTML file.

    Parameters
    ----------------
    f_path : str
        Output file path to export the HTML file to
        (e.g., '/tmp/results.html').
    cols : list
        List of column names.
    rslt_set : list
        Result set where each tuple in the list is a row of data.
    enc : str (optional)
        Encoding to use for HTML file. Defaults to utf-8.
    '''
    df = pd.DataFrame(rslt_set, columns=cols)

    html_tbl = df.to_html(index=False, justify='justify-all')

    with open(f_path, 'w', encoding=enc, newline='') as f:
        f.write(html_tbl)


def exp_rslt_wb(f_path: str, cols: list[str], rslt_set: list[tuple],
                sheet_name: str = 'Sheet1', engine: str = 'odf') -> None:
    '''
    Export a result set to a workbook file.

    Parameters
    ----------------
    f_path : str
        Output file path to export the HTML file to
        (e.g., '/tmp/results.html').
    cols : list
        List of column names.
    rslt_set : list
        Result set where each tuple in the list is a row of data.
    sheet_name : str (optional)
        Name of workbook sheet to save result to. Defaults to Sheet1.
    engine : str (optional)
        Write engine to use (e.g., 'odf', 'openpyxl'). Defaults to odf.
    '''
    df = pd.DataFrame(rslt_set, columns=cols)

    df.to_excel(f_path, index=False, sheet_name=sheet_name, engine=engine)
