# System Functions

A collection of functions to obtain and manage system attributes.

```python
# Enable functions to obtain and manage system attributes
import sysfns
```

## `set_krg_entry()`

Set a keyring entry.

`sysfns.set_krg_entry('system')`

## `get_krg_entry()`

Get a keyring entry.

`username, password = sysfns.get_krg_entry('system', 'guido')`

## `get_krg_srv_info()`

Get system keyring service information.

`cfg_root, kr = sysfns.get_krg_srv_info()`

## `get_locale()`

Get the current setting for the given locale category as a sequence containing the language code and encoding.

`lang_c, lang_enc = sysfns.get_locale()`

## `get_lcl_convs()`

Get local conventions.

`lcl_convs = sysfns.get_lcl_convs()`

## `get_locales()`

Get locales.

`locales = sysfns.get_locales()`

## `scale_bytes()`

Scale bytes to proper format.

`scaled_bytes = sysfns.scale_bytes(4096)`

## `get_sys_info()`

Get system information.

`print(sysfns.get_sys_info())`