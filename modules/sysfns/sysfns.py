'''
A collection of functions to obtain and manage system attributes.

Functions:

    * set_krg_entry() - Set a keyring entry.
    * get_krg_entry() - Get a keyring entry.
    * get_krg_srv_info() - Get system keyring service information.
    * get_locale() - Get the current setting for the given locale category
                     as a sequence containing the language code and encoding.
    * get_lcl_convs() - Get local conventions.
    * get_locales() - Get locales.
    * scale_bytes() - Scale bytes to proper format.
    * get_sys_info() - Get system information.

Requires
----------------
keyring
    Store and access your passwords safely.
psutil
    Cross-platform lib for process and system monitoring in Python.
'''
###########
# Modules #
###########
import datetime as dt  # Enable classes for manipulating dates and times
from getpass import getpass  # Enable portable password input
# Enable access to the POSIX locale database and functionality
import locale
# Enable a portable way of using operating system dependent functionality
import os
# Enable access to underlying platform's identifying data
import platform as pltf

# Enable storing and accessing of passwords safely
import keyring as kr
import keyring.util.platform_ as kr_pltf
# Enable cross-platform library for process and system monitoring
import psutil as psu

#############
# Variables #
#############
WIN = (pltf.system() == 'Windows')  # Determine if running Windows
HOME = os.path.expanduser('~')  # Obtain user home directory


#############
# Functions #
#############
def set_krg_entry(srv_name) -> None:
    '''
    Set a keyring entry.

    srv_name : str
        The service name to use for the keyring entry.
    '''
    username = input('\nEnter username: ').strip()
    password = getpass(prompt='Enter password: ').strip()

    kr.set_password(srv_name, username, password)


def get_krg_entry(srv_name: str, username: str) -> tuple[str, str]:
    '''
    Get a keyring entry.

    Parameters
    ----------------
    srv_name : str
        The keyring entry's service name.
    username : str
        The keyring entry's username.

    Returns
    ----------------
    tuple
        The keyring entry's username and password.
    '''
    creds = kr.get_credential(srv_name, username)

    username = creds.username
    password = creds.password

    return username, password


def get_krg_srv_info() -> tuple[str, str]:
    '''
    Get system keyring service information.

    Returns
    ----------------
    tuple
        The configuration root and the keyring.
    '''
    return str(kr_pltf.config_root()), str(kr.get_keyring())


def get_locale(locale_cat: int = 0) -> tuple[str, str]:
    '''
    Get the current setting for the given locale category
    as a sequence containing the language code and encoding.

    Parameters
    ----------------
    locale_cat : int (optional)
        The locale category to get the current setting for. Defaults to
        0 for LC_CTYPE.

        https://docs.python.org/library/locale.html#locale.LC_CTYPE

    Returns
    ----------------
    tuple
        The current language code and encoding.
    '''
    return locale.getlocale(locale_cat)


def get_lcl_convs() -> dict[str, str | int | list]:
    '''
    Get local conventions.

    Returns
    ----------------
    dict
        Local convention names and values, sorted by convention name.
    '''
    return dict(sorted(locale.localeconv().items()))


def get_locales() -> list[str]:
    '''
    Get locales.

    Returns
    ----------------
    list
        Sorted list of locales.
    '''
    if WIN:
        return sorted(set(locale.windows_locale.values()))
    else:
        return sorted(set(locale.locale_alias.values()))


def scale_bytes(bytes, suffix: str = 'B') -> str:
    '''
    Scale bytes to proper format.

    Parameters
    ----------------
    bytes : int
        Bytes to scale.
    suffix : str (optional)
        Suffix to use in output string. Defaults to B.

    Returns
    ----------------
    str
        The scaled bytes string.
    '''
    factor = 1024

    for unit in ('', 'K', 'M', 'G', 'T', 'P'):
        if bytes < factor:
            return f'{bytes:.2f}{unit}{suffix}'

        bytes /= factor


def get_sys_info() -> str:
    '''
    Get system information.

    Returns
    ----------------
    str
        System information string.
    '''
    nl = '\n'

    uname = pltf.uname()
    pltf_o = f'''\
Platform
{'~' * len('Platform')}
{f'System:': <10} {uname.system}
{f'Node Name:': <10} {uname.node}
{f'Release:': <10} {uname.release}
{f'Version:': <10} {uname.version}
{f'Machine:': <10} {uname.machine}\
'''

    if uname.processor:
        pltf_o += f'''{f'{nl}Processor:': <11} {uname.processor}'''

    boot_time = dt.datetime.fromtimestamp(psu.boot_time())
    last_boot_o = f'''\
Boot Time
{'~' * len('Boot Time')}
{f'Last Boot:': <10} {boot_time.strftime('%m/%d/%Y %H:%M:%S')}\
'''

    cpu_f = psu.cpu_freq()
    cpu_o = f'''\
CPU
{'~' * len('CPU')}
{f'Physical Cores:': <19} {psu.cpu_count(logical=False)}
{f'Total Cores:': <19} {psu.cpu_count(logical=True)}
{f'Max Frequency:': <19} {cpu_f.max:.2f}Mhz
{f'Min Frequency:': <19} {cpu_f.min:.2f}Mhz
{f'Current Frequency:': <19} {cpu_f.current:.2f}Mhz
CPU Usage Per Core:\
'''
    for idx, pct in enumerate(psu.cpu_percent(percpu=True, interval=1)):
        cpu_o += f'''{f'{nl}    Core {idx}:': <20} {pct}%'''
    cpu_o += f'''{f'{nl}Total CPU Usage:': <20} {psu.cpu_percent()}%'''

    vr_mem = psu.virtual_memory()
    swap_mem = psu.swap_memory()
    mem_o = f'''\
Memory
{'~' * len('Memory')}
{f'Total:': <11} {scale_bytes(vr_mem.total)}
{f'Available:': <11} {scale_bytes(vr_mem.available)}
{f'Used:': <11} {scale_bytes(vr_mem.used)}
{f'% Used:': <11} {vr_mem.percent}%

--- SWAP ---
{f'Total:': <11} {scale_bytes(swap_mem.total)}
{f'Available:': <11} {scale_bytes(swap_mem.free)}
{f'Used:': <11} {scale_bytes(swap_mem.used)}
{f'% Used:': <11} {swap_mem.percent}%\
'''

    prtns = psu.disk_partitions()
    disk_io = psu.disk_io_counters()
    disk_o = f'''\
Disk
{'~' * len('Disk')}\
'''
    for prtn in prtns:
        disk_o += f'''
--- {prtn.device} ---
{f'Mountpoint:': <17} {prtn.mountpoint}
{f'File System Type:': <17} {prtn.fstype}
'''

        try:
            prtn_usage = psu.disk_usage(prtn.mountpoint)
        except PermissionError:
            continue

        disk_o += f'''\
{f'Total Size:': <17} {scale_bytes(prtn_usage.total)}
{f'Available:': <17} {scale_bytes(prtn_usage.free)}
{f'Used:': <17} {scale_bytes(prtn_usage.used)}
{f'% Used:': <17} {prtn_usage.percent}%
'''
    disk_o += f'''
=== I/O Statistics ===
{f'Total Read:': <17} {scale_bytes(disk_io.read_bytes)}
{f'Total Write:': <17} {scale_bytes(disk_io.write_bytes)}\
'''

    if_addrs = psu.net_if_addrs()
    net_io = psu.net_io_counters()
    net_o = f'''\
Network
{'~' * len('Network')}\
'''
    for intf, intf_addrs in if_addrs.items():
        for addr in intf_addrs:
            # AddressFamily.AF_INET
            if addr.family == 2:
                net_o += f'''
--- {intf} ---
{f'IP Address:': <21} {addr.address}
{f'Netmask:': <21} {addr.netmask}
{f'Broadcast IP:': <21} {addr.broadcast}
'''
            # AddressFamily.AF_PACKET
            elif addr.family == 17:
                net_o += f'''
--- {intf} ---
{f'MAC Address:': <21} {addr.address}
{f'Netmask:': <21} {addr.netmask}
{f'Broadcast MAC:': <21} {addr.broadcast}
'''
    net_o += f'''
=== Network Statistics ===
{f'Total Bytes Sent:': <21} {scale_bytes(net_io.bytes_sent)}
{f'Total Bytes Received:': <21} {scale_bytes(net_io.bytes_recv)}\
'''

    sen_batt = psu.sensors_battery()
    if sen_batt:
        batt_o = f'''\
Battery
{'~' * len('Battery')}
{f'Percent Left:': <13} {sen_batt.percent:.1f}%\
'''
    if sen_batt.power_plugged is False:
        batt_o += f'''
{f'Time Left:': <13} {dt.timedelta(seconds=sen_batt.secsleft)}\
'''
    if sen_batt.power_plugged is not None:
        batt_o += f'''
{f'Plugged In:': <13} {sen_batt.power_plugged}\
'''

    output = (
        pltf_o
        + '\n\n\n'
        + last_boot_o
        + '\n\n\n'
        + cpu_o
        + '\n\n\n'
        + mem_o
        + '\n\n\n'
        + disk_o
        + '\n\n\n'
        + net_o
        + '\n\n\n'
        + batt_o
    )
    return output
