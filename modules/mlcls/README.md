# Machine Learning Classes

This module contains a collection of Python classes that may be used for machine learning-oriented tasks.

Class examples assume that `mlcls` and `pandas` have been imported, and that a sample data set is saved as a DataFrame in the `df` variable:

```python
# Enable collection of machine learning-oriented classes
import mlcls
import pandas as pd  # Enable data structures and data analysis tools

df = pd.read_csv('foobar.csv')
```

**Table of Contents**

[TOC]

## `TypeSelector()`

Select a specific type of data for transformation.

```python
cat_cols = ['foo', 'bar', 'baz']

mlcls.TypeSelector(cat_cols)
```

## `MlPipe()`

Create a cross-validating machine learning pipeline that performs GridSearchCV exhaustive search for hyperparameter tuning. SMOTE and categorical/numerical variable transformations are optional pipeline features.

```python
# Decision Tree Classification Example

# Enable creation of decision tree classification models
from sklearn.tree import DecisionTreeClassifier

# Define pipeline arguments
cat_cols = ['foo', 'bar', 'baz']
num_cols = ['alpha', 'beta', 'gamma']
param_grid = {'classifier__max_depth': list(range(1, 11))}
metrics = ['f1', 'recall', 'precision', 'roc_auc']

# Create MlPipe instance
model = mlcls.MlPipe(
    DecisionTreeClassifier(random_state=12345),
    param_grid
)

# Execute model pipeline
model.pipe(
    'classifier', metrics,
    X_train, y_train,
    X_test, y_test,
    'skf', 4, imb=True,
    cat_cols=cat_cols, num_cols=num_cols
)

model.dsp_mdl_info(metrics)  # Display final model information
```

```python
# Decision Tree Regression Example With Missing Data

# Enable creation of decision tree regression models
from sklearn.tree import DecisionTreeRegressor

# Define pipeline arguments
cat_cols = ['foo', 'bar', 'baz']
num_cols = ['alpha', 'beta', 'gamma']
param_grid = {'regressor__max_depth': list(range(1, 16))}
metrics = ['neg_root_mean_squared_error']

# Create MlPipe instance
model = mlcls.MlPipe(
    DecisionTreeRegressor(random_state=12345),
    param_grid
)

# Execute model pipeline
model.pipe(
    'regressor', metrics,
    X_train, y_train,
    X_test, y_test,
    'kf', 3,
    cat_mis_strg='missing', num_mis_strg='median',
    cat_cols=cat_cols, num_cols=num_cols
)

model.dsp_mdl_info(metrics)  # Display final model information
```

```python
# Time Series Regression Example

# Enable creation of decision tree regression models
from sklearn.tree import DecisionTreeRegressor

# Define pipeline arguments
num_cols = ['alpha', 'beta', 'gamma']
param_grid = {'regressor__max_depth': list(range(1, 16))}
metrics = ['neg_root_mean_squared_error']

# Create MlPipe instance
model = mlcls.MlPipe(
    DecisionTreeRegressor(random_state=12345),
    param_grid
)

# Execute model pipeline
model.pipe(
    'regressor', metrics,
    X_train, y_train,
    X_test, y_test,
    'tss', 4,
    num_cols=num_cols
)

model.dsp_mdl_info(metrics)  # Display final model information
```