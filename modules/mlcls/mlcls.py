'''
A collection of custom machine learning-oriented classes.

Classes:

    * TypeSelector() - Select a specific type of data for transformation.
    * MlPipe() - Create a cross-validating machine learning pipeline
      that performs GridSearchCV exhaustive search for hyperparameter tuning.

Requires
----------------
imblearn
    Toolbox for imbalanced dataset in machine learning.
numpy
    NumPy is the fundamental package for array computing with Python.
pandas
    Powerful data structures for data analysis, time series, and statistics.
scikit-learn
    A set of python modules for machine learning and data mining.
'''
# Enable Synthetic Minority Oversampling Technique
from imblearn.over_sampling import SMOTE
from imblearn.pipeline import Pipeline  # Enable pipeline creation
import numpy as np  # Enable multi-dimensional arrays and matrices
import pandas as pd  # Enable data structures and data analysis tools
# Estimators base class and transformers mixin class import
from sklearn.base import BaseEstimator, TransformerMixin
# Enable imputation transformer for completing missing values
from sklearn.impute import SimpleImputer
# Enable cross-validators
from sklearn.model_selection import (
    GridSearchCV, KFold, StratifiedKFold, TimeSeriesSplit
)
# Enable concatenation of multiple transformer objects
from sklearn.pipeline import FeatureUnion
# Enable feature transformations
from sklearn.preprocessing import MinMaxScaler, OrdinalEncoder


class TypeSelector(BaseEstimator, TransformerMixin):
    '''
    Select a specific type of data for transformation.

    Parent Classes:

        * BaseEstimator - Base class for all estimators in scikit-learn.
        * TransformerMixin - Mixin class for all transformers in scikit-learn.

    Methods:

        * __init__() - Initialize the class with stateful data.
        * fit() - Return a reference to the instance object
          on which fit was called.
        * transform() - Return values from specified attribute names of X.
    '''

    def __init__(self, col_names: list[str]) -> None:
        '''
        Initialize the class with stateful data.

        Parameters
        -------------
        col_names : list
            List of column names of a specific data type.
        '''
        self.col_names = col_names

    def fit(self, X: pd.core.frame.DataFrame,
            y: pd.core.series.Series = None):
        '''
        Return a reference to the instance object on which fit was called.

        Parameters
        ----------------
        X : pd.core.frame.DataFrame
            A pandas DataFrame.
        y : pd.core.series.Series (optional)
            A pandas Series. Defaults to None.
        '''
        return self

    def transform(self, X: pd.core.frame.DataFrame) -> np.ndarray:
        '''
        Return values from specified attribute names of X.

        Parameters
        ----------------
        X : pd.core.frame.DataFrame
            A pandas DataFrame from which to extract specific
            data type column values.
        '''
        return X[self.col_names].values


class MlPipe:
    '''
    Create a cross-validating machine learning pipeline
    that performs GridSearchCV exhaustive search for hyperparameter tuning.
    SMOTE and categorical/numerical variable transformations are optional
    pipeline features.

    Methods:

        * __init__() - Initialize the class with stateful data.
        * pipe() - Create pipeline to perform required data transformations
        and generate model metrics.
        * dsp_mdl_info() - Display information about the final best model.
    '''

    def __init__(self, model, param_grid: dict[str, list[int]]) -> None:
        '''
        Initialize the class with stateful data.

        Parameters
        ----------------
        model : classifier, regressor
            Classifier or regressor model.
        param_grid : dict
            Dictionary of parameters for hyperparameter tuning.
        '''
        self.model = model
        self.param_grid = param_grid
        self.cv_results_ = None  # For exhaustive search results
        # For best parameters that gave best results for hold out data
        self.best_params_ = None
        # For mean cross-validated score of the best_estimator
        self.best_score_ = None
        self.score = {}  # For test data set scores
        self.final_gs = None  # For final GridSearchCV model

    def pipe(self, task_model: str, metrics: list[str],
             X_train: pd.core.frame.DataFrame,
             y_train: pd.core.series.Series,
             X_test: pd.core.frame.DataFrame,
             y_test: pd.core.series.Series,
             cv_type: str, folds: int, imb: bool = False,
             cat_mis_strg: str = None, num_mis_strg: str = None,
             cat_cols: list[str] = None, num_cols: list[str] = None) \
            -> None:
        '''
        Create pipeline and generate model metrics.

        Parameters
        ----------------
        task_model : str
            Task model type ('classifier' or 'regressor').
        metrics : list
            List of metric names to calculate.
        X_train : pd.core.frame.DataFrame
            Training set features.
        y_train : pd.core.series.Series
            Training set target.
        X_test : pd.core.frame.DataFrame
            Test set features.
        y_test : pd.core.series.Series
            Test set target.
        cv_type : str
            Cross validator type ('skf', for Stratified K-Folds,
            'kf' for K-Folds, or 'tss' for Time Series Split).
        folds : int
            Number of cross-validation folds.
        imb : bool (optional)
            Imbalanced data set or not. Defaults to False. Set to True to
            have SMOTE performed.
        cat_mis_strg : str (optional)
            String to use as a constant to fill missing values.
            Defaults to None.
        num_mis_strg : str (optional)
            Function name to use to calculate a value to fill
            missing values (i.e., 'mean', 'median', 'most_frequent').
            Defaults to None.
        cat_cols : list (optional)
            List of categorical column names. Defaults to None.
        num_cols : list (optional)
            List of numerical column names. Defaults to None.
        '''
        # Create optional argument tests
        cat_and_set = cat_mis_strg is not None and cat_cols is not None
        cat_or_set = cat_mis_strg is not None or cat_cols is not None
        num_and_set = num_mis_strg is not None and num_cols is not None
        num_or_set = num_mis_strg is not None or num_cols is not None

        cat_and_not_set = cat_mis_strg is None and cat_cols is None
        num_and_not_set = num_mis_strg is None and num_cols is None

        # Set categorical steps
        if cat_and_set:
            steps_cat = [
                ('selector_cat', TypeSelector(cat_cols)),
                ('imputer', SimpleImputer(strategy='constant',
                                          fill_value=cat_mis_strg)),
                ('encoder', OrdinalEncoder(handle_unknown='use_encoded_value',
                                           unknown_value=-1))
            ]
        elif cat_mis_strg is not None:
            steps_cat = [
                ('selector_cat', TypeSelector(cat_cols)),
                ('imputer', SimpleImputer(strategy='constant',
                                          fill_value=cat_mis_strg))
            ]
        elif cat_cols is not None:
            steps_cat = [
                ('selector_cat', TypeSelector(cat_cols)),
                ('encoder', OrdinalEncoder(handle_unknown='use_encoded_value',
                                           unknown_value=-1))
            ]

        # Set numerical steps
        if num_and_set:
            steps_num = [
                ('selector_num', TypeSelector(num_cols)),
                ('imputer', SimpleImputer(missing_values=np.nan,
                                          strategy=num_mis_strg)),
                ('scaler', MinMaxScaler())
            ]
        elif num_mis_strg is not None:
            steps_num = [
                ('selector_num', TypeSelector(num_cols)),
                ('imputer', SimpleImputer(missing_values=np.nan,
                                          strategy=num_mis_strg))
            ]
        elif num_cols is not None:
            steps_num = [
                ('selector_num', TypeSelector(num_cols)),
                ('scaler', MinMaxScaler())
            ]

        # Categorical pipeline and transformer
        if cat_or_set:
            pipeline_cat = Pipeline(steps_cat)
            transformer = FeatureUnion(
                transformer_list=[
                    ('pipeline_cat', pipeline_cat)
                ]
            )
        # Numerical pipeline and transformer
        if num_or_set:
            pipeline_num = Pipeline(steps_num)
            transformer = FeatureUnion(
                transformer_list=[
                    ('pipeline_num', pipeline_num)
                ]
            )
        # Categorical/numerical transformer
        if cat_or_set and num_or_set:
            transformer = FeatureUnion(
                transformer_list=[
                    ('pipeline_num', pipeline_num),
                    ('pipeline_cat', pipeline_cat)
                ]
            )

        # Default final pipeline steps
        steps = [
            (task_model, self.model)
        ]
        # Final pipeline steps with variable transformation
        if cat_or_set or num_or_set:
            steps = [
                ('transform', transformer),
                (task_model, self.model)
            ]
        # Final pipeline steps with variable transformation and SMOTE
        if imb and (cat_or_set or num_or_set):
            steps = [
                ('transform', transformer),
                ('smote', SMOTE(random_state=12345)),
                (task_model, self.model)
            ]
        # Final pipeline steps with SMOTE only
        elif imb and (cat_and_not_set and num_and_not_set):
            steps = [
                ('smote', SMOTE(random_state=12345)),
                (task_model, self.model)
            ]
        pipeline = Pipeline(steps)  # Create final pipeline

        # Create cross-validator
        if cv_type == 'skf':
            # Stratified K-Folds
            cross_validator = StratifiedKFold(
                n_splits=folds,
                shuffle=True,
                random_state=12345
            )
        elif cv_type == 'kf':
            # K-Folds
            cross_validator = KFold(
                n_splits=folds,
                shuffle=True,
                random_state=12345
            )
        elif cv_type == 'tss':
            # Time series
            cross_validator = TimeSeriesSplit(n_splits=folds)

        # Enable exhaustive search over specified parameter values
        # for an estimator
        grid_search = GridSearchCV(
            pipeline,
            param_grid=self.param_grid,
            scoring=metrics,
            refit=metrics[0],
            cv=cross_validator
        )
        # Run fit with all sets of parameters
        grid_search.fit(X_train, y_train)
        # Save final grid_search object
        self.final_gs = grid_search

        # Set specified results/score attributes
        self.cv_results_ = pd.DataFrame(grid_search.cv_results_)
        self.best_params_ = grid_search.best_params_
        self.best_score_ = grid_search.best_score_
        for i in range(len(metrics)):
            self.score[metrics[i]] = grid_search.score(X_test, y_test)
            if i < len(metrics) - 1:
                # Refit grid_search to use next metric
                grid_search.set_params(refit=metrics[i + 1])

    def dsp_mdl_info(self, metrics: list[str]) -> None:
        '''
        Display information about the final best model.

        Parameters
        ----------------
        metrics : list
            List of metric names calculated during the pipeline.
        '''
        print('Best Model Parameters\n',
              '---------------------',
              sep='')
        for param, value in self.best_params_.items():
            print(f'{param}: {value}')

        header = f'\nAverage {metrics[0]} Value After CV For Best Model\n'
        header_len = len(header)
        print(header, ('-' * (header_len - 2)) + '\n',
              f'{self.best_score_:.2f}\n',
              sep='')

        print('Best Model Metrics On Test Set\n',
              '------------------------------',
              sep='')
        for metric_name, metric_value in self.score.items():
            print(f'{metric_name}: {metric_value:.2f}')
