'''
A collection of program support functions.

Functions:

    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * init_logging_cl() - Initialize logging with a custom logger.
    * get_creds() - Get username and password credentials.
'''
###########
# Modules #
###########
from getpass import getpass  # Enable portable password input
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable access to some variables/functions used/maintained by the interpreter
import sys
from types import FrameType  # Enable frame object type


#############
# Functions #
#############
def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def init_logging_cl(log_path: str, logger_name: str) -> logging.Logger:
    '''
    Initialize logging with a custom logger.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    logger_name : str
        The name the custom logger should be assigned.

    Returns
    ----------------
    logging.Logger
        A custom logger.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    logger = logging.getLogger(logger_name)  # Create a custom logger
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s- %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel('DEBUG')

    return logger


def get_creds() -> tuple[str, str]:
    '''
    Get username and password credentials.

    Returns
    ----------------
    tuple
        Username and password.
    '''
    username = input('\nEnter your username: ').strip()
    password = getpass('Enter your password: ').strip()

    return username, password
