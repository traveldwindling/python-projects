# Program Support Functions

A collection of program support functions.

```python
# Enable collection of program support functions
import pgmspt
```

<details>
  <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## `hndl_sigint()`

Handle a SIGINT signal.

```python
from signal import SIGINT, signal  # Enable use of signal handlers

signal(SIGINT, hndl_sigint)
```

## `init_logging()`

Initialize logging.

```python
log_path = '/home/dmb/don_t_drink_the_water.log'
pgmspt.init_logging(log_path)
```

## `init_logging_cl()`

Initialize logging with a custom logger.

```python
log_path = '/home/arcade_fire/windowsill.log'
logger = pgmspt.init_logging_cl(log_path, 'windowsill')
```

## `get_creds()`

Get username and password credentials.

`username, password = pgmspt.get_creds()`