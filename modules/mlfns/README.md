# Machine Learning Functions

This module contains a collection of Python functions that may be used for machine learning-oriented tasks.

Function examples assume that `mlfns`, `numpy`, `pandas`, and `train_test_split` have been imported, and that a sample data set is saved as a DataFrame in the `df` variable:

```python
# Enable collection of machine learning-oriented functions
import mlfns
import numpy as np  # Enable scientific computing with Python
import pandas as pd  # Enable data structures and data analysis tools
# Enable splitting of arrays or matrices into random train and test subsets
from sklearn.model_selection import train_test_split

df = pd.read_csv('foobar.csv')
```

**Table of Contents**

[TOC]

## Required Packages/Modules

[dafns](../dafns/)

## `cfm_df_lens()`

Confirm DataFrame lengths match expectations after a split.

```python
# Define features and target
X = df.drop('foo', axis=1)
y = df['foo']

# Two-way data split
X_train, X_test, y_train, y_test = \
    train_test_split(X, y, test_size=0.25, random_state=12345)

# Confirm DataFrame lengths
mlfns.cfm_df_lens(df, X_train, X_test, 0.75, 0.25)
```

```python
# Split DataFrame into three DataFrames (60%, 20%, 20%)
train, validate, test = np.split(df.sample(frac=1, random_state=12345),
                                 [int(.6 * df.shape[0]),
                                 int(.8 * df.shape[0])])

# Reset indices
train = train.reset_index(drop=True)
validate = validate.reset_index(drop=True)
test = test.reset_index(drop=True)

# Define training set features and target
X_train = train.drop(['foo'], axis=1)
y_train = train['foo']

# Define validation set features and target
X_val = validate.drop(['foo'], axis=1)
y_val = validate['foo']

# Define testing set features and target
X_test = test.drop(['foo'], axis=1)
y_test = test['foo']

# Confirm DataFrame lengths
mlfns.cfm_df_lens(df, X_train, X_test, 0.60, 0.20, X_val, 0.20)
```

## `mk_ts_ftrs()`

Create time series features.

```python
# Create time series features
df = mlfns.mk_ts_ftrs(df, 'foo', 3, 10)
```

## `data_trans()`

Perform requested data transformations on DataFrames.

```python
# Define features and target
X = df.drop('foo', axis=1)
y = df['foo']

# Define numerical columns
num_cols = ['bar', 'baz']

# Two-way data split
X_train, X_test, y_train, y_test = \
    train_test_split(X, y, test_size=0.25, random_state=12345)

min_max_sclr = MinMaxScaler()  # Create scaler instance

# Transform features
X_train, X_test = mlfns.data_trans(X_train, X_train,
                                encoding_type='ohe',
                                num_cols=num_cols, scaler=min_max_sclr)
```

```python
# Split DataFrame into three DataFrames (60%, 20%, 20%)
train, validate, test = np.split(df.sample(frac=1, random_state=12345),
                                 [int(.6 * df.shape[0]),
                                 int(.8 * df.shape[0])])

# Reset indices
train = train.reset_index(drop=True)
validate = validate.reset_index(drop=True)
test = test.reset_index(drop=True)

# Define training set features and target
X_train = train.drop(['foo'], axis=1)
y_train = train['foo']

# Define validation set features and target
X_val = validate.drop(['foo'], axis=1)
y_val = validate['foo']

# Define testing set features and target
X_test = test.drop(['foo'], axis=1)
y_test = test['foo']

std_sclr = StandardScaler()  # Create scaler instance

# Transform features
X_train, X_test, X_val = \
    mlfns.data_trans(X_train, X_test, X_val,
                  encoding_type='label',
                  num_cols=num_cols, scaler=std_sclr)
```

## `ftr_imp()`

Determine and plot feature importance values for a specific model.

```python
# Display feature importance for metric
mlfns.ftr_imp(model, X_test, y_test, 'f1', [10, 10])
```

```python
# Optional font size set
mlfns.ftr_imp(model, X_test, y_test, 'f1', [10, 10], fontsize=13)
```