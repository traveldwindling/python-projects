'''
A collection of custom machine learning-oriented functions.

Functions:

    * cfm_df_lens() - Confirm DataFrame lengths match expectations after
                      a split.
    * mk_ts_ftrs() - Create time series features.
    * data_trans() - Return DataFrame after requested data transformations.
    * ftr_imp() - Determine and plot feature importance values for
      a specific model.

Requires
----------------
dafns
    A collection of custom data analysis-oriented functions.
matplotlib
    Matplotlib is a comprehensive library for creating static, animated,
    and interactive visualizations in Python.
pandas
    Powerful data structures for data analysis, time series, and statistics.
scikit-learn
    A set of python modules for machine learning and data mining.
'''
import matplotlib.pyplot as plt  # Enable state-based interface to matplotlib
import pandas as pd  # Enable data structures and data analysis tools
# Enable feature importance calculation
from sklearn.inspection import permutation_importance
# Enable feature transformations
from sklearn.preprocessing import (
    MinMaxScaler,
    Normalizer,
    OrdinalEncoder,
    RobustScaler,
    StandardScaler
)

# Enable custom string and graph formatting
from dafns import format_line, graph_meta


def cfm_df_lens(df: pd.core.frame.DataFrame,
                X_train: pd.core.frame.DataFrame,
                X_test: pd.core.frame.DataFrame,
                train_size: float, test_size: float,
                X_val: pd.core.frame.DataFrame = None,
                val_size: pd.core.frame.DataFrame = None) -> None:
    '''
    Confirm DataFrame lengths match expectations after a split.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        The source DataFrame that was split.
    X_train : pd.core.frame.DataFrame
        The training DataFrame.
    X_test : pd.core.frame.DataFrame
        The testing DataFrame.
    train_size : float
        Training set proportion.
    test_size : float
        Test set proportion.
    X_val : pd.core.frame.DataFrame (optional)
        The validation DataFrame.
    val_size : float (optional)
        Validation set proportion.
    '''
    expected_msg = (
        'Expected DataFrame Sizes\n'
        '~~~~~~~~~~~~~~~~~~~~~~~~\n'
        f'Train: {df.shape[0] * train_size:.0f}\n'
        f'Test: {df.shape[0] * test_size:.0f}'
    )

    actual_msg = (
        'Actual DataFrame Sizes\n'
        '~~~~~~~~~~~~~~~~~~~~~~\n'
        f'Train: {X_train.shape[0]}\n'
        f'Test: {X_test.shape[0]}'
    )

    if X_val is not None and val_size is not None:
        exp_ins_loc = expected_msg.find('Test:')
        val_exp_msg = f'Validation: {df.shape[0] * val_size:.0f}\n'

        expected_msg = (
            expected_msg[:exp_ins_loc]
            + val_exp_msg
            + expected_msg[exp_ins_loc:]
        )

        act_ins_loc = actual_msg.find('Test:')
        val_act_msg = f'Validation: {X_val.shape[0]}\n'

        actual_msg = (
            actual_msg[:act_ins_loc]
            + val_act_msg
            + actual_msg[act_ins_loc:]
        )

    print(expected_msg, actual_msg, sep='\n\n')


def mk_ts_ftrs(df: pd.core.frame.DataFrame,
               y: str, max_lag: int, rolling_mean_size: int) \
               -> pd.core.frame.DataFrame:
    '''
    Create time series features.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        DataFrame to make features from.
    y : str
        Name of target feature to predict.
    max_lag : int
        Number of lag features to create.
    rolling_mean_size : int
        Size of rolling window to calculate rolling mean from.

    Returns
    ----------------
    pd.core.frame.DataFrame
        pandas DataFrame with time series features.
    '''
    # Create calendar features
    df['month'] = df.index.month
    df['day'] = df.index.day
    df['day_of_week'] = df.index.dayofweek
    df['hour'] = df.index.hour

    # Create lag features
    for i in range(1, max_lag + 1):
        df[f'lag_{i}'] = df[y].shift(i)

    # Create rolling mean
    df['rolling_mean'] = \
        df[y].shift().rolling(rolling_mean_size).mean()

    return df.dropna()  # Drop rows missing values


def data_trans(X_train: pd.core.frame.DataFrame,
               X_test: pd.core.frame.DataFrame,
               X_val: pd.core.frame.DataFrame = None,
               encoding_type: str = None, num_cols: list[str] = None,
               scaler: MinMaxScaler | RobustScaler
               | StandardScaler | Normalizer = None) \
               -> tuple[pd.core.frame.DataFrame, ...]:
    '''
    Perform requested data transformations on DataFrames.

    Parameters
    ----------------
    X_train : pd.core.frame.DataFrame
        Training set features.
    X_test : pd.core.frame.DataFrame
        Test set features.
    X_val : pd.core.frame.DataFrame (optional)
        Validation set features. Defaults to None.
    encoding_type : str (optional)
        Type of encoding to perform ('ohe' for One Hot Encoding
        or 'label' for Label Encoding via OrdinalEncoder()). Defaults to None.
    num_cols : list (optional)
        List of numerical column names. Defaults to None.
    scaler : MinMaxScaler | RobustScaler | StandardScaler
             | Normalizer (optional)
        Estimator to perform scaling (MinMaxScaler(), RobustScaler(),
        Normalizer()) or standardization (StandardScaler()). Defaults to None.

    Returns
    ----------------
    tuple
        Transformed pandas DataFrames.
    '''
    # One Hot Encoding or Label Encoding
    if encoding_type == 'ohe':
        # Create dummy variables via OHE
        X_train = pd.get_dummies(X_train, drop_first=True)
        X_test = pd.get_dummies(X_test, drop_first=True)
        if X_val is not None:
            X_val = pd.get_dummies(X_val, drop_first=True)
    elif encoding_type == 'label':
        # Create label encoding class instance
        encoder = OrdinalEncoder(handle_unknown='use_encoded_value',
                                 unknown_value=-1)
        encoder.fit(X_train)  # Fit the encoder
        # Recreate DataFrames with encoded data
        X_train = pd.DataFrame(encoder.transform(X_train),
                               columns=X_train.columns)
        X_test = pd.DataFrame(encoder.transform(X_test),
                              columns=X_test.columns)
        if X_val is not None:
            X_val = pd.DataFrame(encoder.transform(X_val),
                                 columns=X_val.columns)

    # Scaling or Standardization
    if num_cols is not None and scaler is not None:
        # Suppress SettingWithCopyWarning warnings
        pd.options.mode.chained_assignment = None
        # Fit the scaler
        scaler.fit(X_train[num_cols])
        # Transform numerical data
        X_train[num_cols] = scaler.transform(X_train[num_cols])
        X_test[num_cols] = scaler.transform(X_test[num_cols])
        if X_val is not None:
            X_val[num_cols] = scaler.transform(X_val[num_cols])

    if X_val is not None:
        return X_train, X_test, X_val
    else:
        return X_train, X_test


def ftr_imp(trn_mdl, X_test: pd.core.frame.DataFrame,
            y_test: pd.core.series.Series, metric: str,
            fig_size: list[int], fontsize: int = 10) -> None:
    '''
    Determine and plot feature importance values for a specific model.

    Parameters
    ----------------
    trn_mdl : classifier, regressor
        A trained model.
    X_test : pd.core.frame.DataFrame
        Test set features.
    y_test : pd.core.series.Series
        Test set target.
    metric : str
        Scoring metric used to determine feature importance against.
    fig_size : list
        Total figure dimensions (width x height in inches, e.g., [14, 14]).
    font_size : int (optional)
        Graph label font size. Defaults to 10.
    '''
    # Perform permutation importance
    results = permutation_importance(
        trn_mdl,
        X_test,
        y_test,
        scoring=metric
    )

    # Obtain importance values
    importance_vals = results.importances_mean
    # Display feature importance values
    heading = ('Feature', 'Metric Change')
    format_spec = ' ^20'
    format_line(heading, format_spec)
    print('-' * 40)
    for i, imp_val in enumerate(importance_vals):
        print(f'{X_test.columns[i]: ^20}{imp_val: ^20.2f}')

    # Create sorted Series from importance scores
    # and corresponding feature names
    col_imp = pd.Series(importance_vals, index=X_test.columns)\
                .sort_values(ascending=True)

    # Create figure and Axes
    fig, ax = plt.subplots(figsize=fig_size, layout='constrained')
    ax.barh(col_imp.index, col_imp)  # Make plot

    # Set graph metadata
    graph_meta(
        ax,
        'Feature Importance',
        'Metric Change Upon Feature Permutation',
        'Feature Name',
        fontsize
    )
