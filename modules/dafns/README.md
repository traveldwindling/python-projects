# Data Analysis Functions

This module contains a collection of Python functions that may be used for data analysis-oriented tasks.

Function examples assume that `os`, `pandas`, and `dafns` have been imported, and that a sample data set is saved as a DataFrame in the `df` variable:

```python
# Enable a portable way of using operating system dependent functionality
import os
import pandas as pd  # Enable data structures and data analysis tools
# Enable collection of data analysis-oriented functions
import dafns

df = pd.read_csv('foobar.csv')
```

<details>
  <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## Formatting

### `format_line()`

Output a formatted line.

```python
values = ['Foo', 'Bar', 'Baz']

dafns.format_line(values)
```

```python
values = ['Foo', 'Bar', 'Baz']
format_spec = '->6'  # Set optional format specification

dafns.format_line(values, format_spec)
```

## File Management

### `import_file()`

Import a .csv, .xls, .xlsx, .xlsm, .xlsb, .odf, .ods, or .odt file.

```python
file_path = os.path.expandvars('${HOME}/data/foobar.csv')

df = dafns.import_file(file_path)
```

```python
file_path = os.path.expandvars('${HOME}/data/foobar.csv')

df = dafns.import_file(file_path, delimiter='|')  # Set optional delimiter
```

```python
file_path = os.path.expandvars('${HOME}/data/foobar.xlsx')

# Set optional file type, column index, and sheet index
df = dafns.import_file(file_path, file_type='xlsx', index_col=2, sheet_name=1)
```

### `export_file()`

Export a .csv, .xls, .xlsx, .xlsm, .xlsb, .odf, .ods, or .odt file.

```python
file_path = os.path.expandvars('${HOME}/data/foobar.csv')

dafns.export_file(df, file_path)
```

```python
file_path = os.path.expandvars('${HOME}/data/foobar.csv')

dafns.export_file(df, file_path, sep='|')  # Set optional delimiter
```

```python
file_path = os.path.expandvars('${HOME}/data/foobar.xlsx')

dafns.export_file(df, file_path, file_type='xlsx')  # Set optional file type
```

### `files()`

Return absolute file paths for files of a specific type in a provided directory.


```python
files_dir = os.path.expandvars('${HOME}/data')

abs_paths = dafns.files(files_dir)
```

```python
files_dir = os.path.expandvars('${HOME}/data')

abs_paths = dafns.files(files_dir, ext='.ods')  # Set optional file extension
```

### `import_files()`

Create and return a dictionary of DataFrames from provided file paths.

```python
files = [
    {'path': os.path.expandvars('${HOME}/data/foobar.csv')},
    {'path': os.path.expandvars('${HOME}/data/foobaz.csv')}
]

dfs = dafns.import_files(files)
```

```python
# Set optional type, index column, and sheet name
files = [
    {'path': os.path.expandvars('${HOME}/data/foobar.csv')},
    {'path': os.path.expandvars('${HOME}/data/foobaz.xlsx'),
     'type': 'xlsx', 'index_col': 2, 'sheet_name': 1}
]

dfs = dafns.import_files(files)
```

## Summaries

### `df_info()`

Display basic information about a DataFrame.

`dafns.df_info(df)`

### `uni_val_counts()`

Display the unique value counts and proportions for columns of interest.

```python
columns = [
    'foo',
    'bar',
    'baz'
]

dafns.uni_val_counts(df, columns)
```

## Preprocessing

### `std_col_names()`

Standardize DataFrame column names.

`df = dafns.std_col_names(df)`

### `lower_vals()`

Convert column values to lowercase.

```python
col_names = ['foo', 'bar', 'baz']

dafns.lower_vals(df, col_names)
```

### `data_type_convert()`

Convert old data types to new data types.

```python
col_types = {
    'foo': 'int64',
    'bar': 'object',
    'baz': 'datetime64[ns]'
}

df = dafns.data_type_convert(df, col_types)
```

### `extract_dt()`

Extract date/time attribute from datetime column.

`df['month'] = dafns.extract_dt(df['foo'], 'month')`

### `cols_missing()`

Display the missing value counts and proportions for a DataFrame.

`dafns.cols_missing(df)`

### `dup_rows()`

Display information on a DataFrame's duplicate rows.

`dafns.dup_rows(df)`

### `week_start_end()`

Return the week expressed as a beginning and ending day (e.g., `09/17-09/23`).

`df['week'] = df['date_time'].apply(dafns.week_start_end)`

## Calculations

### `des_stats()`

Display descriptive statistics for specified DataFrame columns.

```python
columns = ['foo', 'bar', 'baz']

dafns.des_stats(df, columns)
```

### `display_modes()`

Display modes for specified DataFrame columns.

```python
columns = [
    'foo',
    'bar',
    'baz'
]

dafns.display_modes(df, columns)
```

### `calc_variance()`

Calculate variance.

`dafns.calc_variance(array_like, 'Foo')`

### `group_calcs()`

Create groups and perform calculations.

```python
group_cols = ['foo', 'bar']

dafns.group_calcs(df, group_cols, 'baz', 'sum')
```

```python
group_cols = ['foo', 'bar']

# Optional proportions set
dafns.group_calcs(df, group_cols, 'baz', 'mean', proportions=True)
```

## Outliers

### `outlier_info()`

Display outlier counts and proportions.

```python
col_names = ['foo', 'bar', 'baz']

dafns.outlier_info(df, col_names)
```

## Plotting

### `graph_meta()`

Set graph metadata.

```python
fig, ax = plt.subplots(layout='constrained')
df['foo'].hist(bins=50, ax=ax)
dafns.graph_meta(ax, 'Foo Frequencies', 'Foo', 'Frequency')
```

```python
fig, ax = plt.subplots(layout='constrained')
df['foo'].hist(bins=50, ax=ax)
# Optional font size set
dafns.graph_meta(ax, 'Foo Frequencies', 'Foo', 'Frequency', fontsize=15)
```

### `bar_plots()`

Display bar graphs for specified columns via subplots.

```python
group_cols = ['foo', 'bar', 'baz']
crit_cols = group_cols
action_funcs = ['count'] * 3  # Calculate counts
titles = ['Foo Frequencies', 'Bar Frequencies', 'Baz Frequencies']
x_labels = ['Foo', 'Bar', 'Baz']
y_labels = ['Frequency'] * 3

dafns.bar_plots(df, group_cols, crit_cols, action_funcs, [10, 20], 3, 1, 3,
             titles, x_labels, y_labels)
```

```python
group_cols = ['epsilon'] * 4
crit_cols = ['alpha', 'beta', 'gamma', 'delta']
action_funcs = ['sum'] * 4  # Calculate sums
titles = ['Alpha Sums (Proportions)', 'Beta Sums (Proportions)',
          'Gamma Sums (Proportions)', 'Delta Sums (Proportions)']
x_labels = ['Sums (Proportions)'] * 4
y_labels = ['Epsilon'] * 4

# Optional bar chart type, font size, and proportions set
dafns.bar_plots(df, group_cols, crit_cols, action_funcs, [12, 12], 2, 2, 4,
             titles, x_labels, y_labels, bar_type='barh', fontsize=13,
             proportions=True)
```

```python
group_cols = ['epsilon'] * 4
crit_cols = ['alpha'] * 4
action_funcs = ['count', 'sum', 'mean', 'median']  # Varied calculations
titles = ['Alpha Frequencies', 'Alpha Sums', 'Alpha Means', 'Alpha Medians']
x_labels = ['Frequency', 'Total', 'Mean', 'Median']
y_labels = ['Epsilon'] * 4

# Optional bar chart type and font size set
dafns.bar_plots(df, group_cols, crit_cols, action_funcs, [12, 12], 2, 2, 4,
             titles, x_labels, y_labels, bar_type='barh', fontsize=13)
```

### `hist_plots()`

Display histograms for specified columns via subplots.

```python
columns = ['foo', 'bar', 'baz',
           'alpha', 'beta', 'gamma']
titles = ['Foo Frequencies', 'Bar Frequencies', 'Baz Frequencies',
          'Alpha Frequencies', 'Beta Frequencies', 'Gamma Frequencies']
x_labels = ['Foo', 'Bar', 'Baz',
            'Alpha', 'Beta', 'Gamma']
y_labels = ['Frequency'] * 6

dafns.hist_plots(df, columns, [14, 14], 2, 2, 4,
              titles, x_labels, y_labels)
```

```python
columns = ['foo', 'bar', 'baz',
           'alpha', 'beta', 'gamma']
titles = ['Foo Relative Frequencies', 'Bar Relative Frequencies',
          'Baz Relative Frequencies', 'Alpha Relative Frequencies',
          'Beta Relative Frequencies', 'Gamma Relative Frequencies']
x_labels = ['Foo', 'Bar', 'Baz',
            'Alpha', 'Beta', 'Gamma']
y_labels = ['Relative Frequency'] * 6
x_limits = [[0.0, 1.5], [0.0, 1.5],
            [0.0, 0.6], [0.0, 0.6],
            [0.0, 0.3], [0.0, 0.3]]

# Optional x-axis limits, bins, font size, and proportions set
dafns.hist_plots(df, columns, [14, 14], 2, 2, 4,
              titles, x_labels, y_labels, x_limits=x_limits,
              bins=150, fontsize=13, proportions=True)
```

### `box_plots()`

Display box plots for specified columns via subplots.

```python
columns = ['foo', 'bar', 'baz',
           'alpha', 'beta', 'gamma']
titles = ['Foo', 'Bar', 'Baz',
          'Alpha', 'Beta', 'Gamma']

dafns.box_plots(df, columns, [14, 18], 2, 3, 6, titles)
```

### `stacked_bar()`

Display a stacked bar plot.

```python
group_cols = ['foo', 'bar']
title = 'Number of Values By Foo and Bar'

dafns.stacked_bar(df, group_cols, 'bar', 'count', [10, 10], title,
               'Foo', 'Number of Values')
```

```python
group_cols = ['foo', 'bar']
title = 'Total Values By Foo and Bar'

# Optional bar type, font size, and proportions set
dafns.stacked_bar(df, group_cols, 'bar', 'sum', [10, 10], title,
               'Total Values', 'Foo', bar_type='barh', fontsize=13,
               proportions=True)
```

## Create

### `create_ser()`

Create a Series.

```python
names = ['diana', 'helena', 'talia']
sr = dafns.create_ser(names, 'name')
```

```python
names = ['diana', 'helena', 'talia']
index = ['alpha', 'beta', 'gamma']
# Optional Series index set
sr = dafns.create_ser(names, 'name', index=index)
```

### `create_df()`

Create a DataFrame.

```python
df_data = {
    'name': ['diana', 'helena', 'talia'],
    'affiliation': ['jl', 'bop', 'loa'],
    'meta': ['yes', 'no', 'no']
}
df = dafns.create_df(df_data)
```

```python
df_data = {
    'name': ['diana', 'helena', 'talia'],
    'affiliation': ['jl', 'bop', 'loa'],
    'meta': ['yes', 'no', 'no']
}
index = ['alpha', 'beta', 'gamma']
# Optional DataFrame index set
df = dafns.create_df(df_data, index=index)
```

### `concat_objs()`

Concatenate objects.

```python
df_data = {
    'name': ['diana', 'helena', 'talia'],
    'affiliation': ['jl', 'bop', 'loa'],
    'meta': ['yes', 'no', 'no']
}
df_1 = pd.DataFrame(df_data)

new_row = {
    'name': ['pamela'],
    'affiliation': ['none'],
    'meta': ['yes']
}
df_2 = pd.DataFrame(new_row)

df = dafns.concat_objs([df_1, df_2])
```

```python
names = pd.Series(['diana', 'helena', 'talia'], name='name')
affiliations = pd.Series(['jl', 'bop', 'loa'], name='affiliation')

# Optional concatenation axis set
df = dafns.concat_objs([names, affiliations], axis=1)
```

### `merge_objs()`

Merge objects.

```python
affiliations = {
    'name': ['diana', 'helena', 'talia', 'selena'],
    'affiliation': ['jl', 'bop', 'loa', 'bop'],
    'initials': ['dp', 'hb', 'tag', 'sk']
}
df_affiliations = pd.DataFrame(affiliations)

meta = {
    'name': ['diana', 'helena', 'talia', 'barbara'],
    'meta': ['yes', 'no', 'no', 'no'],
    'initials': ['dp', 'hb', 'tag', 'bg']
}
df_meta = pd.DataFrame(meta)

df_merged = dafns.merge_objs(df_affiliations, df_meta)
```

```python
affiliations = {
    'name': ['diana', 'helena', 'talia', 'selena'],
    'affiliation': ['jl', 'bop', 'loa', 'bop'],
    'initials': ['dp', 'hb', 'tag', 'sk']
}
df_affiliations = pd.DataFrame(affiliations)

meta = {
    'name': ['diana', 'helena', 'talia', 'barbara'],
    'meta': ['yes', 'no', 'no', 'no'],
    'initials': ['dp', 'hb', 'tag', 'bg']
}
df_meta = pd.DataFrame(meta)

# Optional merge type and merge on column(s) set
df_merged = dafns.merge_objs(df_affiliations, df_meta,
                          how='outer', on=['name', 'initials'])
```

### `gen_rnd_df()`

Generate a DataFrame of random data.

`df = dafns.gen_rnd_df()`

```python
# DataFrame with null values and dirty data requested
df = dafns.gen_rnd_df(nulls=True, dirty=True)
```

## Read

### `query_df()`

Filter a DataFrame with a custom query.

```python
df_query = [
    '(name != "helena"',
    'or affiliation != "loa")',
    'and meta == "no"'
]

dafns.query_df(df, df_query)
```

### `sort_obj()`

Sort a pandas object by columns.

`dafns.sort_obj(df, ['name'])`

`dafns.sort_obj(df, ['name', 'affiliation'], ascending=False)`

## Update

### `rename_cols()`

Rename DataFrame columns.

```python
col_names = {
    'name': 'first_name',
    'affiliation': 'team',
    'meta': 'metahuman'
}
df = dafns.rename_cols(df, col_names)
```

### `add_col()`

Add a column to a DataFrame.

```python
new_col = ['1941', '1989', '1971', '1966']

dafns.add_col(df, 'first_appear_year', new_col)
```

### `insert_col()`

Insert a column into a DataFrame.

```python
new_col = ['1941', '1989', '1971', '1966']

dafns.insert_col(df, 1, 'first_appear_year', new_col)
```

### `add_row()`

Add a row to a DataFrame.

```python
new_row = ['dinah', '1947', 'jl', 'yes']

dafns.add_row(df, new_row)
```

### `insert_row()`

Insert a row into a DataFrame.

```python
new_row = {
    'name': 'dinah',
    'first_appear_year': '1947',
    'affiliation': 'jl',
    'meta': 'yes'
}
df = dafns.insert_row(df, 1, new_row)
```

### `update_cell()`

Update a DataFrame cell value.

`dafns.update_cell(df, 4, 'affiliation', 'bop')`

## Delete

### `drop_cols()`

Drop a DataFrame column(s).

```python
cols_to_drop = ['meta']
df = dafns.drop_cols(df, cols_to_drop)
```

### `drop_rows()`

Drop a DataFrame row(s).

```python
rows_to_drop = [1, 3]
df = dafns.drop_rows(df, rows_to_drop)
```