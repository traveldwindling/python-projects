'''
A collection of custom data analysis-oriented functions.

Functions:

    ##############
    # Formatting #
    ##############
    * format_line() - Output a formatted line.

    ###################
    # File Management #
    ###################
    * import_file() - Import a .csv, .xls, .xlsx, .xlsm, .xlsb, .odf, .ods, or
                      .odt file.
    * export_file() - Export a .csv, .xls, .xlsx, .xlsm, .xlsb, .odf, .ods, or
                      .odt file.
    * files() - Return absolute file paths for files of a specific type
                in a provided directory.
    * import_files() - Create and return a dictionary of DataFrames from
                       provided file paths.

    #############
    # Summaries #
    #############
    * df_info() - Display basic information about a DataFrame.
    * uni_val_counts() - Display the unique value counts and
                         proportions for columns of interest.

    #################
    # Preprocessing #
    #################
    * std_col_names() - Standardize DataFrame column names.
    * lower_vals() - Convert column values to lowercase.
    * data_type_convert() - Convert old data types to new data types.
    * extract_dt() - Extract date/time attribute from datetime column.
    * cols_missing() - Display the missing value counts and proportions
                       for a DataFrame.
    * dup_rows() - Display information on a DataFrame's duplicate rows.
    * week_start_end() - Return the week expressed as a beginning and
                         ending day (e.g., '09/17-09/23').

    ################
    # Calculations #
    ################
    * des_stats() - Display descriptive statistics for specified
                    DataFrame columns.
    * display_modes() - Display modes for specified DataFrame columns.
    * calc_variance() - Calculate variance.
    * group_calcs() - Create groups and perform calculations.

    ############
    # Outliers #
    ############
    * outlier_info() - Display outlier counts and proportions.

    ############
    # Plotting #
    ############
    * graph_meta() - Set graph metadata.
    * bar_plots() - Display bar graphs for specified columns via subplots.
    * hist_plots() - Display histograms for specified columns via subplots.
    * box_plots() - Display box plots for specified columns via subplots.
    * stacked_bar() - Display a stacked bar plot.

    ##########
    # Create #
    ##########
    * create_ser() - Create a Series.
    * create_df() - Create a DataFrame.
    * concat_objs() - Concatenate objects.
    * merge_objs() - Merge objects.
    * gen_rnd_df() - Generate a DataFrame of random data.

    ########
    # Read #
    ########
    * query_df() - Filter a DataFrame with a custom query.
    * sort_obj() - Sort an object by columns.

    ##########
    # Update #
    ##########
    * rename_cols() - Rename DataFrame columns.
    * add_col() - Add a column to a DataFrame.
    * insert_col() - Insert a column into a DataFrame.
    * add_row() - Add a row to a DataFrame.
    * insert_row() - Insert a row into a DataFrame.
    * update_cell() - Update a DataFrame cell value.

    ##########
    # Delete #
    ##########
    * drop_cols() - Drop a DataFrame column(s).
    * drop_rows() - Drop a DataFrame row(s).

Requires
----------------
matplotlib
    Matplotlib is a comprehensive library for creating static, animated,
    and interactive visualizations in Python.
numpy
    NumPy is the fundamental package for array computing with Python.
pandas
    Powerful data structures for data analysis, time series, and statistics.
'''
# Enable context manager for temporarily redirecting sys.stdout
# to another file or file-like object
from contextlib import redirect_stdout
import datetime as dt  # Enable classes for manipulating dates and times
# Enable object to represent the difference between two dates or times
from datetime import timedelta
# Enable finding all the pathnames matching a specified pattern according
# to the rules used by the Unix shell
import glob
from io import StringIO  # Enable a text stream using an in-memory text buffer
# Enable a portable way of using operating system dependent functionality
import os
import re  # Enable regular expression matching operations
# Enable a standard interface to extract, format, and print stack traces
import traceback

from IPython.display import display  # Enable display tools
# Enable class that contains most figure elements
from matplotlib.axes import Axes
import matplotlib.pyplot as plt  # Enable state-based interface to matplotlib
import numpy as np  # Enable multi-dimensional arrays and matrices
import pandas as pd  # Enable data structures and data analysis tools


def format_line(values: list[str], format_spec: str = '') -> None:
    '''
    Output a formatted line.

    Parameters
    ----------------
    values : list
        List of string values to output.
    format_spec : str (optional)
        Format specification.
    '''
    output = ''  # Create output string
    # Build up output string line
    for value in values:
        if format_spec:
            output += f'{value:{format_spec}}'
        else:
            output += f'{value: ^12}'
    print(output)


def import_file(file_path: str, file_type: str = 'csv', delimiter: str = None,
                index_col: int | list[int] = None,
                sheet_name: int | list[int] = 0) \
                -> pd.core.frame.DataFrame | None:
    '''
    Import a .csv, .xls, .xlsx, .xlsm, .xlsb, .odf, .ods, or .odt file.

    Parameters
    ----------------
    file_path : str
        Absolute path of file to import.
    file_type : str (optional)
        Type of file that is being imported. Defaults to csv.
    delimiter : str (optional)
        Delimiter to use for imported csv file. Defaults to None.
    index_col : int | list (optional)
        Zero-based index of a column (or list of column integer
        indices for MultiIndex) to use as row labels. Defaults to None.
    sheet_name : int | list (optional)
        Zero-based index of a sheet (or list of sheet integer indices
        for multiple sheets) to import. Defaults to None.

    Returns
    ----------------
    pd.core.frame.DataFrame | None
        A pandas DataFrame, or None.
    '''
    # Define supported optional file types
    opt_file_types = ('xls', 'xlsx', 'xlsm', 'xlsb', 'odf', 'ods', 'odt')

    # Customize imports based on provided arguments
    if file_type == 'csv':
        try:
            df = pd.read_csv(file_path,
                             delimiter=delimiter, index_col=index_col)
        except Exception:
            err_msg = (
                f'{file_path} import failed with the following exception:\n\n'
                + traceback.format_exc()
            )
            print(err_msg)

            return None
        else:
            return df
    elif file_type in opt_file_types:
        try:
            df = pd.read_excel(file_path,
                               index_col=index_col, sheet_name=sheet_name)
        except Exception:
            err_msg = (
                f'{file_path} import failed with the following exception:\n\n'
                + traceback.format_exc()
            )
            print(err_msg)

            return None
        else:
            return df
    else:
        err_msg = (
            f'The {file_type} file type is not supported.\n\n'
            'Supported file types include:\n'
            f'{opt_file_types}'
        )
        print(err_msg)

        return None


def export_file(df: pd.core.frame.DataFrame, file_path: str,
                file_type: str = 'csv', sep: str = ',') -> None:
    '''
    Export a .csv, .xls, .xlsx, .xlsm, .xlsb, .odf, .ods, or .odt file.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    file_path : str
        Absolute path to export file to.
    file_type : str (optional)
        Type of file to export to. Defaults to csv.
    sep : str (optional)
        Delimiter for exported csv file. Defaults to a comma (,).
    '''
    # Perform path validation
    if not os.path.isabs(file_path):
        err_msg = (
            f'{file_path} is not an absolute path.\n'
            'Check the path and try again.'
        )
        print(err_msg)

        return None

    # Perform output directory validation
    if not os.path.isdir(os.path.dirname(file_path)):
        err_msg = (
            f'{os.path.dirname(file_path)} is not a valid directory.\n'
            'Check the path and try again.'
        )
        print(err_msg)

        return None

    # Define supported optional file types
    opt_file_types = ('xls', 'xlsx', 'xlsm', 'xlsb', 'odf', 'ods', 'odt')

    # Customize function calls based on provided arguments
    if file_type == 'csv':
        try:
            df.to_csv(file_path, sep=sep, index=False)
        except Exception:
            err_msg = (
                f'{file_path} export failed with the following exception:\n\n'
                + traceback.format_exc()
            )
            print(err_msg)
    elif file_type in opt_file_types:
        try:
            df.to_excel(file_path, index=False)
        except Exception:
            err_msg = (
                f'{file_path} export failed with the following exception:\n\n'
                + traceback.format_exc()
            )
            print(err_msg)
    else:
        err_msg = (
            f'The {file_type} file type is not supported.\n\n'
            'Supported file types include:\n'
            f'{opt_file_types}'
        )
        print(err_msg)


def files(files_dir: str, ext: str = 'csv') -> list[str] | None:
    '''
    Return absolute file paths for files of a specific type
    in a provided directory.

    Parameters
    ----------------
    files_dir: str
        Absolute directory path to search for files.
    ext : str (optional)
        Extension of file type to target. Defaults to csv.

    Returns
    ----------------
    list | None
        A list of absolute file paths, or None.
    '''
    # Perform directory validation
    if not (os.path.isabs(files_dir)
            and os.path.isdir(files_dir)):
        err_msg = (
            f'{files_dir} is not a valid directory path.\n'
            'Check that the directory exists and that its '
            'path is an absolute path.'
        )
        print(err_msg)

        return None

    # If current directory is not equal to the specified files directory
    if os.getcwd() != files_dir:
        os.chdir(files_dir)  # Change to files directory

    # Create regular expression pattern from ext
    pattern = '*' + ext
    # Create list of files in current directory that match the pattern
    filenames = glob.glob(pattern)

    # Add each absolute file path to abs_paths list
    abs_paths = [
        os.path.abspath(filename)
        for filename in filenames
    ]

    return abs_paths


def import_files(files: list[dict[str, str | int | list[int]]]) \
                 -> dict[str, pd.core.frame.DataFrame] | None:
    '''
    Create and return a dictionary of DataFrames from provided file paths.
    Filenames without extensions become dictionary keys.
    DataFrames become dictionary values.

    Imports .csv, .xls, .xlsx, .xlsm, .xlsb, .odf, .ods, and .odt files.

    Parameters
    ----------------
    files : list
        List of anonymous dictionaries representing files. Key/value pairs
        in each anonymous dictionary contain one or more file attributes:
        path, type, delimiter, index_col, and sheet_name.

        path is required.

    Returns
    ----------------
    dict | None
        A dictionary of DataFrames where the keys are the filenames
        the DataFrames were created from, or None.
    '''
    # Check that each file has a path specified
    path_test = [
        True
        if 'path' in file_dict.keys()
        else
        False
        for file_dict in files
    ]
    if not all(path_test):
        err_msg = (
            'Ensure that a path key is set for each file delineated in '
            'the files argument.'
        )
        print(err_msg)

        return None

    # Obtain filenames with extensions removed
    f_names_no_exts = [
        os.path.splitext(os.path.basename(file_dict['path']))[0]
        for file_dict in files
    ]

    dfs, err_msgs = {}, []

    for file_dict, f_names_no_exts in zip(files, f_names_no_exts):
        # Check for and set optional file attributes
        if 'type' in file_dict.keys():
            file_type = file_dict['type']
        else:
            file_type = 'csv'
        if 'delimiter' in file_dict.keys():
            delimiter = file_dict['delimiter']
        else:
            delimiter = None
        if 'index_col' in file_dict.keys():
            index_col = file_dict['index_col']
        else:
            index_col = None
        if 'sheet_name' in file_dict.keys():
            sheet_name = file_dict['sheet_name']
        else:
            sheet_name = 0

        f = StringIO()
        with redirect_stdout(f):
            # Customize imports based on provided arguments
            if file_type == 'csv':
                df = import_file(file_dict['path'],
                                 file_type=file_type,
                                 delimiter=delimiter,
                                 index_col=index_col)
            else:
                df = import_file(file_dict['path'],
                                 file_type=file_type,
                                 index_col=index_col,
                                 sheet_name=sheet_name)
            # If import successful, add to DataFrame dictionary
            if df is not None:
                dfs[f_names_no_exts] = df
            else:
                # Add import file error output to error message list
                err_msgs.append(f.getvalue() + '\n' + ('~' * 52))

    if err_msgs:
        proc_errs = 'Import errors occurred:\n\n'
        proc_errs += '\n\n'.join(err_msgs)
        print(proc_errs.rstrip())

    if dfs:
        return dfs
    else:
        return None


def df_info(df: pd.core.frame.DataFrame) -> None:
    '''
    Display basic information about a DataFrame.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    print(f"DataFrame Information\n{'-' * len('DataFrame Information')}")
    display(df.info(verbose=True, memory_usage=True, show_counts=True))

    print(f"\n\nDataFrame Head\n{'-' * len('DataFrame Head')}\n")
    display(df.head())

    print(f"\n\nDataFrame Tail\n{'-' * len('DataFrame Tail')}\n")
    display(df.tail())

    print(f"\n\nRandom DataFrame Rows\n{'-' * len('Random DataFrame Rows')}\n")
    if df.shape[0] > 10:
        display(df.sample(10))
    else:
        display(df.sample(df.shape[0]))


def uni_val_counts(df: pd.core.frame.DataFrame,
                   col_names: list[str]) -> None:
    '''
    Display the unique value counts and proportions for columns of interest.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    col_names : list
        List of column names.
    '''
    print('Unique Value Counts\n-------------------\n')
    for col_name in col_names:
        # Obtain column absolute value counts
        vc = df[col_name].value_counts(dropna=False)
        vc.name = 'count'  # Rename Series

        # Obtain column value proportions
        vc_props = df[col_name].value_counts(dropna=False, normalize=True)
        vc_props.name = 'proportion'

        # Concatenate Series
        vc_df = pd.concat([vc, vc_props], axis=1)

        # Display column values
        display(vc_df)
        print('\n')


def std_col_names(df: pd.core.frame.DataFrame) -> pd.core.frame.DataFrame:
    '''
    Standardize DataFrame column names.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.

    Returns
    ----------------
    pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    # Obtain DataFrame column names as a list
    og_columns = df.columns.to_list()
    # Create shallow copy of og_columns list to standardize
    std_columns = og_columns.copy()
    # Create dictionary of regular expression patterns
    # and the values to replace matches with
    patterns = {
        # Turn unwanted characters to a space
        re.compile(r'\n{2,}'): r'\n',
        re.compile(r'\n'): ' ',
        # Squeeze multiple spaces to a single space
        re.compile(' {2,}'): ' ',
        # Get rid of spaces around separator characters
        re.compile(r' *- *'): '-',
        re.compile(r' *_ *'): '_',
    }

    for i in range(len(std_columns)):
        for pattern, replacement in patterns.items():
            # Identify a match
            if pattern.search(std_columns[i]):
                # Replace a match with the associated replacement
                std_columns[i] = re.sub(pattern, replacement, std_columns[i])

    for i in range(len(std_columns)):
        # Replace column name with sanitized value
        std_columns[i] = std_columns[i].lower().strip().replace(' ', '_')

    # Create dictionary of original/standardized column names
    col_names = {
        og_column: std_column
        for og_column, std_column in zip(og_columns, std_columns)
    }

    return df.rename(columns=col_names)  # Rename DataFrame columns


def lower_vals(df: pd.core.frame.DataFrame, col_names: list[str]) -> None:
    '''
    Convert column values to lowercase.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    col_names : list
        List of column names.
    '''
    for column in col_names:
        df[column] = df[column].str.lower()


def data_type_convert(df: pd.core.frame.DataFrame,
                      col_types: dict[str, str]) -> pd.core.frame.DataFrame:
    '''
    Convert old data types to new data types.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    col_types : dict
        A dictionary where the keys are the column names to act on
        and the values are the data type to convert them to.

    Returns
    ----------------
    pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    return df.astype(col_types)


def extract_dt(ser: pd.core.series.Series,
               attr: str) -> pd.core.indexes.base.Index:
    '''
    Extract date/time attribute from datetime column.

    Parameters
    ----------------
    ser : pd.core.series.Series
        A pandas Series.
    attr : str
        The DatetimeIndex attribute to extract from the Series.

    Returns
    ----------------
    pd.core.indexes.base.Index
        A pandas Int64Index index.
    '''
    return getattr(pd.DatetimeIndex(ser), attr)


def cols_missing(df: pd.core.frame.DataFrame) -> None:
    '''
    Display the missing value counts and proportions for a DataFrame.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    # Calculate missing value sums
    nulls = df.isna().sum()
    nulls.name = 'na_count'  # Rename Series
    # Calculate missing value proportions
    nulls_props = nulls / df.shape[0]
    nulls_props.name = 'proportion'
    # Concatenate Series
    nulls_total = pd.concat([nulls, nulls_props], axis=1)
    # Filter out rows without missing values
    nulls_total = nulls_total.loc[nulls_total['na_count'] > 0]
    if df.isna().any(axis=None):
        # Display counts
        print('Missing Values\n--------------\n')
        display(nulls_total.sort_values('proportion', ascending=False))
    else:
        print('No missing values.')


def dup_rows(df: pd.core.frame.DataFrame) -> None:
    '''
    Display information on a DataFrame's duplicate rows.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    print(f'Total number of duplicate rows: {df.duplicated().sum()}')

    if df.duplicated().sum() > 0:
        print('\nDuplicate Rows\n----------------\n')
        display(df.loc[df.duplicated()])


def week_start_end(ds_obj: pd._libs.tslibs.timestamps.Timestamp) -> str:
    '''
    Return the week expressed as a beginning and ending day
    (e.g., '09/17-09/23').

    Parameters
    ----------------
    ds_obj : pd._libs.tslibs.timestamps.Timestamp
        A pandas datetime (Timestamp) object.

    Returns
    ----------------
    str
        The week expressed as a beginning and ending day.
    '''
    week_start = (ds_obj - timedelta(days=(ds_obj.isoweekday() % 7)))
    week_end = week_start + timedelta(days=6)

    week = f"{week_start.strftime('%m/%d')}-{week_end.strftime('%m/%d')}"

    return week


def des_stats(df: pd.core.frame.DataFrame, columns: list[str]) -> None:
    '''
    Display descriptive statistics for specified DataFrame columns.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    columns : list
        List of column names to display descriptive statistics for.
    '''
    for column in columns:
        print(f'Descriptive Statistics for {column}\n----------------')
        display(df[column].describe())
        print('\n\n')


def display_modes(df: pd.core.frame.DataFrame,
                  columns: list[str]) -> None:
    '''
    Display modes for specified DataFrame columns.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    columns : list
        List of column names.
    '''
    for column in columns:
        hdg_len = len(f'{column} Mode(s)')
        print(f"{column} Mode(s)\n{'-' * hdg_len}")
        display(df[column].mode())
        print('\n\n')


def calc_variance(arr_like: np.ndarray | pd.core.series.Series,
                  label: str) -> None:
    '''
    Calculate variance for arrays and array-like objects.

    The ddof parameter is set to 1 to provide the sample group variance.

    Parameters
    ----------------
    arr_like : np.ndarray | pd.core.series.Series
        A NumPy Array or pandas Series.
    label : str
        Output label for array/Series.
    '''
    print(f'{label} Variance: {np.around(np.var(arr_like, ddof=1), 2)}')


def group_calcs(df: pd.core.frame.DataFrame, group_cols: list[str],
                crit_col: str, action_funcs: list[str],
                proportions: bool = False) -> pd.core.frame.DataFrame:
    '''
    Create groups and perform grouped calculations.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    group_cols : list
        List of column names to create groups from.
    crit_col : str
        The criterion column, i.e., the column name whose values calculations
        are performed on.
    action_funcs : list
        List of function names to perform on the criterion
        column (e.g., ['count'] or ['count', 'sum', 'median']).
    proportions : bool (optional)
        To display proportions or not. Defaults to False.

    Returns
    ----------------
    pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    # Calculate values from provided groups, criterion column,
    # and action functions
    calc_values = df.groupby(group_cols)[crit_col].agg(action_funcs)

    if proportions:
        if len(group_cols) > 1:
            # Create an object of primary group totals with the same
            # dimensions as calculated values
            totals = calc_values.groupby(level=0).transform('sum')
            # Create object with proportions data
            props = calc_values / totals
            # Create an object with primary group proportion sums
            sums = props.groupby(level=0).sum()
            # Create MultiIndex from Cartesian product of primary group labels
            # and string 'total'
            sums.index = pd.MultiIndex.from_product([sums.index, ['total']])
            # Add primary group proportion sums to proportions data
            final_values = pd.concat([props, sums]).sort_index()
        else:
            # Convert calculated values data to proportions and convert
            # object to DataFrame
            final_values = pd.DataFrame(calc_values / calc_values.sum())
            # Create DataFrame of total row for criterion column
            total = pd.DataFrame([final_values.sum()], index=['total'])
            # Add total row to final DataFrame
            final_values = pd.concat([final_values, total])
    else:
        if len(group_cols) > 1:
            # Create an object with primary group sums
            sums = calc_values.groupby(level=0).sum()
            sums.index = pd.MultiIndex.from_product([sums.index, ['total']])
            # Add primary group totals to calculated values
            final_values = pd.concat([calc_values, sums]).sort_index()
        else:
            # Convert calculated values object to DataFrame
            final_values = pd.DataFrame(calc_values)
            total = pd.DataFrame([final_values.sum()], index=['total'])
            final_values = pd.concat([final_values, total])

    return pd.DataFrame(final_values)


def outlier_info(df: pd.core.frame.DataFrame, col_names: list[str],
                 df_label: str = None, prc: int = 2) -> None:
    '''
    Display outlier counts and proportions.

    By default, the low outlier threshold is set to the 25th percentile
    minus 1.5 times the interquartile range, and the high outlier threshold is
    set to the 75th percentile plus 1.5 times the interquartile range.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    col_names : list
        List of column names.
    df_label : str (optional)
        Label to set for DataFrame.
    prc : int (optional)
        Precision to use for proportions in returned f-string. Defaults to 2.
    '''
    max_sep_len = 79  # Set max header separator length

    for column in col_names:
        # Determine quartiles and IQR
        q1 = df[column].quantile(0.25)
        q3 = df[column].quantile(0.75)
        iqr = q3 - q1

        # Determine low/high threshold values
        thresh_low = q1 - (1.5 * iqr)
        thresh_high = q3 + (1.5 * iqr)

        # Determine outlier counts and proportions
        count_low = df[column].loc[df[column] < thresh_low].count()
        prop_low = count_low / df.shape[0]

        count_high = df[column].loc[df[column] > thresh_high].count()
        prop_high = count_high / df.shape[0]

        # Define title line
        title_line = f'\n{column} Outliers'
        if df_label is None:
            # Determine header length
            hdg_len = len(f'{column} Outliers')
        else:
            hdg_len = len(f'{column} Outliers ({df_label})')
            title_line += f' ({df_label})'
        if hdg_len >= max_sep_len:
            sec_sep = '-' * max_sep_len
        else:
            sec_sep = '-' * hdg_len
        # Display title line
        print(f'{title_line}\n{sec_sep}')
        # Set up and display heading line
        heading = ('Threshold', 'Count', 'Proportion')
        format_spec = ' ^14'
        format_line(heading, format_spec)
        # Display outlier lines
        print(f'{thresh_low: ^14.2f}{count_low: ^14d}{prop_low: ^14.{prc}f}')
        print(
            f'{thresh_high: ^14.2f}{count_high: ^14d}{prop_high: ^14.{prc}f}\n'
        )


def graph_meta(ax: Axes, title: str,
               x_label: str, y_label: str, fontsize: int = 10) -> None:
    '''
    Set graph metadata.

    Parameters
    ----------------
    ax : Axes
        Matplotlib Axes.
    title : str
        Graph title.
    x_label : str
        Graph x-axis label.
    y_label : str
        Graph y-axis label.
    fontsize : int (optional)
        Graph labels' font size. Defaults to 10.
    '''
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    ax.title.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.yaxis.label.set_fontsize(fontsize)


def bar_plots(df: pd.core.frame.DataFrame, group_cols: list[str],
              crit_cols: list[str], action_funcs: list[str],
              fig_size: list[int], num_rows: int, num_cols: int,
              num_plots: int, titles: list[str], x_labels: list[str],
              y_labels: list[str], bar_type: str = 'bar', fontsize: int = 10,
              proportions: bool = False) -> None:
    '''
    Display bar graphs for specified columns via subplots.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    group_cols : list
        List of column names to create groups from.
    crit_cols : list
        List of criterion columns, i.e., the column names whose values are
        plotted on the y-axes.
    action_funcs : list
        List of function names to perform on the criterion
        column (e.g., ['count'] or ['count', 'sum', 'median']).
    fig_size : list
        Total figure dimensions (width x height in inches, e.g., [14, 14]).
    num_rows : int
        Number of figure rows.
    num_cols : int
        Number of figure columns.
    num_plots : int
        Number of figure subplots.
    titles : list
        List of figure titles.
    x_labels : list
        List of x-axis labels.
    y_labels : list
        List of y-axis labels.
    bar_type : str (optional)
        Type of bar plot (e.g., barh). Defaults to bar.
    fontsize : int (optional)
        Graph label font size. Defaults to 10.
    proportions : bool (optional)
        To display proportions or not. Defaults to False.
    '''
    # Create figure and Axes
    fig, axes = plt.subplots(num_rows, num_cols,
                             figsize=fig_size, layout='constrained')

    if num_plots > 1:
        # Flatten Axes object array
        axes = axes.ravel()

    for i in range(num_plots):
        plt.subplot(num_rows, num_cols, i + 1)  # Retrieve Axes
        # Calculate y-axis values for the critical column using the provided
        # action function
        y_values = df.groupby(group_cols[i])[crit_cols[i]].agg(action_funcs[i])

        if proportions:
            # Convert absolute values to proportions
            y_values = y_values / y_values.sum()

        # Sort values based on bar plot type
        if bar_type == 'barh':
            y_values = y_values.sort_values()
        else:
            y_values = y_values.sort_values(ascending=False)

        if num_plots > 1:
            y_values.plot(kind=bar_type, ax=axes[i])  # Make bar plot
            # Set graph metadata
            graph_meta(axes[i], titles[i], x_labels[i], y_labels[i],
                       fontsize)
        else:
            y_values.plot(kind=bar_type, ax=axes)
            graph_meta(axes, titles[i], x_labels[i], y_labels[i], fontsize)


def hist_plots(df: pd.core.frame.DataFrame, columns: list[str],
               fig_size: list[int], num_rows: int, num_cols: int,
               num_plots: int, titles: list[str], x_labels: list[str],
               y_labels: list[str], x_limits: list[list[float]] = None,
               y_limits: list[list[int]] = None, bins: int = 10,
               fontsize: int = 10, proportions: bool = False) -> None:
    '''
    Display histograms for specified columns via subplots.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    columns : list
        List of column names.
    fig_size : list
        Total figure dimensions (width x height in inches, e.g., [14, 14]).
    num_rows : int
        Number of figure rows.
    num_cols : int
        Number of figure columns.
    num_plots : int
        Number of figure subplots.
    titles : list
        List of figure titles.
    x_labels : list
        List of x-axis labels.
    y_labels : list
        List of y-axis labels.
    x_limits : list (optional)
        List of lists of x-axis range values
        (e.g., [[0.0, 100.00], [0.0, 50.00], [0.0, 25.00]]).
        Defaults to None.
    y_limits : list (optional)
        List of lists of y-axis range values
        (e.g., [[0, 100], [0, 50], [0, 25]]). Defaults to None.
    bins : int (optional)
        Number of histogram bins. Defaults to 10.
    fontsize : int (optional)
        Graph label font size. Defaults to 10.
    proportions : bool (optional)
        To display proportions or not. Defaults to False.
    '''
    # Create figure and Axes
    fig, axes = plt.subplots(num_rows, num_cols,
                             figsize=fig_size, layout='constrained')

    if num_plots > 1:
        # Flatten Axes object array
        axes = axes.ravel()

    for i in range(num_plots):
        plt.subplot(num_rows, num_cols, i + 1)  # Retrieve Axes
        y_values = df.copy()[columns[i]]  # Obtain deep copy of values to plot

        # Create histogram based on provided arguments
        if proportions:
            if num_plots > 1:
                y_values.hist(
                    bins=bins,
                    weights=(np.ones_like(y_values) / y_values.size),
                    ax=axes[i]
                )
            else:
                y_values.hist(
                    bins=bins,
                    weights=(np.ones_like(y_values) / y_values.size),
                    ax=axes
                )
        else:
            if num_plots > 1:
                y_values.hist(bins=bins, ax=axes[i])
            else:
                y_values.hist(bins=bins, ax=axes)

        if x_limits:
            if num_plots > 1:
                axes[i].set_xlim(x_limits[i])
            else:
                axes.set_xlim(x_limits)
        if y_limits:
            if num_plots > 1:
                axes[i].set_ylim(y_limits[i])
            else:
                axes.set_ylim(y_limits)

        if num_plots > 1:
            # Set graph metadata
            graph_meta(axes[i], titles[i], x_labels[i], y_labels[i], fontsize)
        else:
            graph_meta(axes, titles[i], x_labels[i], y_labels[i], fontsize)


def box_plots(df: pd.core.frame.DataFrame, columns: list[str],
              fig_size: list[int], num_rows: int, num_cols: int,
              num_plots: int, titles: list[str]) -> None:
    '''
    Display box plots for specified columns via subplots.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    columns : list
        List of column names.
    fig_size : list
        Total figure dimensions (width x height in inches, e.g., [14, 14]).
    num_rows : int
        Number of figure rows.
    num_cols : int
        Number of figure columns.
    num_plots : int
        Number of figure subplots.
    titles : list
        List of figure titles.
    '''
    # Create figure and Axes
    fig, axes = plt.subplots(num_rows, num_cols,
                             figsize=fig_size, layout='constrained')

    if num_plots > 1:
        # Flatten Axes object array
        axes = axes.ravel()

    for i in range(num_plots):
        plt.subplot(num_rows, num_cols, i + 1)  # Retrieve Axes

        if num_plots > 1:
            df.boxplot(columns[i], ax=axes[i])  # Make boxplot
            axes[i].set_title(titles[i])
            axes[i].set_xticks(ticks=[1], labels=[''])  # Disable x-ticks
        else:
            df.boxplot(columns[i], ax=axes)
            axes.set_title(titles[i])
            axes.set_xticks(ticks=[1], labels=[''])


def stacked_bar(df: pd.core.frame.DataFrame, group_cols: list[str, str],
                crit_col: str, action_func: str, fig_size: list[int],
                title: str, x_label: str, y_label: str, bar_type: str = 'bar',
                fontsize: int = 10, colormap: str = 'tab20',
                proportions: bool = False) -> None:
    '''
    Display a stacked bar plot.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    group_cols : list
        List of two column names to group by. The first column becomes the
        plot's x-axis and the second column is used to create bar stacks.
    crit_col : str
        The criterion column, i.e., the column name whose values are plotted
        on the y-axis.
    action_func : str
        Name of action function to perform on the criterion column
        (e.g., 'count', 'sum, or 'mean').
    fig_size : list
        Total figure dimensions (width x height in inches, e.g., [14, 14]).
    title : str
        Figure title.
    x_label : str
        x-axis label.
    y_label : str
        y-axis label.
    bar_type : str (optional)
        Type of bar plot (e.g., barh). Defaults to bar.
    font_size : int (optional)
        Graph label font size. Defaults to 10.
    colormap : str (optional)
        Colormap to use. Defaults to tab20.
    proportions : bool (optional)
        To display proportions or not. Defaults to False.
    '''
    # Calculate values from provided groups, criterion column,
    # and action function
    calc_values = df.groupby(group_cols)[crit_col].agg(action_func)

    # Pivot the last level of the index labels
    final_values = calc_values.unstack()
    # Add primary group totals to final values
    final_values['total'] = final_values.sum(axis=1)

    # Sort bars based on primary group totals and bar plot type
    if bar_type == 'barh':
        final_values = final_values.sort_values('total')
    else:
        final_values = final_values.sort_values('total', ascending=False)

    # Remove total column
    final_values = final_values.drop('total', axis=1)

    # Obtain final sorting index
    final_srt_idx = final_values.index

    if proportions:
        # Assign numerator identifier to calc_values object
        numerator = calc_values
        # Create an object with primary group totals
        denominator = calc_values.unstack().sum(axis=1)
        # Create an object with proportions data
        final_values = (numerator / denominator).unstack()
        # Sort by final sorting index
        final_values = final_values.reindex(final_srt_idx)

    # Create figure and Axes
    fig, ax = plt.subplots(figsize=fig_size, layout='constrained')
    # Make plot
    final_values.plot(ax=ax, kind=bar_type, stacked=True, figsize=fig_size,
                      colormap=colormap)

    # Set graph metadata
    graph_meta(ax, title, x_label, y_label, fontsize)


def create_ser(ser_data: list, ser_name: str,
               index: pd.RangeIndex | pd.Index | np.ndarray = None) \
               -> pd.core.series.Series:
    '''
    Create a Series.

    Parameters
    ----------------
    ser_data : list
        List to create Series from.
    ser_name : str
        Series name.
    index : pd.RangeIndex | pd.Index | np.ndarray (optional)
        Series index. Defaults to None.

    Returns
    ----------------
    pd.core.series.Series
        A pandas Series.
    '''
    return pd.Series(ser_data, name=ser_name, index=index)


def create_df(df_data: dict[str, list],
              index: pd.RangeIndex | pd.Index | np.ndarray = None) \
              -> pd.core.frame.DataFrame:
    '''
    Create a DataFrame.

    Parameters
    ----------------
    df_data : dict
        Dictionary to create DataFrame from, where the keys are column names
        and the values are lists of column data.
    index: pd.RangeIndex | pd.Index | np.ndarray (optional)
        DataFrame index. Defaults to None.

    Returns
    ----------------
    pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    return pd.DataFrame(df_data, index=index)


def concat_objs(
    objs: list[pd.core.series.Series | pd.core.frame.DataFrame],
    axis: int = 0
) -> pd.core.series.Series | pd.core.frame.DataFrame:
    '''
    Concatenate objects.

    Parameters
    ----------------
    objs : list
        List of pandas Series or DataFrames to concatenate.
    axis : int (optional)
        Axis to concatenate on. Defaults to 0 (i.e., append rows).

    Returns
    ----------------
    pd.core.series.Series | pd.core.frame.DataFrame
        A pandas Series or DataFrame.
    '''
    return pd.concat(objs, axis=axis)


def merge_objs(df: pd.core.frame.DataFrame,
               obj: pd.core.series.Series | pd.core.frame.DataFrame,
               how: str = 'inner', on: str | list[str] = None) \
               -> pd.core.frame.DataFrame:
    '''
    Merge objects.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame that serves as the left object.
    obj : pd.core.series.Series | pd.core.frame.DataFrame
        pandas Series or DataFrame to be merged with. Serves as the right
        object.
    how : str (optional)
        Merge type. Defaults to inner.
    on : str | list (optional)
        Column name (or list of column names) to merge on.
        These columns must be present in both objects. An inner join is
        performed by default (i.e., the intersection of the columns
        in both objects). Defaults to None.

    Returns
    ----------------
    pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    return pd.merge(df, obj, how=how, on=on)


def gen_rnd_df(nulls: bool = False, dirty: bool = False) \
        -> pd.core.frame.DataFrame:
    '''
    Generate a DataFrame of random data.

    Parameters
    ----------------
    nulls : bool (optional)
        Whether returned DataFrame should contains nulls or not.
        Defaults to False.
    dirty : bool (optional)
        Whether returned DataFrame should contain dirty data or not.
        Defaults to False.

    Returns
    ----------------
    pd.core.frame.DataFrame
        A pandas DataFrame.
    '''
    dt_idx = pd.date_range(
        dt.datetime(dt.datetime.now().year - 1, 1, 1, 0, 0, 0),
        dt.datetime(dt.datetime.now().year - 1, 12, 31, 0, 0, 0),
        periods=364
    )

    if dirty:
        a = ['Alpha Alpha', 'betA betA', 'gaMMa gaMMa']
        b = ['fOo', 'BAR', 'baz', 'QuX']

        df = pd.DataFrame(
            {
                'Col 1': [np.random.choice(a) for i in range(12)],
                'col_2': [np.random.choice(b) for i in range(12)],
                'Col_3': [np.random.choice(dt_idx) for i in range(12)],
                ' COL 4': np.random.randn(12),
                '  cOl 5  ': np.random.randint(0, 12, 12)
            }
        )
    else:
        a = ['alpha', 'beta', 'gamma']
        b = ['foo', 'bar', 'baz', 'qux']

        df = pd.DataFrame(
                {
                    'a': [np.random.choice(a) for i in range(12)],
                    'b': [np.random.choice(b) for i in range(12)],
                    'c': [np.random.choice(dt_idx) for i in range(12)],
                    'd': np.random.randn(12),
                    'e': np.random.randint(0, 12, 12)
                }
            )

    if nulls:
        return df.apply(lambda x: x.sample(frac=0.75))
    else:
        return df


def query_df(df: pd.core.frame.DataFrame, df_query: list[str]) -> None:
    '''
    Filter a DataFrame with a custom query.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    df_query : list
        List of substrings that make up a custom query string.
    '''
    display(df.query(' '.join(df_query)))


def sort_obj(obj: pd.core.series.Series | pd.core.frame.DataFrame,
             col_names: list[str], ascending: bool = True) -> None:
    '''
    Sort a pandas object by columns.

    Parameters
    ----------------
    obj : pd.core.series.Series | pd.core.frame.DataFrame
        pandas Series or DataFrame to sort.
    col_names : list
        List of column names to sort by.
    ascending : bool (optional)
        To sort in ascending order or not. Defaults to True.
    '''
    display(obj.sort_values(by=col_names, ascending=ascending))


def rename_cols(df: pd.core.frame.DataFrame,
                col_names: dict[str, str]) -> pd.core.frame.DataFrame:
    '''
    Rename DataFrame columns.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        A pandas DataFrame.
    col_names : dict
        Dictionary of column names, where the keys are the old column names
        and the values are the new column names.

    Returns
    ----------------
    pd.core.frame.DataFrame
        pandas DataFrame with renamed columns.
    '''
    return df.rename(columns=col_names)


def add_col(df: pd.core.frame.DataFrame, col_name: str,
            col_values: list) -> None:
    '''
    Add a column to a DataFrame.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to add a column to.
    col_name : str
        Name of new column.
    col_values : list
        New column values.
    '''
    df[col_name] = col_values


def insert_col(df: pd.core.frame.DataFrame, ins_pos: int, col_name: str,
               col_values: int | pd.core.series.Series | np.ndarray) \
               -> None:
    '''
    Insert a column into a DataFrame.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to insert a column into.
    ins_pos : int
        Index position to insert the column at.
    col_name : str
        Name of new column.
    col_values : int | pd.core.series.Series | np.ndarray
        New column values.
    '''
    df.insert(ins_pos, col_name, col_values, allow_duplicates=True)


def add_row(df: pd.core.frame.DataFrame, row_values: list) -> None:
    '''
    Add a row to a DataFrame.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to add a row to.
    row_values : list
        Row values to add.
    '''
    df.loc[len(df.index)] = row_values


def insert_row(df: pd.core.frame.DataFrame, ins_pos: int,
               row_values: dict[str, list]) -> pd.core.frame.DataFrame:
    '''
    Insert a row into a DataFrame.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to insert a row into.
    ins_pos : int
        Position to insert row into.
    row_values : dict
        Dictionary of row values to insert, where the keys are the column names
        and the values are a list of column values.

    Returns
    ----------------
    pd.core.frame.DataFrame
        New pandas DataFrame with inserted row.
    '''
    # Create a deep copy of the upper half of the DataFrame
    df_upper = df[0:ins_pos].copy()

    # Create a deep copy of the lower half of the DataFrame
    df_lower = df[ins_pos:].copy()

    # Insert the row in the upper half of the DataFrame
    df_upper.loc[ins_pos] = row_values

    # Concatenate the two DataFrames, reset the index
    df_result = pd.concat([df_upper, df_lower]).reset_index(drop=True)

    return df_result


def update_cell(df: pd.core.frame.DataFrame, up_pos: int, col_name: str,
                cell_value: int | float | str) -> None:
    '''
    Update a DataFrame cell value.

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to update a cell in.
    up_pos : int
        Position of row with cell to update.
    col_name : str
        Name of column with cell to update.
    cell_value : int | float | str
        New cell value.
    '''
    df.loc[up_pos, col_name] = cell_value


def drop_cols(df: pd.core.frame.DataFrame,
              cols_to_drop: list[str]) -> pd.core.frame.DataFrame:
    '''
    Drop a DataFrame column(s).

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to drop column(s) from.
    cols_to_drop : list
        List of column name(s) to drop.

    Returns
    ----------------
    pd.core.frame.DataFrame
        pandas DataFrame with column(s) removed.
    '''
    return df.drop(cols_to_drop, axis=1)


def drop_rows(df: pd.core.frame.DataFrame,
              rows_to_drop: list[int]) -> pd.core.frame.DataFrame:
    '''
    Drop a DataFrame row(s).

    Parameters
    ----------------
    df : pd.core.frame.DataFrame
        pandas DataFrame to drop row(s) from.
    rows_to_drop : list
        List of row index (indices) to drop.

    Returns
    ----------------
    pd.core.frame.DataFrame
        pandas DataFrame with row(s) removed.
    '''
    return df.drop(rows_to_drop).reset_index(drop=True)
