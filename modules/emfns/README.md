# Email Functions

A collection of functions to accomplish email-related tasks.

```python
# Enable functions to accomplish email-related tasks
import emfns
```

## `crt_multipart_email()`

Create a multipart plain text email message.

```python
sender = 'max@rockatansky.org'
recipients = ['imperator@furiosa.org', 'toast@knowing.org']
subject = 'The Wasteland'
body = '''\
In this wasteland, I am the one who runs from both the living and the dead. A man reduced to a single instinct.

Survive.\
'''

msg = emfns.crt_multipart_email(sender, recipients, subject, body)
```

## `add_attachment()`

Add an attachment to a plain text email message.

```python
sender = 'the@valkyrie.org'
recipients = ['imperator@furiosa.org']
subject = 'The Green Place'
body = 'See attached.'
msg = emfns.crt_multipart_email(sender, recipients, subject, body)

att_path = '/home/vuvalini/the_green_place.md'
msg = emfns.add_attachment(att_path, msg)
```

## `send_email()`

Send a plain text email message.

```python
username = 'kazuya@mishima.org'
password = input('Enter password: ').strip()

server = 'smtp.mishima.org'

sender = username
recipients = ['heihachi@mishima.org']
subject = 'Legacy'
body = '''\
Children begin by loving their parents; after a time they judge them; rarely, if ever, do they forgive them.

- Oscar Wilde\
'''

atts = [
    '/home/kazuya/jinpachi.md',
    '/home/kazuya/kazumi.png'
]

msg = emfns.crt_multipart_email(sender, recipients, subject, body)

for att in atts:
    msg = emfns.add_attachment(att, msg)

emfns.send_email(msg, server, username, password, sender, recipients)
```