'''
A collection of functions to accomplish email-related tasks.

Functions:

    * crt_multipart_email() - Create a multipart plain text email message.
    * add_attachment() - Add an attachment to a plain text email message.
    * send_email() - Send a plain text email message.
'''
###########
# Modules #
###########
from email import encoders  # Enable convenient encoders
# Enable the base class for all the MIME-specific subclasses of Message
from email.mime.base import MIMEBase
# Enable an intermediate base class for MIME messages that are multipart
from email.mime.multipart import MIMEMultipart
# Enable a class that is used to create MIME objects of major type text
from email.mime.text import MIMEText
# Enable a portable way of using operating system dependent functionality
import os
# Enable an SMTP client session object
import smtplib
# Enable access to Transport Layer Security encryption
# and peer authentication facilities
import ssl


#############
# Functions #
#############
def crt_multipart_email(sender: str, recipients: list[str],
                        subject: str, body: str) -> MIMEMultipart:
    '''
    Create a multipart plain text email message.

    Parameters
    ----------------
    sender : str
        The sending email address.
    recipients : list
        The list of recipient email addresses.
    subject : str
        The email message's subject line.
    body : str
        The email message's body text.

    Returns
    ----------------
    MIMEMultipart
        MIME message.
    '''
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = ','.join(recipients)
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'plain'))

    return msg


def add_attachment(att_path: str, msg: MIMEMultipart) -> MIMEMultipart:
    '''
    Add an attachment to a plain text email message.

    Parameters
    ----------------
    att_path : str
        The path to the file to attach.
    msg : MIMEMultipart
        MIME message.

    Returns
    ----------------
    MIMEMultipart
        MIME message.
    '''
    with open(att_path, 'rb') as attachment:
        # Add file as application/octet-stream
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(attachment.read())

        encoders.encode_base64(part)  # Encode file in ASCII characters
        # Add header as key/value pair to attachment part
        part.add_header(
            'Content-Disposition',
            f'attachment; filename={os.path.basename(att_path)}'
        )
        msg.attach(part)  # Add attachment to email

        return msg


def send_email(msg: MIMEMultipart, server: str,
               username: str, password: str,
               sender: str, recipients: list[str],
               smtp_port: int = 465, sec_conn: bool = True) -> None:
    '''
    Send a plain text email message.

    Parameters
    ----------------
    msg : MIMEMultipart
        A multipart plain text email message.
    server : str
        Address of the SMTP server.
    username : str
        Email username to log into the SMTP server.
    password : str
        Email password to log into the SMTP server.
    sender : str
        Email address to send email from.
    recipients : list
        Email addresses to receive the email message.
    smtp_port : int (optional)
        Port used by the SMTP server. Defaults to 465.
    sec_conn : bool (optional)
        Whether initial SMTP server connection is secure or not.
        Defaults to True.
    '''
    msg_text = msg.as_string()

    context = ssl.create_default_context()

    if sec_conn:
        with smtplib.SMTP_SSL(server, smtp_port, context=context) as server:
            server.login(username, password)
            server.sendmail(sender, recipients, msg_text)
    else:
        with smtplib.SMTP(server, smtp_port) as server:
            server.starttls(context=context)

            server.login(username, password)
            server.sendmail(sender, recipients, msg_text)
