'''
A collection of functions that validate various inputs.

Functions:

    * val_url() - Validate a URL.
    * val_data_src() - Validate a data source.
    * val_delimiter() - Validate a file delimiter.
    * val_file() - Validate a file.
    * val_dir() - Validate a directory.
    * val_o_path() - Validate an absolute path to save an output file to.
    * val_regex() - Validate a regular expression.
    * val_web_page() - Validate a web page.
    * val_idx() - Validate an item index.
    * val_date_fmt() - Validate a date format.
'''
import datetime as dt  # Enable classes for manipulating dates and times
# Enable a portable way of using operating system dependent functionality
import os
# Enable representing concrete paths of the system's path flavor
from pathlib import Path
# Enable regular expression matching operations similar to those found in Perl
import re
from urllib.request import urlopen  # Enable opening of URLs


def val_url(url_pat: re.Pattern, url: str) -> bool:
    '''
    Validate a URL.

    Parameters
    ----------------
    url_pat : re.Pattern
        URL regular expression object to validate against.
    url : str
        URL to be validated.

    Returns
    ----------------
    bool
        Whether URL is a valid data source.
    '''
    return bool(url_pat.search(url))


def val_data_src(url_pat: re.Pattern, data_src: str) -> bool:
    '''
    Validate a data source.

    Parameters
    ----------------
    url_pat : re.Pattern
        URL regular expression object to validate against.
    data_src : str
        Data source to be validated. Can be a URL or local file path.

    Returns
    ----------------
    bool
        Whether data_src is a valid data source.
    '''
    return bool(url_pat.search(data_src) or os.path.isfile(data_src))


def val_delimiter(delim_pat: re.Pattern, delimiter: str) -> bool:
    '''
    Validate a file delimiter.

    Parameters
    ----------------
    delim_pat : re.Pattern
        Delimiter regular expression object to validate against.
    delimiter : str
        Delimiter to use. Must be a single symbolic character.

    Returns
    ----------------
    bool
        Whether delimiter is a valid delimiter.
    '''
    return bool(delim_pat.search(delimiter))


def val_file(file_to_val: str) -> bool:
    '''
    Validate a file.

    Parameters
    ----------------
    file_to_val : str
        File to be validated.

    Returns
    ----------------
    bool
        Whether provided file path is valid or not.
    '''
    return os.path.isfile(file_to_val)


def val_dir(dir_to_val: str) -> bool:
    '''
    Validate a directory.

    Parameters
    ----------------
    dir_to_val : str
        Directory to validate.

    Returns
    ----------------
    bool
        Whether provided directory path is valid or not.
    '''
    return os.path.isdir(dir_to_val)


def val_o_path(o_path: str) -> bool:
    '''
    Validate an absolute path to save an output file to.

    Parameters
    ----------------
    o_path : str
        Path to save output file to.

    Returns
    ----------------
    bool
        Whether provided output path is valid or not.
    '''
    o_path = Path(o_path)

    if os.path.isabs(o_path):
        return os.path.isdir(os.path.dirname(o_path))
    else:
        return False


def val_regex(pat_to_val: str) -> bool:
    '''
    Validate a regular expression.

    Parameters
    ----------------
    pat_to_val : str
        Regular expression pattern.

    Returns
    ----------------
    bool
        Whether provided regular expression pattern is valid or not.
    '''
    try:
        re.compile(pat_to_val)
        return True
    except re.error:
        return False


def val_web_page(web_page: str) -> bool:
    '''
    Validate a web page.

    Parameters
    ----------------
    web_page : str
        Web page to be validated.

    Returns
    ----------------
    bool
        Whether the web page is valid or not.
    '''
    try:
        urlopen(web_page)
    except Exception:
        return False
    else:
        return True


def val_idx(idx: int, num_of_items: int) -> bool:
    '''
    Validate an item index.

    Parameters
    ----------------
    idx : int
        Index to validate.
    num_of_items : int
        Number of items in the data structure to validate against.

    Returns
    ----------------
    bool
        Whether index is a valid item index.
    '''
    try:
        idx = abs(int(idx))
    except ValueError:
        return False
    else:
        return bool(idx < num_of_items)


def val_date_fmt(date: str, fmt: str) -> bool:
    '''
    Validate a date format.

    Parameters
    ----------------
    date : str
        Date to be validated.
    fmt: str
        Date format.

    Returns
    ----------------
    bool
        Whether the date format is valid or not.
    '''
    try:
        dt.datetime.strptime(date, fmt)
    except ValueError:
        return False
    else:
        return True
