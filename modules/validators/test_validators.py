#!/usr/bin/env python3
'''Validator function tests.'''
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable a rich set of tools for constructing and running tests
import unittest as ut

# Enable a collection of functions that validate various inputs
import validators as v

# Define URL regular expression
URL_REGEX = r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.'
URL_REGEX += r'[a-zA-Z0-9()]{1,18}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)'
# Create URL regular expression object
URL_PAT = re.compile(URL_REGEX)

# Create delimiter regular expression object
DELIM_PAT = re.compile(r'^[^a-zA-Z0-9]{1}$')


class TestValURL(ut.TestCase):
    def test_valid_url(self):
        '''Test valid URL.'''
        url = 'https://www.fsf.org/'
        self.assertTrue(v.val_url(URL_PAT, url))

    def test_invalid_url(self):
        '''Test invalid URL.'''
        url = 'https://uf<nS#Q oQ"1nB'
        self.assertFalse(v.val_url(URL_PAT, url))

    def test_long_tld(self):
        '''Test URL with long top-level domain.'''
        url = 'https://jp.starlabs.systems/'
        self.assertTrue(v.val_url(URL_PAT, url))


class TestValDataSrc(ut.TestCase):
    def test_valid_url(self):
        '''Test data source is valid URL.'''
        src = 'https://en.wikipedia.org/wiki/Timnit_Gebru'
        self.assertTrue(v.val_data_src(URL_PAT, src))

    def test_valid_file(self):
        '''Test data source is valid file.'''
        src = 'validators.py'
        self.assertTrue(v.val_data_src(URL_PAT, src))

    def test_invalid_url(self):
        '''Test data source is invalid URL.'''
        src = 'http://RS TW> 32W"AJ#P'
        self.assertFalse(v.val_data_src(URL_PAT, src))

    def test_invalid_file(self):
        '''Test data source is invalid file.'''
        src = 'TB3wnerhSw'
        self.assertFalse(v.val_data_src(URL_PAT, src))


class TestValDelimiter(ut.TestCase):
    def test_valid_delimiter(self):
        '''Test valid delimiter.'''
        self.assertTrue(v.val_delimiter(DELIM_PAT, ','))

    def test_invalid_delimiter(self):
        '''Test invalid delimiter.'''
        self.assertFalse(v.val_delimiter(DELIM_PAT, '&#@'))

    def test_non_symbolic_character(self):
        '''Test non-symbolic character.'''
        self.assertFalse(v.val_delimiter(DELIM_PAT, 't'))


class TestValFile(ut.TestCase):
    def test_valid_file(self):
        '''Test valid file.'''
        f = 'validators.py'
        self.assertTrue(v.val_file(f))

    def test_invalid_file(self):
        '''Test invalid file.'''
        f = 'f8pn1akH3Y'
        self.assertFalse(v.val_file(f))


class TestValDir(ut.TestCase):
    def test_valid_directory(self):
        '''Test valid directory.'''
        d = '../validators'
        self.assertTrue(v.val_dir(d))

    def test_invalid_directory(self):
        '''Test invalid directory.'''
        d = 'ecLJ7mdk0t'
        self.assertFalse(v.val_dir(d))


class TestValOPath(ut.TestCase):
    def test_valid_path(self):
        '''Test valid path.'''
        p = '/tmp/the_three_laws.md'
        self.assertTrue(v.val_o_path(p))

    def test_not_abs_path(self):
        '''Test not absolute path.'''
        p = 'tmp/the_three_laws.md'
        self.assertFalse(v.val_o_path(p))

    def test_invalid_path(self):
        '''Test invalid path.'''
        p = '/tmp/brau1589/the_three_laws.md'
        self.assertFalse(v.val_o_path(p))


class TestValRegex(ut.TestCase):
    def test_valid_regex(self):
        '''Test valid regular expression.'''
        r = r'[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+'
        self.assertTrue(v.val_regex(r))

    def test_invalid_regex(self):
        '''Test invalid regular expression.'''
        r = r'['
        self.assertFalse(v.val_regex(r))


class TestValWebpage(ut.TestCase):
    def test_valid_webpage(self):
        '''Test valid webpage.'''
        w = 'https://en.wikipedia.org/wiki/Chelsey_Glasson'
        self.assertTrue(v.val_web_page(w))

    def test_invalid_webpage(self):
        '''Test invalid webpage.'''
        w = 'https://WpU D#fzH2"CT'
        self.assertFalse(v.val_web_page(w))


class TestValIdx(ut.TestCase):
    def test_valid_index(self):
        '''Test valid index.'''
        idx, num_of_items = 0, len([1, 2, 3])
        self.assertTrue(v.val_idx(idx, num_of_items))

    def test_invalid_index(self):
        '''Test invalid index.'''
        idx, num_of_items = 3, len([1, 2, 3])
        self.assertFalse(v.val_idx(idx, num_of_items))


class TestValDateFmt(ut.TestCase):
    def test_valid_date_fmt(self):
        '''Test valid date format.'''
        date, fmt = '1986/05/13', '%Y/%m/%d'
        self.assertTrue(v.val_date_fmt(date, fmt))

    def test_invalid_date_fmt(self):
        '''Test invalid date format.'''
        date, fmt = '13-05-1986', '%Y/%m/%d'
        self.assertFalse(v.val_date_fmt(date, fmt))


if __name__ == '__main__':
    ut.main()
