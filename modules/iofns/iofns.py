'''
A collection of I/O-oriented functions.

Functions:

    * crt_dt_stamp() - Create a date/time stamp.
    * cvt_dt_to_str() - Convert a date/time object to a date/time string.
    * cvt_str_to_dt() - Convert a date/time string to a date/time object.
    * crt_timedelta() - Create a timedelta object.
    * get_weekday() - Get weekday name as a string.
    * get_mon_rng() - Get first day and number of days in a month.
    * get_months() - Get months for a date range.
    * clean_name() - Create a clean object name.
    * list_dir() - Return lists of directory object names and absolute paths.
    * crt_dir() - Create a directory if it does not exist.
    * wipe_dir() - Wipe a directory's contents.
    * wr_file() - Write a text file.
    * rd_file() - Read a text file.
    * wr_csv() - Write a CSV file.
    * rd_csv() - Read a CSV file.
    * get_worksheets() - Return a dictionary of worksheets for a workbook.
    * crt_txt_stm() - Create a text stream using an in-memory text buffer.
    * wr_json() - Write JSON content.
    * rd_json() - Read JSON content.
    * rd_toml() - Read TOML content.
    * crt_tmp_f_obj() - Create a temporary file-like object.
    * crt_tmp_dir_obj() - Create a temporary directory-like object.
    * crt_zip_arc() - Create a ZIP archive.
    * get_arc_names() - Get ZIP archive member names.
    * extr_zip_arc() - Extract a ZIP archive.
    * shelve_obj() - Shelve Python objects.
    * wr_pkl() - Write pickled content.
    * rd_pkl() - Read pickled content.
    * set_perms() - Set permissions for a file system object
                    as the current user.

Requires
----------------
odfpy
    Python API and tools to manipulate OpenDocument files
openpyxl
    A Python library to read/write Excel 2010 xlsx/xlsm files
pandas
    Powerful data structures for data analysis, time series, and statistics
'''
###########
# Modules #
###########
import calendar as cal  # Enable useful functions related to the calendar
# Enable classes to read and write tabular data in CSV format
import csv
import datetime as dt  # Enable classes for manipulating dates and times
# Enable a duration expressing the difference between two date, time,
# or datetime instances
from datetime import timedelta as td
from io import StringIO  # Enable text stream using an in-memory text buffer
# Enable a lightweight data interchange format inspired by
# JavaScript object literal syntax
import json
# Enable functions and classes that implement an event logging system
# Enable a portable way of using operating system dependent functionality
import os
# Enable binary protocols for serializing and de-serializing
# a Python object structure
import pickle as pkl
# Enable regular expression matching operations similar to those found in Perl
import re
import shelve  # Enable a persistent, dictionary-like object
from shlex import split  # Enable splitting a string using shell-like syntax
# Enable a number of high-level operations on files and collections of files
import shutil
# Enable spawning of new processes, connecting to their input/output/error
# pipes, and obtaining of their return codes
import subprocess as sp
# Enable creating temporary files and directories
import tempfile
import tomllib  # Enable an interface for parsing TOML
from typing import Any  # Enable special kind of type
# Enable ZIP compression and ZipFile objects
from zipfile import ZIP_DEFLATED, ZipFile

import pandas as pd  # Enable data structures and data analysis tools


#############
# Functions #
#############
def crt_dt_stamp(dt_fmt: str = '%Y-%m-%d', date_only: bool = False,
                 time_only: bool = False) -> str:
    '''
    Create a date/time stamp.

    Parameters
    ----------------
    dt_fmt : str (optional)
        A date/time format string. Defaults to %Y-%m-%d-%s.
    date_only : bool (optional)
        Flag to indicate return of a date stamp in the format
        of %Y-%m-%d. Defaults to False.
    time_only : bool (optional)
        Flag to indicate return of a time stamp in the format
        of %s. Defaults to False.

    Returns
    ----------------
    str
        A date/time stamp.
    '''
    if date_only and time_only:
        raise Exception('date_only and time_only are mutually exclusive.')

    if date_only:
        return dt.datetime.now().strftime('%Y-%m-%d')

    if time_only:
        return f'{dt.datetime.now().timestamp():.0f}'

    if dt_fmt == '%Y-%m-%d':
        return (
            dt.datetime.now().strftime(
                f'{dt_fmt}-{dt.datetime.now().timestamp():.0f}'
            )
        )
    else:
        return dt.datetime.now().strftime(dt_fmt)


def cvt_dt_to_str(dt_obj: 'dt.datetime | dt.datetime.date | dt.datetime.time',
                  dt_fmt: str) -> str:
    '''
    Convert a date/time object to a date/time string.

    https://docs.python.org/library/datetime.html#strftime-and-strptime-format-codes

    Parameters
    ----------------
    dt_obj : dt.datetime | dt.datetime.date | dt.datetime.time
        A date/time object.
    dt_fmt : str
        Output date/time format.

    Returns
    ----------------
    str
        A date/time string.
    '''
    return dt_obj.strftime(dt_fmt)


def cvt_str_to_dt(dt_str: str, dt_fmt: str) \
        -> 'dt.datetime | dt.datetime.date | dt.datetime.time':
    '''
    Convert a date/time string to a date/time object.

    https://docs.python.org/library/datetime.html#strftime-and-strptime-format-codes

    Parameters
    ----------------
    dt_str : str
        A date/time string.
    dt_fmt : str
        Input date/time format.

    Returns
    ----------------
    dt.datetime | dt.datetime.date | dt.datetime.time
        A date/time object.
    '''
    return dt.datetime.strptime(dt_str, dt_fmt)


def crt_timedelta(weeks: int = 0, days: int = 1, hours: int = 0,
                  minutes: int = 0, seconds: int = 0) -> td:
    '''
    Create a timedelta object.

    Parameters
    ----------------
    weeks : int (optional)
        Number of weeks. Defaults to 0.
    days : int (optional)
        Number of days. Defaults to 1.
    hours : int (optional)
        Number of hours. Defaults to 0.
    minutes : int (optional)
        Number of minutes. Defaults to 0.
    seconds : int (optional)
        Number of seconds. Defaults to 0.

    Returns
    ----------------
    td
        A timedelta object.
    '''
    return td(weeks=weeks, days=days, hours=hours,
              minutes=minutes, seconds=seconds)


def get_weekday(year: int, month: int, day: int,
                day_abbr: bool = False) -> str:
    '''
    Get weekday name as a string.

    Parameters
    ----------------
    year : int
        Year of day.
    month : int
        Month of day.
    day : int
        Day of the week.
    day_abbr : bool (optional)
        Whether or not to provide weekday name as an abbreviation.
        Defaults to False.

    Returns
    ----------------
    str
        Day of the week.
    '''
    if day_abbr:
        weekday_names = list(cal.day_abbr)
    else:
        weekday_names = list(cal.day_name)

    return weekday_names[cal.weekday(year, month, day)]


def get_mon_rng(year: int, month: int,
                day_abbr: bool = False) -> tuple[int, str, int]:
    '''
    Get first day and number of days in a month.

    The first day of the month is returned as both an integer
    (where the week starts at Monday as 0) and as a string name.

    Parameters
    ----------------
    year : int
        Year of month.
    month : int
        Month of the year.
    day_abbr : bool (optional)
        Whether or not to provide first day of month string name
        as an abbreviation. Defaults to False.

    Returns
    ----------------
    tuple
        First day of month as integer, first day of month as string name,
        number of days in the month.
    '''
    if day_abbr:
        weekday_names = list(cal.day_abbr)
    else:
        weekday_names = list(cal.day_name)

    weekday_num, num_of_days = cal.monthrange(year, month)

    weekday_name = weekday_names[weekday_num]

    return weekday_num, weekday_name, num_of_days


def get_months(start_date: str, end_date: str, date_fmt: str,
               mon_fmt: str = '%b-%Y') -> list[str]:
    '''
    Get months for a date range.

    Parameters
    ----------------
    start_date : str
        The start date.
    end_date : str
        The end date.
    date_fmt : str
        The date format.
    mon_fmt : str (optional)
        The months' output format.

    Returns
    ----------------
    list
        Months in the specified date range.
    '''
    s_date = dt.datetime.strptime(start_date, date_fmt)
    e_date = dt.datetime.strptime(end_date, date_fmt)

    prd_idx = pd.period_range(start=s_date, end=e_date, freq='M')
    months = [
        period.strftime(mon_fmt)
        for period in prd_idx
    ]

    return months


def clean_name(name_pat: re.Pattern, obj_name: str) -> str:
    '''
    Create a clean object name.

    Parameters
    ----------------
    name_pat : re.Pattern
        Name regular expression object that denotes which characters should be
        replaced with an underscore.
    obj_name : str
        String to clean and use as a filename.

    Returns
    ----------------
    str
        Cleaned string to use as a filename.
    '''
    # Replace most non-alphanumeric characters with underscores
    obj_name = re.sub(name_pat, '_', obj_name)
    # Squeeze underscores, lowercase name, strip leading/ending underscores
    obj_name = re.sub(r'([_])(\1+)', r'\1', obj_name.lower()).strip('_')

    return obj_name


def list_dir(i_dir: str) -> tuple[list[str], list[str]]:
    '''
    Return lists of directory object names and absolute paths.

    Parameters
    ----------------
    i_dir : str
        Path of directory to get listing from.

    Returns
    ----------------
    tuple
        Lists of directory object names and absolute paths.
    '''
    clg_dir = os.getcwd()  # Obtain current working directory

    os.chdir(i_dir)  # Change to provided directory

    obj_names = os.listdir(i_dir)  # Get object names
    # Get object absolute paths
    obj_abs_paths = [os.path.abspath(obj) for obj in obj_names]
    os.chdir(clg_dir)  # Change back to calling directory

    return obj_names, obj_abs_paths


def crt_dir(dir_to_crt: str) -> None:
    '''
    Create a directory if it does not exist.

    Parameters
    ----------------
    dir_to_crt : str
        Directory to create.
    '''
    if not os.path.isdir(dir_to_crt):
        os.makedirs(dir_to_crt)


def wipe_dir(dir_path: str) -> None:
    '''
    Wipe a directory's contents.

    Parameters
    ----------------
    dir_path : str
        Directory path to wipe.
    '''
    with os.scandir(dir_path) as objs:
        for obj in objs:
            if obj.is_file() or obj.is_symlink():
                os.unlink(obj.path)
            else:
                shutil.rmtree(obj.path)


def wr_file(f_path: str, data: str, encoding: str = 'utf-8',
            append: bool = False) -> None:
    '''
    Write a text file.

    Parameters
    ----------------
    f_path : str
        Path of the text file to write.
    data : str
        Data to write to the text file.
    encoding : str (optional)
        Encoding of the text file to write. Defaults to utf-8.
    append : bool (optional)
        Whether to append text or not. Defaults to False.
    '''
    if append:
        with open(f_path, 'a', encoding=encoding, newline='') as f:
            f.write(data)
    else:
        with open(f_path, 'w', encoding=encoding, newline='') as f:
            f.write(data)


def rd_file(f_path: str, encoding: str = 'utf-8') -> str:
    '''
    Read a text file.

    Parameters
    ----------------
    f_path : str
        Path of the text file to read.
    encoding : str (optional)
        Encoding of the text file to read. Defaults to utf-8.

    Returns
    ----------------
    str
        The text of the read file as a single string.
    '''
    with open(f_path, 'r', encoding=encoding, newline='') as f:
        data = f.read()

    return data


def wr_csv(f_path: str, data: list[tuple], encoding: str = 'utf-8',
           delimiter: str = '|', append: bool = False) -> None:
    '''
    Write a CSV file.

    Parameters
    ----------------
    f_path : str
        Path of the CSV file to write.
    data : list
        Data to write to the CSV file.
    encoding : str (optional)
        Encoding of the CSV file to write. Defaults to utf-8.
    delimiter : str (optional)
        Delimiter to use in the CSV file to read. Defaults to a |.
    append : bool (optional)
        Whether to append data or not. Defaults to False.
    '''
    if append:
        with open(f_path, 'a', encoding=encoding, newline='') as f:
            f_csv = csv.writer(f, delimiter=delimiter)
            f_csv.writerows(data)
    else:
        with open(f_path, 'w', encoding=encoding, newline='') as f:
            f_csv = csv.writer(f, delimiter=delimiter)
            f_csv.writerows(data)


def rd_csv(f_path: str, encoding: str = 'utf-8', delimiter: str = '|',
           headings: bool = True) -> tuple[tuple, list[tuple]] | list[tuple]:
    '''
    Read a CSV file.

    Parameters
    ----------------
    f_path : str
        Path of the CSV file to read.
    encoding : str (optional)
        Encoding of the CSV file to read. Defaults to utf-8.
    delimiter : str (optional)
        Delimiter to use in the CSV file to read. Defaults to a |.
    headings : bool (optional)
        Whether the CSV file to read contains a headings line
        or not. Defaults to True.

    Returns
    ----------------
    tuple | list
        Either a tuple of the headings tuple and rows list, or the rows list.
    '''
    if headings:
        with open(f_path, 'r', encoding=encoding, newline='') as f:
            f_csv = csv.reader(f, delimiter=delimiter)
            headings = tuple(next(f_csv))
            rows = [tuple(row) for row in f_csv]

        return headings, rows
    else:
        with open(f_path, 'r', encoding=encoding, newline='') as f:
            f_csv = csv.reader(f, delimiter=delimiter)
            rows = [tuple(row) for row in f_csv]

        return rows


def get_worksheets(f_path: str) -> dict[int, str]:
    '''
    Return a dictionary of worksheets for a workbook. The key is
    the worksheet numerical index and the value is the worksheet name.

    Parameters
    ----------------
    f_path : str
        Path of the workbook.

    Returns
    ----------------
    dict
        Dictionary of worksheet indices and names.
    '''
    wb = pd.ExcelFile(f_path)
    worksheets = dict(enumerate(wb.sheet_names))

    return worksheets


def crt_txt_stm(text: str) -> StringIO:
    '''
    Create a text stream using an in-memory text buffer.

    Parameters
    ----------------
    text : str
        Text to use to create a text stream.

    Returns
    ----------------
    StringIO
        A text stream.
    '''
    return StringIO(text)


def wr_json(data: str, json_file: bool = False, f_path: str = '',
            encoding: str = 'utf-8') -> str | None:
    '''
    Write JSON content.

    Parameters
    ----------------
    data : str
        Data to write to JSON.
    json_file : bool (optional)
        Whether f_path is provided or not. Defaults to False.
    f_path : str (optional)
        Path of the JSON file to write.
    encoding : str (optional)
        Encoding of the JSON file to write. Defaults to utf-8.

    Returns
    ----------------
    str | None
        Either a JSON encoded string or None.
    '''
    if json_file:
        with open(f_path, 'w', encoding=encoding, newline='') as f:
            json.dump(data, f)

        return None
    else:
        json_cont = json.dumps(data)

        return json_cont


def rd_json(json_cont: str, json_file: bool = True, encoding: str = 'utf-8') \
        -> dict | list | str | int | float | bool | None:
    '''
    Read JSON content.

    Parameters
    ----------------
    json_cont : str
        JSON content to read. Either a file path to a JSON file,
        or a JSON-encoded string.
    json_file : bool (optional)
        Whether json_cont is a file path or not. Defaults to True.
    encoding : str (optional)
        Encoding of the JSON file to read. Defaults to utf-8.

    Returns
    ----------------
    dict | list | str | int | float | bool | None
        The JSON content converted to a Python object.
    '''
    if not json_file:
        data = json.loads(json_cont)
    else:
        with open(json_cont, 'r', encoding=encoding, newline='') as f:
            data = json.load(f)

    return data


def rd_toml(toml_cont: str, toml_file: bool = True) -> dict:
    '''
    Read TOML content.

    Parameters
    ----------------
    toml_cont : str
        TOML content to read. Either a file path to a TOML file,
        or a TOML-encoded string.
    toml_file : bool (optional)
        Whether toml_cont is a file path or not. Defaults to True.

    Returns
    ----------------
    dict
        The TOML content converted to a Python dictionary.
    '''
    if not toml_file:
        cfg = tomllib.loads(toml_cont)
    else:
        with open(toml_cont, 'rb') as f:
            cfg = tomllib.load(f)

    return cfg


def crt_tmp_f_obj(prefix: str = '', suffix: str = '', dir: str = '') \
        -> tempfile._TemporaryFileWrapper:
    '''
    Create a temporary file-like object.

    Parameters
    ----------------
    prefix : str (optional)
        Prefix to begin temporary file name with.
    suffix : str (optional)
        suffix to end temporary file name with.
    dir : str (optional)
        Directory to create temporary file in.

    Returns
    ----------------
    tempfile._TemporaryFileWrapper
        A temporary file-like object.
    '''
    return tempfile.NamedTemporaryFile(
        'w+t',
        newline='',
        delete=False,
        prefix=prefix,
        suffix=suffix,
        dir=dir
    )


def crt_tmp_dir_obj(prefix: str = '', suffix: str = '', dir: str = '') \
        -> tempfile.TemporaryDirectory:
    '''
    Create a temporary directory-like object.

    Parameters
    ----------------
    prefix : str (optional)
        Prefix to begin temporary directory name with.
    suffix : str (optional)
        suffix to end temporary directory name with.
    dir : str (optional)
        Directory to create temporary directory in.

    Returns
    ----------------
    tempfile.TemporaryDirectory
        A temporary directory-like object.
    '''
    return tempfile.TemporaryDirectory(prefix=prefix, suffix=suffix, dir=dir)


def crt_zip_arc(obj: str, f_path: str) -> None:
    '''
    Create a ZIP archive.

    Parameters
    ----------------
    obj : str
        Path of object to archive.
    f_path : str
        Path of zip archive to create.
    '''
    if os.path.isfile(obj):
        dir_path = os.path.dirname(obj)
        f_name = os.path.basename(obj)

        with ZipFile(f_path, 'a', ZIP_DEFLATED) as z:
            z.write(
                os.path.join(dir_path, f_name),
                # Enable zipping a directory from any working directory
                # without having absolute paths in the archive
                os.path.relpath(os.path.join(dir_path, f_name),
                                os.path.join(obj, '..'))
            )
    elif os.path.isdir(obj):
        with ZipFile(f_path, 'a', ZIP_DEFLATED) as z:
            for dir_path, sub_dir_names, f_names in os.walk(obj):
                for f_name in f_names:
                    z.write(
                        os.path.join(dir_path, f_name),
                        os.path.relpath(os.path.join(dir_path, f_name),
                                        os.path.join(obj, '..'))
                    )


def get_arc_names(f_path: str) -> list[str]:
    '''
    Get ZIP archive member names.

    Parameters
    ----------------
    f_path : str
        Path of zip archive to get member names from.

    Returns
    ----------------
    list
        List of ZIP archive member names.
    '''
    return ZipFile(f_path).namelist()


def extr_zip_arc(f_path: str, o_dir: str = '.', f_name: str = None) -> None:
    '''
    Extract a ZIP archive.

    Parameters
    ----------------
    f_path : str
        Path of zip archive to extract.
    o_dir : str (optional)
        Path of output directory. Defaults to current directory.
    f_name : str (optional)
        Name of file to extract from the zip archive. Defaults to None.
    '''
    if f_name:
        ZipFile(f_path).extract(f_name, o_dir)
    else:
        ZipFile(f_path).extractall(o_dir)


def shelve_obj(f_path: str, key_objs: dict[str, Any]) -> None:
    '''
    Shelve Python objects.

    Parameters
    ----------------
    f_path : str
        File path of shelf file to create or append.
    key_objs : dict
        Dictionary of keys and objects to add to the shelf file.
    '''
    with shelve.open(f_path) as s:
        for key, obj in key_objs.items():
            s[key] = obj


def wr_pkl(data: Any, f_path: str = '') -> str | None:
    '''
    Write pickled content.

    Parameters
    ----------------
    data : Any
        Data to pickle.
    f_path : str (optional)
        Path of the pickled file to write.

    Returns
    ----------------
    str | None
        Pickled string or None.
    '''
    if f_path:
        with open(f_path, 'wb') as f:
            pkl.dump(data, f)

        return None
    else:
        obj = pkl.dumps(data)

        return obj


def rd_pkl(pkl_content: str) -> Any:
    '''
    Read pickled content.

    Parameters
    ----------------
    pkl_content : str
        Pickled content to read. Either a file path to a pickled file,
        or a pickled string.

    Returns
    ----------------
    Any
        The unpickled object.
    '''
    if os.path.isfile(pkl_content):
        with open(pkl_content, 'rb') as f:
            obj = pkl.load(f)
    elif isinstance(pkl_content, bytes):
        obj = pkl.loads(pkl_content)

    return obj


def set_perms(num_perms: int, obj_path: str, recursive: bool = False) -> None:
    '''
    Set permissions for a file system object as the current user.

    Parameters
    ----------------
    num_perms : int
        Numeric mode permission bits.
    obj_path : str
        Object path.
    recursive : bool (optional)
        Whether the permissions command should act recursively or not.
        Defaults to False.
    '''
    if recursive:
        cmd = f"chmod -R {num_perms} '{obj_path}'"
    else:
        cmd = f"chmod {num_perms} '{obj_path}'"
    sp.run(split(cmd))
