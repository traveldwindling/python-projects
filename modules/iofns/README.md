# I/O Functions

A collection of I/O-oriented functions.

```python
# Enable collection of I/O-oriented functions
import iofns
```

<details>
  <summary><strong>Table of Contents</strong></summary>

[TOC]

</details>

## `crt_dt_stamp()`

Create a date/time stamp.

```python
# Date/time stamp
dt_stamp = iofns.crt_dt_stamp()

# Date/time stamp (custom format)
dt_stamp_custom = iofns.crt_dt_stamp(dt_fmt='%m/%d/%Y %H:%M')

# Date stamp
date_stamp = iofns.crt_dt_stamp(date_only=True)

# Time stamp
time_stamp = iofns.crt_dt_stamp(time_only=True)
```

## `cvt_dt_to_str()`

Convert a date/time object to a date/time string.

```python
import datetime as dt  # Enable classes for manipulating dates and times

dt_str = iofns.cvt_dt_to_str(dt.datetime.now(), '%m/%Y')
```

## `cvt_str_to_dt()`

Convert a date/time string to a date/time object.

`dt_obj = iofns.cvt_str_to_dt('09/2023', '%m/%Y')`

## `crt_timedelta()`

Create a timedelta object.

`td_obj = iofns.crt_timedelta(days=6640)`

## `get_weekday()`

Get weekday name as a string.

`wd_name = iofns.get_weekday(2005, 9, 14)`

## `get_mon_rng()`

Get first day and number of days in a month.

`wd_num, wd_name, num_of_days = iofns.get_mon_rng(2009, 4)`

## `get_months()`

Get months for a date range.

```python
start_date = '09/05/2003'
end_date = '04/04/2009'
date_fmt = '%m/%d/%Y'

months = iofns.get_months(start_date, end_date, date_fmt)
```

## `clean_name()`

Create a clean object name.

```python
name_regex = r'[^a-zA-Z0-9]'
name_pat = re.compile(name_regex)

sicily = iofns.clean_name(name_pat, 'Scosso Vive 2016')
```

## `list_dir()`

Return lists of directory object names and absolute paths.

`obj_names, obj_abs_paths = iofns.list_dir('/home/aspic/i_remember_siri')`

## `crt_dir()`

Create a directory if it does not exist.

```python
dir_to_crt = '/home/rufus/shinra'
iofns.crt_dir(dir_to_crt)
```

## `wipe_dir()`

Wipe a directory's contents.

```python
dir_to_wipe = '/home/rufus/jenova'
iofns.wipe_dir(dir_to_wipe)
```

## `wr_file()`

Write a text file.

```python
# Write
f_path = '/home/avalanche/reunion.md'
data = (
    '> The Shinra Electric Power Company discovered a way to use the '
    'Lifestream as an energy source. '
    "Because of Shinra's energy, we were able to live very comfortable lives. "
    "But wasn't that because we were taking away from the planet's life? "
    'A lot of people thought so.'
    '\n'
    '\n'
    '> There was one SOLDIER named Sephiroth who was better than the rest. '
    'But when he found out about the terrible experiments that made him, '
    'he began to hate Shinra. '
    'And then, over time, he began to hate everything.'
    '\n'
    '\n'
    '> In the end, the planet itself had to make the battles stop for good. '
    'The planet used the Lifestream as a weapon, and when it '
    'burst out of the Earth, all the fighting, all the greed and sadness, '
    'everything was washed away.\n'
    '>\n'
    '> Sadness was the price to see it end.'
)
iofns.wr_file(f_path, data)

# Append
f_path = '/home/avalanche/reunion.md'
data = (
    '\n\n'
    '> Tell me what you treasure most. '
    'Give me the pleasure of taking it away.\n'
    '>\n'
    "> I pity you. You just don't get it at all. "
    "There's not a thing I don't cherish."
)
iofns.wr_file(f_path, data, append=True)
```

## `rd_file()`

Read a text file.

```python
f_path = '/home/avalanche/reunion.md'
advent_children = iofns.rd_file(f_path)
```

## `wr_csv()`

Write a CSV file.

```python
# Write
f_path = '/home/carl_sagan/who_speaks_for_earth.csv'
data = [
    ('line_num', 'line_text'),
    ('1',
     'We revile the Conquistadors for their '
     'cruelty and short-sightedness.'),
    ('2', 'For choosing death.'),
    ('3', 'We admire Lapérouse and the Tlingit for their courage and wisdom.'),
    ('4', 'For choosing life.')
]
iofns.wr_csv(f_path, data)

# Append
f_path = '/home/carl_sagan/who_speaks_for_earth.csv'
data = [
    ('5',
     'The choice is with us still, '
     'but the civilization now in jeopardy is all humanity.')
]
iofns.wr_csv(f_path, data, append=True)
```

## `rd_csv()`

Read a CSV file.

```python
f_path = '/home/carl_sagan/who_speaks_for_earth.csv'
headings, rows = iofns.rd_csv(f_path)
```

## `get_worksheets`

Return a dictionary of worksheets for a workbook.

```python
f_path = '/home/greta_thunberg/school_strike_for_climate.ods'
worksheets = iofns.get_worksheets(f_path)
```

## `crt_txt_stm()`

Create a text stream using an in-memory text buffer.

```python
american_anomie = (
    'We have no time left to be tyrannized by the practical. '
    'I have children. I may fail, but I, at least, want my children to say, '
    '''"He tried." And that's our job. '''
    'We have destroyed life for those who come after us, '
    'and we have a moral responsibility '
    'to stand up and fight on behalf of life.'
)
chris_hedges = iofns.crt_txt_stm(american_anomie)
```

## `wr_json()`

Write JSON content.

```python
# Write to a string
data = [
    'So far, how much did you lose by fighting for the climate, ',
    'to get the system to change? ',
    'Your time? Your wealth? Your reputation? Your comfort? How much?'
]
rcMgD2BwE72F = iofns.wr_json(data)

# Write to a file
f_path = '/home/rcMgD2BwE72F/ipcc.json'
data = [
    "I can't believe we're becoming fatalist without a fight. ",
    "I'd rather join the rebellion ",
    'than watch the world burn and adapt to hell.'
]
iofns.wr_json(data, json_file=True, f_path=f_path)
```

## `rd_json()`

Read JSON content.

```python
# Read from a file
json_cont = '/home/rcMgD2BwE72F/ipcc.json'
data = iofns.rd_json(json_cont)

# Read from a JSON-encoded string
data = iofns.rd_json(rcMgD2BwE72F, json_file=False)
```

## `rd_toml()`

Read TOML content.

```python
# Read from a file
toml_cont = '/home/tpw/config.toml'
cfg = iofns.rd_toml(toml_cont)

# Read from a TOML-encoded string
toml_cont = '''\
python-version = "3.12.4"
python-implementation = "CPython"\
'''
cfg = iofns.rd_toml(toml_cont, toml_file=False)
```

## `crt_tmp_f_obj()`

Create a temporary file-like object.

```python
tmp_f_obj = iofns.crt_tmp_f_obj()

# Custom prefix, suffix, and directory
tmp_f_obj = iofns.crt_tmp_f_obj(prefix='tmp_', suffix='_file', dir='/tmp')
```

## `crt_tmp_dir_obj()`

Create a temporary directory-like object.

```python
tmp_dir_obj = iofns.crt_tmp_dir_obj()

# Custom prefix, suffix, and directory
tmp_dir_obj = iofns.crt_tmp_dir_obj(prefix='tmp_', suffix='_dir', dir='/tmp')
```

## `crt_zip_arc()`

Create a ZIP archive.

```python
obj = 'princess_mononoke/'
f_path = '/home/hayao_miyazaki/princess_mononoke.zip'

iofns.crt_zip_arc(obj, f_path)
```

## `get_arc_names()`

Get ZIP archive member names.

```python
f_path = '/home/hayao_miyazaki/princess_mononoke.zip'

names = iofns.get_arc_names(f_path)
```

## `extr_zip_arc()`

Extract a ZIP archive.

```python
f_path = '/home/hayao_miyazaki/princess_mononoke.zip'

iofns.extr_zip_arc(f_path)
```

## `shelve_obj()`

```python
f_path = '/home/ashitaka/osa'
key_objs = {
    'part_1': 'This world and its people are cursed, ',
    'part_2': 'but we still wish to live.'
}

iofns.shelve_obj(f_path, key_objs)
```

## `wr_pkl()`

Write pickled content.

```python
# Pickled string
data = (
    "Under previous masters, he'd demanded his wages be given to him. "
    'They had always found ways to cheat him — '
    'charging him for his housing, his food. '
    "That's how lighteyes were. Roshone, Amaram, Katarotam..."
)
lighteyes = iofns.wr_pkl(data)

# Pickled file
f_path = '/home/brandon_sanderson/lighteyes.pickle'
data = (
    'Each lighteyes Kaladin had known, whether as a slave or a free man, '
    'had shown himself to be corrupt to the core, '
    'for all his outward poise and beauty. '
    'They were like rotting corpses clothed in beautiful silk.'
)
iofns.wr_pkl(data, f_path=f_path)
```

## `rd_pkl()`

Read pickled content.

```python
# Pickled string
obj = iofns.rd_pkl(lighteyes)

# Pickled file
pkl_content = '/home/brandon_sanderson/lighteyes.pickle'
obj = iofns.rd_pkl(pkl_content)
```

## `set_perms()`

Set permissions for a file system object as the current user.

```python
num_perms = 700
obj_path = '/home/sahad/projects/pluto.py'
iofns.set_perms(num_perms, obj_path)
```