# Templates

Python configuration/script/package templates.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Required Packages/Modules</th>
      <th>Required Third-Party Packages</th>
      <th>Variables to Set</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="default_ds/default_ds.ipynb"><code>default_ds.ipynb</code></a></td>
      <td>A Jupyter Notebook template for new data projects.</td>
      <td><a href="../modules/dafns/dafns.py"><em>dafns</em></a></td>
      <td><em>flake8</em>, <em>ipython</em>, <em>itables</em>, <em>jupyterlab</em>, <em>matplotlib</em>, <em>numpy</em>, <em>pandas</em>, <em>pyarrow</em>, <em>pycodestyle_magic</em>, <em>scipy</em>, <em>ydata_profiling</em></td>
      <td><em>PROJ_DIR</em>, <em>MOD_DIR</em>, <em>DATA_DIR</em>, <em>ASSETS_DIR</em></td>
    </tr>
    <tr>
      <td><a href="default_py/default_py.ipynb"><code>default_py.ipynb</code></a></td>
      <td>A Jupyter Notebook template to explore Python code.</td>
      <td></td>
      <td><em>flake8</em>, <em>ipython</em>, <em>jupyterlab</em>, <em>pycodestyle_magic</em></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="ex_pkg-0.0.1/">Example Package</a></td>
      <td>An example template for a CLI application Python package.</td>
      <td><em>ex_submod_2</em>, <em>ex_submod_3</em></td>
      <td><em>psutil</em></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="pythonstartup.py"><code>pythonstartup.py</code></a></td>
      <td><a href="https://docs.python.org/using/cmdline.html#envvar-PYTHONSTARTUP">Commands to execute</a> before the first prompt is displayed in interactive mode.</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="py_test.py"><code>py_test.py</code></a></td>
      <td>Template for testing Python code.</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="py_scr.py"><code>py_scr.py</code></a></td>
      <td>Python script template.</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>