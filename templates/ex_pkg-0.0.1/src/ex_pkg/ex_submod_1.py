'''\
An example template for a CLI application Python package.

A log is saved to 'ex_log_path'.

Functions:

    * parse_args() - Parse program arguments.
    * main() - Define starting point for execution of the program.

Requires
----------------
ex_submod_2
    A collection of program support functions.
ex_submod_3
    A collection of functions to obtain and manage system attributes.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
# Enable functions and classes that implement an event logging system
import logging
# Enable a portable way of using operating system dependent functionality
import os
from signal import signal, SIGINT  # Enable use of signal handlers

from ex_pkg.ex_submod_2 import hndl_sigint, init_logging
from ex_pkg.ex_submod_3 import get_sys_info

#############
# Variables #
#############
HOME = os.path.expanduser('~')  # Obtain user home directory
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

LOG_DIR = os.path.join(HOME, PGM_NAME, 'logs')  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path


#############
# Functions #
#############
def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Add custom log location option
    log_help = 'Log directory path.'
    parser.add_argument('-l', '--log-directory', type=str, help=log_help)

    # Add Antigravity Easter egg option
    ag_help = 'Antigravity Easter egg.'
    parser.add_argument('-a', '--antigravity', help=ag_help,
                        action='store_true')

    # Add Zen of Python Easter egg option
    zop_help = 'Zen of Python Easter egg.'
    parser.add_argument('-z', '--zen-of-python', help=zop_help,
                        action='store_true')

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def main() -> None:
    '''Define starting point for execution of the program.'''
    _, args = parse_args()  # Parse program arguments

    signal(SIGINT, hndl_sigint)  # Set SIGINT handler

    log_path = (
        os.path.join(args.log_directory, f'{PGM_NAME}.log')
        if args.log_directory
        else
        LOG_PATH
    )
    try:
        init_logging(log_path)  # Initialize logging
    except Exception as exc:
        err_msg = '\nLog directory creation failed with '
        err_msg += f'the following error: {exc}\n'
        print(err_msg)

        raise

    logging.debug('Starting program')

    logging.debug('Determining program output')
    if args.antigravity:
        import antigravity
    elif args.zen_of_python:
        import this
    else:
        print(get_sys_info())

    logging.debug('Ending program')
