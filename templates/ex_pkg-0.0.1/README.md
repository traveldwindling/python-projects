# Example Package

An example template for a CLI application Python package.

## Virtual Environment Creation

Create a [virtual environment](https://docs.python.org/3/tutorial/venv.html), activate it, and install `build`:

```console
$ python3 -m venv --upgrade-deps "${HOME}/venvs/ex_pkg/" &&
    . "${HOME}/venvs/ex_pkg/bin/activate" &&
    python3 -m pip install build
```

## Editable Install

Optionally, try modifying the source code of `ex_pkg` and testing the changes using an [editable install](https://pip.pypa.io/en/stable/topics/local-project-installs/#editable-installs):

`(ex_pkg) $ python3 -m pip install -e "${HOME}/ex_pkg-0.0.1/"`

## Building Distributions

When ready, build the [source distribution (sdist)](https://packaging.python.org/en/latest/glossary/#term-Source-Distribution-or-sdist) and [binary distribution (wheel)](https://packaging.python.org/en/latest/glossary/#term-Wheel):

`(ex_pkg) $ python3 -m build '/home/north_no_2/ex_pkg-0.0.1/'`

## Package Installation

Install either the sdist or [pure Python wheel](https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#pure-python-wheels):

```console
(ex_pkg) $ python3 -m pip install \
    "${HOME}/ex_pkg-0.0.1/dist/ex_pkg-0.0.1.tar.gz"
```

```console
(ex_pkg) $ python3 -m pip install \
    "${HOME}/ex_pkg-0.0.1/dist/ex_pkg-0.0.1-py3-none-any.whl"
```

## Package Execution

Run the package as a command-line interface (CLI) application:

`(ex_pkg) $ ex-pkg`

Alternatively, run the package as a module:

`(ex_pkg) $ python3 -m ex_pkg`