import calendar as cal  # Enable useful functions related to the calendar
# Enable Python interface for the 'lsprof' profiler
import cProfile
# Enable classes to read and write tabular data in CSV format
import csv
import datetime as dt  # Enable classes for manipulating dates and times
from gc import collect  # Enable a full garbage collection run
# Enable returning the text of the source code for an object
from inspect import getsource
from io import StringIO  # Enable text stream using an in-memory text buffer
# Enable access to the POSIX locale database and functionality
import locale
# Enable a portable way of using operating system dependent functionality
import os
# Enable binary protocols for serializing and de-serializing
# a Python object structure
import pickle as pkl
# Enable returning Python version and system/OS name
from platform import python_version, system
# Enable printing the formatted representation of object on stream
from pprint import pprint
from pydoc import pager as pg  # Enable string pagination
# Enable pseudo-random number generators for various distributions
import random
# Enable regular expression matching operations similar to those found in Perl
import re
from shlex import split  # Enable splitting a string using shell-like syntax
# Enable a number of high-level operations on files and collections of files
import shutil
from shutil import which  # Enable returning the path to an executable
# Enable spawning of new processes, connecting to their input/output/error
# pipes, and obtaining of their return codes
import subprocess as sp
# Enable access to some variables used/maintained by the interpreter and to
# functions that strongly interact with the interpreter
import sys
import time  # Enable various time-related functions
# Enable suspending execution of the calling thread
# for the given number of seconds
from time import sleep
# Enable a simple way to time small bits of Python code
import timeit
# Enable a standard interface to extract, format, and print stack traces
import traceback
from urllib.request import urlopen  # Enable opening of URLs
# Enable a high-level interface to allow displaying Web-based
# documents to users
import webbrowser as wb

THIRD_PARTY_IMP_STMTS = [
    'import bs4  # Enable pulling data out of HTML and XML files',
    'import dask.array as da  # Enable parallel Dask array',
    'import dask.bag as db  # Enable parallel collection of Python objects',
    'import dask.dataframe as dd  # Enable parallel pandas DataFrame',
    'from IPython.display import display  # Enable display tools',
    (
        '# Enable creating static, animated, and interactive visualizations\n'
        'import matplotlib as mpl\n'
        '# Enable state-based interface to matplotlib\n'
        'import matplotlib.pyplot as plt'
    ),
    'import numpy as np  # Enable multi-dimensional arrays and matrices',
    'import pandas as pd  # Enable data structures and data analysis tools',
    (
        '# Enable software for mathematics, science, and engineering\n'
        'from scipy import stats as st'
    ),
    'import sqlalchemy as sa  # Enable database abstraction library'
]
for imp_stmt in THIRD_PARTY_IMP_STMTS:
    try:
        exec(imp_stmt)
    except ImportError:
        pass

# Add modules directory to the search path for modules
mods_path = os.path.abspath('modules')
if os.path.isdir(mods_path):
    sys.path.append(mods_path)


#############
# Functions #
#############
def clr() -> None:
    '''Clear the screen.'''
    WIN = (system() == 'Windows')  # Determine if running Windows
    if WIN:
        os.system('cls')
    else:
        os.system('clear')
