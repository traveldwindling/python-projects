#!/usr/bin/env python3
'''\
SCRIPT_PURPOSE

A log is saved to 'ex_log_path'.

Classes:

    * CLASS - CLASS_DESCRIPTION.

Functions:

    * parse_args() - Parse program arguments.
    * run_init_cks() - Run initialization checks.
    * hndl_sigint() - Handle a SIGINT signal.
    * init_logging() - Initialize logging.
    * main() - Define starting point for execution of the program.

Requires
----------------
REQUIREMENT
    REQUIREMENT_DESCRIPTION.

Variables to set:
----------------
VARIABLE_TO_SET
    VARIABLE_DESCRIPTION.\
'''
###########
# Modules #
###########
import argparse  # Enable program argument parsing
import datetime as dt  # Enable classes for manipulating dates and times
# Enable functions and classes that implement an event logging system
import logging
# Enable rotation of disk log files
from logging.handlers import RotatingFileHandler
# Enable a portable way of using operating system dependent functionality
import os
# Enable returning Python version as string and the system/OS name
from platform import python_version, system
from signal import signal, SIGINT  # Enable use of signal handlers
# Enable access to some variables/functions used/maintained by the interpreter
import sys
# Enable a standard interface to extract, format, and print stack traces
import traceback
from types import FrameType  # Enable frame object type

#############
# Variables #
#############
# Define formatting variables
FB = '\033[1m'
FI = '\033[3m'
FR = '\033[0m'

DT_FORMAT = '%Y-%m-%d_%H:%M'  # Set datetime format
# Create date/time stamp using specified datetime format
DT_STAMP = dt.datetime.now().strftime(DT_FORMAT)
D_FORMAT = '%Y-%m-%d'  # Set date format
# Create date stamp using specified date format
D_STAMP = dt.datetime.now().strftime(D_FORMAT)
# Create UNIX epoch timestamp
TSTAMP = f'{dt.datetime.now().timestamp():.0f}'

HOME = os.path.expanduser('~')  # Obtain user home directory
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]

WIN = (system() == 'Windows')  # Determine if running Windows

LOG_ROOT = os.path.join(PGM_ROOT, 'logs')  # Set log root
LOG_DIR = os.path.join(LOG_ROOT, PGM_NAME)  # Set log directory
LOG_PATH = os.path.join(LOG_DIR, f'{PGM_NAME}.log')  # Set log path


###########
# Classes #
###########



#############
# Functions #
#############
# Validation Functions Start #
# Validation Functions End #


# Set Functions Start #
# Set Functions End #


def parse_args() -> tuple[argparse.ArgumentParser, argparse.Namespace]:
    '''
    Parse program arguments.

    Returns
    ----------------
    argparse.ArgumentParser
        An argparse ArgumentParser object.
    argparse.Namespace
        An argparse namespace class.
    '''
    # Create filtered docstring
    fltr_ds = (__doc__.replace('ex_log_path', LOG_PATH))

    # Create argument parser
    parser = argparse.ArgumentParser(
        description=fltr_ds,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Convert argument strings to objects and assign them as
    # namespace attributes
    args = parser.parse_args()

    return parser, args


def run_init_cks() -> None:
    '''Run initialization checks.'''
    ...


def hndl_sigint(sig_rcvd: int, frame: FrameType | None) -> None:
    '''
    Handle a SIGINT signal.

    Parameters
    ----------------
    sig_rcvd : int
        Signal number.
    frame : frame
        Current stack frame.
    '''
    print('\n\nSIGINT or Ctrl+c detected. Exiting gracefully.')
    sys.exit(130)


def init_logging(log_path: str) -> None:
    '''
    Initialize logging.

    Each log file has a maximum size of 10,485,760 bytes. Once a log file is
    near this limit, the file is rolled over and a number is appended to its
    file name (e.g., .1, .2). A maximum of three rolled over files are saved.

    Parameters
    ----------------
    log_path : str
        The path that the log should be saved to.
    '''
    # Set log directory location
    log_dir = os.path.dirname(os.path.abspath(log_path))

    # If log directory does not exist, create it
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    # Set specified format string for the handler
    logging_fmt = '%(asctime)s - %(levelname)s- %(message)s'
    log_date_fmt = '%Y-%m-%d %H:%M:%S'  # Set specified date/time format
    handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=3)
    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_fmt,
        datefmt=log_date_fmt,
        handlers=[handler]
    )


def main() -> None:
    '''Define starting point for execution of the program.'''
    _, _ = parse_args()

    run_init_cks()

    signal(SIGINT, hndl_sigint)

    try:
        init_logging(LOG_PATH)
    except Exception as exc:
        err_msg = f'\nLog initialization failed: {FB}{exc}{FR}\n'
        print(err_msg)
        raise

    logging.debug('Starting program')

    logging.debug(f'Python version: {python_version()}')

    logging.debug('Ending program')


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
