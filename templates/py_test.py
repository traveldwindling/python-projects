#!/usr/bin/env python3
'''Template for testing Python code.'''
# breakpoint()  # Drop into the debugger at the call site
# from IPython import embed; embed(colors='neutral')  # Embed iPython
###########
# Modules #
###########
# Enable Python interface for the 'lsprof' profiler
import cProfile
import datetime as dt  # Enable classes for manipulating dates and times
# Enable a portable way of using operating system dependent functionality
import os
# Enable pseudo-random number generators for various distributions
import random
# Enable regular expression matching operations similar to those found in Perl
import re
# Enable a number of high-level operations on files and collections of files
import shutil
from shlex import split  # Enable splitting a string using shell-like syntax
# Enable spawning of new processes, connecting to their input/output/error
# pipes, and obtaining of their return codes
import subprocess as sp
# Enable access to some variables/functions used/maintained by the interpreter
import sys
import time  # Enable various time-related functions
# Enable a simple way to time small bits of Python code
import timeit

#############
# Variables #
#############
# Define formatting variables
FB = '\033[1m'
FI = '\033[3m'
FR = '\033[0m'

DT_FORMAT = '%Y-%m-%d_%H:%M'  # Set datetime format
# Create date/time stamp using specified datetime format
DT_STAMP = dt.datetime.now().strftime(DT_FORMAT)
D_FORMAT = '%Y-%m-%d'  # Set date format
# Create date stamp using specified date format
D_STAMP = dt.datetime.now().strftime(D_FORMAT)
# Create UNIX epoch timestamp
TSTAMP = f'{dt.datetime.now().timestamp():.0f}'

HOME = os.path.expanduser('~')  # Obtain user home directory
# Obtain program root directory
PGM_ROOT = os.path.dirname(os.path.realpath(__file__))
# Obtain program name
PGM_NAME = os.path.splitext(os.path.basename(__file__))[0]


###########
# Classes #
###########



#############
# Functions #
#############
def main() -> None:
    '''Define starting point for execution of the program.'''
    pass


###########
# Program #
###########
if __name__ == '__main__':
    main()  # Start program
